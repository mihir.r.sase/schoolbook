package com.shimbi.schoolbook.utils.workers;

import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.shimbi.schoolbook.db.helper.MyDataBase;
import com.shimbi.schoolbook.db.tables.DbAssignment;
import com.shimbi.schoolbook.home.assignments.models.AssignmentResponseModel;
import com.shimbi.schoolbook.utils.AuthModel;
import com.shimbi.schoolbook.utils.Constants;
import com.shimbi.schoolbook.utils.RetrofitClient;
import com.shimbi.schoolbook.utils.SaveSharedPreference;

import java.io.IOException;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.work.Worker;
import androidx.work.WorkerParameters;
import retrofit2.Call;
import retrofit2.Response;

public class FetchAssignmentsWorker extends Worker {


    private static final String TAG = "FetchAssignmentsWorker";
    private String username;
    private String userEmail;
    private String userType;
    private String userId;
    private Context context;

    public FetchAssignmentsWorker(@NonNull Context context, @NonNull WorkerParameters workerParams) {
        super(context, workerParams);
        this.context = context;
    }

    @NonNull
    @Override
    public Result doWork() {
        int flag;
        if (getInputData().getString("From").equals("Initial")) {
            flag = 1;
        } else {
            flag = 0;
        }
        AuthModel authModel = SaveSharedPreference.getData();
        userId = authModel.uid;
        username = authModel.username;
        userEmail = authModel.userEmail;
        userType = authModel.userType;


        Call<AssignmentResponseModel> apicall = RetrofitClient.get().getAssignmentData(userId, userType,
                flag == 1 ? "" : SaveSharedPreference.getAssignmentsSyncDate(),
                flag == 1 ? "" : String.valueOf(SaveSharedPreference.getAssignmnetLatestID()));
        try {
            Response<AssignmentResponseModel> response = apicall.execute();

            if (response.body() != null) {

                AssignmentResponseModel model = response.body();

                List<AssignmentResponseModel.AssignmentResultModel> assignmnets = model.getResult();
                if (assignmnets.size() != 0) {
                    SaveSharedPreference.setAssignmentsSyncDate(model.getSyncdate());
                    SaveSharedPreference.setAssignmentsLatestId(assignmnets.get(0).getEventId());

                    for (int i = 0; i < assignmnets.size(); i++) {

                        DbAssignment dbAssignment = new DbAssignment(assignmnets.get(i));

                        if (flag == 1) {

                            MyDataBase.db.insertToAssignment(dbAssignment);
                        } else {
                            if (MyDataBase.db.checkIfPresentInAssignmnets(assignmnets.get(i).getEventId()) == 0) {
                                MyDataBase.db.insertToAssignment(dbAssignment);
                                Log.e(TAG, "doWork: Insertion Successful");
                            } else {
                                MyDataBase.db.updateAssignmnets(dbAssignment);
                                Log.e(TAG, "doWork: Updating Successful");
                            }
                        }
                    }
                    return Result.success();

                } else {
                    if (flag != 1) {
                        Log.e(TAG, "doWork: No New Data");
                        return Result.failure();
                    }
                    return Result.success();
                }


            } else {

                Log.e(TAG, "doWork: Failed");
                return Result.failure();
            }

        } catch (IOException e) {
            e.printStackTrace();
            Log.e(TAG, "doWork: ", e);
            return Result.failure();
        } finally {
            if (flag == 1) {
                SaveSharedPreference.setSyncStep(3);
                context.sendBroadcast(new Intent(Constants.ACTION));
            }

        }
    }
}
