package com.shimbi.schoolbook.utils.workers;

import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.shimbi.schoolbook.db.helper.MyDataBase;
import com.shimbi.schoolbook.db.tables.DbEvents;
import com.shimbi.schoolbook.home.events.models.EventsResponseModel;
import com.shimbi.schoolbook.utils.AuthModel;
import com.shimbi.schoolbook.utils.Constants;
import com.shimbi.schoolbook.utils.RetrofitClient;
import com.shimbi.schoolbook.utils.SaveSharedPreference;

import java.io.IOException;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.work.Worker;
import androidx.work.WorkerParameters;
import retrofit2.Call;
import retrofit2.Response;

public class FetchEventsWorker extends Worker {

    private static final String TAG = "FetchEventsWorker";
    private String username;
    private String userEmail;
    private String userType;
    private String userId;
    private Context context;

    public FetchEventsWorker(@NonNull Context context, @NonNull WorkerParameters workerParams) {
        super(context, workerParams);
        this.context = context;
    }

    @NonNull
    @Override
    public Result doWork() {

        int flag;
        if (getInputData().getString("From").equals("Initial")) {
            flag = 1;
        } else {
            flag = 0;
        }
        AuthModel authModel = SaveSharedPreference.getData();
        userId = authModel.uid;
        username = authModel.username;
        userEmail = authModel.userEmail;
        userType = authModel.userType;


        Call<EventsResponseModel> apicall = RetrofitClient.get().getEventData(userId, userType,
                flag == 1 ? "" : SaveSharedPreference.getEventsSyncDate(),
                flag == 1 ? "" : String.valueOf(SaveSharedPreference.getEventsLatestID()));
        try {
            Response<EventsResponseModel> response = apicall.execute();

            if (response.body() != null) {

                EventsResponseModel model = response.body();
                List<EventsResponseModel.MyEvent> events = model.getResult();

                if (events.size() != 0) {

                    SaveSharedPreference.setEventsSyncDate(model.getSyncdate());
                    SaveSharedPreference.setEventsLatestId(events.get(events.size() - 1).getEventId());
                    for (int i = 0; i < events.size(); i++) {


                        DbEvents dbEvents = new DbEvents(events.get(i));
                        if (flag == 1) {

                            MyDataBase.db.insertToEvents(dbEvents);
                        } else {
                            if (MyDataBase.db.checkIfPresentInEvents(events.get(i).getEventId()) == 0) {
                                MyDataBase.db.insertToEvents(dbEvents);
                                Log.e(TAG, "doWork: Insertion Successful");
                            } else {
                                MyDataBase.db.updateEvents(dbEvents);
                                Log.e(TAG, "doWork: Updating Successful" + dbEvents.getEventId());
                            }
                        }
                    }
                    Log.e(TAG, "doWork: success");
                    return Result.success();
                } else {
                    if (flag == 0) {
                        Log.e(TAG, "doWork: No New Data");
                        return Result.failure();
                    }
                    return Result.success();
                }


            } else {
                Log.e(TAG, "doWork: Failed");
                return Result.failure();
            }

        } catch (IOException e) {
            e.printStackTrace();
            Log.e(TAG, "doWork: ", e);
            return Result.failure();
        } finally {
            if (flag == 1) {

                SaveSharedPreference.setSyncStep(1);

                context.sendBroadcast(new Intent(Constants.ACTION));
            }
        }
    }
}
