package com.shimbi.schoolbook.utils.workers;

import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.shimbi.schoolbook.db.helper.MyDataBase;
import com.shimbi.schoolbook.db.tables.DbMessages;
import com.shimbi.schoolbook.home.messages.models.MessageResponseModel;
import com.shimbi.schoolbook.utils.AuthModel;
import com.shimbi.schoolbook.utils.Constants;
import com.shimbi.schoolbook.utils.RetrofitClient;
import com.shimbi.schoolbook.utils.SaveSharedPreference;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.work.Worker;
import androidx.work.WorkerParameters;
import retrofit2.Call;
import retrofit2.Response;

public class FetchMessagesWorker extends Worker {

    private static final String TAG = "FetchMessagesWorker";
    private String username;
    private String userEmail;
    private String userType;
    private String userId;
    private Context context;

    public FetchMessagesWorker(@NonNull Context context, @NonNull WorkerParameters workerParams) {
        super(context, workerParams);
        this.context = context;
    }

    @NonNull
    @Override
    public Result doWork() {

        int flag;
        if (getInputData().getString("From").equals("Initial")) {
            flag = 1;
        } else {
            flag = 0;
        }
        AuthModel authModel = SaveSharedPreference.getData();
        userId = authModel.uid;
        username = authModel.username;
        userEmail = authModel.userEmail;
        userType = authModel.userType;

        Call<MessageResponseModel> apicall = RetrofitClient.get().getMessageData(userId, userType,
                flag == 1 ? "" : SaveSharedPreference.getMessagesSyncDate(),
                flag == 1 ? "" : String.valueOf(SaveSharedPreference.getMessagesLatestID()));
        try {
            Response<MessageResponseModel> response = apicall.execute();
            if (response.body() != null) {

                MessageResponseModel model = response.body();
                List<MessageResponseModel.MessageResultModel> messages = model.getResult();

                if (messages.size() != 0) {
                    SaveSharedPreference.setMessagesSyncDate(model.getSyncdate());
                    SaveSharedPreference.setMessagesLatestId(messages.get(0).getEventId());

                    for (int i = 0; i < messages.size(); i++) {
                        DbMessages dbMessages = new DbMessages(messages.get(i));

                        if (flag == 1) {

                            MyDataBase.db.insertToMessage(dbMessages);
                        } else {
                            if (MyDataBase.db.checkIfPresentInMessages(messages.get(i).getEventId()) == 0) {
                                MyDataBase.db.insertToMessage(dbMessages);
                                Log.e(TAG, "doWork: Insertion Successful");
                            } else {
                                MyDataBase.db.updateMessages(dbMessages);
                                Log.e(TAG, "doWork: Updating Successful");
                            }
                        }
                    }
                    return Result.success();

                } else {
                    if (flag != 1) {
                        Log.e(TAG, "doWork: No New Data");
                        return Result.failure();
                    }
                    return Result.success();
                }


            } else {

                Log.e(TAG, "doWork: Failed");
                return Result.failure();
            }


        } catch (Exception e) {
            Log.e(TAG, "doWork: ", e);
            return Result.failure();
        } finally {
            if (flag == 1) {
                SaveSharedPreference.setSyncStep(4);

                context.sendBroadcast(new Intent(Constants.ACTION));
            }
        }
    }
}
