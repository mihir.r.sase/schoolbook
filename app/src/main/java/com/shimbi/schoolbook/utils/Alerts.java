package com.shimbi.schoolbook.utils;

import android.content.Context;
import android.widget.Toast;

public class Alerts {


    /**
     * @param context app Context
     * @param message message to be displayed
     */
    public static void displayToast(Context context, String message) {
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
    }


}
