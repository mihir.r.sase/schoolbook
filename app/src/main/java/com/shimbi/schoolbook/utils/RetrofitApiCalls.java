package com.shimbi.schoolbook.utils;

import com.shimbi.schoolbook.home.assignments.models.AssignmentDetailsResponseModel;
import com.shimbi.schoolbook.home.assignments.models.AssignmentDeteleResponseModel;
import com.shimbi.schoolbook.home.assignments.models.AssignmentResponseModel;
import com.shimbi.schoolbook.home.events.models.EventDetailsResponseModel;
import com.shimbi.schoolbook.home.events.models.EventsResponseModel;
import com.shimbi.schoolbook.home.fees.models.FeesResponseModel;
import com.shimbi.schoolbook.home.holidays.models.HolidayResponseModel;
import com.shimbi.schoolbook.home.leaves.models.DeleteLeaveResponseModel;
import com.shimbi.schoolbook.home.leaves.models.LeaveAproveListResponseModel;
import com.shimbi.schoolbook.home.leaves.models.LeaveListResponseModel;
import com.shimbi.schoolbook.home.messages.models.MessageDetailsResponseModel;
import com.shimbi.schoolbook.home.messages.models.MessageDropDownModel;
import com.shimbi.schoolbook.home.messages.models.MessageResponseModel;
import com.shimbi.schoolbook.home.models.AddAttachmentResponseModel;
import com.shimbi.schoolbook.home.models.DropDownModel;
import com.shimbi.schoolbook.home.models.GetProfileResponseModel;
import com.shimbi.schoolbook.home.models.SuccessResponseModel;
import com.shimbi.schoolbook.home.notice.models.NoticeDeleteResponseModel;
import com.shimbi.schoolbook.home.notice.models.NoticeDetailsResponseModel;
import com.shimbi.schoolbook.home.notice.models.NoticeResponseModel;
import com.shimbi.schoolbook.home.photos.models.DeletePhotoResponseModel;
import com.shimbi.schoolbook.home.photos.models.GetLikesResponseModel;
import com.shimbi.schoolbook.home.photos.models.LikePhotoResponseModel;
import com.shimbi.schoolbook.home.photos.models.PhotosListResponseModel;
import com.shimbi.schoolbook.home.requests.models.RequestSuccessModel;
import com.shimbi.schoolbook.home.requests.models.TcDropdownModel;
import com.shimbi.schoolbook.home.transport.models.AssignedRouteModel;
import com.shimbi.schoolbook.home.transport.models.NotificationSuccessModel;
import com.shimbi.schoolbook.home.transport.models.StudentJourneyStatusModel;
import com.shimbi.schoolbook.home.transport.models.TransportDataModel;
import com.shimbi.schoolbook.login.models.ForgetPasswordResponseModel;
import com.shimbi.schoolbook.login.models.LoginResponseModel;

import org.json.JSONArray;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface RetrofitApiCalls {


    //
    //Login API Calls
    //

    @FormUrlEncoded
    @POST("login/login_user_new")
    Call<LoginResponseModel> performLogin(@Field("user_name") String username
            , @Field("password") String password
            , @Field("GCMregId") String gcmRegId
            , @Field("device_info") String deviceInfo
            , @Field("version_code") String versionCode
            , @Field("version_name") String versionName);

    @FormUrlEncoded
    @POST("login/change_password")
    Call<Integer> changePassword(@Field("uid") String uid
            , @Field("user_type") String userType
            , @Field("new_password") String newPassword
            , @Field("old_password") String oldPassword);


    @FormUrlEncoded
    @POST("login/forgetPassword")
    Call<ForgetPasswordResponseModel> forgotPassword(@Field("email") String email);

    //
    //Notice Module API Calls
    //

    @FormUrlEncoded
    @POST("notice/notice_list")
    Call<NoticeResponseModel> getNoticeData(@Field("uid") String uid
            , @Field("user_type") String user_type
            , @Field("syncdate") String syncdate
            , @Field("latestid") String latestid);

    @FormUrlEncoded
    @POST("notice/notice_data?page=1")
    Call<NoticeDetailsResponseModel> getNoticeDetails(@Field("event_id") String eventId
            , @Field("syncdate") String syncdate);

    @FormUrlEncoded
    @POST("notice/notice_reply_post")
    Call<Integer> sendCommentOnNotice(@Field("event_id") String eventId
            , @Field("uid") String uid
            , @Field("user_type") String userType
            , @Field("comment") String comment);


    @FormUrlEncoded
    @POST("notice/delete")
    Call<NoticeDeleteResponseModel> deleteNotice(@Field("uid") String uid
            , @Field("user_type") String userType
            , @Field("event_id") String eventId);

    @FormUrlEncoded
    @POST("notice/notice_add")
    Call<Integer> addNotice(@Field("uid") String uid
            , @Field("user_type") String userType
            , @Field("class") Integer classId
            , @Field("no_reply") Integer noReply
            , @Field("title") String title
            , @Field("details") String details
            , @Field("notice_sms") String noticeSms
            , @Field("send_sms_flag") Integer sendSmsFlag
            , @Field("event_id") String eventId
            , @Field("attachment") JSONArray files
            , @Field("delete_attachment") JSONArray deletedFiles);


    //
    //Assignment Module API Calls
    //


    @FormUrlEncoded
    @POST("assignment/assignment_list")
    Call<AssignmentResponseModel> getAssignmentData(@Field("uid") String uid
            , @Field("user_type") String user_type
            , @Field("syncdate") String syncdate
            , @Field("latestid") String latestid);

    @FormUrlEncoded
    @POST("assignment/assignment_data?page=1")
    Call<AssignmentDetailsResponseModel> getAssignmentDetails(@Field("event_id") String eventId
            , @Field("syncdate") String syncdate);

    @FormUrlEncoded
    @POST("assignment/assignment_reply_post")
    Call<Integer> sendCommentOnAssignment(@Field("event_id") String eventId
            , @Field("uid") String uid
            , @Field("user_type") String userType
            , @Field("comment") String comment);


    @FormUrlEncoded
    @POST("assignment/delete")
    Call<AssignmentDeteleResponseModel> deleteAssignment(@Field("uid") String uid
            , @Field("user_type") String userType
            , @Field("event_id") String eventId);


    @FormUrlEncoded
    @POST("assignment/assignment_add")
    Call<Integer> addAssignment(@Field("uid") Integer uid
            , @Field("user_type") Integer userType
            , @Field("class") Integer _class
            , @Field("submitdate") String submitDate
            , @Field("title") String title
            , @Field("details") String details
            , @Field("event_id") Integer eventId
            , @Field("attachment") JSONArray files
            , @Field("delete_attachment") JSONArray deletedFiles
    );


    //
    //Events Module API Calls
    //
    @FormUrlEncoded
    @POST("event/event_list")
    Call<EventsResponseModel> getEventData(@Field("uid") String uid
            , @Field("user_type") String userType
            , @Field("syncdate") String syncdate
            , @Field("latestid") String latestid);

    @FormUrlEncoded
    @POST("event/event_add")
    Call<Integer> addEvent(@Field("uid") String uid
            , @Field("user_type") String userType
            , @Field("class") Integer classId
            , @Field("no_reply") Integer noReplyFlag
            , @Field("title") String title
            , @Field("details") String details
            , @Field("startdate") String startDate
            , @Field("starttime") String startTime
            , @Field("enddate") String endDate
            , @Field("endtime") String endTime
            , @Field("eventstatue") Integer eventStatue
            , @Field("event_id") Integer eventId
            , @Field("uploaded_filename") String uploadedFileName
            , @Field("imagePath") String originalImage);

    @FormUrlEncoded
    @POST("event/event_data")
    Call<EventDetailsResponseModel> getEventDetails(@Field("event_id") Integer eventId);

    @FormUrlEncoded
    @POST("event/event_reply_post")
    Call<Integer> sendCommentOnEvents(@Field("event_id") String eventId
            , @Field("uid") String uid
            , @Field("user_type") String userType
            , @Field("comment") String comment);


    @FormUrlEncoded
    @POST("event/delete")
    Call<AssignmentDeteleResponseModel> deleteEvent(@Field("uid") String uid
            , @Field("user_type") String userType
            , @Field("event_id") String eventId);

    //
    //Message Module API Calls
    //

    @FormUrlEncoded
    @POST("message/message_list")
    Call<MessageResponseModel> getMessageData(@Field("uid") String uid
            , @Field("user_type") String user_type
            , @Field("syncdate") String syncdate
            , @Field("latestid") String latestid);


    @FormUrlEncoded
    @POST("message/message_data?page=1")
    Call<MessageDetailsResponseModel> getMessageDetails(@Field("event_id") String eventId
            , @Field("syncdate") String syncdate);

    @FormUrlEncoded
    @POST("message/message_reply_post")
    Call<Integer> sendCommentOnMessage(@Field("event_id") String eventId
            , @Field("uid") String uid
            , @Field("user_type") String userType
            , @Field("comment") String comment);

    @FormUrlEncoded
    @POST("message/get_dropdown_value")
    Call<MessageDropDownModel> getMessageDropDownDetails(@Field("uid") String uid
            , @Field("user_type") String userType
            , @Field("class") int classFlag
            , @Field("teacher") int teacherFlag
            , @Field("group") int groupFlag
            , @Field("user") int userFlag
            , @Field("student") int studentFlag
            , @Field("class_id") Integer class_id);

    @FormUrlEncoded
    @POST("message/message_add")
    Call<SuccessResponseModel> addMessage(@Field("uid") String uid
            , @Field("user_type") String userType
            , @Field("subject") String title
            , @Field("details") String description
            , @Field("class") Integer classId
            , @Field("student") Integer studentId
            , @Field("user") Integer userId
            , @Field("event_id") String eventId
            , @Field("uploaded_filename") String uploadedFileName
            , @Field("imagePath") String originalImage);


    //
    //Holidays Module API Calls
    //
    @FormUrlEncoded
    @POST("holiday/holiday_list")
    Call<List<HolidayResponseModel>> getHolidayData(@Field("uid") String uid
            , @Field("user_type") String user_type
            , @Field("syncdate") String syncdate);


    //
    //Leaves Module API Calls
    //

    @FormUrlEncoded
    @POST("leaves/leave_list")
    Call<LeaveListResponseModel> getLeaveList(@Field("uid") String uid
            , @Field("user_type") String userType
            , @Field("syncdate") String syncdate);


    @FormUrlEncoded
    @POST("leaves/leave_add")
    Call<SuccessResponseModel> addLeave(@Field("uid") String uid
            , @Field("user_type") String userType
            , @Field("start_date") String startDate
            , @Field("end_date") String endDate
            , @Field("reason") String reason);


    @FormUrlEncoded
    @POST("leaves/leave_delete")
    Call<DeleteLeaveResponseModel> deleteLeave(@Field("uid") Integer uid
            , @Field("user_type") Integer userType
            , @Field("leave_id") Integer leaveId);

    @FormUrlEncoded
    @POST("leaves/leave_edit")
    Call<SuccessResponseModel> editLeave(@Field("uid") String uid
            , @Field("user_type") String userType
            , @Field("leave_id") String leaveId
            , @Field("start_date") String startDate
            , @Field("end_date") String endDate
            , @Field("reason") String reason);

    @FormUrlEncoded
    @POST("leaves/teacher_leave_approval")
    Call<List<LeaveAproveListResponseModel>> getLeaveApporvalList(@Field("uid") String uid
            , @Field("user_type") String userType);

    @FormUrlEncoded
    @POST("leaves/approve_reject_leave")
    Call<SuccessResponseModel> approveRejectLeave(@Field("uid") String uid
            , @Field("user_type") String userType
            , @Field("leave_id") String leaveId
            , @Field("type") String type            // Deny/Approve
            , @Field("reject_reason") String rejectReason);

    //
    //Fees Module Api Calls
    //

    @FormUrlEncoded
    @POST("fees/index")
    Call<FeesResponseModel> getFeesDetails(@Field("uid") String uid
            , @Field("user_type") String userType);


    //
    // Photos Module
    //

    @FormUrlEncoded
    @POST("photos/photo_list")
    Call<PhotosListResponseModel> getPhotos(@Field("uid") String uid
            , @Field("user_type") String userType
            , @Field("syncdate") String syncdate
            , @Field("latestid") String latestid);

    @FormUrlEncoded
    @POST("photos/delete_photo")
    Call<DeletePhotoResponseModel> deletePhoto(@Field("uid") String uid
            , @Field("user_type") String userType
            , @Field("photo_id") Integer photoId);

    @FormUrlEncoded
    @POST("photos/get_photo_likes")
    Call<GetLikesResponseModel> getPhotoLikes(@Field("uid") String uid
            , @Field("user_type") String userType
            , @Field("photo_id") Integer photoId);

    @FormUrlEncoded
    @POST("photos/like_photo")
    Call<LikePhotoResponseModel> likePhoto(@Field("uid") String uid
            , @Field("user_type") String userType
            , @Field("photo_id") Integer photoId);

    @FormUrlEncoded
    @POST("photos/add_photo")
    Call<SuccessResponseModel> addPhoto(@Field("uid") String uid
            , @Field("user_type") String userType
            , @Field("album_name") String albumName
            , @Field("photo_title") String photoTitle
            , @Field("photo_description") String photoDesc
            , @Field("uploaded_filename") String uploadedFileName
            , @Field("imagePath") String originalImage);


    //
    //Request Module API Calls
    //
    @FormUrlEncoded
    @POST("myprofile/set_dropdown/tc")
    Call<TcDropdownModel> getTcDropdownDetails();

    @FormUrlEncoded
    @POST("myprofile/save_tc")
    Call<RequestSuccessModel> saveTc(@Field("uid") String uid
            , @Field("user_type") String userType
            , @Field("tc_type") String tcType
            , @Field("leaving_reason") String leavingReason);

    @FormUrlEncoded
    @POST("myprofile/save_bonafide")
    Call<RequestSuccessModel> saveBonafide(@Field("uid") String uid
            , @Field("user_type") String userType
            , @Field("bonafide_reason") String bonafideReason);

    @FormUrlEncoded
    @POST("myprofile/get_tc_status")
    Call<RequestSuccessModel> getTcStatus(@Field("uid") String uid
            , @Field("user_type") String userType);

    @FormUrlEncoded
    @POST("myprofile/get_bonafide_status")
    Call<RequestSuccessModel> getBonafideStatus(@Field("uid") String uid
            , @Field("user_type") String userType);


    //
    //Profile Module API Calls
    //
    @FormUrlEncoded
    @POST("myprofile/index")
    Call<GetProfileResponseModel> getUserProfile(@Field("uid") String uid
            , @Field("user_type") String userType);


    @FormUrlEncoded
    @POST("myprofile/profile_update")
    Call<SuccessResponseModel> updateUserProfile(@Field("uid") String uid
            , @Field("user_type") String userType
            , @Field("email") String userEmail
            , @Field("mobile") String userMobile
            , @Field("address") String address
            , @Field("profile_img_name") String profileImageName
            , @Field("profile_img") String profileImage);


    //
    // Transport Module API Calls
    //
    @FormUrlEncoded
    @POST("transport/getAssignRoute")
    Call<AssignedRouteModel> getAssignedRoute(@Field("user_id") String uid
            , @Field("user_type") String userType);

    @FormUrlEncoded
    @POST("transport/index")
    Call<TransportDataModel> getTransportData(@Field("user_id") String uid
            , @Field("user_type") String userType
            , @Field("route_id") String routeId);

    @FormUrlEncoded
    @POST("transport/sendNotificationFromDriver")
    Call<NotificationSuccessModel> sendNotificationFromDriver(@Field("pickup_id") Integer pickupId
            , @Field("markerTitle") String markerTitle
            , @Field("route_id") Integer routeId
            , @Field("bus_id") Integer busId
            , @Field("module") String module
            , @Field("Time") String dateTime);

    @FormUrlEncoded
    @POST("transport/sendStudentNotification")
    Call<NotificationSuccessModel> sendStudentNotification(@Field("markerTitle") String markerTitle
            , @Field("route_id") Integer routeId
            , @Field("bus_id") Integer busId
            , @Field("attend") String attend
            , @Field("absent") String absent
            , @Field("flag") String flag
            , @Field("Time") String dateTime);

    @FormUrlEncoded
    @POST("transport/updateStudentPickupPoint")
    Call<NotificationSuccessModel> updatePickupPoint(@Field("shift") String shift
            , @Field("update_flag") Integer updateFlag
            , @Field("pickup_id") Integer pickupId
            , @Field("latitude") String latitude
            , @Field("longitude") String longitude
            , @Field("drop_latitude") String dropLatitude
            , @Field("drop_longitude") String dropLongitude
            , @Field("update_drop_flag") Integer updateDropFlag);

    @FormUrlEncoded
    @POST("transport/getNotificationStatus")
    Call<StudentJourneyStatusModel> getStudentJourneyStatus(@Field("user_id") String uid
            , @Field("userType") String user_type);


    //
    //Other API Calls
    //
    @FormUrlEncoded
    @POST("dropdown/get_dropdown_value")
    Call<DropDownModel> getDropDownDetails(@Field("uid") String uid
            , @Field("user_type") String userType
            , @Field("class") int classFlag
            , @Field("teacher") int teacherFlag
            , @Field("group") int groupFlag
            , @Field("department") int deptFlag);


    @FormUrlEncoded
    @POST("assignment/add_attachment")
    Call<AddAttachmentResponseModel> addAttachment(@Field("uid") String uid
            , @Field("user_type") String userType
            , @Field("attachment_path") String file
            , @Field("attachment_name") String filename
            , @Field("module") String module);


}
