package com.shimbi.schoolbook.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.google.gson.Gson;

public class SaveSharedPreference {

    private static final String TAG = "SaveSharedPreference";
    private static final String ASSIGNMENTS_SYNC_DATE = "ASSIGNMENTS_SYNC_DATE";
    private static final String NOTICES_SYNC_DATE = "NOTICES_SYNC_DATE";
    private static final String EVENTS_SYNC_DATE = "EVENTS_SYNC_DATE";
    private static final String MESSAGES_SYNC_DATE = "MESSAGES_SYNC_DATE";
    private static final String LEAVES_SYNC_DATE = "LEAVES_SYNC_DATE";
    private static final String PHOTOS_SYNC_DATE = "PHOTOS_SYNC_DATE";
    private static final String ASSIGNMENTS_LATEST_ID = "ASSIGNMENTS_LATEST_ID";
    private static final String NOTICES_LATEST_ID = "NOTICES_LATEST_ID";
    private static final String EVENTS_LATEST_ID = "EVENTS_LATEST_ID";
    private static final String MESSAGES_LATEST_ID = "MESSAGES_LATEST_ID";
    private static final String PHOTOS_LATEST_ID = "PHOTOS_LATEST_ID";
    private static final String SYNC_STEP = "SYNC_STEP";
    private static final String MULTIPLE_STUDENT_DETAILS = "MULTIPLE_STUDENT_DETAILS";
    private static final String BASE_URL_RELEASE = "BASE_URL_RELEASE";
    private static final String JOURNEY_STATUS = "JOURNEY_STATUS";
    private static final String TRANSPORT_START_PICKUP_ID = "TRANSPORT_START_PICKUP_ID";
    private static final String TRANSPORT_END_PICKUP_ID = "TRANSPORT_END_PICKUP_ID";
    private static final String ROUTE_ID = "ROUTE_ID";
    private static final String BUS_ID = "BUS_ID";
    private static final String LAST_ACCESS_DATE = "LAST_ACCESS_DATE";
    private static SharedPreferences pref;
    private static AuthModel auth;

    public static void init(Context context) {
        pref = PreferenceManager.getDefaultSharedPreferences(context);
        getData();
    }

    public static Boolean getBaseUrl() {
        return pref.getBoolean(BASE_URL_RELEASE, false);
    }

    public static void setBaseUrl(Boolean isBaseUrlRelease) {
        pref.edit().putBoolean(BASE_URL_RELEASE, isBaseUrlRelease).apply();
    }

    public static String getLastAccessDate() {
        return pref.getString(LAST_ACCESS_DATE, "");
    }

    public static void setLastAccessDate(String date) {
        pref.edit().putString(LAST_ACCESS_DATE, date).apply();
    }

    public static Integer getRouteId() {
        return pref.getInt(ROUTE_ID, -1);
    }

    public static void setRouteId(Integer routeId) {
        pref.edit().putInt(ROUTE_ID, routeId).apply();
    }

    public static Integer getBusId() {
        return pref.getInt(BUS_ID, -1);
    }

    public static void setBusId(Integer busId) {
        pref.edit().putInt(BUS_ID, busId).apply();
    }

    public static Integer getStartPickupId() {
        return pref.getInt(TRANSPORT_START_PICKUP_ID, -1);
    }

    public static void setStartPickupId(Integer pickupId) {
        pref.edit().putInt(TRANSPORT_START_PICKUP_ID, pickupId).apply();
    }

    public static Integer getEndPickupId() {
        return pref.getInt(TRANSPORT_END_PICKUP_ID, -1);
    }

    public static void setEndPickupId(Integer pickupId) {
        pref.edit().putInt(TRANSPORT_END_PICKUP_ID, pickupId).apply();
    }

    public static Integer getJorneyStatus() {
        return pref.getInt(JOURNEY_STATUS, 1);
    }

    public static void setJourneyStatus(Integer journeyStatus) {
        pref.edit().putInt(JOURNEY_STATUS, journeyStatus).apply();
    }

    public static String getAssignmentsSyncDate() {
        return pref.getString(ASSIGNMENTS_SYNC_DATE, "Default");
    }

    public static void setAssignmentsSyncDate(String syncDate) {
        pref.edit().putString(ASSIGNMENTS_SYNC_DATE, syncDate).apply();
    }

    public static String getNoticesSyncDate() {
        return pref.getString(NOTICES_SYNC_DATE, "Default");
    }

    public static void setNoticesSyncDate(String syncDate) {
        pref.edit().putString(NOTICES_SYNC_DATE, syncDate).apply();
    }

    public static String getEventsSyncDate() {
        return pref.getString(EVENTS_SYNC_DATE, "Default");
    }

    public static void setEventsSyncDate(String syncDate) {
        pref.edit().putString(EVENTS_SYNC_DATE, syncDate).apply();
    }

    public static String getMessagesSyncDate() {
        return pref.getString(MESSAGES_SYNC_DATE, "Default");
    }

    public static void setMessagesSyncDate(String syncDate) {
        pref.edit().putString(MESSAGES_SYNC_DATE, syncDate).apply();
    }

    public static String getPhotosSyncDate() {
        return pref.getString(PHOTOS_SYNC_DATE, "");
    }

    public static void setPhotosSyncDate(String syncDate) {
        pref.edit().putString(PHOTOS_SYNC_DATE, syncDate).apply();
    }

    public static String getLeavesSyncDate() {
        return pref.getString(LEAVES_SYNC_DATE, "Default");
    }

    public static void setLeavesSyncDate(String syncDate) {
        pref.edit().putString(LEAVES_SYNC_DATE, syncDate).apply();
    }


    public static int getAssignmnetLatestID() {
        return pref.getInt(ASSIGNMENTS_LATEST_ID, 0);
    }

    public static void setAssignmentsLatestId(int latestId) {
        pref.edit().putInt(ASSIGNMENTS_LATEST_ID, latestId).apply();
    }

    public static int getNoticesLatestID() {
        return pref.getInt(NOTICES_LATEST_ID, 0);
    }

    public static void setNoticesLatestId(int latestId) {
        pref.edit().putInt(NOTICES_LATEST_ID, latestId).apply();
    }

    public static int getEventsLatestID() {
        return pref.getInt(EVENTS_LATEST_ID, 0);
    }

    public static void setEventsLatestId(int latestId) {
        pref.edit().putInt(EVENTS_LATEST_ID, latestId).apply();
    }

    public static int getMessagesLatestID() {
        return pref.getInt(MESSAGES_LATEST_ID, 0);
    }

    public static void setMessagesLatestId(int latestId) {
        pref.edit().putInt(MESSAGES_LATEST_ID, latestId).apply();
    }

    public static int getphotosLatestID() {
        return pref.getInt(PHOTOS_LATEST_ID, 0);
    }

    public static void setPhotosLatestId(int latestId) {
        pref.edit().putInt(PHOTOS_LATEST_ID, latestId).apply();
    }


    public static int getSyncStep() {
        return pref.getInt(SYNC_STEP, 0);
    }

    public static void setSyncStep(int syncStep) {
        pref.edit().putInt(SYNC_STEP, syncStep).apply();
    }

    public static String getMultipleStudentDetails() {
        return pref.getString(MULTIPLE_STUDENT_DETAILS, "Default");
    }

    public static void setMultipleStudentDetails(String studentDetials) {
        pref.edit().putString(MULTIPLE_STUDENT_DETAILS, studentDetials).apply();
    }


    public static void clearAll() {
        pref.edit().clear().apply();
    }


    public static void saveData(AuthModel authModel) {
        auth = authModel;
        String json = null;
        if (authModel != null) {
            Gson gson = new Gson();
            json = gson.toJson(authModel);
        }
        pref.edit().putString("MyObject", json).apply();
    }

    public static AuthModel getData() {
        Gson gson = new Gson();
        String json = pref.getString("MyObject", null);
        if (json != null) {
            auth = gson.fromJson(json, AuthModel.class);
        } else {
            auth = null;
        }
        return auth;
    }

    public static boolean getLoggedStatus() {
        return auth != null;
    }

}
