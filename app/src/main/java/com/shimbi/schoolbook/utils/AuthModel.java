package com.shimbi.schoolbook.utils;

import com.shimbi.schoolbook.login.models.LoginResponseModel;
import com.shimbi.schoolbook.login.models.StudentCountModel;

public class AuthModel {

    public String username,
            uid,
            userType,
            userEmail,
            leaveApproveFlag,
            messageAddFlag,
            assignmentAddFlag,
            photoAddFlag,
            attendanceAddFlag,
            rightsTransport;

    public Integer eventAddFlag, noticeAddFlag,studentCount;


    public AuthModel(LoginResponseModel m,int i) {
        this.username = m.getFullname();
        this.uid = m.getUid();
        this.userType = m.getUserType();
        this.userEmail = m.getEmail();
        this.leaveApproveFlag = m.getLeaveApprove();
        this.messageAddFlag = m.getMessageAdd();
        this.assignmentAddFlag = m.getAssignmentAdd();
        this.eventAddFlag = m.getEventAdd();
        this.noticeAddFlag = m.getNoticeAdd();
        this.photoAddFlag = m.getPhotoAdd();
        this.attendanceAddFlag = m.getAttendanceAdd();
        this.rightsTransport = m.getRightsTransport();
        this.studentCount=i;
    }

    public AuthModel(StudentCountModel s,LoginResponseModel m) {
        this.username = s.getUserName();
        this.uid = s.getUserId();
        this.userType = s.getUserType();
        this.userEmail = s.getUserEmail();
        this.leaveApproveFlag = m.getLeaveApprove();
        this.messageAddFlag = m.getMessageAdd();
        this.assignmentAddFlag = m.getAssignmentAdd();
        this.eventAddFlag = m.getEventAdd();
        this.noticeAddFlag = m.getNoticeAdd();
        this.photoAddFlag = m.getPhotoAdd();
        this.attendanceAddFlag = m.getAttendanceAdd();
        this.rightsTransport = m.getRightsTransport();
        this.studentCount=m.getStudentCount().size();
    }

    public AuthModel(StudentCountModel s, AuthModel m) {
        this.username = s.getUserName();
        this.uid = s.getUserId();
        this.userType = s.getUserType();
        this.userEmail = s.getUserEmail();
        this.leaveApproveFlag = m.leaveApproveFlag;
        this.messageAddFlag = m.messageAddFlag;
        this.assignmentAddFlag = m.assignmentAddFlag;
        this.eventAddFlag = m.eventAddFlag;
        this.noticeAddFlag = m.noticeAddFlag;
        this.photoAddFlag = m.photoAddFlag;
        this.attendanceAddFlag = m.attendanceAddFlag;
        this.rightsTransport = m.rightsTransport;
        this.studentCount = m.studentCount;

    }




}
