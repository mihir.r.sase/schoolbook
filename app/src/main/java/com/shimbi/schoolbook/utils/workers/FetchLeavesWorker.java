package com.shimbi.schoolbook.utils.workers;

import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.shimbi.schoolbook.db.helper.MyDataBase;
import com.shimbi.schoolbook.db.tables.DbLeaveHoliday;
import com.shimbi.schoolbook.db.tables.DbLeaves;
import com.shimbi.schoolbook.home.leaves.models.LeaveListResponseModel;
import com.shimbi.schoolbook.utils.AuthModel;
import com.shimbi.schoolbook.utils.Constants;
import com.shimbi.schoolbook.utils.RetrofitClient;
import com.shimbi.schoolbook.utils.SaveSharedPreference;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.work.Worker;
import androidx.work.WorkerParameters;
import retrofit2.Call;
import retrofit2.Response;

public class FetchLeavesWorker extends Worker {

    private static final String TAG = "FetchLeavesWorker";
    private String username;
    private String userEmail;
    private String userType;
    private String userId;
    private Context context;

    public FetchLeavesWorker(@NonNull Context context, @NonNull WorkerParameters workerParams) {
        super(context, workerParams);
        this.context = context;
    }

    @NonNull
    @Override
    public Result doWork() {
        AuthModel authModel = SaveSharedPreference.getData();
        userId = authModel.uid;
        username = authModel.username;
        userEmail = authModel.userEmail;
        userType = authModel.userType;

        Call<LeaveListResponseModel> apicall = RetrofitClient.get().getLeaveList(userId, userType, "");
        try {
            Response<LeaveListResponseModel> response = apicall.execute();
            if (response.body() != null) {
                SaveSharedPreference.setLeavesSyncDate(response.body().getSyncdate());
                List<LeaveListResponseModel.Leave> leaveList = response.body().getLeave();
                List<LeaveListResponseModel.Holiday> holidayList = response.body().getHoliday();
                if (leaveList.size() != 0) {
                    for (int i = 0; i < leaveList.size(); i++) {
                        DbLeaves dbLeaves = new DbLeaves(leaveList.get(i));
                        MyDataBase.db.insertToLeaves(dbLeaves);
                    }
                }
                if (holidayList.size() != 0) {

                    for (int i = 0; i < holidayList.size(); i++) {
                        DbLeaveHoliday dbLeaveHoliday = new DbLeaveHoliday(holidayList.get(i));
                        MyDataBase.db.insertToLeaveHoliday(dbLeaveHoliday);
                    }
                }


                return Result.success();
            } else {
                Log.e(TAG, "doWork: Failed");
                return Result.failure();
            }
        } catch (Exception e) {
            Log.e(TAG, "doWork: ", e);
            return Result.failure();
        } finally {
            SaveSharedPreference.setSyncStep(6);
            context.sendBroadcast(new Intent(Constants.ACTION));
        }
    }
}
