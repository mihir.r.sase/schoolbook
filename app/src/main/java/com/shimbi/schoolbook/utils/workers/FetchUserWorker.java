package com.shimbi.schoolbook.utils.workers;

import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.shimbi.schoolbook.db.helper.MyDataBase;
import com.shimbi.schoolbook.db.tables.DbUser;
import com.shimbi.schoolbook.home.models.GetProfileResponseModel;
import com.shimbi.schoolbook.utils.AuthModel;
import com.shimbi.schoolbook.utils.Constants;
import com.shimbi.schoolbook.utils.RetrofitClient;
import com.shimbi.schoolbook.utils.SaveSharedPreference;

import androidx.annotation.NonNull;
import androidx.work.Worker;
import androidx.work.WorkerParameters;
import retrofit2.Call;
import retrofit2.Response;

public class FetchUserWorker extends Worker {

    private static final String TAG = "FetchUserWorker";
    private String username;
    private String userEmail;
    private String userType;
    private String userId;
    private Context context;

    public FetchUserWorker(@NonNull Context context, @NonNull WorkerParameters workerParams) {
        super(context, workerParams);
        this.context = context;
    }

    @NonNull
    @Override
    public Result doWork() {

        AuthModel authModel = SaveSharedPreference.getData();
        userId = authModel.uid;
        username = authModel.username;
        userEmail = authModel.userEmail;
        userType = authModel.userType;

        Call<GetProfileResponseModel> call = RetrofitClient.get().getUserProfile(userId, userType);

        try {
            Response<GetProfileResponseModel> response = call.execute();
            if (response.body() != null) {

                GetProfileResponseModel model = response.body();

                DbUser dbUser = new DbUser(model, userId, userType);
                MyDataBase.db.insertToUser(dbUser);
                return Result.success();


            } else {
                Log.e(TAG, "doWork: Failed");

                return Result.failure();
            }


        } catch (Exception e) {
            e.printStackTrace();
            Log.e(TAG, "doWork: ", e);
            return Result.failure();
        } finally {
            SaveSharedPreference.setSyncStep(7);
            context.sendBroadcast(new Intent(Constants.ACTION));
        }

    }
}
