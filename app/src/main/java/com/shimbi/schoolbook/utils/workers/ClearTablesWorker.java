package com.shimbi.schoolbook.utils.workers;

import android.content.Context;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.work.Worker;
import androidx.work.WorkerParameters;

public class ClearTablesWorker extends Worker {

    private static final String TAG = "ClearTablesWorker";

    public ClearTablesWorker(@NonNull Context context, @NonNull WorkerParameters workerParams) {
        super(context, workerParams);
    }

    @NonNull
    @Override
    public Result doWork() {

        Log.e(TAG, "doWork: IN");
//        MyDataBase.db.deleteRouteDetails();
//        MyDataBase.db.deletePickupLocations();
//        MyDataBase.db.deleteTransportUsers();
//        MyDataBase.db.deleteAllFromEmbarkedUsers();
//        MyDataBase.db.deleteAllFromYetToBeEmbarked();
//        MyDataBase.db.deleteVisitedBusStop();
//        SaveSharedPreference.setJourneyStatus(1);
//        SaveSharedPreference.setStartPickupId(-1);
//        SaveSharedPreference.setEndPickupId(-1);
//        SaveSharedPreference.setRouteId(-1);
//        SaveSharedPreference.setBusId(-1);
//
//
//        Calendar currentDate = Calendar.getInstance();
//        Calendar dueDate = Calendar.getInstance();
//
//        dueDate.set(Calendar.HOUR_OF_DAY, 1);
//        dueDate.set(Calendar.MINUTE, 0);
//        dueDate.set(Calendar.SECOND, 0);
//
//        if (dueDate.before(currentDate)) {
//            dueDate.add(Calendar.HOUR_OF_DAY, 24);
//        }
//
//        long timeDiff = dueDate.getTimeInMillis() - currentDate.getTimeInMillis();
//
//        OneTimeWorkRequest clearTableRequest = new OneTimeWorkRequest.Builder(ClearTablesWorker.class)
//                .setInitialDelay(timeDiff, TimeUnit.MILLISECONDS)
//                .addTag(Constants.TASK_CLEAR_TABLES).build();
//
//        WorkManager.getInstance().enqueueUniqueWork(Constants.TASK_CLEAR_TABLES, ExistingWorkPolicy.KEEP, clearTableRequest);
        return Result.success();
    }
}
