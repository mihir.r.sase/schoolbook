package com.shimbi.schoolbook.utils.workers;

import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.shimbi.schoolbook.db.helper.MyDataBase;
import com.shimbi.schoolbook.db.tables.DbNotice;
import com.shimbi.schoolbook.home.notice.models.NoticeResponseModel;
import com.shimbi.schoolbook.utils.AuthModel;
import com.shimbi.schoolbook.utils.Constants;
import com.shimbi.schoolbook.utils.RetrofitClient;
import com.shimbi.schoolbook.utils.SaveSharedPreference;

import java.io.IOException;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.work.Worker;
import androidx.work.WorkerParameters;
import retrofit2.Call;
import retrofit2.Response;

public class FetchNoticesWorker extends Worker {


    private static final String TAG = "FetchNoticesWorker";
    private String username;
    private String userEmail;
    private String userType;
    private String userId;
    private Context context;

    public FetchNoticesWorker(@NonNull Context context, @NonNull WorkerParameters workerParams) {
        super(context, workerParams);
        this.context = context;
    }

    @NonNull
    @Override
    public Result doWork() {
        int flag;
        if (getInputData().getString("From").equals("Initial")) {
            flag = 1;
        } else {
            flag = 0;
        }

        AuthModel authModel = SaveSharedPreference.getData();
        userId = authModel.uid;
        username = authModel.username;
        userEmail = authModel.userEmail;
        userType = authModel.userType;

        Call<NoticeResponseModel> apicall = RetrofitClient.get().getNoticeData(userId, userType,
                flag == 1 ? "" : SaveSharedPreference.getNoticesSyncDate(),
                flag == 1 ? "" : String.valueOf(SaveSharedPreference.getNoticesLatestID()));
        try {
            Response<NoticeResponseModel> response = apicall.execute();

            if (response.body() != null) {

                NoticeResponseModel model = response.body();

                List<NoticeResponseModel.NoticeResultModel> notices = model.getResult();
                if (notices.size() != 0) {
                    SaveSharedPreference.setNoticesSyncDate(model.getSyncdate());
                    SaveSharedPreference.setNoticesLatestId(notices.get(0).getEventId());

                    for (int i = 0; i < notices.size(); i++) {


                        DbNotice dbNotice = new DbNotice(notices.get(i));
                        if (flag == 1) {

                            MyDataBase.db.insertToNotice(dbNotice);
                        } else {
                            if (MyDataBase.db.checkIfPresentInNotice(notices.get(i).getEventId()) == 0) {
                                MyDataBase.db.insertToNotice(dbNotice);
                                Log.e(TAG, "doWork: Insertion Successful");
                            } else {
                                MyDataBase.db.updateNotices(dbNotice);
                                Log.e(TAG, "doWork: Updating Successful");
                            }
                        }

                    }
                    return Result.success();
                } else {
                    if (flag != 1) {
                        Log.e(TAG, "doWork: No New Data");
                        return Result.failure();
                    }
                    return Result.success();
                }

            } else {
                Log.e(TAG, "doWork: Failed");

                return Result.failure();
            }

        } catch (IOException e) {
            e.printStackTrace();
            Log.e(TAG, "doWork: ", e);
            return Result.failure();
        } finally {
            if (flag == 1) {
                SaveSharedPreference.setSyncStep(2);

                context.sendBroadcast(new Intent(Constants.ACTION));
            }
        }
    }
}
