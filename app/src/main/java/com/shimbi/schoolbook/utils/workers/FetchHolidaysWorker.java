package com.shimbi.schoolbook.utils.workers;

import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.shimbi.schoolbook.db.helper.MyDataBase;
import com.shimbi.schoolbook.db.tables.DbHolidays;
import com.shimbi.schoolbook.home.holidays.models.HolidayResponseModel;
import com.shimbi.schoolbook.utils.AuthModel;
import com.shimbi.schoolbook.utils.Constants;
import com.shimbi.schoolbook.utils.RetrofitClient;
import com.shimbi.schoolbook.utils.SaveSharedPreference;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.work.Worker;
import androidx.work.WorkerParameters;
import retrofit2.Call;
import retrofit2.Response;

public class FetchHolidaysWorker extends Worker {

    private static final String TAG = "FetchHolidaysWorker";
    private String username;
    private String userEmail;
    private String userType;
    private String userId;
    private Context context;

    public FetchHolidaysWorker(@NonNull Context context, @NonNull WorkerParameters workerParams) {
        super(context, workerParams);
        this.context = context;
    }

    @NonNull
    @Override
    public Result doWork() {

        int flag;
        if (getInputData().getString("From").equals("Initial")) {
            flag = 1;
        } else {
            flag = 0;
        }

        AuthModel authModel = SaveSharedPreference.getData();
        userId = authModel.uid;
        username = authModel.username;
        userEmail = authModel.userEmail;
        userType = authModel.userType;

        Call<List<HolidayResponseModel>> apicall = RetrofitClient.get().getHolidayData(userId, userType,
                "");
        try {
            Response<List<HolidayResponseModel>> response = apicall.execute();
            if (response.body() != null) {

                List<HolidayResponseModel> holidays = response.body();

                if (holidays.size() != 0) {
//                    SaveSharedPreference.setMessagesSyncDate(model.getSyncdate());
                    for (int i = 0; i < holidays.size(); i++) {
                        DbHolidays dbHolidays = new DbHolidays(holidays.get(i));

                        if (flag == 1) {

                            MyDataBase.db.insertToHolidays(dbHolidays);
                        } else {
                            if (MyDataBase.db.checkIfPresentInHolidays(Integer.valueOf(holidays.get(i).getHolidayId())) == 0) {
                                MyDataBase.db.insertToHolidays(dbHolidays);
                                Log.e(TAG, "doWork: Insertion Successful");
                            } else {
                                MyDataBase.db.updateHolidays(dbHolidays);
                                Log.e(TAG, "doWork: Updating Successful");
                            }
                        }
                    }
                    return Result.success();


                } else {
                    if (flag != 1) {
                        Log.e(TAG, "doWork: No New Data");
                        return Result.failure();
                    }
                    return Result.success();
                }

            } else {


                Log.e(TAG, "doWork: Failed");
                return Result.failure();
            }


        } catch (Exception e) {
            Log.e(TAG, "doWork: ", e);
            return Result.failure();
        } finally {
            if (flag == 1) {
                SaveSharedPreference.setSyncStep(5);
                context.sendBroadcast(new Intent(Constants.ACTION));
            }
        }
    }
}
