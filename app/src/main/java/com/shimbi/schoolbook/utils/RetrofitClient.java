package com.shimbi.schoolbook.utils;

import android.content.Context;
import android.os.Build;
import android.util.Log;

import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.security.ProviderInstaller;
import com.google.gson.GsonBuilder;
import com.shimbi.schoolbook.BuildConfig;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetrofitClient {

    private static final String TAG = "RetrofitClient";
    private static Retrofit retrofit = null;
    private static RetrofitApiCalls calls = null;

    public static RetrofitApiCalls get() {

        if (BuildConfig.DEBUG) {
            return init();
        } else {
            if (retrofit == null) {
                return init();
            }
        }
        return calls;

    }


    private static RetrofitApiCalls init() {
        OkHttpClient.Builder okBuilder = new OkHttpClient.Builder();
        okBuilder.connectTimeout(30, TimeUnit.SECONDS).writeTimeout(30, TimeUnit.SECONDS).readTimeout(30, TimeUnit.SECONDS);

        if (BuildConfig.DEBUG) {
            HttpLoggingInterceptor logging = new HttpLoggingInterceptor();

            logging.setLevel(HttpLoggingInterceptor.Level.BODY);

            okBuilder.addInterceptor(logging)
                    .build();
        }
        String baseUrl;
        if (BuildConfig.DEBUG) {

            if (SaveSharedPreference.getBaseUrl()) {
                baseUrl = Constants.BASE_URL_RELEASE;
            } else {
                baseUrl = Constants.BASE_URL_DEBUG;
            }
        } else {
            baseUrl = Constants.BASE_URL_RELEASE;
        }


        OkHttpClient okHttpClient = null;
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            okHttpClient = UnsafeOkHttpClient.getUnsafeOkHttpClient();

        } else {
            okHttpClient = okBuilder.build();
        }
        retrofit = new Retrofit.Builder().baseUrl(baseUrl)
                .addConverterFactory(GsonConverterFactory.create(new GsonBuilder()
                        .setLenient()
                        .create()))
                .client(okHttpClient)
                .build();

        calls = retrofit.create(RetrofitApiCalls.class);
        return calls;
    }

    /**
     * Used to remove ssl handshake error for api 16 to api 20
     *
     * @param context current application context
     */
    public static void updateAndroidSecurityProvider(Context context) {
        try {
            Log.e(TAG, "updateAndroidSecurityProvider: In update security");
            ProviderInstaller.installIfNeeded(context);
        } catch (GooglePlayServicesRepairableException e) {
            Log.e(TAG, "updateAndroidSecurityProvider: In exception");
            Alerts.displayToast(context, "Please Update Google play services to use the app");
            // Thrown when Google Play Services is not installed, up-to-date, or enabled
            // Show dialog to allow users to install, update, or otherwise enable Google Play services.
        } catch (GooglePlayServicesNotAvailableException e) {
            // Log.e("SecurityException", "Google Play Services not available. " + e);
            Alerts.displayToast(context, "Google play services are required to use the app");
        }
    }


}
