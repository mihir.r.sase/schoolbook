package com.shimbi.schoolbook.utils.calendar;



import java.util.Calendar;
import java.util.List;

import androidx.annotation.Nullable;

public interface OnDateSelectedListener {

    /**
     * Called after click on day of the month
     * @param dayCalendar Calendar of selected day
     * @param events Events of selected day
     */
    void onDateSelected(Calendar dayCalendar, @Nullable List<CalendarEvent> events);
}