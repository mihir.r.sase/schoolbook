package com.shimbi.schoolbook.utils;

public class Constants {

    public static final String BASE_URL_RELEASE = "https://www.studentbook.stmathewsacademy.edu.in/android_webservice_v1/";
    public static final String BASE_URL_DEBUG = "https://www.sb2.urdemo.net/android_webservice_v1/";

    public static final String TASK_SYNC_EVENTS = "TASK_SYNC_EVENTS";
    public static final String TASK_SYNC_ASSIGNMENT = "TASK_SYNC_ASSIGNMENT";
    public static final String TASK_SYNC_NOTICE = "TASK_SYNC_NOTICE";
    public static final String TASK_SYNC_MESSAGE = "TASK_SYNC_MESSAGE";
    public static final String TASK_SYNC_HOLIDAYS = "TASK_SYNC_HOLIDAYS";
    public static final String TASK_SYNC_LEAVES = "TASK_SYNC_LEAVES";
    public static final String TASK_SYNC_PhOTOS = "TASK_SYNC_PHOTOS";
    public static final String TASK_SYNC_USERS = "TASK_SYNC_USERS";

    public static final String TASK_FETCH_EVENTS = "TASK_FETCH_EVENTS";
    public static final String TASK_FETCH_NOTICES = "TASK_FETCH_NOTICES";
    public static final String TASK_FETCH_ASSIGNMENTS = "TASK_FETCH_ASSIGNMENTS";
    public static final String TASK_FETCH_MESSAGES = "TASK_FETCH_MESSAGES";
    public static final String TASK_FETCH_HOLIDAYS = "TASK_FETCH_HOLIDAYS";
    public static final String TASK_FETCH_LEAVES = "TASK_FETCH_LEAVES";
    public static final String TASK_CLEAR_TABLES = "TASK_CLEAR_TABLES";

    public static final String ACTION = "ACTION_SYNC_STEP_RECEIVER";

}
