package com.shimbi.schoolbook.utils;

import android.content.Context;
import android.net.ConnectivityManager;
import android.os.Build;

import androidx.work.Constraints;
import androidx.work.NetworkType;

public class NetworkStatusChecker {


    /**
     * Help to know status of Network conn
     *
     * @param context app context
     * @return bool
     */
    public static boolean isNetworkConnected(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);

        if (cm != null) {
            return cm.getActiveNetworkInfo() == null || !cm.getActiveNetworkInfo().isConnected();
        }
        return true;
    }

    public static Constraints getNetWorkConstraints() {
        // Create a Constraints object that defines when the task should run

        Constraints.Builder builder = new Constraints.Builder();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            builder.setRequiresDeviceIdle(false);
        }
        builder.setRequiresCharging(false);
        builder.setRequiredNetworkType(NetworkType.CONNECTED);
        return builder.build();
    }

//
//    @Override
//    public void onReceive(Context context, Intent intent) {
//
//        if (intent.getAction().equals(AppCons)) {
//            // update the connection
//        }
//    }


}
