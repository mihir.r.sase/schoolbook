package com.shimbi.schoolbook.splash;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.WindowManager;

import com.shimbi.schoolbook.R;
import com.shimbi.schoolbook.home.activities.ActivityHome;
import com.shimbi.schoolbook.login.activites.ActivityLogin;
import com.shimbi.schoolbook.login.activites.ActivitySync;
import com.shimbi.schoolbook.utils.MyNotificationManager;
import com.shimbi.schoolbook.utils.SaveSharedPreference;

import androidx.appcompat.app.AppCompatActivity;

public class ActivitySplash extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_splash);


        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                MyNotificationManager.createNotificationChannel(ActivitySplash.this);
                if (SaveSharedPreference.getLoggedStatus()) {

                    if (SaveSharedPreference.getSyncStep() == 7) {
                        Intent intent = new Intent(ActivitySplash.this, ActivityHome.class)
                                .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(intent);
                    } else {
                        Intent intent = new Intent(ActivitySplash.this, ActivitySync.class)
                                .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(intent);
                    }

                } else {
                    Intent intent = new Intent(ActivitySplash.this, ActivityLogin.class)
                            .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);
                }
            }
        }, 3000);
    }
}
