package com.shimbi.schoolbook.home.notice.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class NoticeResponseModel {
    @SerializedName("syncdate")
    @Expose
    private String syncdate;
    @SerializedName("no_of_record")
    @Expose
    private Integer noOfRecord;
    @SerializedName("total_pages")
    @Expose
    private Integer totalPages;
    @SerializedName("result")
    @Expose
    private List<NoticeResultModel> result = null;

    public String getSyncdate() {
        return syncdate;
    }

    public void setSyncdate(String syncdate) {
        this.syncdate = syncdate;
    }

    public Integer getNoOfRecord() {
        return noOfRecord;
    }

    public void setNoOfRecord(Integer noOfRecord) {
        this.noOfRecord = noOfRecord;
    }

    public Integer getTotalPages() {
        return totalPages;
    }

    public void setTotalPages(Integer totalPages) {
        this.totalPages = totalPages;
    }

    public List<NoticeResultModel> getResult() {
        return result;
    }

    public void setResult(List<NoticeResultModel> result) {
        this.result = result;
    }

    @Override
    public String toString() {
        return "NoticeResponseModel{" +
                "syncdate='" + syncdate + '\'' +
                ", noOfRecord=" + noOfRecord +
                ", totalPages=" + totalPages +
                ", result=" + result +
                '}';
    }

    public class NoticeResultModel {


        @SerializedName("username")
        @Expose
        private String username;
        @SerializedName("event_id")
        @Expose
        private Integer eventId;
        @SerializedName("no_reply")
        @Expose
        private Integer noReply;
        @SerializedName("event_title")
        @Expose
        private String eventTitle;
        @SerializedName("description")
        @Expose
        private String description;
        @SerializedName("added_date")
        @Expose
        private String addedDate;
        @SerializedName("formatted_date")
        @Expose
        private String formattedDate;
        @SerializedName("picture")
        @Expose
        private String picture;
        @SerializedName("userimage")
        @Expose
        private String userimage;
        @SerializedName("class_key")
        @Expose
        private String classKey;
        @SerializedName("class_value")
        @Expose
        private String classValue;
        @SerializedName("startdate")
        @Expose
        private String startdate;
        @SerializedName("enddate")
        @Expose
        private String enddate;
        @SerializedName("starttime")
        @Expose
        private String starttime;
        @SerializedName("endtime")
        @Expose
        private String endtime;
        @SerializedName("eventstatue")
        @Expose
        private String eventstatue;
        @SerializedName("group_name")
        @Expose
        private String groupName;
        @SerializedName("notice_sms")
        @Expose
        private String noticeSms;
        @SerializedName("send_sms_flag")
        @Expose
        private Integer sendSmsFlag;
        @SerializedName("submiton_date")
        @Expose
        private String submitonDate;
        @SerializedName("event_date")
        @Expose
        private String eventDate;
        @SerializedName("flag")
        @Expose
        private String flag;

        public NoticeResultModel(String username, String userimage, String eventTitle, String description,
                                 String addedDate, Integer eventId, Integer noReply,
                                 String classValue, Integer sendSmsFlag, String noticeSms) {
            this.username = username;
            this.userimage = userimage;
            this.eventTitle = eventTitle;
            this.description = description;
            this.addedDate = addedDate;
            this.eventId = eventId;
            this.noReply = noReply;
            this.classValue = classValue;
            this.sendSmsFlag = sendSmsFlag;
            this.noticeSms = noticeSms;
        }

        @Override
        public String toString() {
            return "NoticeResultModel{" +
                    "username='" + username + '\'' +
                    ", eventId=" + eventId +
                    ", noReply=" + noReply +
                    ", eventTitle='" + eventTitle + '\'' +
                    ", description='" + description + '\'' +
                    ", addedDate='" + addedDate + '\'' +
                    ", formattedDate='" + formattedDate + '\'' +
                    ", picture='" + picture + '\'' +
                    ", userimage='" + userimage + '\'' +
                    ", classKey='" + classKey + '\'' +
                    ", classValue='" + classValue + '\'' +
                    ", startdate='" + startdate + '\'' +
                    ", enddate='" + enddate + '\'' +
                    ", starttime='" + starttime + '\'' +
                    ", endtime='" + endtime + '\'' +
                    ", eventstatue='" + eventstatue + '\'' +
                    ", groupName='" + groupName + '\'' +
                    ", noticeSms='" + noticeSms + '\'' +
                    ", sendSmsFlag=" + sendSmsFlag +
                    ", submitonDate='" + submitonDate + '\'' +
                    ", eventDate='" + eventDate + '\'' +
                    ", flag='" + flag + '\'' +
                    '}';
        }

        public String getUsername() {
            return username;
        }

        public void setUsername(String username) {
            this.username = username;
        }

        public Integer getEventId() {
            return eventId;
        }

        public void setEventId(Integer eventId) {
            this.eventId = eventId;
        }

        public Integer getNoReply() {
            return noReply;
        }

        public void setNoReply(Integer noReply) {
            this.noReply = noReply;
        }

        public String getEventTitle() {
            return eventTitle;
        }

        public void setEventTitle(String eventTitle) {
            this.eventTitle = eventTitle;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public String getAddedDate() {
            return addedDate;
        }

        public void setAddedDate(String addedDate) {
            this.addedDate = addedDate;
        }

        public String getFormattedDate() {
            return formattedDate;
        }

        public void setFormattedDate(String formattedDate) {
            this.formattedDate = formattedDate;
        }

        public String getPicture() {
            return picture;
        }

        public void setPicture(String picture) {
            this.picture = picture;
        }

        public String getUserimage() {
            return userimage;
        }

        public void setUserimage(String userimage) {
            this.userimage = userimage;
        }

        public String getClassKey() {
            return classKey;
        }

        public void setClassKey(String classKey) {
            this.classKey = classKey;
        }

        public String getClassValue() {
            return classValue;
        }

        public void setClassValue(String classValue) {
            this.classValue = classValue;
        }

        public String getStartdate() {
            return startdate;
        }

        public void setStartdate(String startdate) {
            this.startdate = startdate;
        }

        public String getEnddate() {
            return enddate;
        }

        public void setEnddate(String enddate) {
            this.enddate = enddate;
        }

        public String getStarttime() {
            return starttime;
        }

        public void setStarttime(String starttime) {
            this.starttime = starttime;
        }

        public String getEndtime() {
            return endtime;
        }

        public void setEndtime(String endtime) {
            this.endtime = endtime;
        }

        public String getEventstatue() {
            return eventstatue;
        }

        public void setEventstatue(String eventstatue) {
            this.eventstatue = eventstatue;
        }

        public String getGroupName() {
            return groupName;
        }

        public void setGroupName(String groupName) {
            this.groupName = groupName;
        }

        public String getNoticeSms() {
            return noticeSms;
        }

        public void setNoticeSms(String noticeSms) {
            this.noticeSms = noticeSms;
        }

        public Integer getSendSmsFlag() {
            return sendSmsFlag;
        }

        public void setSendSmsFlag(Integer sendSmsFlag) {
            this.sendSmsFlag = sendSmsFlag;
        }

        public String getSubmitonDate() {
            return submitonDate;
        }

        public void setSubmitonDate(String submitonDate) {
            this.submitonDate = submitonDate;
        }

        public String getEventDate() {
            return eventDate;
        }

        public void setEventDate(String eventDate) {
            this.eventDate = eventDate;
        }

        public String getFlag() {
            return flag;
        }

        public void setFlag(String flag) {
            this.flag = flag;
        }
    }
}
