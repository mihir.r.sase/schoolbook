package com.shimbi.schoolbook.home.leaves.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class LeaveAproveListResponseModel {

    @SerializedName("leave_id")
    @Expose
    private String leaveId;
    @SerializedName("added_id")
    @Expose
    private String addedId;
    @SerializedName("added_date")
    @Expose
    private String addedDate;
    @SerializedName("added_type")
    @Expose
    private String addedType;
    @SerializedName("modified_id")
    @Expose
    private String modifiedId;
    @SerializedName("modified_date")
    @Expose
    private String modifiedDate;
    @SerializedName("modified_type")
    @Expose
    private String modifiedType;
    @SerializedName("leave_start_date")
    @Expose
    private String leaveStartDate;
    @SerializedName("leave_end_date")
    @Expose
    private String leaveEndDate;
    @SerializedName("leave_reason")
    @Expose
    private String leaveReason;
    @SerializedName("student_id")
    @Expose
    private String studentId;
    @SerializedName("leave_status")
    @Expose
    private String leaveStatus;
    @SerializedName("reject_reason")
    @Expose
    private String rejectReason;
    @SerializedName("added_from")
    @Expose
    private String addedFrom;
    @SerializedName("leave_document")
    @Expose
    private String leaveDocument;
    @SerializedName("user_name")
    @Expose
    private String userName;

    public LeaveAproveListResponseModel(LeaveAproveListResponseModel leaveAproveListResponseModel) {
    }


    public String getLeaveId() {
        return leaveId;
    }

    public void setLeaveId(String leaveId) {
        this.leaveId = leaveId;
    }

    public String getAddedId() {
        return addedId;
    }

    public void setAddedId(String addedId) {
        this.addedId = addedId;
    }

    public String getAddedDate() {
        return addedDate;
    }

    public void setAddedDate(String addedDate) {
        this.addedDate = addedDate;
    }

    public String getAddedType() {
        return addedType;
    }

    public void setAddedType(String addedType) {
        this.addedType = addedType;
    }

    public String getModifiedId() {
        return modifiedId;
    }

    public void setModifiedId(String modifiedId) {
        this.modifiedId = modifiedId;
    }

    public String getModifiedDate() {
        return modifiedDate;
    }

    public void setModifiedDate(String modifiedDate) {
        this.modifiedDate = modifiedDate;
    }

    public String getModifiedType() {
        return modifiedType;
    }

    public void setModifiedType(String modifiedType) {
        this.modifiedType = modifiedType;
    }

    public String getLeaveStartDate() {
        return leaveStartDate;
    }

    public void setLeaveStartDate(String leaveStartDate) {
        this.leaveStartDate = leaveStartDate;
    }

    public String getLeaveEndDate() {
        return leaveEndDate;
    }

    public void setLeaveEndDate(String leaveEndDate) {
        this.leaveEndDate = leaveEndDate;
    }

    public String getLeaveReason() {
        return leaveReason;
    }

    public void setLeaveReason(String leaveReason) {
        this.leaveReason = leaveReason;
    }

    public String getStudentId() {
        return studentId;
    }

    public void setStudentId(String studentId) {
        this.studentId = studentId;
    }

    public String getLeaveStatus() {
        return leaveStatus;
    }

    public void setLeaveStatus(String leaveStatus) {
        this.leaveStatus = leaveStatus;
    }

    public String getRejectReason() {
        return rejectReason;
    }

    public void setRejectReason(String rejectReason) {
        this.rejectReason = rejectReason;
    }

    public String getAddedFrom() {
        return addedFrom;
    }

    public void setAddedFrom(String addedFrom) {
        this.addedFrom = addedFrom;
    }

    public String getLeaveDocument() {
        return leaveDocument;
    }

    public void setLeaveDocument(String leaveDocument) {
        this.leaveDocument = leaveDocument;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }
}
