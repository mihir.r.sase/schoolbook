package com.shimbi.schoolbook.home.photos.fragments;


import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.shimbi.schoolbook.R;
import com.shimbi.schoolbook.db.helper.MyDataBase;
import com.shimbi.schoolbook.db.tables.DbPhoto;
import com.shimbi.schoolbook.home.activities.ActivityHome;
import com.shimbi.schoolbook.home.photos.adapters.PhotosAdapter;
import com.shimbi.schoolbook.home.photos.models.PhotosListResponseModel;
import com.shimbi.schoolbook.utils.AuthModel;
import com.shimbi.schoolbook.utils.SaveSharedPreference;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * A simple {@link Fragment} subclass.
 */
public class FragmentAlbum extends Fragment {

    private static final String TAG = "FragmentAlbum";
    public static String username;
    public static String userEmail;
    public static String userType;
    public static String userId;
    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;
    @BindView(R.id.swipe_container)
    SwipeRefreshLayout swipeRefreshLayout;
    @BindView(R.id.album_name)
    TextView tvAlbumName;
    private PhotosAdapter adapter;
    private GridLayoutManager gridLayoutManager;
    private List<PhotosListResponseModel.Photo> myPhotoList = new ArrayList<>();
    public ProgressBar syncProgress;
    private ImageView noData;

    public FragmentAlbum() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        ActivityHome activityHome = (ActivityHome) getActivity();
        activityHome.setTitleAndLogo("Photos", R.drawable.ic_photo_library_black_24dp);
        ActivityHome.fab.setVisibility(View.GONE);
        if (getActivity() != null) {
            syncProgress = getActivity().findViewById(R.id.progress_sync);
            syncProgress.setVisibility(View.GONE);
            noData = getActivity().findViewById(R.id.no_data);
            noData.setVisibility(View.GONE);
        }

        return inflater.inflate(R.layout.fragment_album, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);

        init();

        loadData();


    }

    private void init() {

        AuthModel authModel = SaveSharedPreference.getData();
        userId = authModel.uid;
        username = authModel.username;
        userEmail = authModel.userEmail;
        userType = authModel.userType;

        if (authModel.photoAddFlag.equals("1")) {
            ActivityHome.fab.setVisibility(View.VISIBLE);
        }

        adapter = new PhotosAdapter(getActivity());
        gridLayoutManager = new GridLayoutManager(this.getActivity(), 3);
        recyclerView.setLayoutManager(gridLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(adapter);


    }

    private void loadData() {

        try {
            PhotosListResponseModel model = new PhotosListResponseModel();
            Integer albumId = Integer.valueOf(getArguments().getString("albumId"));
            tvAlbumName.setText(MyDataBase.db.getAlbumName(albumId));
            MyDataBase.db.getPhotosFromAlbum(albumId).observe(this, new Observer<List<DbPhoto>>() {
                @Override
                public void onChanged(List<DbPhoto> dbPhotos) {
                    myPhotoList.clear();
                    for (int i = 0; i < dbPhotos.size(); i++) {
                        myPhotoList.add(model.new Photo(dbPhotos.get(i)));
                    }
                    adapter.addAllPhotos(myPhotoList);
                }
            });


        } catch (Exception e) {
            Log.e(TAG, "loadData: ", e);
        }

    }

}
