package com.shimbi.schoolbook.home.fees.activities;


import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.google.android.material.appbar.MaterialToolbar;
import com.shimbi.schoolbook.BuildConfig;
import com.shimbi.schoolbook.R;
import com.shimbi.schoolbook.utils.Alerts;
import com.shimbi.schoolbook.utils.AuthModel;
import com.shimbi.schoolbook.utils.Constants;
import com.shimbi.schoolbook.utils.NetworkStatusChecker;
import com.shimbi.schoolbook.utils.SaveSharedPreference;

import androidx.appcompat.app.AppCompatActivity;
import butterknife.BindView;
import butterknife.ButterKnife;

public class ActivityFeeStructure extends AppCompatActivity {

    @BindView(R.id.llProgressBar)
    View llProgressBar;
    private String username;
    private String userEmail;
    private String userType;
    private String userId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fee_structure);

        ButterKnife.bind(this);

        init();

        WebView webView = findViewById(R.id.web_view);

        if (!NetworkStatusChecker.isNetworkConnected(getApplicationContext())) {
            progressVisibility(true);
            webView.setWebViewClient(new WebViewClient() {
                @Override
                public boolean shouldOverrideUrlLoading(WebView view, String url) {
                    view.loadUrl(url);
                    return true;
                }

                @Override
                public void onPageFinished(WebView view, String url) {
                    progressVisibility(false);

                }

                @Override
                public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
                    Alerts.displayToast(getApplicationContext(), "Loading Failed!");

                }
            });
            String baseUrl;
            if (BuildConfig.DEBUG) {

                if (SaveSharedPreference.getBaseUrl()) {
                    baseUrl = Constants.BASE_URL_RELEASE;
                } else {
                    baseUrl = Constants.BASE_URL_DEBUG;
                }
            } else {
                baseUrl = Constants.BASE_URL_RELEASE;
            }
            webView.loadUrl(baseUrl + "fees/fee_table");
        } else {
            Alerts.displayToast(getApplicationContext(), "Please check your connection and try again.");
        }
    }

    private void init() {
        AuthModel authModel = SaveSharedPreference.getData();
        userId = authModel.uid;
        username = authModel.username;
        userEmail = authModel.userEmail;
        userType = authModel.userType;

        MaterialToolbar materialToolbar = findViewById(R.id.appbar);
        setSupportActionBar(materialToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle presses on the action bar items
        int id = item.getItemId();
        if (id == android.R.id.home) {
            finish();
        }


        return super.onOptionsItemSelected(item);
    }

    /**
     * Display progress bar and uses TOUCHABLE flags
     *
     * @param visibility Give True to display dialog, False to remove
     */
    private void progressVisibility(boolean visibility) {
        if (visibility) {
            llProgressBar.setVisibility(View.VISIBLE);
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                    WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
        } else {
            llProgressBar.setVisibility(View.GONE);
            getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
        }
    }
}
