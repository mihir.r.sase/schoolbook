package com.shimbi.schoolbook.home.photos.adapters;

import android.content.Context;
import android.net.Uri;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.android.material.textfield.TextInputEditText;
import com.shimbi.schoolbook.R;
import com.shimbi.schoolbook.home.photos.models.PhotoAttachmentModel;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;

public class PhotoAttachmentAdapter extends RecyclerView.Adapter<PhotoAttachmentAdapter.MyViewHolder> {

    private static final String TAG = "PhotoAttachmentAdapter";
    Context context;
    private List<String> attachmentList;
    private List<String> pathList;
    private List<Uri> uriList;
    private Map<Integer, String> titleList;
    private Map<Integer, String> descList;

    public PhotoAttachmentAdapter(Context context) {
        attachmentList = new ArrayList<>();
        pathList = new ArrayList<>();
        uriList = new ArrayList<>();
        titleList = new HashMap<>();
        descList = new HashMap<>();
        this.context = context;
    }

    public void add(String file, Uri uri) {
        attachmentList.add(file);
        // pathList.add(filePath);
        uriList.add(uri);
        titleList.put(attachmentList.size() - 1, "");
        descList.put(attachmentList.size() - 1, "");
        notifyItemInserted(attachmentList.size() - 1);
    }

    public List<PhotoAttachmentModel> getAll() {
        List<PhotoAttachmentModel> list = new ArrayList<>();
        for (int i = 0; i < getItemCount(); i++) {
            list.add(new PhotoAttachmentModel(attachmentList.get(i),
                    titleList.get(i) == null ? "" : titleList.get(i),
                    descList.get(i) == null ? "" : descList.get(i),
                    pathList.get(i)));
        }
        return list;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_attach_photo, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        try {
            String filename = attachmentList.get(position);
            holder.tvFileName.setText(filename);
            holder.etImageTitle.setText(titleList.get(position));
            holder.etImageDescription.setText(descList.get(position));
            holder.cancel_button.setOnClickListener(view -> {
                attachmentList.remove(holder.getAdapterPosition());
                titleList.remove(holder.getAdapterPosition());
                descList.remove(holder.getAdapterPosition());
                //pathList.remove(holder.getAdapterPosition());
                uriList.remove(holder.getAdapterPosition());
                notifyDataSetChanged();
            });
            Glide.with(context).load(uriList.get(position)).into(holder.imageView);

            holder.etImageTitle.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                    titleList.put(position, charSequence.toString());

                }

                @Override
                public void afterTextChanged(Editable editable) {

                }
            });

            holder.etImageDescription.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                    descList.put(position, charSequence.toString());

                }

                @Override
                public void afterTextChanged(Editable editable) {

                }
            });


        } catch (Exception e) {
            Log.e(TAG, "onBindViewHolder: ", e);
        }
    }

    @Override
    public int getItemCount() {
        return attachmentList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.et_image_title)
        public TextInputEditText etImageTitle;
        @BindView(R.id.tv_file_name)
        TextView tvFileName;
        @BindView(R.id.cancel_button)
        ImageView cancel_button;
        @BindView(R.id.et_image_description)
        TextInputEditText etImageDescription;
        @BindView(R.id.iv_image)
        ImageView imageView;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

    }
}
