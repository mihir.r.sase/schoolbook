package com.shimbi.schoolbook.home.leaves.adapters;

import android.content.Context;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.content.res.Resources;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.android.material.card.MaterialCardView;
import com.google.android.material.chip.Chip;
import com.google.gson.Gson;
import com.shimbi.schoolbook.R;
import com.shimbi.schoolbook.home.leaves.activities.ActivityNewLeave;
import com.shimbi.schoolbook.home.leaves.models.LeaveListResponseModel;
import com.shimbi.schoolbook.utils.calendar.TimeFormatter;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class LeavesAdapter extends RecyclerView.Adapter<LeavesAdapter.LeavesHolder> {

    private static final String TAG = "LeavesAdapter";
    Context context;
    private List<LeaveListResponseModel.Leave> leaveList;
    private TimeFormatter mTimeFormatter = new TimeFormatter();

    public LeavesAdapter() {
        this.leaveList = new ArrayList<>();
    }

    @NonNull
    @Override
    public LeavesHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_leave, parent, false);
        context = parent.getContext();
        return new LeavesHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull LeavesHolder holder, int position) {
        LeaveListResponseModel.Leave leave = leaveList.get(position);

        try {
            SimpleDateFormat formatter = new SimpleDateFormat("MMM dd, yyyy");
            Resources resources = context.getResources();
            if (leave.getLeaveStatus().equals("holiday")) {
                holder.tvTitle.setText("Holiday");
                holder.tvDesc.setText(leave.getHolidayName());
                holder.chip.setText("Holiday");
                holder.chip.setChipBackgroundColor(ColorStateList.valueOf(resources.getColor(R.color.colorHoliday)));
                Date date1 = new SimpleDateFormat("yyyy-MM-dd").parse(leave.getHolidayFromDate());
                holder.tvStartDate.setText(formatter.format(date1));
                if (leave.getHolidayFromDate().equals(leave.getHolidayToDate())) {
                    holder.tvTo.setVisibility(View.GONE);
                    holder.tvEndDate.setVisibility(View.GONE);
                } else {
                    holder.tvTo.setVisibility(View.VISIBLE);
                    holder.tvEndDate.setVisibility(View.VISIBLE);
                    Date date2 = new SimpleDateFormat("yyyy-MM-dd").parse(leave.getHolidayToDate());
                    holder.tvEndDate.setText(formatter.format(date2));
                }
            } else {
                holder.tvTitle.setText("Leave");
                holder.tvDesc.setText(leave.getLeaveReason());
                Date date1 = new SimpleDateFormat("yyyy-MM-dd").parse(leave.getLeaveStartDate());
                holder.tvStartDate.setText(formatter.format(date1));
                if (leave.getLeaveStartDate().equals(leave.getLeaveEndDate())) {
                    holder.tvTo.setVisibility(View.GONE);
                    holder.tvEndDate.setVisibility(View.GONE);
                } else {
                    Date date2 = new SimpleDateFormat("yyyy-MM-dd").parse(leave.getLeaveEndDate());
                    holder.tvEndDate.setText(formatter.format(date2));
                    holder.tvTo.setVisibility(View.VISIBLE);
                    holder.tvEndDate.setVisibility(View.VISIBLE);
                }

                if (leaveList.get(position).getLeaveStatus().equals("Hold")) {
                    holder.chip.setText("On Hold");
                    holder.chip.setChipBackgroundColor(ColorStateList.valueOf(resources.getColor(R.color.colorHold)));
                } else if (leaveList.get(position).getLeaveStatus().equals("Approve")) {
                    holder.chip.setText("Approved");
                    holder.chip.setChipBackgroundColor(ColorStateList.valueOf(resources.getColor(R.color.colorApprove)));
                } else if (leaveList.get(position).getLeaveStatus().equals("Deny")) {
                    holder.chip.setText("Denied");
                    holder.chip.setChipBackgroundColor(ColorStateList.valueOf(resources.getColor(R.color.colorDeny)));
                }

            }
        } catch (Exception e) {
            Log.e(TAG, "onBindViewHolder: ", e);
        }

        holder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (leave.getLeaveStatus().equals("Hold")) {
                    Intent intent = new Intent(view.getContext(), ActivityNewLeave.class);
                    Bundle bundle = new Bundle();
                    bundle.putString("leave", new Gson().toJson(leave));
                    intent.putExtras(bundle);
                    view.getContext().startActivity(intent);

                }
            }
        });

    }


    @Override
    public int getItemCount() {
        return leaveList.size();
    }

    public void addEvents(List<LeaveListResponseModel.Leave> events) {
        for (int i = 0; i < events.size(); i++) {
            leaveList.add(events.get(i));
            notifyItemInserted(i);
        }
    }

    public void addEvent(LeaveListResponseModel.Leave event) {
        leaveList.add(event);
        notifyItemInserted(leaveList.size());
    }

    public void clearList() {
        leaveList.clear();
        notifyDataSetChanged();
    }

    class LeavesHolder extends RecyclerView.ViewHolder {

        TextView tvStartDate, tvTitle, tvDesc, tvEndDate, tvTo;
        MaterialCardView cardView;
        Chip chip;

        LeavesHolder(View itemView) {
            super(itemView);

            cardView = itemView.findViewById(R.id.cardView);
            tvStartDate = itemView.findViewById(R.id.tv_start_date);
            tvTitle = itemView.findViewById(R.id.tv_title);
            tvDesc = itemView.findViewById(R.id.tv_desc);
            tvEndDate = itemView.findViewById(R.id.tv_end_date);
            tvTo = itemView.findViewById(R.id.tv_to);
            chip = itemView.findViewById(R.id.chip);
        }
    }
}
