package com.shimbi.schoolbook.home.transport.fragments;


import android.Manifest;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.location.Location;
import android.media.MediaPlayer;
import android.os.Build;
import android.os.Bundle;
import android.os.Looper;
import android.os.VibrationEffect;
import android.os.Vibrator;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.ResolvableApiException;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResponse;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.tasks.Task;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.shimbi.schoolbook.R;
import com.shimbi.schoolbook.db.helper.MyDataBase;
import com.shimbi.schoolbook.db.tables.DbEmbarkedUsers;
import com.shimbi.schoolbook.db.tables.DbPickupLocations;
import com.shimbi.schoolbook.db.tables.DbRouteDetails;
import com.shimbi.schoolbook.db.tables.DbTransportSync;
import com.shimbi.schoolbook.db.tables.DbTransportUsers;
import com.shimbi.schoolbook.db.tables.DbVisitedBusStop;
import com.shimbi.schoolbook.db.tables.DbYetToBeEmbarked;
import com.shimbi.schoolbook.home.transport.adapters.TransportDialogAdapter;
import com.shimbi.schoolbook.home.transport.models.AssignedRouteModel;
import com.shimbi.schoolbook.home.transport.models.NotificationSuccessModel;
import com.shimbi.schoolbook.home.transport.models.TransportDataModel;
import com.shimbi.schoolbook.services.TransportService;
import com.shimbi.schoolbook.utils.Alerts;
import com.shimbi.schoolbook.utils.AuthModel;
import com.shimbi.schoolbook.utils.ConnectionStateMonitor;
import com.shimbi.schoolbook.utils.RetrofitClient;
import com.shimbi.schoolbook.utils.SaveSharedPreference;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class FragmentAdvance extends Fragment {

    private static final String TAG = "FragmentAdvance";
    private static final int RADIUS = 200;

    @BindView(R.id.bt_begin_journey)
    MaterialButton btBeginJourney;
    @BindView(R.id.bt_embark)
    MaterialButton btEmbark;
    @BindView(R.id.bt_disembark)
    MaterialButton btDisembark;
    @BindView(R.id.tv_time1)
    TextView tvTime1;
    @BindView(R.id.tv_am1)
    TextView tvAm1;
    @BindView(R.id.tv_time3)
    TextView tvTime3;
    @BindView(R.id.tv_am3)
    TextView tvAm3;
    @BindView(R.id.circle_1)
    ImageView circle1;
    @BindView(R.id.circle_2)
    ImageView circle2;
    @BindView(R.id.circle_3)
    ImageView circle3;
    @BindView(R.id.fab_settings)
    FloatingActionButton fabSettings;
    @BindView(R.id.fab_warning)
    FloatingActionButton fabWarning;
    @BindView(R.id.fab_change_loc)
    FloatingActionButton fabLocation;
    @BindView(R.id.fab_switch_routes)
    FloatingActionButton fabSwitchRoute;
    SimpleDateFormat formatter1 = new SimpleDateFormat("hh:mm");
    SimpleDateFormat formatter2 = new SimpleDateFormat("a");
    private List<DbRouteDetails> routeDetailsList = new ArrayList<>();
    private List<DbPickupLocations> pickupLocationsList = new ArrayList<>();
    private List<DbTransportUsers> transportUsersList = new ArrayList<>();
    private List<DbEmbarkedUsers> embarkedUsersList = new ArrayList<>();
    private List<DbYetToBeEmbarked> yetToBeEmbarkedList = new ArrayList<>();
    private int startPickupId, endPickupId;
    private int currentPickupId;
    private String markerTitle;
    private FusedLocationProviderClient mFusedLocationClient;
    private LocationRequest locationRequest;
    private LocationCallback locationCallback;
    private String username;
    private String userEmail;
    private String userType;
    private String userId;
    private Integer journeyStatus;
    private Integer routeId;
    private Integer busId;
    private Double latitude;
    private Double longitude;
    private MediaPlayer mediaPlayer;
    private Vibrator vibrator;
    private boolean fabToggle = false;
    public View llProgressBar;


    public FragmentAdvance() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_advance, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);

        init();

        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(getActivity());
        locationRequest = LocationRequest.create();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationRequest.setInterval(8 * 1000);
        locationCallback = new LocationCallback() {
            @Override
            public void onLocationResult(LocationResult locationResult) {

                if (locationResult == null) {
                    return;
                }
                for (Location location : locationResult.getLocations()) {
                    if (location != null) {
                        latitude = location.getLatitude();
                        longitude = location.getLongitude();
                        onLocationChanged(location);
                    }
                }
            }
        };
        checkForLocationPermission();

        btBeginJourney.setOnClickListener(view1 -> beginJourney());

        btEmbark.setOnClickListener(v -> embarkStudents());

        btDisembark.setOnClickListener(v -> disembarkStudents());

        fabSettings.setOnClickListener(v -> {
            if (fabToggle) {
                fabToggle = false;
                fabSwitchRoute.setVisibility(View.GONE);
                fabWarning.setVisibility(View.GONE);
                fabLocation.setVisibility(View.GONE);
            } else {
                fabToggle = true;
                fabSwitchRoute.setVisibility(View.VISIBLE);
                fabWarning.setVisibility(View.VISIBLE);
                fabLocation.setVisibility(View.VISIBLE);
            }
        });

        fabWarning.setOnClickListener(v -> displayWarningDialog());

        fabLocation.setOnClickListener(v -> displayLocationChangeDialog());

        fabSwitchRoute.setOnClickListener(v -> displayRouteSelectionDialog(true));

        if (SaveSharedPreference.getJorneyStatus() < 4) {

            ConnectionStateMonitor connectionStateMonitor = new ConnectionStateMonitor(getActivity());
            connectionStateMonitor.observe(this, new Observer<Boolean>() {
                @Override
                public void onChanged(Boolean aBoolean) {
                    if (aBoolean) {
                        Alerts.displayToast(getContext(), "Connected");
                        List<DbTransportSync> list = MyDataBase.db.getTransportSync();
                        if (!list.isEmpty()) {
                            for (int i = 0; i < list.size(); i++) {
                                sendNotificationSync(list.get(i));
                            }
                        }
                    } else {
                        // network lost
                        Alerts.displayToast(getContext(), "Disconnected");
                    }
                }
            });
        }
    }


    private void init() {
        AuthModel authModel = SaveSharedPreference.getData();
        userId = authModel.uid;
        userType = authModel.userType;
        username = authModel.username;
        userEmail = authModel.userEmail;

        if (getActivity() != null) {
            llProgressBar = getActivity().findViewById(R.id.llProgressBar);
        }

        mediaPlayer = MediaPlayer.create(getContext(), R.raw.notification);
        vibrator = (Vibrator) getActivity().getSystemService(Context.VIBRATOR_SERVICE);

        journeyStatus = SaveSharedPreference.getJorneyStatus();
        if (journeyStatus == 1) {
            btBeginJourney.setEnabled(true);
            btEmbark.setEnabled(false);
            btDisembark.setEnabled(false);
            circle1.setImageResource(R.drawable.circle);
            circle2.setImageResource(R.drawable.circle_border);
            circle3.setImageResource(R.drawable.circle_border);
        } else if (journeyStatus == 2) {
            btBeginJourney.setEnabled(false);
            btEmbark.setEnabled(false);
            btDisembark.setEnabled(false);
            circle1.setImageResource(R.drawable.circle_border);
            circle2.setImageResource(R.drawable.circle);
            circle3.setImageResource(R.drawable.circle_border);
        } else if (journeyStatus == 3) {
            btBeginJourney.setEnabled(false);
            btEmbark.setEnabled(false);
            btDisembark.setEnabled(true);
            circle1.setImageResource(R.drawable.circle_border);
            circle2.setImageResource(R.drawable.circle_border);
            circle3.setImageResource(R.drawable.circle);
        } else {
            btBeginJourney.setEnabled(false);
            btEmbark.setEnabled(false);
            btDisembark.setEnabled(false);
            circle1.setImageResource(R.drawable.circle_border);
            circle2.setImageResource(R.drawable.circle_border);
            circle3.setImageResource(R.drawable.circle_border);
        }
//        TutoShowcase.from(getActivity())
//                .setContentView(R.layout.toto_content)
//                .on(R.id.view_pager)
//                .displaySwipableLeft()
//                .show();
    }

    private void loadData() {


        if (journeyStatus == 1) {
            getRouteDetailsFromServer();
        } else {
            getRouteDetailsFromLocal();
            getTransportDataFromLocal();
            getEmbarkStatusDataFromLocal();
        }
        if (journeyStatus > 3) {
            FragmentTransport.changeFragment(1);
        }
    }

    private void onLocationChanged(Location location) {

        float distance;
        for (int i = 0; i < pickupLocationsList.size(); i++) {

            Location location1 = new Location("");
            if (pickupLocationsList.get(i).getLatitude() == null || pickupLocationsList.get(i).getLongitude() == null) {
                break;
            }
            location1.setLatitude(Double.parseDouble(pickupLocationsList.get(i).getLatitude()));
            location1.setLongitude(Double.parseDouble(pickupLocationsList.get(i).getLongitude()));
            distance = location.distanceTo(location1);
            Log.e(TAG, "Distance [" + (i + 1) + "] : " + distance);

            if (distance < RADIUS && SaveSharedPreference.getJorneyStatus() < 4) {
                currentPickupId = pickupLocationsList.get(i).getPickupId();
                markerTitle = pickupLocationsList.get(i).getPickupName();
                if (currentPickupId == endPickupId) {
                    // Radius entry of end stop
                    if (!embarkedUsersList.isEmpty()) {
                        btBeginJourney.setEnabled(false);
                        btEmbark.setEnabled(false);
                        btDisembark.setEnabled(true);
                        circle1.setImageResource(R.drawable.circle_border);
                        circle2.setImageResource(R.drawable.circle_border);
                        circle3.setImageResource(R.drawable.circle);
                    }
                } else {
                    // Radius entry of pickup points other than end stop

                    //handle radius entry operations
                    if (!MyDataBase.db.hasVisitedBusStop(currentPickupId)) {
                        MyDataBase.db.updateVisitedBusStop(currentPickupId, true);
                        mediaPlayer.start();
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                            vibrator.vibrate(VibrationEffect.createOneShot(3000, VibrationEffect.DEFAULT_AMPLITUDE));
                        } else {
                            vibrator.vibrate(3000);
                        }
                        sendRadiusEntryNotification(currentPickupId, markerTitle, "transport");
                    }

                    List<DbYetToBeEmbarked> currentStudentList = MyDataBase.db.getYetToBeEmbarked(currentPickupId, routeId);
                    if (currentStudentList.isEmpty())
                        btEmbark.setEnabled(false);
                    else
                        btEmbark.setEnabled(true);
                    btBeginJourney.setEnabled(false);
                    btDisembark.setEnabled(false);
                    circle1.setImageResource(R.drawable.circle_border);
                    circle2.setImageResource(R.drawable.circle);
                    circle3.setImageResource(R.drawable.circle_border);

                }
                break;
            } else {
                btEmbark.setEnabled(false);
                btDisembark.setEnabled(false);
                currentPickupId = -1;
                markerTitle = "";

            }
        }
    }

    private void beginJourney() {
        SaveSharedPreference.setJourneyStatus(2);
        MyDataBase.db.updateJourneyStatuse(2, routeId);
        btBeginJourney.setEnabled(false);
        btEmbark.setEnabled(false);
        btDisembark.setEnabled(false);
        circle1.setImageResource(R.drawable.circle_border);
        circle2.setImageResource(R.drawable.circle);
        circle3.setImageResource(R.drawable.circle_border);
        turnOnGps();
        mFusedLocationClient.requestLocationUpdates(locationRequest, locationCallback, Looper.getMainLooper());


        Date date = new Date();
        tvTime1.setVisibility(View.VISIBLE);
        tvAm1.setVisibility(View.VISIBLE);
        tvTime1.setText(formatter1.format(date));
        tvAm1.setText(formatter2.format(date));
        MyDataBase.db.updateJourneyStartTime(date, routeId);
    }

    private void embarkStudents() {


        List<DbYetToBeEmbarked> currentStudentList = MyDataBase.db.getYetToBeEmbarked(currentPickupId, routeId);

        Dialog dialog = new Dialog(getActivity(), R.style.DialogStyle);
        dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        dialog.setContentView(R.layout.dialog_transport_select_students);
        dialog.setCanceledOnTouchOutside(true);
        dialog.setCancelable(true);
        dialog.show();


        TransportDialogAdapter adapter = new TransportDialogAdapter(currentStudentList);
        RecyclerView recyclerView = dialog.findViewById(R.id.recycler_view);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.setAdapter(adapter);

        MaterialButton btSelect = dialog.findViewById(R.id.bt_select);
        MaterialButton btCancel = dialog.findViewById(R.id.bt_cancel);
        btSelect.setOnClickListener(v -> {
            List<DbYetToBeEmbarked> selectedStudentList = adapter.getSelectedStudentList();
            for (int i = 0; i < selectedStudentList.size(); i++) {
                MyDataBase.db.insertToEmbarkedUsers(new DbEmbarkedUsers(selectedStudentList.get(i)));
                MyDataBase.db.deleteFromYetToBeEmbarked(selectedStudentList.get(i).getStudentId(), selectedStudentList.get(i).getUserType());
                embarkedUsersList = MyDataBase.db.getEmbarkedUsers(routeId);
                yetToBeEmbarkedList = MyDataBase.db.getYetToBeEmbarkedRouteId(routeId);
            }
            sendEmbarkDisEmbarkNotification(selectedStudentList, markerTitle, "Embark");
            dialog.dismiss();
        });
        btCancel.setOnClickListener(v -> dialog.dismiss());

    }

    private void disembarkStudents() {

        List<DbYetToBeEmbarked> currentStudentList = MyDataBase.db.getYetToBeEmbarkedRouteId(routeId);

        Dialog dialog = new Dialog(getActivity(), R.style.DialogStyle);
        dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        dialog.setContentView(R.layout.dialog_transport_select_students);
        dialog.setCanceledOnTouchOutside(false);
        dialog.setCancelable(false);
        dialog.show();

        TransportDialogAdapter adapter = new TransportDialogAdapter(currentStudentList);
        RecyclerView recyclerView = dialog.findViewById(R.id.recycler_view);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.setAdapter(adapter);

        MaterialButton btSelect = dialog.findViewById(R.id.bt_select);
        MaterialButton btCancel = dialog.findViewById(R.id.bt_cancel);
        TextView tvTitle = dialog.findViewById(R.id.tv_title);
        tvTitle.setText("Remaining Students");
        btSelect.setText("Disembark");
        btSelect.setOnClickListener(v -> {
            List<DbYetToBeEmbarked> selectedStudentList = adapter.getSelectedStudentList();
            for (int i = 0; i < selectedStudentList.size(); i++) {
                MyDataBase.db.insertToEmbarkedUsers(new DbEmbarkedUsers(selectedStudentList.get(i)));
                MyDataBase.db.deleteFromYetToBeEmbarked(selectedStudentList.get(i).getStudentId(), selectedStudentList.get(i).getUserType());
                embarkedUsersList = MyDataBase.db.getEmbarkedUsers(routeId);
                yetToBeEmbarkedList = MyDataBase.db.getYetToBeEmbarkedRouteId(routeId);
            }
            sendEmbarkDisEmbarkNotification(selectedStudentList, markerTitle, "disEmbark");
            dialog.dismiss();

            Alerts.displayToast(getContext(), embarkedUsersList.size() + " students disembarked at school");

            // clear data from tables and reset for return journey
            for (int i = 0; i < pickupLocationsList.size(); i++) {
                MyDataBase.db.updateVisitedBusStop(pickupLocationsList.get(i).getPickupId(), false);
            }
            MyDataBase.db.deleteAllFromEmbarkedUsers();
            MyDataBase.db.deleteAllFromYetToBeEmbarked();
            SaveSharedPreference.setJourneyStatus(4);
            MyDataBase.db.updateJourneyStatuse(4, routeId);
            btBeginJourney.setEnabled(false);
            btEmbark.setEnabled(false);
            btDisembark.setEnabled(false);
            circle1.setImageResource(R.drawable.circle_border);
            circle2.setImageResource(R.drawable.circle_border);
            circle3.setImageResource(R.drawable.circle_border);

            Date date = new Date();
            tvTime3.setVisibility(View.VISIBLE);
            tvAm3.setVisibility(View.VISIBLE);
            tvTime3.setText(formatter1.format(date));
            tvAm3.setText(formatter2.format(date));
            MyDataBase.db.updateSchoolDisembarkTime(date, routeId);

        });
        btCancel.setOnClickListener(v -> dialog.dismiss());


    }


    private void displayWarningDialog() {
        Dialog dialog = new Dialog(getActivity(), R.style.DialogStyle);
        dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        dialog.setContentView(R.layout.dialog_add_leave_reason);
        dialog.setCanceledOnTouchOutside(true);
        dialog.setCancelable(true);
        dialog.show();

        MaterialButton btSend = dialog.findViewById(R.id.bt_save);
        TextView title = dialog.findViewById(R.id.text);
        TextInputEditText etMessage = dialog.findViewById(R.id.et_reason);
        TextInputLayout layout = dialog.findViewById(R.id.layout);
        TextInputEditText etLocation = dialog.findViewById(R.id.et_location);

        btSend.setText("Send");
        title.setText("Emergency");
        layout.setVisibility(View.VISIBLE);

        btSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String message = etMessage.getText().toString();
                String location = etLocation.getText().toString();
                if (message.isEmpty()) {
                    etMessage.setError("Enter Message");
                    etMessage.requestFocus();
                    return;
                }
                if (location.isEmpty()) {
                    etLocation.setError("Enter Current Location");
                    etLocation.requestFocus();
                    return;
                }

                sendWarningNotification(message, location);
                dialog.dismiss();
            }
        });

    }

    private void displayLocationChangeDialog() {

        List<DbYetToBeEmbarked> currentStudentList = new ArrayList<>();
        for (int i = 0; i < transportUsersList.size(); i++) {
            currentStudentList.add(new DbYetToBeEmbarked(transportUsersList.get(i)));
        }

        Dialog dialog = new Dialog(getActivity(), R.style.DialogStyle);
        dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        dialog.setContentView(R.layout.dialog_transport_select_students);
        dialog.setCanceledOnTouchOutside(true);
        dialog.setCancelable(true);
        dialog.show();

        TransportDialogAdapter adapter = new TransportDialogAdapter(currentStudentList);
        RecyclerView recyclerView = dialog.findViewById(R.id.recycler_view);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.setAdapter(adapter);

        MaterialButton btSelect = dialog.findViewById(R.id.bt_select);
        MaterialButton btCancel = dialog.findViewById(R.id.bt_cancel);
        TextView tvTitle = dialog.findViewById(R.id.tv_title);
        tvTitle.setText("Update location to current location");
        btSelect.setText("Update");

        btSelect.setOnClickListener(v -> {
            List<DbYetToBeEmbarked> selectedStudentList = adapter.getSelectedStudentList();
            if (selectedStudentList.size() > 1) {
                Alerts.displayToast(getContext(), "Please select only one student");
                return;
            }
            if (selectedStudentList.size() != 0) {
                updatePickupLocation(selectedStudentList);
            }
            dialog.dismiss();
        });
        btCancel.setOnClickListener(v -> dialog.dismiss());

    }

    private void displayRouteSelectionDialog(boolean flag) {

        List<DbYetToBeEmbarked> currentStudentList = new ArrayList<>();
        for (int i = 0; i < routeDetailsList.size(); i++) {
            currentStudentList.add(new DbYetToBeEmbarked(routeDetailsList.get(i)));
        }

        Dialog dialog = new Dialog(getActivity(), R.style.DialogStyle);
        dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        dialog.setContentView(R.layout.dialog_transport_select_students);

        TransportDialogAdapter adapter = new TransportDialogAdapter(currentStudentList);
        RecyclerView recyclerView = dialog.findViewById(R.id.recycler_view);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.setAdapter(adapter);

        MaterialButton btSelect = dialog.findViewById(R.id.bt_select);
        MaterialButton btCancel = dialog.findViewById(R.id.bt_cancel);
        TextView tvTitle = dialog.findViewById(R.id.tv_title);
        tvTitle.setText("Select Route");

        if (flag) {
            dialog.setCanceledOnTouchOutside(true);
            dialog.setCancelable(true);
        } else {
            dialog.setCanceledOnTouchOutside(false);
            dialog.setCancelable(false);
            btCancel.setVisibility(View.GONE);
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(0, LinearLayout.LayoutParams.WRAP_CONTENT, 2);
            btSelect.setLayoutParams(params);
        }
        dialog.show();


        btSelect.setOnClickListener(v -> {
            List<DbYetToBeEmbarked> selectedStudentList = adapter.getSelectedStudentList();
            if (selectedStudentList.size() > 1) {
                Alerts.displayToast(getContext(), "Please select only one route");
                return;
            }
            if (selectedStudentList.size() != 0) {
                routeId = selectedStudentList.get(0).getStudentId();
                busId = selectedStudentList.get(0).getUserType();
                SaveSharedPreference.setRouteId(routeId);
                SaveSharedPreference.setBusId(busId);
                SaveSharedPreference.setJourneyStatus(MyDataBase.db.getJourneyStatus(routeId));
                pickupLocationsList.clear();
                getTransportDataFromServer();
                init();
            }
            dialog.dismiss();
        });
        btCancel.setOnClickListener(v -> dialog.dismiss());

    }

    private void updatePickupLocation(List<DbYetToBeEmbarked> selectedStudentList) {

        String shift = "";
        Integer updateFlag = null;
        Integer updateDropFlag = null;
        String lat = null, lng = null, dropLat = null, dropLng = null;
        if (SaveSharedPreference.getJorneyStatus() < 4) {
            shift = "Morning";
            updateFlag = 1;
            lat = String.valueOf(latitude);
            lng = String.valueOf(longitude);
        } else {
            shift = "Evening";
            updateDropFlag = 1;
            dropLat = String.valueOf(latitude);
            dropLng = String.valueOf(longitude);
        }
        Log.e(TAG, "updatePickupLocation: " + lat + " " + lng);

        for (int i = 0; i < selectedStudentList.size(); i++) {
            Integer pickupId = selectedStudentList.get(i).getPickupId();

            progressVisibility(true);
            Call<NotificationSuccessModel> call = RetrofitClient.get().updatePickupPoint(shift, updateFlag, pickupId, lat, lng, dropLat, dropLng, updateDropFlag);
            call.enqueue(new Callback<NotificationSuccessModel>() {
                @Override
                public void onResponse(Call<NotificationSuccessModel> call, Response<NotificationSuccessModel> response) {

                    NotificationSuccessModel model = response.body();
                    Alerts.displayToast(getContext(), model.getMsg());
                    progressVisibility(false);
                }

                @Override
                public void onFailure(Call<NotificationSuccessModel> call, Throwable t) {
                    progressVisibility(false);
                    Log.e(TAG, "onFailure: ", t);
                }
            });
        }
    }


    private void sendRadiusEntryNotification(int pickupId, String markerTitle, String module) {

        progressVisibility(true);

        DateFormat df = new SimpleDateFormat("yyyy-mm-dd HH:mm:ss");
        Date date = new Date();
        Call<NotificationSuccessModel> call = RetrofitClient.get().sendNotificationFromDriver(pickupId, markerTitle, routeId, busId, module, df.format(date));
        call.enqueue(new Callback<NotificationSuccessModel>() {
            @Override
            public void onResponse(Call<NotificationSuccessModel> call, Response<NotificationSuccessModel> response) {

                if (response.body() != null) {
                    NotificationSuccessModel model = response.body();
                    Alerts.displayToast(getContext(), "Notification sent successfully!");
                } else {
                    Alerts.displayToast(getContext(), "Notification Sending Failed!");
                }
                progressVisibility(false);
            }

            @Override
            public void onFailure(Call<NotificationSuccessModel> call, Throwable t) {
                progressVisibility(false);
                Alerts.displayToast(getContext(), "Notification Sending Failed!");
                Log.e(TAG, "onFailure: ", t);
            }
        });

    }

    private void sendEmbarkDisEmbarkNotification(List<DbYetToBeEmbarked> selectedStudentList, String markerTitle, String flag) {


        DateFormat df = new SimpleDateFormat("yyyy-mm-dd HH:mm:ss");
        Date date = new Date();

        StringBuilder attend = new StringBuilder();
        StringBuilder absent = new StringBuilder();
        String delimiter = "";
        if (flag.equals("disEmbark")) {
            List<DbYetToBeEmbarked> remainingStudentList = MyDataBase.db.getYetToBeEmbarkedRouteId(routeId);
            delimiter = "";
            for (int i = 0; i < remainingStudentList.size(); i++) {
                if (remainingStudentList.get(i).getUserType() == 3) {
                    absent.append(delimiter);
                    absent.append(remainingStudentList.get(i).getStudentId());
                    delimiter = ",";
                }
            }

            List<DbEmbarkedUsers> embarkedStudentList = MyDataBase.db.getEmbarkedUsers(routeId);
            delimiter = "";
            for (int i = 0; i < embarkedStudentList.size(); i++) {
                if (embarkedStudentList.get(i).getUserType() == 3) {
                    attend.append(delimiter);
                    attend.append(embarkedStudentList.get(i).getStudentId());
                    delimiter = ",";
                }
            }
        } else {
            for (int i = 0; i < selectedStudentList.size(); i++) {
                if (selectedStudentList.get(i).getUserType() == 3) {
                    attend.append(delimiter);
                    attend.append(selectedStudentList.get(i).getStudentId());
                    delimiter = ",";
                }
            }
        }


        MyDataBase.db.insertToTransportSync(new DbTransportSync(markerTitle, routeId, busId, attend.toString(), absent.toString(), flag, df.format(date)));
        progressVisibility(true);
        Call<NotificationSuccessModel> call = RetrofitClient.get().sendStudentNotification(markerTitle, routeId, busId, attend.toString(), absent.toString(), flag, df.format(date));
        call.enqueue(new Callback<NotificationSuccessModel>() {
            @Override
            public void onResponse(Call<NotificationSuccessModel> call, Response<NotificationSuccessModel> response) {
                if (response.body() != null) {
                    NotificationSuccessModel model = response.body();
                    Alerts.displayToast(getContext(), model.getMsg());
                } else {
                    Alerts.displayToast(getContext(), "Notification Sending Failed!");
                }
                try {
                    MyDataBase.db.deleteFromTransportSync(df.format(date));
                } catch (Exception e) {
                    Log.e(TAG, "onResponse: ", e);
                }
                progressVisibility(false);
            }

            @Override
            public void onFailure(Call<NotificationSuccessModel> call, Throwable t) {
                progressVisibility(false);
                Alerts.displayToast(getContext(), "Notification Sending Failed!");
                Log.e(TAG, "onFailure: ", t);
            }
        });
    }

    private void sendNotificationSync(DbTransportSync dbTransportSync) {

        progressVisibility(true);
        Call<NotificationSuccessModel> call = RetrofitClient.get().sendStudentNotification(dbTransportSync.getMarkerTitle(), dbTransportSync.getRouteId()
                , dbTransportSync.getBusId(), dbTransportSync.getAttend(), dbTransportSync.getAbsent(), dbTransportSync.getFlag(), dbTransportSync.getDateTime());
        call.enqueue(new Callback<NotificationSuccessModel>() {
            @Override
            public void onResponse(Call<NotificationSuccessModel> call, Response<NotificationSuccessModel> response) {
                if (response.body() != null) {
                    NotificationSuccessModel model = response.body();
                    Alerts.displayToast(getContext(), model.getMsg());
                } else {
                    Alerts.displayToast(getContext(), "Notification Sending Failed!");
                }
                try {
                    MyDataBase.db.deleteFromTransportSync(dbTransportSync.getDateTime());
                } catch (Exception e) {
                    Log.e(TAG, "onResponse: ", e);
                }
                progressVisibility(false);
            }

            @Override
            public void onFailure(Call<NotificationSuccessModel> call, Throwable t) {
                progressVisibility(false);
                Alerts.displayToast(getContext(), "Notification Sending Failed!");
                Log.e(TAG, "onFailure: ", t);
            }
        });
    }
    private void sendWarningNotification(String message, String location) {

        DateFormat df = new SimpleDateFormat("yyyy-mm-dd HH:mm:ss");
        Date date = new Date();

        StringBuilder attend = new StringBuilder();
        String delimiter = "";
        for (int i = 0; i < transportUsersList.size(); i++) {
            if (transportUsersList.get(i).getUserType() == 3) {
                attend.append(delimiter);
                attend.append(transportUsersList.get(i).getStudentId());
                delimiter = ",";
            }
        }

        progressVisibility(true);
        Call<NotificationSuccessModel> call = RetrofitClient.get().sendStudentNotification(location, routeId, busId, attend.toString(), message, "Emergency", df.format(date));
        call.enqueue(new Callback<NotificationSuccessModel>() {
            @Override
            public void onResponse(Call<NotificationSuccessModel> call, Response<NotificationSuccessModel> response) {
                if (response.body() != null) {
                    NotificationSuccessModel model = response.body();
                    Alerts.displayToast(getContext(), model.getMsg());
                } else {
                    Alerts.displayToast(getContext(), "Notification Sending Failed!");
                }
                progressVisibility(false);
            }

            @Override
            public void onFailure(Call<NotificationSuccessModel> call, Throwable t) {
                progressVisibility(false);
                Alerts.displayToast(getContext(), "Notification Sending Failed!");
                Log.e(TAG, "onFailure: ", t);
            }
        });

    }


    private void getRouteDetailsFromLocal() {

        routeDetailsList = MyDataBase.db.getRouteDetails();
        routeId = SaveSharedPreference.getRouteId();
        busId = SaveSharedPreference.getBusId();

        int status = SaveSharedPreference.getJorneyStatus();
        if (status > 1) {
            Date startTime = MyDataBase.db.getJourneyStartTime(routeId);
            tvTime1.setVisibility(View.VISIBLE);
            tvAm1.setVisibility(View.VISIBLE);
            tvTime1.setText(formatter1.format(startTime));
            tvAm1.setText(formatter2.format(startTime));
        }
        if (status > 3) {
            Date disembarkTime = MyDataBase.db.getSchoolDisembarkTime(routeId);
            tvTime3.setVisibility(View.VISIBLE);
            tvAm3.setVisibility(View.VISIBLE);
            tvTime3.setText(formatter1.format(disembarkTime));
            tvAm3.setText(formatter2.format(disembarkTime));
        }
    }

    private void getRouteDetailsFromServer() {

        progressVisibility(true);
        Call<AssignedRouteModel> call = RetrofitClient.get().getAssignedRoute(userId, userType);
        call.enqueue(new Callback<AssignedRouteModel>() {
            @Override
            public void onResponse(Call<AssignedRouteModel> call, Response<AssignedRouteModel> response) {

                if (response.body() != null) {
                    AssignedRouteModel model = response.body();
                    if (model.getStatus() == 1) {
                        List<AssignedRouteModel.RouteDetail> routeDetailList = model.getRouteDetails();
                        if (routeDetailList != null && routeDetailList.size() != 0) {
                            for (int i = 0; i < routeDetailList.size(); i++) {
                                DbRouteDetails dbRouteDetails = new DbRouteDetails(routeDetailList.get(i));
                                MyDataBase.db.insertToRouteDetails(dbRouteDetails);
                                routeDetailsList.add(new DbRouteDetails(routeDetailList.get(i)));
                            }

                            displayRouteSelectionDialog(false);

                        }
                    }
                } else {
                    Alerts.displayToast(getContext(), "Loading Failed");
                }
                progressVisibility(false);
            }

            @Override
            public void onFailure(Call<AssignedRouteModel> call, Throwable t) {
                Log.e(TAG, "onFailure: ", t);
                Alerts.displayToast(getContext(), "Loading Failed");
                progressVisibility(false);
            }
        });
    }

    private void getTransportDataFromLocal() {

        pickupLocationsList = MyDataBase.db.getPickupLocations(routeId);
        transportUsersList = MyDataBase.db.getTransportUsers(routeId);
        startPickupId = SaveSharedPreference.getStartPickupId();
        endPickupId = SaveSharedPreference.getEndPickupId();

    }

    private void getTransportDataFromServer() {

        progressVisibility(true);
        Call<TransportDataModel> call = RetrofitClient.get().getTransportData(userId, userType, String.valueOf(routeId));
        call.enqueue(new Callback<TransportDataModel>() {
            @Override
            public void onResponse(Call<TransportDataModel> call, Response<TransportDataModel> response) {

                if (response.body() != null) {
                    TransportDataModel model = response.body();
                    List<TransportDataModel.PickupLocArr> pickupLocList = model.getPickupLocArr();
                    List<TransportDataModel.UserRec> userRecList = model.getUserRec();

                    if (pickupLocList != null && pickupLocList.size() != 0) {
                        for (int i = 0; i < pickupLocList.size(); i++) {
                            DbPickupLocations dbPickupLocations = new DbPickupLocations(pickupLocList.get(i));
                            pickupLocationsList.add(dbPickupLocations);
                            MyDataBase.db.insertToPickupLocations(dbPickupLocations);
                            MyDataBase.db.insertToVisitedBusStop(new DbVisitedBusStop(pickupLocList.get(i)));
                        }

                        startPickupId = Integer.parseInt(pickupLocList.get(0).getPickupId());
                        endPickupId = Integer.parseInt(pickupLocList.get(1).getPickupId());
                        SaveSharedPreference.setStartPickupId(startPickupId);
                        SaveSharedPreference.setEndPickupId(endPickupId);
                    }


                    if (userRecList != null && userRecList.size() != 0) {
                        for (int i = 0; i < userRecList.size(); i++) {
                            DbTransportUsers dbTransportUsers = new DbTransportUsers(userRecList.get(i));
                            transportUsersList.add(dbTransportUsers);
                            MyDataBase.db.insertToTransportUsers(dbTransportUsers);

                            DbYetToBeEmbarked dbYetToBeEmbarked = new DbYetToBeEmbarked(dbTransportUsers);
                            yetToBeEmbarkedList.add(dbYetToBeEmbarked);
                            MyDataBase.db.insertToYetToBeEmbarked(dbYetToBeEmbarked);
                        }
                    }


                } else {
                    Alerts.displayToast(getContext(), "Loading Failed");
                }
                progressVisibility(false);

            }

            @Override
            public void onFailure(Call<TransportDataModel> call, Throwable t) {
                Log.e(TAG, "onFailure: ", t);
                Alerts.displayToast(getContext(), "Loading Failed");
                progressVisibility(false);
            }
        });
    }

    private void getEmbarkStatusDataFromLocal() {

        embarkedUsersList = MyDataBase.db.getEmbarkedUsers(routeId);
        yetToBeEmbarkedList = MyDataBase.db.getYetToBeEmbarkedRouteId(routeId);
    }


    private void checkForLocationPermission() {

        if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION}, 1000);
        } else {
            loadData();
            if (journeyStatus != 1) {
                turnOnGps();
                mFusedLocationClient.requestLocationUpdates(locationRequest, locationCallback, Looper.getMainLooper());
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        switch (requestCode) {
            case 1000: {
                // If request is cancelled, the result arrays are empty.

                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    loadData();
                    if (journeyStatus != 1) {
                        turnOnGps();
                        mFusedLocationClient.requestLocationUpdates(locationRequest, locationCallback, Looper.getMainLooper());
                    }
                } else {
                    Alerts.displayToast(getContext(), "Permission Denied");
                    getActivity().finish();
                }
                break;
            }
        }
    }

    private void turnOnGps() {
        LocationSettingsRequest.Builder settingsBuilder = new LocationSettingsRequest.Builder()
                .addLocationRequest(locationRequest);
        settingsBuilder.setAlwaysShow(true);
        Task<LocationSettingsResponse> result = LocationServices.getSettingsClient(getActivity())
                .checkLocationSettings(settingsBuilder.build());


        result.addOnCompleteListener(task -> {
            try {
                LocationSettingsResponse response =
                        task.getResult(ApiException.class);
            } catch (ApiException ex) {
                switch (ex.getStatusCode()) {
                    case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                        try {
                            ResolvableApiException resolvableApiException =
                                    (ResolvableApiException) ex;
                            resolvableApiException
                                    .startResolutionForResult(getActivity(),
                                            200);
                            //onActivity result to candle gps is in ActivityHome.java
                        } catch (IntentSender.SendIntentException e) {
                            Log.e(TAG, "turnOnGps: ", e);
                        }
                        break;
                    case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:

                        break;
                }
            }
        });
    }

    public void progressVisibility(boolean visibility) {

        if (getActivity() != null) {

            if (visibility) {
                llProgressBar.setVisibility(View.VISIBLE);
                getActivity().getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                        WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);

            } else {
                llProgressBar.setVisibility(View.GONE);
                getActivity().getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
            }
        }
    }

//    @Override
//    public void onDestroy() {
//        super.onDestroy();
//        if (getContext() != null) {
//            Intent serviceIntent = new Intent(getContext(), TransportService.class);
//            serviceIntent.putExtra("inputExtra", "Foreground Service Example in Android");
//            ContextCompat.startForegroundService(getContext(), serviceIntent);
//        }
//
//    }


    @Override
    public void onStop() {
        super.onStop();
        int js = SaveSharedPreference.getJorneyStatus();
        if (js == 2 || js == 3) {
            if (getContext() != null) {
                mFusedLocationClient.removeLocationUpdates(locationCallback);
                Intent serviceIntent = new Intent(getContext(), TransportService.class);
                serviceIntent.putExtra("journeyStatus", js);
                serviceIntent.putExtra("routeId", SaveSharedPreference.getRouteId());
                ContextCompat.startForegroundService(getContext(), serviceIntent);
            }
        }

    }

    @Override
    public void onStart() {
        super.onStart();
        if (getActivity() != null) {
            Intent serviceIntent = new Intent(getContext(), TransportService.class);
            getActivity().stopService(serviceIntent);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if (getActivity() != null) {
            Intent serviceIntent = new Intent(getContext(), TransportService.class);
            getActivity().stopService(serviceIntent);
        }
    }
}
