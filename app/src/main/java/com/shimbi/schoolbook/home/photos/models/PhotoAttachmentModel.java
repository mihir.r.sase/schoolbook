package com.shimbi.schoolbook.home.photos.models;

public class PhotoAttachmentModel {

    String filename, imageTitle, imageDescription, filePath;

    public PhotoAttachmentModel(String filename, String imageTitle, String imageDescription, String filePath) {
        this.filename = filename;
        this.imageTitle = imageTitle;
        this.imageDescription = imageDescription;
        this.filePath = filePath;
    }

    @Override
    public String toString() {
        return "PhotoAttachmentModel{" +
                "filename='" + filename + '\'' +
                ", imageTitle='" + imageTitle + '\'' +
                ", imageDescription='" + imageDescription + '\'' +
                ", filePath='" + filePath + '\'' +
                '}';
    }

    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    public String getImageTitle() {
        return imageTitle;
    }

    public void setImageTitle(String imageTitle) {
        this.imageTitle = imageTitle;
    }

    public String getImageDescription() {
        return imageDescription;
    }

    public void setImageDescription(String imageDescription) {
        this.imageDescription = imageDescription;
    }

    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }
}
