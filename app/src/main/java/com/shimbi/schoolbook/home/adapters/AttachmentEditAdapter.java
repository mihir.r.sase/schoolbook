package com.shimbi.schoolbook.home.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.shimbi.schoolbook.R;
import com.shimbi.schoolbook.home.models.AttachmentModel;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;

public class AttachmentEditAdapter extends RecyclerView.Adapter<AttachmentEditAdapter.MyViewHolder> {


    private static final String TAG = "AttachmentEditAdapter";
    List<AttachmentModel> list;
    List<String> deleteList = new ArrayList<>();

    public AttachmentEditAdapter(List<AttachmentModel> list) {
        this.list = list;
    }

    public List<String> getDeleteList() {
        return deleteList;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_attach_file, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        AttachmentModel model = list.get(position);
        holder.textInputLayout.setVisibility(View.GONE);
        holder.tvFileName.setText(model.getFilename());
        holder.cancel_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                deleteList.add(list.get(holder.getAdapterPosition()).getFinalFileName());
                list.remove(holder.getAdapterPosition());
                notifyItemRemoved(holder.getAdapterPosition());
            }
        });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {


        @BindView(R.id.et_information)
        public TextInputEditText etInformation;
        @BindView(R.id.tv_file_name)
        TextView tvFileName;
        @BindView(R.id.cancel_button)
        ImageView cancel_button;
        @BindView(R.id.progress_upload)
        ProgressBar progressBar;
        @BindView(R.id.text_input_layout)
        TextInputLayout textInputLayout;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
