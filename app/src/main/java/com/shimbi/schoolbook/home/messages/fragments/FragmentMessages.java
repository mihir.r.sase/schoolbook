package com.shimbi.schoolbook.home.messages.fragments;


import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.work.Constraints;
import androidx.work.Data;
import androidx.work.OneTimeWorkRequest;
import androidx.work.WorkInfo;
import androidx.work.WorkManager;

import com.shimbi.schoolbook.R;
import com.shimbi.schoolbook.db.helper.MyDataBase;
import com.shimbi.schoolbook.db.tables.DbMessages;
import com.shimbi.schoolbook.home.activities.ActivityHome;
import com.shimbi.schoolbook.home.messages.activities.ActivityNewMessage;
import com.shimbi.schoolbook.home.messages.adapters.MessageAdapter;
import com.shimbi.schoolbook.home.messages.models.MessageResponseModel;
import com.shimbi.schoolbook.utils.AuthModel;
import com.shimbi.schoolbook.utils.Constants;
import com.shimbi.schoolbook.utils.NetworkStatusChecker;
import com.shimbi.schoolbook.utils.SaveSharedPreference;
import com.shimbi.schoolbook.utils.workers.FetchMessagesWorker;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * A simple {@link Fragment} subclass.
 */
public class FragmentMessages extends Fragment {

    private static final String TAG = "FragmentMessages";
    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;
    @BindView(R.id.swipe_container)
    SwipeRefreshLayout swipeRefreshLayout;
    private String username;
    private String userEmail;
    private String userType;
    private String userId;
    private Integer studentCount;
    private MessageAdapter messageAdapter;
    private LinearLayoutManager layoutManager;
    private List<MessageResponseModel.MessageResultModel> messageList = new ArrayList<>();
    public ProgressBar syncProgress;
    public ImageView noData;

    public FragmentMessages() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        ActivityHome activityHome = (ActivityHome) getActivity();
        activityHome.setTitleAndLogo("Messages", R.drawable.ic_message_black_24dp);
        ActivityHome.fab.setVisibility(View.GONE);
        if (getActivity() != null) {
            syncProgress = getActivity().findViewById(R.id.progress_sync);
            syncProgress.setVisibility(View.GONE);
            noData = getActivity().findViewById(R.id.no_data);
            noData.setVisibility(View.GONE);
        }

        return inflater.inflate(R.layout.fragment_messages, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);


        init();
        // loadData();
        loadDataFromLocal();


        loadDataOnline();

        ActivityHome.fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getContext(), ActivityNewMessage.class));
            }
        });

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                loadDataOnline();
                swipeRefreshLayout.setRefreshing(false);
            }
        });
    }

    private void init() {
        AuthModel authModel = SaveSharedPreference.getData();
        userId = authModel.uid;
        username = authModel.username;
        userEmail = authModel.userEmail;
        userType = authModel.userType;
        studentCount = authModel.studentCount;
        Log.e(TAG, "init: " + userId + " " + userType);

        messageAdapter = new MessageAdapter(messageList);
        layoutManager = new LinearLayoutManager(this.getActivity());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(messageAdapter);


        if (ActivityHome.messagageAddFlag.equals("1")) {
            ActivityHome.fab.setVisibility(View.VISIBLE);
        } else {
            ActivityHome.fab.setVisibility(View.GONE);
        }

    }

    private void loadDataFromLocal() {

        MyDataBase.db.getMessagesData().observe(this, new Observer<List<DbMessages>>() {
            @Override
            public void onChanged(List<DbMessages> dbMessages) {
                try {

                    MessageResponseModel model = new MessageResponseModel();
                    messageList.clear();
                    for (int i = 0; i < dbMessages.size(); i++) {

                        messageList.add(model.new MessageResultModel(
                                dbMessages.get(i).getUserName(),
                                dbMessages.get(i).getUserImage(),
                                dbMessages.get(i).getEventId(),
                                dbMessages.get(i).getTitle(),
                                dbMessages.get(i).getDescription(),
                                dbMessages.get(i).getAddedDate()
                        ));
                    }
                    messageAdapter.notifyDataSetChanged();
                    if (messageList.size() == 0) {
                        noData.setVisibility(View.VISIBLE);
                    } else {
                        noData.setVisibility(View.GONE);
                    }

                } catch (Exception e) {
                    Log.e(TAG, "onChanged: ", e);
                }
            }
        });
    }

    private void loadDataOnline() {

        if (!NetworkStatusChecker.isNetworkConnected(getContext())) {
            Constraints constraints = NetworkStatusChecker.getNetWorkConstraints();
            syncProgress.setVisibility(View.VISIBLE);
            OneTimeWorkRequest messages = new OneTimeWorkRequest.Builder(FetchMessagesWorker.class)
                    .addTag(Constants.TASK_SYNC_MESSAGE)
                    .setConstraints(constraints)
                    .setInputData(new Data.Builder().putString("From", "Sync").build())
                    .build();
            WorkManager manager = WorkManager.getInstance();
            manager.beginWith(messages).enqueue();
            WorkManager.getInstance().getWorkInfoByIdLiveData(messages.getId()).observe(this, new Observer<WorkInfo>() {
                @Override
                public void onChanged(WorkInfo workInfo) {
                    if (workInfo != null && workInfo.getState() == WorkInfo.State.SUCCEEDED) {
                        syncProgress.setVisibility(View.GONE);
                    } else if (workInfo != null && workInfo.getState() == WorkInfo.State.FAILED) {
                        syncProgress.setVisibility(View.GONE);
                    }
                }
            });
        }
    }


}
