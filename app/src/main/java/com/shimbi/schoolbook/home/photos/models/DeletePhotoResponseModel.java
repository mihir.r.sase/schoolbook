package com.shimbi.schoolbook.home.photos.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class DeletePhotoResponseModel {
    @SerializedName("success")
    @Expose
    private Integer success;
    @SerializedName("album")
    @Expose
    private List<Album> album = null;

    public Integer getSuccess() {
        return success;
    }

    public void setSuccess(Integer success) {
        this.success = success;
    }

    public List<Album> getAlbum() {
        return album;
    }

    public void setAlbum(List<Album> album) {
        this.album = album;
    }


    public class Album {

        @SerializedName("album_name")
        @Expose
        private String albumName;
        @SerializedName("photo_cat_id")
        @Expose
        private String photoCatId;
        @SerializedName("photo")
        @Expose
        private String photo;

        public String getAlbumName() {
            return albumName;
        }

        public void setAlbumName(String albumName) {
            this.albumName = albumName;
        }

        public String getPhotoCatId() {
            return photoCatId;
        }

        public void setPhotoCatId(String photoCatId) {
            this.photoCatId = photoCatId;
        }

        public String getPhoto() {
            return photo;
        }

        public void setPhoto(String photo) {
            this.photo = photo;
        }
    }
}
