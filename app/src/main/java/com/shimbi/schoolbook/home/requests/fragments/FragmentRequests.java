package com.shimbi.schoolbook.home.requests.fragments;


import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.shimbi.schoolbook.R;
import com.shimbi.schoolbook.home.activities.ActivityHome;
import com.shimbi.schoolbook.home.requests.activities.RequestCertificateActivity;
import com.shimbi.schoolbook.utils.AuthModel;
import com.shimbi.schoolbook.utils.SaveSharedPreference;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * A simple {@link Fragment} subclass.
 */
public class FragmentRequests extends Fragment {

    @BindView(R.id.tc_certificate)
    public LinearLayout tcCertificate;
    @BindView(R.id.bonafide_certificate)
    public LinearLayout bonafideCertificate;
    private String username;
    private String userEmail;
    private String userType;
    private String userId;
    public ProgressBar syncProgress;
    private ImageView noData;
    public FragmentRequests() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        ActivityHome activityHome = (ActivityHome) getActivity();
        activityHome.setTitle("Requests");
        ActivityHome.fab.setVisibility(View.GONE);
        if (getActivity() != null) {
            syncProgress = getActivity().findViewById(R.id.progress_sync);
            syncProgress.setVisibility(View.GONE);
            noData = getActivity().findViewById(R.id.no_data);
            noData.setVisibility(View.GONE);
        }
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_requests, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
        init();

        Intent intent = new Intent(getActivity(), RequestCertificateActivity.class);

        tcCertificate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                intent.putExtra("certificate", 1);
                startActivity(intent);
            }
        });

        bonafideCertificate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                intent.putExtra("certificate", 2);
                startActivity(intent);
            }
        });
    }

    private void init() {
        AuthModel authModel = SaveSharedPreference.getData();
        userId = authModel.uid;
        username = authModel.username;
        userEmail = authModel.userEmail;
        userType = authModel.userType;
    }
}
