package com.shimbi.schoolbook.home.events.fragments;


import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.work.Constraints;
import androidx.work.Data;
import androidx.work.OneTimeWorkRequest;
import androidx.work.WorkInfo;
import androidx.work.WorkManager;

import com.github.florent37.tutoshowcase.TutoShowcase;
import com.shimbi.schoolbook.R;
import com.shimbi.schoolbook.db.helper.MyDataBase;
import com.shimbi.schoolbook.db.tables.DbEvents;
import com.shimbi.schoolbook.home.activities.ActivityHome;
import com.shimbi.schoolbook.home.events.activities.ActivityNewEvent;
import com.shimbi.schoolbook.home.events.adapters.EventsAdapter;
import com.shimbi.schoolbook.home.events.models.EventsResponseModel;
import com.shimbi.schoolbook.utils.AuthModel;
import com.shimbi.schoolbook.utils.Constants;
import com.shimbi.schoolbook.utils.NetworkStatusChecker;
import com.shimbi.schoolbook.utils.SaveSharedPreference;
import com.shimbi.schoolbook.utils.calendar.CalendarEvent;
import com.shimbi.schoolbook.utils.calendar.CalendarMonthNameFormatter;
import com.shimbi.schoolbook.utils.calendar.CalendarView;
import com.shimbi.schoolbook.utils.calendar.OnDateSelectedListener;
import com.shimbi.schoolbook.utils.calendar.OnLoadEventsListener;
import com.shimbi.schoolbook.utils.calendar.OnMonthChangedListener;
import com.shimbi.schoolbook.utils.workers.FetchEventsWorker;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.concurrent.TimeUnit;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * A simple {@link Fragment} subclass.
 */
public class FragmentEvents extends Fragment implements OnMonthChangedListener,
        OnDateSelectedListener, OnLoadEventsListener {

    private static final String TAG = "FragmentEvents";
    private String username;
    private String userEmail;
    private String userType;
    private String userId;
    private Integer studentCount;
    List<DbEvents> eventList;
    public ProgressBar syncProgress;
    private EventsAdapter mEventsAdapter;
    @BindView(R.id.recycler_view)
    RecyclerView mRecyclerView;

    @BindView(R.id.calendarView)
    CalendarView mCalendarView;
    @BindView(R.id.tv_month)
    TextView tvMonth;
    int month, year;
    private ImageView noData;

    private CalendarMonthNameFormatter mFormatter = new CalendarMonthNameFormatter();

    public FragmentEvents() {
        // Required empty public constructor
    }

    View v;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        ActivityHome activityHome = (ActivityHome) getActivity();
        activityHome.setTitleAndLogo("Events", R.drawable.ic_event_black_24dp);
        ActivityHome.fab.setVisibility(View.GONE);
        if (getActivity() != null) {
            syncProgress = getActivity().findViewById(R.id.progress_sync);
            syncProgress.setVisibility(View.GONE);
            noData = getActivity().findViewById(R.id.no_data);
            noData.setVisibility(View.GONE);
        }


        AuthModel authModel = SaveSharedPreference.getData();
        userId = authModel.uid;
        username = authModel.username;
        userEmail = authModel.userEmail;
        userType = authModel.userType;
        studentCount = authModel.studentCount;
        //loadData();
        loadFromLocal();
        loadDataOnline();

        return inflater.inflate(R.layout.fragment_events, container, false);
    }


    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {


        ButterKnife.bind(this, view);
        v = view;
        init();


        ActivityHome.fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                startActivity(new Intent(getContext(), ActivityNewEvent.class));
            }
        });
    }


    private void init() {
        TutoShowcase.from(getActivity())
                .setContentView(R.layout.toto_calendar)
                .on(R.id.drawerLayout)
                .displaySwipableLeft()
                .show();

        if (ActivityHome.eventAddFlag == 1) {
            ActivityHome.fab.setVisibility(View.VISIBLE);
        } else {
            ActivityHome.fab.setVisibility(View.GONE);
        }


        mEventsAdapter = new EventsAdapter();
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        mRecyclerView.setAdapter(mEventsAdapter);


        mCalendarView.setOnMonthChangedListener(this);
        mCalendarView.setOnLoadEventsListener(this);
        mCalendarView.setOnDateSelectedListener(this);

        Calendar calendar = Calendar.getInstance();

        calendar.set(2015, Calendar.JANUARY, 1);
        mCalendarView.setMinimumDate(calendar.getTimeInMillis());

        calendar.set(2020, Calendar.OCTOBER, 1);
        mCalendarView.setMaximumDate(calendar.getTimeInMillis());


    }

    /**
     * initially load data from sqlite
     */
    private void loadFromLocal() {

        MyDataBase.db.getEventsDataSync().observe(this, new Observer<List<DbEvents>>() {
            @Override
            public void onChanged(List<DbEvents> dbEvents) {
                try {
                    eventList = dbEvents;


                } catch (Exception e) {
                    Log.e(TAG, "onChanged: ", e);
                }
            }
        });


    }

    /**
     * check for updates and add to sqlite
     */
    private void loadDataOnline() {
        Constraints constraints = NetworkStatusChecker.getNetWorkConstraints();
        if (!NetworkStatusChecker.isNetworkConnected(getContext())) {
            syncProgress.setVisibility(View.VISIBLE);
            OneTimeWorkRequest events = new OneTimeWorkRequest.Builder(FetchEventsWorker.class)
                    .addTag(Constants.TASK_SYNC_EVENTS)
                    .setConstraints(constraints)
                    .setInputData(new Data.Builder().putString("From", "Sync").build())
                    .build();
            WorkManager manager = WorkManager.getInstance();
            manager.beginWith(events).enqueue();
            WorkManager.getInstance().getWorkInfoByIdLiveData(events.getId()).observe(this, new Observer<WorkInfo>() {
                @Override
                public void onChanged(WorkInfo workInfo) {
                    if (workInfo != null && workInfo.getState() == WorkInfo.State.SUCCEEDED) {
                        FragmentEvents fragmentEvents = (FragmentEvents) getFragmentManager().findFragmentById(R.id.flContent);
                        getFragmentManager().beginTransaction().detach(fragmentEvents).attach(fragmentEvents).commit();
                        syncProgress.setVisibility(View.GONE);
                    } else {
                        syncProgress.setVisibility(View.GONE);
                    }
                }
            });
        }
    }

    @Override
    public void onDateSelected(Calendar dayCalendar, @Nullable List<CalendarEvent> events) {
        mEventsAdapter.clearList();

        if (events == null) {
            return;
        }

        for (CalendarEvent event : events) {
            if (event instanceof EventsResponseModel.MyEvent) {
                mEventsAdapter.addEvent((EventsResponseModel.MyEvent) event);
            }
        }
    }


    /**
     * Set text of textview to current month
     *
     * @param monthCalendar Calendar of focused month
     */
    @Override
    public void onMonthChanged(Calendar monthCalendar) {
        month = monthCalendar.get(Calendar.MONTH);
        year = monthCalendar.get(Calendar.YEAR);
        tvMonth.setText(mFormatter.format(monthCalendar) + ", " + monthCalendar.get(Calendar.YEAR));
    }

    @Override
    public List<? extends CalendarEvent> onLoadEvents(int year, int month) {
        // Fill by random events

        List<EventsResponseModel.MyEvent> events = new ArrayList<>();

        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.YEAR, year);
        calendar.set(Calendar.MONTH, month);


        eventList = MyDataBase.db.getEventsData();

        try {
            if (eventList != null) {


                EventsResponseModel model = new EventsResponseModel();
                for (int i = 0; i < eventList.size(); i++) {
                    SimpleDateFormat formatter = new SimpleDateFormat("MMM dd, yyyy");
                    String start = eventList.get(i).getStartDate();
                    String end = eventList.get(i).getEndDate();
                    Date date1 = new SimpleDateFormat("dd-MM-yyyy").parse(start);
                    Date date2 = new SimpleDateFormat("dd-MM-yyyy").parse(end);

                    String[] dateParts = start.split("-");
                    String startday = dateParts[0];
                    String startmonth = dateParts[1];
                    String startyear = dateParts[2];

                    String[] dateParts2 = end.split("-");
                    String endday = dateParts2[0];
                    String endmonth = dateParts2[1];
                    String endyear = dateParts2[2];


                    if (start.equals(end)) {
                        // if single day event
                        calendar.set(Integer.valueOf(startyear), Integer.valueOf(startmonth) - 1, Integer.valueOf(startday));
                        events.add(model.new MyEvent(eventList.get(i).getTitle(),
                                eventList.get(i).getDescription(),
                                formatter.format(date1),
                                formatter.format(date2),
                                calendar.getTimeInMillis(),
                                getRandomColor(),
                                eventList.get(i).getUsername(),
                                eventList.get(i).getNoReplyFlag(),
                                eventList.get(i).getEventId(),
                                eventList.get(i).getAddedDate(),
                                eventList.get(i).getEventDate(),
                                eventList.get(i).getUserImage()
                        ));
                    } else {
                        // multiple days event

                        long diff = date2.getTime() - date1.getTime();
                        // days = number of days of ongoing event
                        long days = TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);

                        int color = getRandomColor();

                        //add first day
                        calendar.set(Integer.valueOf(startyear), Integer.valueOf(startmonth) - 1, Integer.valueOf(startday));
                        events.add(model.new MyEvent(eventList.get(i).getTitle(),
                                eventList.get(i).getDescription(),
                                formatter.format(date1),
                                formatter.format(date2),
                                calendar.getTimeInMillis(),
                                color,
                                eventList.get(i).getUsername(),
                                eventList.get(i).getNoReplyFlag(),
                                eventList.get(i).getEventId(),
                                eventList.get(i).getAddedDate(),
                                eventList.get(i).getEventDate(),
                                eventList.get(i).getUserImage()
                        ));

                        Date prev = date1;
                        for (int j = 0; j < days; j++) {
                            //get next day from prev day
                            Date next = getNextDate(prev);
                            Calendar calendar1 = new GregorianCalendar();
                            calendar1.setTime(next);
                            calendar.set(calendar1.get(Calendar.YEAR), calendar1.get(Calendar.MONTH), calendar1.get(Calendar.DAY_OF_MONTH));
                            //add remaining days
                            events.add(model.new MyEvent(eventList.get(i).getTitle(),
                                    eventList.get(i).getDescription(),
                                    formatter.format(date1),
                                    formatter.format(date2),
                                    calendar.getTimeInMillis(),
                                    color,
                                    eventList.get(i).getUsername(),
                                    eventList.get(i).getNoReplyFlag(),
                                    eventList.get(i).getEventId(),
                                    eventList.get(i).getAddedDate(),
                                    eventList.get(i).getEventDate(),
                                    eventList.get(i).getUserImage()
                            ));
                            prev = next;

                        }
                    }

                }


            }

        } catch (Exception e) {
            Log.e(TAG, "onLoadEvents: ", e);
        }


        return events;
    }

    /**
     * Used to find the next date
     *
     * @param date current date
     * @return next date
     */
    public static Date getNextDate(Date date) {

        try {
            Calendar today = Calendar.getInstance();
            today.setTime(date);
            today.add(Calendar.DAY_OF_YEAR, 1);
            return today.getTime();
        } catch (Exception e) {
            return null;
        }
    }

    /**
     * generated random color for events
     *
     * @return color id
     */
//    private int getRandomColor() {
//        Resources resources = getResources();
//
//        int rand = (int) (Math.random() * 4);
//        switch (rand) {
//            case 0:
//                return resources.getColor(R.color.colorGooglePink);
//            case 1:
//                return resources.getColor(R.color.colorGooglePurple);
//            case 2:
//                return resources.getColor(R.color.colorGoogleBlue);
//            case 3:
//                return resources.getColor(R.color.colorGoogleGreen);
//        }
//
//        return 0x000000;
//    }
    private int getRandomColor() {
        Resources resources = getResources();
        return resources.getColor(R.color.colorGoogleGreen);
    }
}
