package com.shimbi.schoolbook.home.holidays.fragments;


import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.work.Constraints;
import androidx.work.Data;
import androidx.work.OneTimeWorkRequest;
import androidx.work.WorkInfo;
import androidx.work.WorkManager;

import com.shimbi.schoolbook.R;
import com.shimbi.schoolbook.db.helper.MyDataBase;
import com.shimbi.schoolbook.db.tables.DbHolidays;
import com.shimbi.schoolbook.home.activities.ActivityHome;
import com.shimbi.schoolbook.home.holidays.adapters.HolidaysAdapter;
import com.shimbi.schoolbook.home.holidays.models.HolidayResponseModel;
import com.shimbi.schoolbook.utils.AuthModel;
import com.shimbi.schoolbook.utils.Constants;
import com.shimbi.schoolbook.utils.NetworkStatusChecker;
import com.shimbi.schoolbook.utils.SaveSharedPreference;
import com.shimbi.schoolbook.utils.workers.FetchHolidaysWorker;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * A simple {@link Fragment} subclass.
 */
public class FragmentHolidays extends Fragment {

    private static final String TAG = "FragmentHolidays";
    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;
    private String username;
    private String userEmail;
    private String userType;
    private String userId;
    private Integer studentCount;
    private HolidaysAdapter adapter;
    private LinearLayoutManager layoutManager;
    private List<HolidayResponseModel> holidayList = new ArrayList<>();
    public ProgressBar syncProgress;
    private ImageView noData;


    public FragmentHolidays() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        ActivityHome activityHome = (ActivityHome) getActivity();
        activityHome.setTitleAndLogo("Holidays", R.drawable.ic_holidays_black_24dp);
        ActivityHome.fab.setVisibility(View.GONE);
        if (getActivity() != null) {
            syncProgress = getActivity().findViewById(R.id.progress_sync);
            syncProgress.setVisibility(View.GONE);
            noData = getActivity().findViewById(R.id.no_data);
            noData.setVisibility(View.GONE);
        }

        return inflater.inflate(R.layout.fragment_holidays, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
        init();


        loadDataFromLocal();
        loadDataOnline();

    }

    private void init() {
        AuthModel authModel = SaveSharedPreference.getData();
        userId = authModel.uid;
        username = authModel.username;
        userEmail = authModel.userEmail;
        userType = authModel.userType;
        studentCount = authModel.studentCount;
        Log.e(TAG, "init: " + userId + " " + userType);

        adapter = new HolidaysAdapter(holidayList);
        layoutManager = new LinearLayoutManager(this.getActivity());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(adapter);

    }

    private void loadDataFromLocal() {

        MyDataBase.db.getHolidays().observe(this, new Observer<List<DbHolidays>>() {
            @Override
            public void onChanged(List<DbHolidays> dbHolidays) {

                try {

                    holidayList.clear();
                    for (int i = 0; i < dbHolidays.size(); i++) {
                        holidayList.add(new HolidayResponseModel(
                                String.valueOf(dbHolidays.get(i).getHolidayId()),
                                dbHolidays.get(i).getUserId(),
                                dbHolidays.get(i).getHolidayName(),
                                dbHolidays.get(i).getFromDate(),
                                dbHolidays.get(i).getToDate()
                        ));
                    }
                } catch (Exception e) {
                    Log.e(TAG, "onChanged: ", e);
                }
                if (holidayList.size() == 0) {
                    noData.setVisibility(View.VISIBLE);
                } else {
                    noData.setVisibility(View.GONE);
                }
                adapter.notifyDataSetChanged();
            }
        });
    }

    private void loadDataOnline() {
        if (!NetworkStatusChecker.isNetworkConnected(getContext())) {
            Constraints constraints = NetworkStatusChecker.getNetWorkConstraints();
            syncProgress.setVisibility(View.VISIBLE);
            OneTimeWorkRequest holidays = new OneTimeWorkRequest.Builder(FetchHolidaysWorker.class)
                    .addTag(Constants.TASK_SYNC_HOLIDAYS)
                    .setConstraints(constraints)
                    .setInputData(new Data.Builder().putString("From", "Sync").build())
                    .build();
            WorkManager manager = WorkManager.getInstance();
            manager.beginWith(holidays).enqueue();
            WorkManager.getInstance().getWorkInfoByIdLiveData(holidays.getId()).observe(this, new Observer<WorkInfo>() {
                @Override
                public void onChanged(WorkInfo workInfo) {
                    if (workInfo != null && workInfo.getState() == WorkInfo.State.SUCCEEDED) {
                        syncProgress.setVisibility(View.GONE);
                    } else if (workInfo != null && workInfo.getState() == WorkInfo.State.FAILED) {
                        syncProgress.setVisibility(View.GONE);
                    }
                }
            });
        }
    }
}
