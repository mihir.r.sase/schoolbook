package com.shimbi.schoolbook.home.photos.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.shimbi.schoolbook.db.tables.DbAlbum;
import com.shimbi.schoolbook.db.tables.DbPhoto;

import java.util.List;

public class PhotosListResponseModel {


    @SerializedName("photos")
    @Expose
    public List<Photo> photos = null;
    @SerializedName("album")
    @Expose
    public List<Album> album = null;
    @SerializedName("syncdate")
    @Expose
    public String syncDate;

    public String getSyncDate() {
        return syncDate;
    }

    public void setSyncDate(String syncDate) {
        this.syncDate = syncDate;
    }

    public List<Photo> getPhotos() {
        return photos;
    }

    public void setPhotos(List<Photo> photos) {
        this.photos = photos;
    }

    public List<Album> getAlbum() {
        return album;
    }

    public void setAlbum(List<Album> album) {
        this.album = album;
    }

    public class Album {

        @SerializedName("album_name")
        @Expose
        public String albumName;
        @SerializedName("photo_cat_id")
        @Expose
        public String photoCatId;
        @SerializedName("photo")
        @Expose
        public String photo;
        @SerializedName("photos")
        @Expose
        public List<Photo> albumPhotos = null;

        public String getAlbumName() {
            return albumName;
        }

        public void setAlbumName(String albumName) {
            this.albumName = albumName;
        }

        public String getPhotoCatId() {
            return photoCatId;
        }

        public void setPhotoCatId(String photoCatId) {
            this.photoCatId = photoCatId;
        }

        public String getPhoto() {
            return photo;
        }

        public void setPhoto(String photo) {
            this.photo = photo;
        }

        public List<Photo> getAlbumPhotos() {
            return albumPhotos;
        }

        public void setAlbumPhotos(List<Photo> albumPhotos) {
            this.albumPhotos = albumPhotos;
        }

//        public class AlbumPhoto {
//
//            @SerializedName("user_id")
//            @Expose
//            public String userId;
//            @SerializedName("user_type")
//            @Expose
//            public String userType;
//            @SerializedName("photo_id")
//            @Expose
//            public String photoId;
//            @SerializedName("photo_title")
//            @Expose
//            public String photoTitle;
//            @SerializedName("photo_file")
//            @Expose
//            public String photoFile;
//            @SerializedName("added_date")
//            @Expose
//            public String addedDate;
//            @SerializedName("description")
//            @Expose
//            public String description;
//            @SerializedName("like_users")
//            @Expose
//            public Object likeUsers;
//            @SerializedName("like_count")
//            @Expose
//            public String likeCount;
//            @SerializedName("delete_flag")
//            @Expose
//            public Integer deleteFlag;
//
//
//            public String getUserId() {
//                return userId;
//            }
//
//            public void setUserId(String userId) {
//                this.userId = userId;
//            }
//
//            public String getUserType() {
//                return userType;
//            }
//
//            public void setUserType(String userType) {
//                this.userType = userType;
//            }
//
//            public String getPhotoId() {
//                return photoId;
//            }
//
//            public void setPhotoId(String photoId) {
//                this.photoId = photoId;
//            }
//
//            public String getPhotoTitle() {
//                return photoTitle;
//            }
//
//            public void setPhotoTitle(String photoTitle) {
//                this.photoTitle = photoTitle;
//            }
//
//            public String getPhotoFile() {
//                return photoFile;
//            }
//
//            public void setPhotoFile(String photoFile) {
//                this.photoFile = photoFile;
//            }
//
//            public String getAddedDate() {
//                return addedDate;
//            }
//
//            public void setAddedDate(String addedDate) {
//                this.addedDate = addedDate;
//            }
//
//            public String getDescription() {
//                return description;
//            }
//
//            public void setDescription(String description) {
//                this.description = description;
//            }
//
//            public Object getLikeUsers() {
//                return likeUsers;
//            }
//
//            public void setLikeUsers(Object likeUsers) {
//                this.likeUsers = likeUsers;
//            }
//
//            public String getLikeCount() {
//                return likeCount;
//            }
//
//            public void setLikeCount(String likeCount) {
//                this.likeCount = likeCount;
//            }
//
//            public Integer getDeleteFlag() {
//                return deleteFlag;
//            }
//
//            public void setDeleteFlag(Integer deleteFlag) {
//                this.deleteFlag = deleteFlag;
//            }
//        }
    }

    public class Photo {

        @SerializedName("user_id")
        @Expose
        public String userId;
        @SerializedName("user_type")
        @Expose
        public String userType;
        @SerializedName("photo_id")
        @Expose
        public String photoId;
        @SerializedName("photo_title")
        @Expose
        public String photoTitle;
        @SerializedName("photo_file")
        @Expose
        public String photoFile;
        @SerializedName("added_date")
        @Expose
        public String addedDate;
        @SerializedName("description")
        @Expose
        public String description;
        @SerializedName("like_users")
        @Expose
        public String likeUsers;
        @SerializedName("like_count")
        @Expose
        public String likeCount;
        @SerializedName("delete_flag")
        @Expose
        public Integer deleteFlag;

        public boolean isPhoto = false;

        public String albumId;

        public List<Photo> albumPhotosList;

        public Photo(DbPhoto photo) {
            this.userId = photo.getUserId();
            this.userType = photo.getUserType();
            this.photoId = String.valueOf(photo.getPhotoId());
            this.photoTitle = photo.getPhotoTitle();
            this.photoFile = photo.getPhotoFile();
            this.addedDate = photo.getAddedDate();
            this.description = photo.getDescription();
            this.likeUsers = photo.getLikeUsers();
            this.likeCount = photo.getLikeCount();
            this.deleteFlag = photo.getDeleteFlag();
            this.albumId = String.valueOf(photo.getAlbumId());
            this.isPhoto = true;
        }

        public Photo(DbAlbum dbAlbum) {
            this.photoTitle = dbAlbum.getAlbumName();
            this.albumId = String.valueOf(dbAlbum.getAlbumId());
            this.photoFile = dbAlbum.getPhoto();
        }

        public boolean isPhoto() {
            return isPhoto;
        }

        public void setPhoto(boolean photo) {
            isPhoto = photo;
        }

        public List<Photo> getAlbumPhotosList() {
            return albumPhotosList;
        }

        public void setAlbumPhotosList(List<Photo> albumPhotosList) {
            this.albumPhotosList = albumPhotosList;
        }

        public String getAlbumId() {
            return albumId;
        }

        public void setAlbumId(String albumId) {
            this.albumId = albumId;
        }

        public String getUserId() {
            return userId;
        }

        public void setUserId(String userId) {
            this.userId = userId;
        }

        public String getUserType() {
            return userType;
        }

        public void setUserType(String userType) {
            this.userType = userType;
        }

        public String getPhotoId() {
            return photoId;
        }

        public void setPhotoId(String photoId) {
            this.photoId = photoId;
        }

        public String getPhotoTitle() {
            return photoTitle;
        }

        public void setPhotoTitle(String photoTitle) {
            this.photoTitle = photoTitle;
        }

        public String getPhotoFile() {
            return photoFile;
        }

        public void setPhotoFile(String photoFile) {
            this.photoFile = photoFile;
        }

        public String getAddedDate() {
            return addedDate;
        }

        public void setAddedDate(String addedDate) {
            this.addedDate = addedDate;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public String getLikeUsers() {
            return likeUsers;
        }

        public void setLikeUsers(String likeUsers) {
            this.likeUsers = likeUsers;
        }

        public String getLikeCount() {
            return likeCount;
        }

        public void setLikeCount(String likeCount) {
            this.likeCount = likeCount;
        }

        public Integer getDeleteFlag() {
            return deleteFlag;
        }

        public void setDeleteFlag(Integer deleteFlag) {
            this.deleteFlag = deleteFlag;
        }
    }
}
