package com.shimbi.schoolbook.home.events.activities;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Rect;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.appbar.MaterialToolbar;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.card.MaterialCardView;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.shimbi.schoolbook.R;
import com.shimbi.schoolbook.db.helper.MyDataBase;
import com.shimbi.schoolbook.home.adapters.BottomAttachmentAdapter;
import com.shimbi.schoolbook.home.assignments.models.AssignmentDeteleResponseModel;
import com.shimbi.schoolbook.home.events.adapters.EventDetailsAdapter;
import com.shimbi.schoolbook.home.events.models.EventDetailsResponseModel;
import com.shimbi.schoolbook.home.models.AttachmentDetailsModel;
import com.shimbi.schoolbook.utils.Alerts;
import com.shimbi.schoolbook.utils.AuthModel;
import com.shimbi.schoolbook.utils.RetrofitClient;
import com.shimbi.schoolbook.utils.SaveSharedPreference;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ActivityEventDetails extends AppCompatActivity {

    private static final String TAG = "ActivityEventDetails";
    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;
    @BindView(R.id.llProgressBar)
    View llProgressBar;
    @BindView(R.id.et_comment)
    EditText editTextComment;
    @BindView(R.id.send)
    FloatingActionButton buttonSend;
    @BindView(R.id.comment_card)
    CardView commentCard;
    @BindView(R.id.blur_Background)
    View blurBackground;
    BottomSheetBehavior sheetBehavior;
    @BindView(R.id.bottom_sheet)
    MaterialCardView bottomSheet;
    @BindView(R.id.recycler_view_attachment)
    RecyclerView recyclerViewAttachment;

    private String username;
    private String userEmail;
    private String userType;
    private String userId;
    private Integer eventId;
    private Integer eventsAddFlag;
    // private EventsResponseModel.MyEvent currentEvent;
    private EventDetailsAdapter adapter;
    private List<EventDetailsResponseModel.Post> eventList = new ArrayList<>();

    private BottomAttachmentAdapter bottomAttachmentAdapter;
    private List<AttachmentDetailsModel> attachments = new ArrayList<>();

    private String title, desc, startDate, endDate, addedUserId, addedUserType;
    private Integer noReply;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_event_details);
        ButterKnife.bind(this);

        sheetBehavior = BottomSheetBehavior.from(bottomSheet);
        init();
        loadData();

        bottomSheet.setBackgroundResource(R.drawable.card_background);

        buttonSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String comment = editTextComment.getText().toString();
                if (comment.isEmpty()) {
                    return;
                }
                sendComment(comment);
                editTextComment.setText("");
            }
        });
    }

    private void init() {
        AuthModel authModel = SaveSharedPreference.getData();
        userId = authModel.uid;
        username = authModel.username;
        userEmail = authModel.userEmail;
        userType = authModel.userType;
        eventsAddFlag = authModel.eventAddFlag;

        try {
//            currentEvent = new Gson().fromJson(getIntent().getExtras().getString("events"), EventsResponseModel.MyEvent.class);
            eventId = getIntent().getIntExtra("eventId", -1);


        } catch (Exception e) {
            Log.e(TAG, "init: ", e);
        }




        adapter = new EventDetailsAdapter(eventList);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(adapter);
        adapter.setAttachmnetButtonClickListner(new EventDetailsAdapter.AttachmentButtonClickListener() {
            @Override
            public void onClick() {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    if (checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                        requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 100);
                    } else {
                        toggleBottomDrawer();
                    }
                }
            }
        });

        sheetBehavior.addBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(@NonNull View bottomSheet, int newState) {
                if (newState == BottomSheetBehavior.STATE_COLLAPSED) {
                    blurBackground.setVisibility(View.GONE);
                }

            }

            @Override
            public void onSlide(@NonNull View bottomSheet, float slideOffset) {

            }
        });

        bottomAttachmentAdapter = new BottomAttachmentAdapter(attachments);
        RecyclerView.LayoutManager layoutManager1 = new LinearLayoutManager(getApplicationContext());
        recyclerViewAttachment.setLayoutManager(layoutManager1);
        recyclerViewAttachment.setItemAnimator(new DefaultItemAnimator());
        recyclerViewAttachment.setAdapter(bottomAttachmentAdapter);


    }

    private void loadData() {

        progressVisibility(true);
        Call<EventDetailsResponseModel> apicall = RetrofitClient.get().getEventDetails(eventId);
        apicall.enqueue(new Callback<EventDetailsResponseModel>() {
            @Override
            public void onResponse(Call<EventDetailsResponseModel> call, @NonNull Response<EventDetailsResponseModel> response) {

                EventDetailsResponseModel model = response.body();
                EventDetailsResponseModel.PostData postData = model.getPostData();
                title = postData.getEventTitle();
                desc = postData.getDescription();
                startDate = postData.getStartdate();
                endDate = postData.getEnddate();
                noReply = postData.getNoReply();
                addedUserId = postData.getAddedUserId();
                addedUserType = postData.getAddedUserType();
                if (noReply == 1) {
                    editTextComment.setVisibility(View.GONE);
                    buttonSend.setVisibility(View.GONE);
                    commentCard.setVisibility(View.GONE);
                }

                MaterialToolbar materialToolbar = findViewById(R.id.appbar);
                setSupportActionBar(materialToolbar);
                getSupportActionBar().setDisplayHomeAsUpEnabled(true);
                getSupportActionBar().setDisplayShowHomeEnabled(true);
                try {

                    EventDetailsResponseModel.Result result = model.getResult();
                    List<EventDetailsResponseModel.Attachment> attachmentList = result.getAttachment();

                    eventList.add(model.new Post(
                            postData.getUsername(),
                            postData.getUserimage(),
                            postData.getAddedDate(),
                            postData.getDescription(),
                            postData.getEventTitle(),
                            postData.getEventDate(),
                            attachmentList.size() != 0 ? attachmentList.get(0).getDownloadLink() : ""
                    ));
                    List<EventDetailsResponseModel.Post> posts = result.getPost();


                    if (posts != null) {
                        Log.e(TAG, "onResponse: 3");
                        for (int i = 0; i < posts.size(); i++) {
                            eventList.add(model.new Post(
                                    posts.get(i).getName(),
                                    posts.get(i).getImage(),
                                    posts.get(i).getDate(),
                                    posts.get(i).getComment()
                            ));
                        }
                    }
                    adapter.notifyDataSetChanged();

                    if (attachmentList.size() == 0) {
                        adapter.setIsAssignmentEmpty(true);
                    }

                    for (int i = 0; i < attachmentList.size(); i++) {
                        attachments.add(new AttachmentDetailsModel(attachmentList.get(i)));
                    }
                    bottomAttachmentAdapter.notifyDataSetChanged();

                } catch (Exception e) {
                    Alerts.displayToast(getApplicationContext(), "Loading Failed!");
                    Log.e(TAG, "onResponse: ", e);
                }


                progressVisibility(false);

            }

            @Override
            public void onFailure(Call<EventDetailsResponseModel> call, Throwable t) {
                Log.e(TAG, "onFailure: ", t);
                Alerts.displayToast(getApplicationContext(), "Loading Failed!!");
                progressVisibility(false);
            }
        });
    }


    /**
     * used to send a comment
     *
     * @param comment comment to be sent
     */
    private void sendComment(String comment) {
        progressVisibility(true);

        Call<Integer> sendComment = RetrofitClient.get().sendCommentOnEvents(String.valueOf(eventId), userId, userType, comment);
        sendComment.enqueue(new Callback<Integer>() {
            @Override
            public void onResponse(Call<Integer> call, Response<Integer> response) {
                if (response.body() == 1) {
                    Alerts.displayToast(getApplicationContext(), "Message Sent");
                    eventList.clear();
                    loadData();
                } else {
                    Log.e(TAG, "onFailure: Response = 0");
                    Alerts.displayToast(getApplicationContext(), "Sending Failed!");
                }
                progressVisibility(false);
            }

            @Override
            public void onFailure(Call<Integer> call, Throwable t) {
                Log.e(TAG, "onFailure: ", t);
                Alerts.displayToast(getApplicationContext(), "Sending Failed!");
                progressVisibility(false);
            }
        });
    }

    /**
     * delete's the assignment if user has rights
     */
    public void deleteAssignment() {
        progressVisibility(true);

        Call<AssignmentDeteleResponseModel> apicall = RetrofitClient.get().deleteEvent(userId, userType, String.valueOf(eventId));
        apicall.enqueue(new Callback<AssignmentDeteleResponseModel>() {
            @Override
            public void onResponse(Call<AssignmentDeteleResponseModel> call, Response<AssignmentDeteleResponseModel> response) {
                if (response.body() != null) {
                    AssignmentDeteleResponseModel model = response.body();
                    if (model.getResult().equals("1")) {
                        Alerts.displayToast(getApplicationContext(), "Deletion Successful");
                        finish();
                        MyDataBase.db.deleteEvent(eventId);
                        progressVisibility(false);
                    } else {
                        Alerts.displayToast(getApplicationContext(), "Deletion Failed");
                        progressVisibility(false);
                    }
                } else {
                    Alerts.displayToast(getApplicationContext(), "Deletion Failed");
                    progressVisibility(false);
                }
            }

            @Override
            public void onFailure(Call<AssignmentDeteleResponseModel> call, Throwable t) {
                Log.e(TAG, "onFailure: ", t);
                Alerts.displayToast(getApplicationContext(), "Deletion Failed");
                progressVisibility(false);
            }
        });
    }

    private void toggleBottomDrawer() {
        if (sheetBehavior.getState() != BottomSheetBehavior.STATE_EXPANDED) {
            blurBackground.setVisibility(View.VISIBLE);
            sheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
        } else {
            sheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.attach_file, menu);

        menu.findItem(R.id.menu_attachment).setVisible(false);
        if (!(userId.equals(addedUserId) && userType.equals(addedUserType))) {
            menu.findItem(R.id.menu_edit).setVisible(false);
            menu.findItem(R.id.menu_delete).setVisible(false);
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle presses on the action bar items
        int id = item.getItemId();
        switch (id) {
            case android.R.id.home:
                finish();
                break;
            case R.id.menu_edit:
                Intent intent = new Intent(ActivityEventDetails.this, ActivityNewEvent.class);
                Bundle bundle = new Bundle();
                bundle.putInt("eventId", eventId);
                bundle.putString("title", title);
                bundle.putString("desc", desc);
                bundle.putString("startDate", startDate);
                bundle.putString("endDate", endDate);
                bundle.putInt("noReply", noReply);
                intent.putExtras(bundle);
                startActivity(intent);
                break;
            case R.id.menu_delete:
                new MaterialAlertDialogBuilder(ActivityEventDetails.this, R.style.DialogStyle)
                        .setTitle("Do you want to delete this assignment?")
                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                deleteAssignment();
                            }
                        })
                        .setNegativeButton("No", null)
                        .setCancelable(false)
                        .show();
                break;

        }


        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == 100) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                toggleBottomDrawer();
            } else {
                new MaterialAlertDialogBuilder(ActivityEventDetails.this, R.style.DialogStyle)
                        .setTitle("Storage Access Permission Not Granted")
                        .setMessage("Please grant storage permission to download attached files.")
                        .setPositiveButton("Okay", null)
                        .setCancelable(false)
                        .show();
            }
        }
    }

    /**
     * Display progress bar and uses TOUCHABLE flags
     *
     * @param visibility Give True to display dialog, False to remove
     */
    private void progressVisibility(boolean visibility) {
        if (visibility) {
            llProgressBar.setVisibility(View.VISIBLE);
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                    WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
        } else {
            llProgressBar.setVisibility(View.GONE);
            getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
        }
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent event) {
        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            if (sheetBehavior.getState() == BottomSheetBehavior.STATE_EXPANDED) {

                Rect outRect = new Rect();
                bottomSheet.getGlobalVisibleRect(outRect);

                if (!outRect.contains((int) event.getRawX(), (int) event.getRawY()))
                    sheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
            }
        }
        return super.dispatchTouchEvent(event);
    }

}
