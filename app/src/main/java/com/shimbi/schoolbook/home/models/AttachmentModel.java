package com.shimbi.schoolbook.home.models;

public class AttachmentModel {

    String filename, fileInformation, filePath, finalFileName;

    public AttachmentModel(String filename, String fileInformation, String filePath, String finalFileName) {
        this.filename = filename;
        this.fileInformation = fileInformation;
        this.filePath = filePath;
        this.finalFileName = finalFileName;
    }

    public AttachmentModel(AttachmentDetailsModel attachments) {
        this.filename = attachments.getFileOriginalName();
        this.fileInformation = attachments.getFileInfo();
        this.finalFileName = attachments.getTempFileName();
    }

    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    public String getFileInformation() {
        return fileInformation;
    }

    public void setFileInformation(String fileInformation) {
        this.fileInformation = fileInformation;
    }

    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    public String getFinalFileName() {
        return finalFileName;
    }

    public void setFinalFileName(String finalFileName) {
        this.finalFileName = finalFileName;
    }

    @Override
    public String toString() {
        return "AttachmentModel{" +
                "filename='" + filename + '\'' +
                ", fileInformation='" + fileInformation + '\'' +
                ", filePath='" + filePath + '\'' +
                ", finalFileName='" + finalFileName + '\'' +
                '}';
    }
}
