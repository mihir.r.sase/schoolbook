package com.shimbi.schoolbook.home.messages.adapters;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.shimbi.schoolbook.R;
import com.shimbi.schoolbook.home.messages.models.MessageDetailsResponseModel;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

public class MessageDetailsAdapter extends RecyclerView.Adapter<MessageDetailsAdapter.MyViewHolder> {

    private List<MessageDetailsResponseModel.Post> messageList;
    AttachmentButtonClickListener listener;
    private static final String TAG = "MessageDetailsAdapter";
    Context context;

    public boolean isAssignmentEmpty = false;

    public void setIsAssignmentEmpty(boolean isAssignmentEmpty) {
        this.isAssignmentEmpty = isAssignmentEmpty;
    }

    public MessageDetailsAdapter(List<MessageDetailsResponseModel.Post> messageList) {
        this.messageList = messageList;
    }

    public void setAttachmnetButtonClickListner(AttachmentButtonClickListener listener) {
        this.listener = listener;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_message_list_details, parent, false);
        context = parent.getContext();
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {

        MessageDetailsResponseModel.Post comment = messageList.get(position);
        holder.tvUsername.setText(comment.getName());
        holder.tvDate.setText(comment.getDate());
        holder.tvComment.setText(comment.getComment());

        Log.e(TAG, "onBindViewHolder: " + comment.getImage());
        Glide.with(context).load(comment.getImage()).centerCrop()
                .placeholder(R.drawable.default_profile)
                .into(holder.profileImage);
        if (position == (messageList.size() - 1)) {
            holder.lineConnector.setVisibility(View.GONE);
        } else {
            holder.lineConnector.setVisibility(View.VISIBLE);
        }
        if (position == 0) {
            holder.tvTitle.setVisibility(View.VISIBLE);
            holder.tvTitle.setText(messageList.get(position).getTitle());
            holder.relativeLayoutOfTimeline.setVisibility(View.GONE);
            holder.ivAttachment.setVisibility(View.VISIBLE);
        } else {
            holder.tvTitle.setVisibility(View.GONE);
            holder.relativeLayoutOfTimeline.setVisibility(View.VISIBLE);
            holder.ivAttachment.setVisibility(View.GONE);
        }
        holder.ivAttachment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (listener != null) {
                    listener.onClick();
                }
            }
        });

        if (isAssignmentEmpty) {
            holder.ivAttachment.setVisibility(View.GONE);
        }
    }

    @Override
    public int getItemCount() {
        return messageList.size();
    }

    public interface AttachmentButtonClickListener {
        void onClick();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tv_username)
        TextView tvUsername;
        @BindView(R.id.tv_date)
        TextView tvDate;
        @BindView(R.id.tv_comment)
        TextView tvComment;
        @BindView(R.id.layout_of_timeline)
        RelativeLayout relativeLayoutOfTimeline;
        @BindView(R.id.line_connector)
        View lineConnector;
        @BindView(R.id.tv_title)
        TextView tvTitle;
        @BindView(R.id.profile_image)
        CircleImageView profileImage;
        @BindView(R.id.iv_attachment)
        ImageView ivAttachment;


        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
