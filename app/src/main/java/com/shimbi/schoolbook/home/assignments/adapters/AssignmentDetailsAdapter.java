package com.shimbi.schoolbook.home.assignments.adapters;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.shimbi.schoolbook.R;
import com.shimbi.schoolbook.home.assignments.models.AssignmentDetailsResponseModel;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

public class AssignmentDetailsAdapter extends RecyclerView.Adapter<AssignmentDetailsAdapter.MyViewHolder> {

    private static final String TAG = "AssignmentDetailsAdapte";
    private List<AssignmentDetailsResponseModel.AssignmentDetailsResultModel.AssignmentDetailsPostModel> assignmentDetailsList;
    Context context;
    AttachmentButtonClickListener listener;

    public boolean isAssignmentEmpty = false;

    public void setIsAssignmentEmpty(boolean isAssignmentEmpty) {
        this.isAssignmentEmpty = isAssignmentEmpty;
    }

    public AssignmentDetailsAdapter(List<AssignmentDetailsResponseModel.AssignmentDetailsResultModel.AssignmentDetailsPostModel> assignmentDetailsList) {
        this.assignmentDetailsList = assignmentDetailsList;
    }

    public void setAttachmnetButtonClickListner(AttachmentButtonClickListener listener) {
        this.listener = listener;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_assignment_list_detail, parent, false);
        context = parent.getContext();
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        int pos = holder.getAdapterPosition();
        AssignmentDetailsResponseModel.AssignmentDetailsResultModel.AssignmentDetailsPostModel assignmentComment = assignmentDetailsList.get(pos);
        holder.tvUsername.setText(assignmentComment.getName());
        holder.tvDate.setText(assignmentComment.getDate());
        holder.tvComment.setText(assignmentComment.getComment());

        Log.e(TAG, "onBindViewHolder: " + assignmentComment.getImage());
        Glide.with(context).load(assignmentComment.getImage()).centerCrop()
                .placeholder(R.drawable.default_profile)
                .into(holder.profileImage);

        if (pos == (assignmentDetailsList.size() - 1)) {
            holder.lineConnector.setVisibility(View.GONE);
        } else {
            holder.lineConnector.setVisibility(View.VISIBLE);
        }
        if (pos == 0) {
            holder.tvTitle.setVisibility(View.VISIBLE);
            holder.tvTitle.setText(assignmentComment.getTitle());
            holder.tvSubmissionDate.setVisibility(View.VISIBLE);
            holder.tvSubmissionDate.setText(assignmentComment.getSubmissionDate());
            holder.relativeLayoutOfTimeline.setVisibility(View.GONE);
            holder.ivAttachment.setVisibility(View.VISIBLE);
        } else {
            holder.tvTitle.setVisibility(View.GONE);
            holder.tvSubmissionDate.setVisibility(View.GONE);
            holder.relativeLayoutOfTimeline.setVisibility(View.VISIBLE);
            holder.ivAttachment.setVisibility(View.GONE);
        }

        holder.ivAttachment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (listener != null) {
                    listener.onClick();
                }
            }
        });

        if (isAssignmentEmpty) {
            holder.ivAttachment.setVisibility(View.GONE);
        }
    }

    @Override
    public int getItemCount() {
        return assignmentDetailsList.size();
    }

    public interface AttachmentButtonClickListener {
        void onClick();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tv_username)
        TextView tvUsername;
        @BindView(R.id.tv_date)
        TextView tvDate;
        @BindView(R.id.tv_comment)
        TextView tvComment;
        @BindView(R.id.layout_of_timeline)
        RelativeLayout relativeLayoutOfTimeline;
        @BindView(R.id.line_connector)
        View lineConnector;
        @BindView(R.id.tv_title)
        TextView tvTitle;
        @BindView(R.id.profile_image)
        CircleImageView profileImage;
        @BindView(R.id.submissionDate)
        TextView tvSubmissionDate;
        @BindView(R.id.iv_attachment)
        ImageView ivAttachment;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
