package com.shimbi.schoolbook.home.holidays.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class HolidayResponseModel {

    @SerializedName("holiday_id")
    @Expose
    private String holidayId;
    @SerializedName("uid")
    @Expose
    private String uid;
    @SerializedName("holiday_name")
    @Expose
    private String holidayName;
    @SerializedName("from_date")
    @Expose
    private String fromDate;
    @SerializedName("to_date")
    @Expose
    private String toDate;


    public HolidayResponseModel(String holidayId, String uid, String holidayName, String fromDate, String toDate) {
        this.holidayId = holidayId;
        this.uid = uid;
        this.holidayName = holidayName;
        this.fromDate = fromDate;
        this.toDate = toDate;
    }

    public String getHolidayId() {
        return holidayId;
    }

    public void setHolidayId(String holidayId) {
        this.holidayId = holidayId;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getHolidayName() {
        return holidayName;
    }

    public void setHolidayName(String holidayName) {
        this.holidayName = holidayName;
    }

    public String getFromDate() {
        return fromDate;
    }

    public void setFromDate(String fromDate) {
        this.fromDate = fromDate;
    }

    public String getToDate() {
        return toDate;
    }

    public void setToDate(String toDate) {
        this.toDate = toDate;
    }
}
