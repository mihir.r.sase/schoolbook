package com.shimbi.schoolbook.home.activities;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.net.Uri;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.util.Patterns;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.bumptech.glide.Glide;
import com.google.android.material.appbar.MaterialToolbar;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.shimbi.schoolbook.R;
import com.shimbi.schoolbook.db.helper.MyDataBase;
import com.shimbi.schoolbook.home.models.SuccessResponseModel;
import com.shimbi.schoolbook.utils.Alerts;
import com.shimbi.schoolbook.utils.AuthModel;
import com.shimbi.schoolbook.utils.PathUtil;
import com.shimbi.schoolbook.utils.RetrofitClient;
import com.shimbi.schoolbook.utils.SaveSharedPreference;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ActivityEditProfile extends AppCompatActivity {

    private static final String TAG = "ActivityEditProfile";
    @BindView(R.id.tv_username)
    TextView tvUsername;
    @BindView(R.id.tv_class)
    TextView tvClass;
    @BindView(R.id.tv_dob)
    TextView tvDob;
    @BindView(R.id.et_email)
    TextInputEditText etEmail;
    @BindView(R.id.et_mobile)
    TextInputEditText etMobile;
    @BindView(R.id.et_address)
    TextInputEditText etAddress;
    @BindView(R.id.bt_save)
    MaterialButton buttonSave;
    @BindView(R.id.llProgressBar)
    View llProgressBar;
    @BindView(R.id.fab_edit_pic)
    FloatingActionButton fabEdit;
    @BindView(R.id.profile_image)
    CircleImageView profileImage;
    @BindView(R.id.container_personal_email)
    LinearLayout containerPersonalEmail;
    @BindView(R.id.tv_official_email)
    TextView tvOfficialEmail;
    @BindView(R.id.et_email_layout)
    TextInputLayout etEmailLayout;
    String imagePath = null;
    String imageName = null;
    String base64Image = null;
    private String username;
    private String userEmail;
    private String userType;
    private String userId;
    private String eventID;
    private Integer studentCount;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_profile);
        ButterKnife.bind(this);
        init();
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_NOSENSOR);

        buttonSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String email = etEmail.getText().toString();
                String mobile = etMobile.getText().toString();
                String address = etAddress.getText().toString();

                if (!validateInput(email, mobile, address)) {
                    return;
                }

                saveData(email, mobile, address);
            }
        });

        fabEdit.setOnClickListener(view -> CropImage.activity().setGuidelines(CropImageView.Guidelines.ON).setCropShape(CropImageView.CropShape.OVAL).setAspectRatio(1, 1)
                .start(ActivityEditProfile.this));
    }


    private void init() {
        AuthModel authModel = SaveSharedPreference.getData();
        userId = authModel.uid;
        userType = authModel.userType;
        studentCount = authModel.studentCount;

        MaterialToolbar materialToolbar = findViewById(R.id.appbar);
        setSupportActionBar(materialToolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }

        try {
            Intent intent = getIntent();
            tvUsername.setText(intent.getExtras().getString("username"));
            tvClass.setText(intent.getExtras().getString("userClass"));
            tvDob.setText(intent.getExtras().getString("userDob"));
            etEmail.setText(intent.getExtras().getString("userEmail"));
            etMobile.setText(intent.getExtras().getString("userMobile"));
            etAddress.setText(intent.getExtras().getString("userAddress"));
            Glide.with(this).load(intent.getExtras().getString("userImage"))
                    .placeholder(intent.getExtras().getString("userGender").equals("Male") ? R.drawable.default_profile : R.drawable.default_profile_girl)
                    .into(profileImage);
            if (userType.equals("1")) {
                containerPersonalEmail.setVisibility(View.VISIBLE);
                tvOfficialEmail.setText(intent.getExtras().getString("userEmail"));
                etEmailLayout.setVisibility(View.GONE);
            }
        } catch (Exception e) {
            Log.e(TAG, "init: " + e);
        }


    }

    private boolean validateInput(String email, String mobile, String address) {

        if (email.isEmpty()) {
            etEmail.setError("Please Enter Email Address");
            return false;
        }
        if (!Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            etEmail.setError("Please Enter Valid Email Address");
            return false;
        }
        if (mobile.isEmpty()) {
            etMobile.setError("Please Enter Mobile Number");
            return false;
        }
        if (mobile.length() != 10) {
            etMobile.setError("Please Enter Valid Mobile Number");
            return false;
        }
        if (address.isEmpty()) {
            etAddress.setError("Please Enter Address");
            return false;
        }

        return true;
    }

    private void saveData(String email, String mobile, String address) {


        progressVisibility(true);
        Call<SuccessResponseModel> apicall = RetrofitClient.get().updateUserProfile(userId, userType, email, mobile, address, imageName, base64Image);
        apicall.enqueue(new Callback<SuccessResponseModel>() {
            @Override
            public void onResponse(Call<SuccessResponseModel> call, Response<SuccessResponseModel> response) {
                if (response.body() != null) {
                    SuccessResponseModel status = response.body();
                    if (status.getSuccess().equals("1")) {
                        Alerts.displayToast(getApplicationContext(), "Success!");
                        MyDataBase.db.updateUser(email, mobile, address);
                        Intent intent = new Intent(ActivityEditProfile.this, ActivityProfile.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(intent);
                    } else {
                        progressVisibility(false);
                        Alerts.displayToast(getApplicationContext(), "Updating Failed");
                    }
                    progressVisibility(false);
                } else {
                    progressVisibility(false);
                    Alerts.displayToast(getApplicationContext(), "Updating Failed");
                }
            }

            @Override
            public void onFailure(Call<SuccessResponseModel> call, Throwable t) {
                Log.e(TAG, "onFailure: ", t);
                Alerts.displayToast(getApplicationContext(), "Updating Failed");
                progressVisibility(false);
            }
        });
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                imageName = "";
                Uri resultUri = result.getUri();
                Glide.with(this).load(resultUri).into(profileImage);

                try {
                    imagePath = PathUtil.getPath(getApplicationContext(), resultUri);
                    String extention = imagePath.substring(imagePath.lastIndexOf(".") + 1);
                    Long tsLong = System.currentTimeMillis() / 1000;
                    String timestamp = tsLong.toString();
                    imageName = "ProfilePic_" + userId + "_" + userType + "_" + timestamp + "." + extention;
                    base64Image = getStringFile(imagePath);
                } catch (Exception e) {
                    e.printStackTrace();
                }

            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Exception error = result.getError();
            }
        }
    }


    public String getStringFile(String filePath) {
        byte[] AsBytes = readBytesFromFile(filePath);
        String AsBase64String = Base64.encodeToString(AsBytes, Base64.DEFAULT);
        return AsBase64String;
    }

    private byte[] readBytesFromFile(String filePath) {

        FileInputStream fileInputStream = null;
        byte[] bytesArray = null;
        File file = new File(filePath);
        int bytesRead, bytesAvailable, bufferSize;
        int maxBufferSize = 1 * 2048 * 2048;
        if (!file.isFile()) {
            Log.e(TAG, "readBytesFromFile: Not A File");
        }
        try {
            fileInputStream = new FileInputStream(file);
            bytesArray = new byte[(int) file.length()];
            bytesAvailable = fileInputStream.available();
            bufferSize = bytesAvailable;
            bytesArray = new byte[bufferSize];

            int byt = fileInputStream.read(bytesArray);
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (fileInputStream != null) {
                try {
                    fileInputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return bytesArray;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle presses on the action bar items
        int id = item.getItemId();

        if (id == android.R.id.home) {
            finish();
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * Display progress bar and uses TOUCHABLE flags
     *
     * @param visibility Give True to display dialog, False to remove
     */
    private void progressVisibility(boolean visibility) {
        if (visibility) {
            llProgressBar.setVisibility(View.VISIBLE);
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                    WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
        } else {
            llProgressBar.setVisibility(View.GONE);
            getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
        }
    }
}
