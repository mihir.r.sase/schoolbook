package com.shimbi.schoolbook.home.events.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.shimbi.schoolbook.utils.calendar.CalendarEvent;

import java.util.List;

public class EventsResponseModel {


    @SerializedName("syncdate")
    @Expose
    private String syncdate;
    @SerializedName("no_of_record")
    @Expose
    private Integer noOfRecord;
    @SerializedName("total_pages")
    @Expose
    private Integer totalPages;
    @SerializedName("result")
    @Expose
    private List<MyEvent> result = null;

    public String getSyncdate() {
        return syncdate;
    }

    public void setSyncdate(String syncdate) {
        this.syncdate = syncdate;
    }

    public Integer getNoOfRecord() {
        return noOfRecord;
    }

    public void setNoOfRecord(Integer noOfRecord) {
        this.noOfRecord = noOfRecord;
    }

    public Integer getTotalPages() {
        return totalPages;
    }

    public void setTotalPages(Integer totalPages) {
        this.totalPages = totalPages;
    }

    public List<MyEvent> getResult() {
        return result;
    }

    public void setResult(List<MyEvent> result) {
        this.result = result;
    }


    public class MyEvent extends CalendarEvent {

        @SerializedName("username")
        @Expose
        private String username;
        @SerializedName("event_id")
        @Expose
        private Integer eventId;
        @SerializedName("no_reply")
        @Expose
        private Integer noReply;
        @SerializedName("event_title")
        @Expose
        private String eventTitle;
        @SerializedName("description")
        @Expose
        private String description;
        @SerializedName("added_date")
        @Expose
        private String addedDate;
        @SerializedName("formatted_date")
        @Expose
        private String formattedDate;
        @SerializedName("picture")
        @Expose
        private String picture;
        @SerializedName("userimage")
        @Expose
        private String userimage;
        @SerializedName("class_key")
        @Expose
        private String classKey;
        @SerializedName("class_value")
        @Expose
        private String classValue;
        @SerializedName("startdate")
        @Expose
        private String startdate;
        @SerializedName("enddate")
        @Expose
        private String enddate;
        @SerializedName("starttime")
        @Expose
        private String starttime;
        @SerializedName("endtime")
        @Expose
        private String endtime;
        @SerializedName("eventstatue")
        @Expose
        private String eventstatue;
        @SerializedName("group_name")
        @Expose
        private String groupName;
        @SerializedName("notice_sms")
        @Expose
        private String noticeSms;
        @SerializedName("send_sms_flag")
        @Expose
        private Integer sendSmsFlag;
        @SerializedName("submiton_date")
        @Expose
        private String submitonDate;
        @SerializedName("flag")
        @Expose
        private String flag;
        @SerializedName("event_date")
        @Expose
        private String eventDate;

        public MyEvent(String title, String desc, String startdate, String enddate, long startTimeInMillis, int indicatorColor, String username, Integer noReply
                , Integer eventId, String addedDate, String eventDate, String userimage) {
            super(startTimeInMillis, indicatorColor);
            this.eventTitle = title;
            this.description = desc;
            this.startdate = startdate;
            this.enddate = enddate;
            this.username = username;
            this.noReply = noReply;
            this.eventId = eventId;
            this.addedDate = addedDate;
            this.eventDate = eventDate;
            this.userimage = userimage;

        }



        @Override
        public String toString() {
            return "MyEvent{" +
                    "username='" + username + '\'' +
                    ", eventId=" + eventId +
                    ", noReply=" + noReply +
                    ", eventTitle='" + eventTitle + '\'' +
                    ", description='" + description + '\'' +
                    ", addedDate='" + addedDate + '\'' +
                    ", formattedDate='" + formattedDate + '\'' +
                    ", picture='" + picture + '\'' +
                    ", userimage='" + userimage + '\'' +
                    ", classKey='" + classKey + '\'' +
                    ", classValue='" + classValue + '\'' +
                    ", startdate='" + startdate + '\'' +
                    ", enddate='" + enddate + '\'' +
                    ", starttime='" + starttime + '\'' +
                    ", endtime='" + endtime + '\'' +
                    ", eventstatue='" + eventstatue + '\'' +
                    ", groupName='" + groupName + '\'' +
                    ", noticeSms='" + noticeSms + '\'' +
                    ", sendSmsFlag=" + sendSmsFlag +
                    ", submitonDate='" + submitonDate + '\'' +
                    ", flag='" + flag + '\'' +
                    ", eventDate='" + eventDate + '\'' +
                    '}';
        }


        public String getUsername() {
            return username;
        }

        public Integer getEventId() {
            return eventId;
        }

        public Integer getNoReply() {
            return noReply;
        }

        public String getEventTitle() {
            return eventTitle;
        }

        public String getDescription() {
            return description;
        }

        public String getAddedDate() {
            return addedDate;
        }

        public String getFormattedDate() {
            return formattedDate;
        }

        public String getPicture() {
            return picture;
        }

        public String getUserimage() {
            return userimage;
        }

        public String getClassKey() {
            return classKey;
        }

        public String getClassValue() {
            return classValue;
        }

        public String getStartdate() {
            return startdate;
        }

        public String getEnddate() {
            return enddate;
        }

        public String getStarttime() {
            return starttime;
        }

        public String getEndtime() {
            return endtime;
        }

        public String getEventstatue() {
            return eventstatue;
        }

        public String getGroupName() {
            return groupName;
        }

        public String getNoticeSms() {
            return noticeSms;
        }

        public Integer getSendSmsFlag() {
            return sendSmsFlag;
        }

        public String getSubmitonDate() {
            return submitonDate;
        }

        public String getFlag() {
            return flag;
        }

        public String getEventDate() {
            return eventDate;
        }
    }


}
