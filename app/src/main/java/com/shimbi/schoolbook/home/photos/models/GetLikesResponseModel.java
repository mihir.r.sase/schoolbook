package com.shimbi.schoolbook.home.photos.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GetLikesResponseModel {
    @SerializedName("like_users")
    @Expose
    private String likeUsers;
    @SerializedName("like_count")
    @Expose
    private String likeCount;
    @SerializedName("like_login_user")
    @Expose
    private Integer likeLoginUser;

    public String getLikeUsers() {
        return likeUsers;
    }

    public void setLikeUsers(String likeUsers) {
        this.likeUsers = likeUsers;
    }

    public String getLikeCount() {
        return likeCount;
    }

    public void setLikeCount(String likeCount) {
        this.likeCount = likeCount;
    }

    public Integer getLikeLoginUser() {
        return likeLoginUser;
    }

    public void setLikeLoginUser(Integer likeLoginUser) {
        this.likeLoginUser = likeLoginUser;
    }
}
