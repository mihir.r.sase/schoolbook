package com.shimbi.schoolbook.home.fees.fragments;


import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.button.MaterialButton;
import com.shimbi.schoolbook.R;
import com.shimbi.schoolbook.home.activities.ActivityHome;
import com.shimbi.schoolbook.home.fees.activities.ActivityFeeStructure;
import com.shimbi.schoolbook.home.fees.adapters.FeeAdapter;
import com.shimbi.schoolbook.home.fees.models.FeesResponseModel;
import com.shimbi.schoolbook.utils.Alerts;
import com.shimbi.schoolbook.utils.AuthModel;
import com.shimbi.schoolbook.utils.NetworkStatusChecker;
import com.shimbi.schoolbook.utils.RetrofitClient;
import com.shimbi.schoolbook.utils.SaveSharedPreference;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class FragmentFees extends Fragment {

    private static final String TAG = "FragmentFees";
    @BindView(R.id.tv_username)
    TextView tvUsername;
    @BindView(R.id.tv_amount_paid)
    TextView tvAmountPaid;
    @BindView(R.id.tv_email)
    TextView tvEmail;
    @BindView(R.id.tv_upcoming)
    TextView tvUpcoming;
    @BindView(R.id.tv_overdue)
    TextView tvOverdue;
    @BindView(R.id.recycler_view_upcoming)
    RecyclerView recyclerViewUpcoming;
    @BindView(R.id.recycler_view_overdue)
    RecyclerView recyclerViewOverdue;
    @BindView(R.id.bt_fee_structure)
    MaterialButton btFeeStructure;



    private FeeAdapter feeAdapterUpcoming;
    private FeeAdapter feeAdapterOverdue;
    private List<FeesResponseModel.Upcoming> upcomingList = new ArrayList<>();
    private List<FeesResponseModel.Upcoming> overdueList = new ArrayList<>();

    private String userType;
    private String userId;
    private String userName;
    private String userEmail;
    public ProgressBar syncProgress;
    public View llProgressBar;
    private ImageView noData;

    public FragmentFees() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        ActivityHome activityHome = (ActivityHome) getActivity();
        activityHome.setTitleAndLogo("Fees", R.drawable.ic_fee);
        ActivityHome.fab.setVisibility(View.GONE);
        if (getActivity() != null) {
            syncProgress = getActivity().findViewById(R.id.progress_sync);
            syncProgress.setVisibility(View.GONE);
            noData = getActivity().findViewById(R.id.no_data);
            noData.setVisibility(View.GONE);
        }

        return inflater.inflate(R.layout.fragment_fees, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
        init();
        if (!NetworkStatusChecker.isNetworkConnected(getContext())) {

            loadData();
        } else {
            Alerts.displayToast(getContext(), "Please check your connection and try again");
        }

        btFeeStructure.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getActivity(), ActivityFeeStructure.class));
            }
        });
    }

    private void init() {
        AuthModel authModel = SaveSharedPreference.getData();
        userId = authModel.uid;
        userType = authModel.userType;
        userName = authModel.username;
        userEmail = authModel.userEmail;


        RecyclerView.LayoutManager layoutManager1 = new LinearLayoutManager(getContext());
        feeAdapterUpcoming = new FeeAdapter(upcomingList);
        recyclerViewUpcoming.setLayoutManager(layoutManager1);
        recyclerViewUpcoming.setItemAnimator(new DefaultItemAnimator());
        recyclerViewUpcoming.setAdapter(feeAdapterUpcoming);

        RecyclerView.LayoutManager layoutManager2 = new LinearLayoutManager(getContext());
        feeAdapterOverdue = new FeeAdapter(overdueList);
        recyclerViewOverdue.setLayoutManager(layoutManager2);
        recyclerViewOverdue.setItemAnimator(new DefaultItemAnimator());
        recyclerViewOverdue.setAdapter(feeAdapterOverdue);
        if (getActivity() != null) {
            llProgressBar = getActivity().findViewById(R.id.llProgressBar);
        }
    }

    private void loadData() {

        progressVisibility(true);

        Call<FeesResponseModel> apicall = RetrofitClient.get().getFeesDetails(userId, userType);
        apicall.enqueue(new Callback<FeesResponseModel>() {
            @Override
            public void onResponse(Call<FeesResponseModel> call, Response<FeesResponseModel> response) {

                if (response.body() != null) {

                    FeesResponseModel model = response.body();
                    List<FeesResponseModel.Paid> paid = model.getPaid();
                    List<FeesResponseModel.Upcoming> upcomings = model.getUpcoming();
                    List<FeesResponseModel.Overdue> overdues = model.getOverdue();


                    tvUsername.setText(userName);
                    tvEmail.setText(userEmail);
                    if (paid.size() != 0) {
                        tvAmountPaid.setText("₹" + paid.get(0).getAmount());
                    }
                    if (upcomings.size() != 0) {

                        for (int i = 0; i < upcomings.size(); i++) {
                            upcomingList.add(model.new Upcoming(upcomings.get(i)));
                        }
                        feeAdapterUpcoming.notifyDataSetChanged();

                    } else {
                        tvUpcoming.setText(R.string.no_upcoming_fees);
                    }

                    if (overdues.size() != 0) {
                        for (int i = 0; i < overdues.size(); i++) {
                            overdueList.add(model.new Upcoming(overdues.get(i)));
                        }
                        feeAdapterOverdue.notifyDataSetChanged();

                    } else {
                        tvOverdue.setText(R.string.no_overdue_fees);
                    }

                } else {
                    Alerts.displayToast(getContext(), "Loading Failed");
                }
                progressVisibility(false);
            }

            @Override
            public void onFailure(Call<FeesResponseModel> call, Throwable t) {
                Log.e(TAG, "onFailure: ", t);
                progressVisibility(false);
                Alerts.displayToast(getContext(), "Loading Failed");
            }
        });

    }

    public void progressVisibility(boolean visibility) {

        if (getActivity() != null) {

            if (visibility) {
                llProgressBar.setVisibility(View.VISIBLE);
                getActivity().getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                        WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);

            } else {
                llProgressBar.setVisibility(View.GONE);
                getActivity().getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
            }
        }
    }
}
