package com.shimbi.schoolbook.home.photos.adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.android.material.card.MaterialCardView;
import com.google.gson.Gson;
import com.shimbi.schoolbook.R;
import com.shimbi.schoolbook.db.helper.MyDataBase;
import com.shimbi.schoolbook.home.photos.activities.ActivityPhotoView;
import com.shimbi.schoolbook.home.photos.fragments.FragmentAlbum;
import com.shimbi.schoolbook.home.photos.models.PhotosListResponseModel;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityOptionsCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;

public class PhotosAdapter extends RecyclerView.Adapter<PhotosAdapter.MyViewHolder> {

    private List<PhotosListResponseModel.Photo> photoList;
    private List<PhotosListResponseModel.Photo> albumList;
    private List<PhotosListResponseModel.Photo> list;
    private Context context;
    private Activity activity;

    public PhotosAdapter(Activity activity) {
        this.photoList = new ArrayList<>();
        this.albumList = new ArrayList<>();
        this.list = new ArrayList<>();
        this.activity = activity;
    }

    public void addAllAlbums(List<PhotosListResponseModel.Photo> albums) {
        albumList.clear();
        albumList.addAll(albums);
        list.clear();
        list.addAll(albumList);
        list.addAll(photoList);
        notifyDataSetChanged();
    }

    public void addAllPhotos(List<PhotosListResponseModel.Photo> photos) {
        photoList.clear();
        photoList.addAll(photos);
        list.clear();
        list.addAll(albumList);
        list.addAll(photoList);
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_photo, parent, false);
        context = parent.getContext();
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        PhotosListResponseModel.Photo model = list.get(position);
        Glide.with(context).load(model.getPhotoFile())
//                .centerCrop()
                .placeholder(R.drawable.placeholder)
                .into(holder.imageView);

        if (!model.isPhoto()) {
            holder.albumTitle.setVisibility(View.VISIBLE);
            holder.tvAlbumTitle.setText(model.getPhotoTitle());
            holder.tvPhotoCount.setText(MyDataBase.db.getPhotoCountInAlbum(Integer.valueOf(model.getAlbumId())) + " photos");

        } else {
            holder.albumTitle.setVisibility(View.GONE);
        }


        holder.imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (!model.isPhoto()) {

                    Fragment fragment = null;
                    try {


                        Bundle bundle = new Bundle();
                        bundle.putString("albumId", model.getAlbumId());
                        fragment = FragmentAlbum.class.newInstance();
                        fragment.setArguments(bundle);
                        ((FragmentActivity) context).getSupportFragmentManager().beginTransaction()
                                .replace(R.id.flContent, fragment, fragment.getTag())
                                .addToBackStack(fragment.getTag())
                                .commit();

                    } catch (Exception e) {
                        e.printStackTrace();
                    }


                } else {

                    ActivityOptionsCompat activityOptionsCompat = ActivityOptionsCompat.makeSceneTransitionAnimation(activity, holder.imageView, "imageMain");
                    Bundle bundle = new Bundle();
                    bundle.putString("photos", new Gson().toJson(model));
                    Intent intent = new Intent(view.getContext(), ActivityPhotoView.class);
                    intent.putExtras(bundle);
                    view.getContext().startActivity(intent, activityOptionsCompat.toBundle());
                }
            }
        });


    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.iv_image)
        ImageView imageView;
        @BindView(R.id.card)
        MaterialCardView card;
        @BindView(R.id.album_title)
        LinearLayout albumTitle;
        @BindView(R.id.tv_album_title)
        TextView tvAlbumTitle;
        @BindView(R.id.tv_photo_count)
        TextView tvPhotoCount;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
