package com.shimbi.schoolbook.home.activities;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import com.bumptech.glide.Glide;
import com.google.android.material.appbar.MaterialToolbar;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.navigation.NavigationView;
import com.shimbi.schoolbook.R;
import com.shimbi.schoolbook.db.helper.MyDataBase;
import com.shimbi.schoolbook.home.about_us.FragmentAboutUs;
import com.shimbi.schoolbook.home.assignments.activities.ActivityAssignmentDetails;
import com.shimbi.schoolbook.home.assignments.fragments.FragmentAssignments;
import com.shimbi.schoolbook.home.attendance.FragmentAttendance;
import com.shimbi.schoolbook.home.events.activities.ActivityEventDetails;
import com.shimbi.schoolbook.home.events.fragments.FragmentEvents;
import com.shimbi.schoolbook.home.fees.fragments.FragmentFees;
import com.shimbi.schoolbook.home.fragments.FragmentHome;
import com.shimbi.schoolbook.home.holidays.fragments.FragmentHolidays;
import com.shimbi.schoolbook.home.leaves.fragments.FragmentLeaves;
import com.shimbi.schoolbook.home.messages.activities.ActivityMessageDetails;
import com.shimbi.schoolbook.home.messages.fragments.FragmentMessages;
import com.shimbi.schoolbook.home.notice.activities.ActivityNoticeDetails;
import com.shimbi.schoolbook.home.notice.fragments.FragmentNotice;
import com.shimbi.schoolbook.home.photos.fragments.FragmentPhotos;
import com.shimbi.schoolbook.home.report_card.FragmentReportCard;
import com.shimbi.schoolbook.home.requests.fragments.FragmentRequests;
import com.shimbi.schoolbook.home.time_table.FragmentTimeTable;
import com.shimbi.schoolbook.home.transport.fragments.FragmentTransport;
import com.shimbi.schoolbook.login.activites.ActivityLogin;
import com.shimbi.schoolbook.utils.Alerts;
import com.shimbi.schoolbook.utils.AuthModel;
import com.shimbi.schoolbook.utils.SaveSharedPreference;

import java.util.Objects;

import de.hdodenhof.circleimageview.CircleImageView;

public class ActivityHome extends AppCompatActivity {

    private static final String TAG = "ActivityHome";
    public static String username;
    public static String userEmail;
    public static String userType;
    public static String userId;
    public static String leaveApproveFlag;
    public static String messagageAddFlag;
    public static String assignmentAddFlag;
    public static Integer eventAddFlag;
    public static Integer noticeAddFlag;
    public static String photoAddFlag;
    public static String attendanceAddFlag;
    public static String transportRights;
    public static FloatingActionButton fab;
    public MaterialToolbar materialToolbar;

    public Integer studentCount;


    private void init() {

        AuthModel authModel = SaveSharedPreference.getData();
        userId = authModel.uid;
        username = authModel.username;
        userEmail = authModel.userEmail;
        userType = authModel.userType;
        studentCount = authModel.studentCount;

        leaveApproveFlag = authModel.leaveApproveFlag;
        messagageAddFlag = authModel.messageAddFlag;
        assignmentAddFlag = authModel.assignmentAddFlag;
        eventAddFlag = authModel.eventAddFlag;
        noticeAddFlag = authModel.noticeAddFlag;
        photoAddFlag = authModel.photoAddFlag;
        attendanceAddFlag = authModel.attendanceAddFlag;
        transportRights = authModel.rightsTransport;


        Log.e(TAG, "init: " + userId + " " + userType + " " + studentCount);

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        fab = findViewById(R.id.fab);


        init();


        initNavigationDrawer();


        FragmentManager fragmentManager = getSupportFragmentManager();
        try {
            fragmentManager.beginTransaction().replace(R.id.flContent, FragmentHome.class.newInstance()).commit();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void handleNotification() {

        Integer eventId = -1;
        String module = getIntent().getStringExtra("module");
        if (module != null) {
            if (!module.equals("transport"))
                eventId = getIntent().getIntExtra("eventId", -1);

            Fragment fragment = null;
            Intent intent = null;

            Class fragmentClass = null;
            switch (module) {
                case "event":
                    intent = new Intent(this, ActivityEventDetails.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    intent.putExtra("eventId", eventId);
                    startActivity(intent);
                    break;

                case "notice":
                    intent = new Intent(this, ActivityNoticeDetails.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    intent.putExtra("eventId", eventId);
                    startActivity(intent);
                    break;

                case "assignment":
                    intent = new Intent(this, ActivityAssignmentDetails.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    intent.putExtra("eventId", eventId);
                    startActivity(intent);
                    break;

                case "message_communication":
                    intent = new Intent(this, ActivityMessageDetails.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    intent.putExtra("eventId", eventId);
                    startActivity(intent);
                    break;

                case "photo":
                    fragmentClass = FragmentPhotos.class;
                    break;

                case "leave":

                case "student_approve_leave":

                case "student_reject_leave":

                case "teacher_approve_leave":

                case "teacher_reject_leave":
                    fragmentClass = FragmentLeaves.class;
                    break;

                case "fee_structure":

                case "fee_link_generation":
                    fragmentClass = FragmentFees.class;
                    break;

                case "transport":
                    fragmentClass = FragmentTransport.class;
                    break;

                default:
                    fragmentClass = FragmentHome.class;


            }

            try {
                if (fragmentClass != null)
                    fragment = (Fragment) fragmentClass.newInstance();
            } catch (Exception e) {
                e.printStackTrace();
            }

            FragmentManager fragmentManager = getSupportFragmentManager();
            if (fragment != null)
                fragmentManager.beginTransaction().replace(R.id.flContent, fragment, fragment.getTag())
                        .addToBackStack(fragment.getTag())
                        .commit();
        }

    }


    private void initNavigationDrawer() {
        DrawerLayout drawerLayout = findViewById(R.id.drawerLayout);
        NavigationView navigationView = findViewById(R.id.navigation_view);
        materialToolbar = findViewById(R.id.appbar);
        setSupportActionBar(materialToolbar);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        navigationView.setCheckedItem(R.id.nav_home);

        View headerView = navigationView.getHeaderView(0);
        TextView navUsername = headerView.findViewById(R.id.nav_fullname);
        TextView navEmail = headerView.findViewById(R.id.nav_email);
        MaterialButton btChangePassword = headerView.findViewById(R.id.bt_change_pass);
        MaterialButton btProfile = headerView.findViewById(R.id.bt_profile);
        ImageView btLogout = headerView.findViewById(R.id.iv_logout);
        LinearLayout container = headerView.findViewById(R.id.container);
        CircleImageView profileImage = headerView.findViewById(R.id.profile_image);
        navUsername.setText(username);
        navEmail.setText(userEmail);


        Glide.with(ActivityHome.this).load(MyDataBase.db.getUserProfilePic(userId))
                .placeholder(MyDataBase.db.getUserGender(userId).equals("Male") ? R.drawable.default_profile : R.drawable.default_profile_girl)
                .into(profileImage);


        btChangePassword.setOnClickListener(view -> startActivity(new Intent(getApplicationContext(), ActivityChangePassword.class)));

        btProfile.setOnClickListener(view -> startActivity(new Intent(getApplicationContext(), ActivityProfile.class)));
        btLogout.setOnClickListener(view -> new MaterialAlertDialogBuilder(ActivityHome.this, R.style.DialogStyle)
                .setTitle("Do you want to logout?")
                .setPositiveButton("Yes", (dialogInterface, i) -> {
                    MyDataBase.obj.clearAllTables();
                    SaveSharedPreference.saveData(null);
                    SaveSharedPreference.clearAll();
                    Intent intent = new Intent(getApplicationContext(), ActivityLogin.class)
                            .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);

                })
                .setNegativeButton("No", null)
                .setCancelable(false)
                .show());
        container.setOnClickListener(view -> startActivity(new Intent(getApplicationContext(), ActivityProfile.class)));
        profileImage.setOnClickListener(view -> startActivity(new Intent(getApplicationContext(), ActivityProfile.class)));

        //hide nav items for different users
        Menu menu = navigationView.getMenu();
        if (!userType.equals("3")) {
            menu.findItem(R.id.nav_requests).setVisible(false);
            menu.findItem(R.id.nav_report_card).setVisible(false);
            menu.findItem(R.id.nav_fees).setVisible(false);
            // menu.findItem(R.id.menu_change_user).setVisible(false);
        }

        navigationView.setNavigationItemSelectedListener(item -> {

            int id = item.getItemId();
            Fragment fragment = null;
            Class fragmentClass;
            switch (id) {
                case R.id.nav_notice:
                    fragmentClass = FragmentNotice.class;
                    break;
                case R.id.nav_assignments:
                    fragmentClass = FragmentAssignments.class;
                    break;
                case R.id.nav_events:
                    fragmentClass = FragmentEvents.class;
                    break;
                case R.id.nav_messages:
                    fragmentClass = FragmentMessages.class;
                    break;
                case R.id.nav_leaves:
                    fragmentClass = FragmentLeaves.class;
                    break;
                case R.id.nav_holidays:
                    fragmentClass = FragmentHolidays.class;
                    break;
                case R.id.nav_time_table:
                    fragmentClass = FragmentTimeTable.class;
                    break;
                case R.id.nav_photos:
                    fragmentClass = FragmentPhotos.class;
                    break;
                case R.id.nav_transport:
                    fragmentClass = FragmentTransport.class;
                    break;
                case R.id.nav_attendance:
                    fragmentClass = FragmentAttendance.class;
                    break;
                case R.id.nav_report_card:
                    fragmentClass = FragmentReportCard.class;
                    break;
                case R.id.nav_about_us:
                    fragmentClass = FragmentAboutUs.class;
                    break;
                case R.id.nav_fees:
                    fragmentClass = FragmentFees.class;
                    break;
                case R.id.nav_requests:
                    fragmentClass = FragmentRequests.class;
                    break;
                case R.id.nav_home:
                default:
                    fragmentClass = FragmentHome.class;
            }

            try {
                fragment = (Fragment) fragmentClass.newInstance();
            } catch (Exception e) {
                e.printStackTrace();
            }

            navigationView.setCheckedItem(id);
            FragmentManager fragmentManager = getSupportFragmentManager();
            if (fragment != null) {
                fragmentManager.beginTransaction().replace(R.id.flContent, fragment, fragment.getTag())
                        .addToBackStack(fragment.getTag())
                        .commit();
            }

            drawerLayout.closeDrawers();
            return true;
        });


        ActionBarDrawerToggle actionBarDrawerToggle = new ActionBarDrawerToggle(this, drawerLayout,
                materialToolbar, R.string.navigation_open, R.string.navigation_close) {

            @Override
            public void onDrawerClosed(View v) {
                super.onDrawerClosed(v);
            }

            @Override
            public void onDrawerOpened(View v) {
                super.onDrawerOpened(v);
            }
        };
        drawerLayout.addDrawerListener(actionBarDrawerToggle);
        actionBarDrawerToggle.syncState();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_home, menu);
        menu.findItem(R.id.menu_status).setVisible(false);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle presses on the action bar items
        int id = item.getItemId();
        if (id == R.id.menu_notification) {
            startActivity(new Intent(this, ActivityNotificationCenter.class));
        }


        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 200) {
            if (resultCode != RESULT_OK) {
                Alerts.displayToast(getApplicationContext(), "Turn On Location!");
                finish();
            }

        }
    }

    public void setTitleAndLogo(String title, int logo) {
        Objects.requireNonNull(getSupportActionBar()).setTitle(title);
        getSupportActionBar().setLogo(logo);

    }

    public void setTitle(String title) {
        Objects.requireNonNull(getSupportActionBar()).setTitle(title);
        getSupportActionBar().setLogo(null);

    }


    @Override
    protected void onStart() {
        super.onStart();
        handleNotification();
    }
}
