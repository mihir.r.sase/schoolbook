package com.shimbi.schoolbook.home.activities;

import android.app.Dialog;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.bumptech.glide.Glide;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.shimbi.schoolbook.R;
import com.shimbi.schoolbook.db.helper.MyDataBase;
import com.shimbi.schoolbook.db.tables.DbUser;
import com.shimbi.schoolbook.home.models.GetProfileResponseModel;
import com.shimbi.schoolbook.login.activites.ActivitySync;
import com.shimbi.schoolbook.login.models.StudentCountModel;
import com.shimbi.schoolbook.utils.Alerts;
import com.shimbi.schoolbook.utils.AuthModel;
import com.shimbi.schoolbook.utils.NetworkStatusChecker;
import com.shimbi.schoolbook.utils.RetrofitClient;
import com.shimbi.schoolbook.utils.SaveSharedPreference;

import org.jetbrains.annotations.NotNull;

import java.lang.reflect.Type;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ActivityProfile extends AppCompatActivity {

    private static final String TAG = "ActivityProfile";
    @BindView(R.id.profile_image)
    CircleImageView profileImage;
    @BindView(R.id.fab_edit_profile)
    FloatingActionButton fabEdit;
    @BindView(R.id.back)
    ImageView back;
    @BindView(R.id.tv_username)
    TextView tvUsername;
    @BindView(R.id.tv_email)
    TextView tvEmail;
    @BindView(R.id.tv_class)
    TextView tvClass;
    @BindView(R.id.tv_dob)
    TextView tvDob;
    @BindView(R.id.tv_mobile)
    TextView tvMobile;
    @BindView(R.id.tv_address)
    TextView tvAddress;
    @BindView(R.id.llProgressBar)
    View llProgressBar;
    @BindView(R.id.iv_change_user)
    ImageView ivChangeUser;
    AuthModel authModel;
    private String username;
    private String userEmail;
    private String userType;
    private String userId;
    private String userClass;
    private String userDob;
    private String userMobile;
    private String userAddress;
    private String userGender;
    private Integer studentCount;
    private String userImage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        ButterKnife.bind(this);
        init();
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_NOSENSOR);

        if (NetworkStatusChecker.isNetworkConnected(getApplicationContext())) {
            loadDataFromLocal();
        } else {
            loadDataFromServer();
        }


        fabEdit.setOnClickListener(view -> {

            Intent intent = new Intent(ActivityProfile.this, ActivityEditProfile.class);
            Bundle bundle = new Bundle();
            bundle.putString("username", username);
            bundle.putString("userEmail", userEmail);
            bundle.putString("userClass", userClass);
            bundle.putString("userDob", userDob);
            bundle.putString("userMobile", userMobile);
            bundle.putString("userAddress", userAddress);
            bundle.putString("userImage", userImage);
            bundle.putString("userGender", userGender);
            intent.putExtras(bundle);
            startActivity(intent);
        });

        back.setOnClickListener(view -> finish());

        ivChangeUser.setOnClickListener(view -> changeUser());


    }


    private void init() {
        authModel = SaveSharedPreference.getData();
        userId = authModel.uid;
        userType = authModel.userType;
        studentCount = authModel.studentCount;


        if (userType.equals("3") && studentCount > 1) {

            ivChangeUser.setVisibility(View.VISIBLE);
        } else {
            ivChangeUser.setVisibility(View.GONE);
        }
    }


    private void loadDataFromServer() {
        progressVisibility(true);

        Call<GetProfileResponseModel> apicall = RetrofitClient.get().getUserProfile(userId, userType);
        apicall.enqueue(new Callback<GetProfileResponseModel>() {
            @Override
            public void onResponse(@NotNull Call<GetProfileResponseModel> call, @NotNull Response<GetProfileResponseModel> response) {
                if (response.body() != null) {
                    try {
                        GetProfileResponseModel model = response.body();
                        username = model.getName();
                        userEmail = model.getEmail();
                        userClass = model.get_class();
                        userMobile = model.getMobile();
                        userAddress = model.getAddress();
                        userImage = model.getProfileImg();
                        userGender = model.getGender();

                        Date date = new SimpleDateFormat("yyyy-mm-dd").parse(model.getDob());
                        if (date != null) {
                            userDob = new SimpleDateFormat("dd MMM, yyyy").format(date);
                        }

                        Glide.with(ActivityProfile.this).load(userImage)
                                .placeholder(userGender.equals("Male") ? R.drawable.default_profile : R.drawable.default_profile_girl)
                                .into(profileImage);
                        tvUsername.setText(username);
                        tvEmail.setText(userEmail);
                        tvClass.setText(userClass);
                        tvMobile.setText(userMobile);
                        tvAddress.setText(userAddress);
                        tvDob.setText(userDob);

                    } catch (Exception e) {
                        Log.e(TAG, "onResponse: ", e);
                    }
                    progressVisibility(false);
                    try {
                        DbUser dbUser = MyDataBase.db.getUserData(userId);
                        Log.e(TAG, "onResponse: " + dbUser);
                        if (dbUser == null) {
                            MyDataBase.db.insertToUser(new DbUser(userId, userType, username, userEmail, userClass, userDob, userMobile, userAddress, userImage, userGender));
                        }
                    } catch (Exception e) {
                        Log.e(TAG, "onResponse: ", e);
                    }

                } else {
                    Alerts.displayToast(getApplicationContext(), "Loading Failed!");
                    progressVisibility(false);
                }
            }

            @Override
            public void onFailure(@NotNull Call<GetProfileResponseModel> call, @NotNull Throwable t) {
                Log.e(TAG, "onFailure: ", t);
                Alerts.displayToast(getApplicationContext(), "Loading Failed!");
                progressVisibility(false);
            }
        });

    }

    private void loadDataFromLocal() {

        try {
            DbUser model = MyDataBase.db.getUserData(userId);
            username = model.getUsername();
            userEmail = model.getUserEmail();
            userClass = model.getUserClass();
            userDob = model.getUserDob();
            userMobile = model.getUserMobile();
            userAddress = model.getUseraddress();
            userImage = model.getUserImage();
            userGender = model.getGender();
            Glide.with(ActivityProfile.this).load(userImage)
                    .placeholder(userGender.equals("Male") ? R.drawable.default_profile : R.drawable.default_profile_girl)
                    .into(profileImage);
            tvUsername.setText(username);
            tvEmail.setText(userEmail);
            tvClass.setText(userClass);
            tvDob.setText(userDob);
            tvMobile.setText(userMobile);
            tvAddress.setText(userAddress);
        } catch (Exception e) {
            Log.e(TAG, "loadDataFromLocal: ", e);
        }

    }


    private void changeUser() {

        try {
            String studentData = SaveSharedPreference.getMultipleStudentDetails();
            Type listType = new TypeToken<ArrayList<StudentCountModel>>() {
            }.getType();
            List<StudentCountModel> studentList = new Gson().fromJson(studentData, listType);


            showStudentSelectionDialog(studentList);

        } catch (Exception e) {
            Log.e(TAG, "changeUser: ", e);
        }


    }

    private void showStudentSelectionDialog(final List<StudentCountModel> studentCountModel) {
        final Dialog dialog = new Dialog(ActivityProfile.this, R.style.DialogStyle);
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.dialog_select_single);

        final String[] studentNameList = new String[studentCountModel.size()];
        for (int i = 0; i < studentCountModel.size(); i++) {
            studentNameList[i] = studentCountModel.get(i).getUserName();
        }

        ListView listView = dialog.findViewById(R.id.listview);
        ArrayAdapter arrayAdapter = new ArrayAdapter(ActivityProfile.this, R.layout.item_select_student, R.id.textview_name, studentNameList);
        listView.setAdapter(arrayAdapter);

        listView.setOnItemClickListener((adapterView, view, i, l) -> {
            Log.e(TAG, "onItemClick: " + studentCountModel.get(i).getUserName());


            MyDataBase.obj.clearAllTables();
            SaveSharedPreference.saveData(null);
            SaveSharedPreference.clearAll();

            String json = new Gson().toJson(studentCountModel);
            SaveSharedPreference.setMultipleStudentDetails(json);

            AuthModel newAuthModel = new AuthModel(studentCountModel.get(i), authModel);

            SaveSharedPreference.saveData(newAuthModel);


            dialog.dismiss();
            Intent intent = new Intent(ActivityProfile.this, ActivitySync.class)
                    .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);

        });
        dialog.show();
    }


    /**
     * Display progress bar and uses TOUCHABLE flags
     *
     * @param visibility Give True to display dialog, False to remove
     */
    private void progressVisibility(boolean visibility) {
        if (visibility) {
            llProgressBar.setVisibility(View.VISIBLE);
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                    WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
        } else {
            llProgressBar.setVisibility(View.GONE);
            getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
        }
    }
}
