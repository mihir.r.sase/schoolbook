package com.shimbi.schoolbook.home.assignments.adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.shimbi.schoolbook.R;
import com.shimbi.schoolbook.home.assignments.activities.ActivityAssignmentDetails;
import com.shimbi.schoolbook.home.assignments.models.AssignmentResponseModel;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;


public class AssignmentAdapter extends RecyclerView.Adapter<AssignmentAdapter.MyViewHolder> {

    private static final String TAG = "AssignmentAdapter";
    private List<AssignmentResponseModel.AssignmentResultModel> assignmentList;
    Context context;

    public AssignmentAdapter(List<AssignmentResponseModel.AssignmentResultModel> assignmentList) {
        this.assignmentList = assignmentList;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_assignment_list, parent, false);
        context = parent.getContext();
        return new AssignmentAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        AssignmentResponseModel.AssignmentResultModel assignmentDetails = assignmentList.get(position);
        holder.tvUsername.setText(assignmentDetails.getUsername());
        holder.tvDate.setText(assignmentDetails.getAddedDate());
        holder.tvTitle.setText(assignmentDetails.getEventTitle());
        holder.tvDesc.setText(assignmentDetails.getDescription());
        holder.submissionDate.setText(assignmentDetails.getEventDate());


        Glide.with(context).load(assignmentDetails.getUserimage()).centerCrop()
                .placeholder(R.drawable.default_profile)
                .into(holder.profileImage);


        holder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(view.getContext(), ActivityAssignmentDetails.class);
                intent.putExtra("eventId", assignmentDetails.getEventId());
                view.getContext().startActivity(intent);
                // Alerts.displayToast(view.getContext(), assignmentDetails.getEventId());
            }
        });
    }

    @Override
    public int getItemCount() {
        return assignmentList.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tv_username)
        TextView tvUsername;
        @BindView(R.id.tv_date)
        TextView tvDate;
        @BindView(R.id.tv_title)
        TextView tvTitle;
        @BindView(R.id.tv_desc)
        TextView tvDesc;
        @BindView(R.id.notice_card)
        CardView cardView;
        @BindView(R.id.submissionDate)
        TextView submissionDate;
        @BindView(R.id.profile_image)
        CircleImageView profileImage;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
