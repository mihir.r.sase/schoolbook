package com.shimbi.schoolbook.home.leaves.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.shimbi.schoolbook.utils.calendar.CalendarEvent;

import java.util.List;

public class LeaveListResponseModel {

    @SerializedName("syncdate")
    @Expose
    private String syncdate;
    @SerializedName("leave")
    @Expose
    private List<Leave> leave = null;
    @SerializedName("holiday")
    @Expose
    private List<Holiday> holiday = null;

    public String getSyncdate() {
        return syncdate;
    }

    public void setSyncdate(String syncdate) {
        this.syncdate = syncdate;
    }

    public List<Leave> getLeave() {
        return leave;
    }

    public void setLeave(List<Leave> leave) {
        this.leave = leave;
    }

    public List<Holiday> getHoliday() {
        return holiday;
    }

    public void setHoliday(List<Holiday> holiday) {
        this.holiday = holiday;
    }

    public class Leave extends CalendarEvent {
        @SerializedName("leave_flag")
        @Expose
        private Integer leaveFlag;
        @SerializedName("leave_id")
        @Expose
        private String leaveId;
        @SerializedName("leave_reason")
        @Expose
        private String leaveReason;
        @SerializedName("reason")
        @Expose
        private String reason;
        @SerializedName("leave_status")
        @Expose
        private String leaveStatus;
        @SerializedName("added_date")
        @Expose
        private String addedDate;
        @SerializedName("leave_start_date")
        @Expose
        private String leaveStartDate;
        @SerializedName("leave_end_date")
        @Expose
        private String leaveEndDate;

        String holidayName;
        String holidayFromDate;
        String holidayToDate;

        public Leave(long startTimeInMillis, int indicatorColor, Integer leaveFlag, String leaveId, String leaveReason, String reason, String leaveStatus, String addedDate, String leaveStartDate, String leaveEndDate) {
            super(startTimeInMillis, indicatorColor);
            this.leaveFlag = leaveFlag;
            this.leaveId = leaveId;
            this.leaveReason = leaveReason;
            this.reason = reason;
            this.leaveStatus = leaveStatus;
            this.addedDate = addedDate;
            this.leaveStartDate = leaveStartDate;
            this.leaveEndDate = leaveEndDate;
        }

        public Leave(long startTimeInMillis, int indicatorColor, String leaveStatus, String holidayName, String holidayFromDate, String holidayToDate) {
            super(startTimeInMillis, indicatorColor);
            this.leaveStatus = leaveStatus;
            this.holidayName = holidayName;
            this.holidayFromDate = holidayFromDate;
            this.holidayToDate = holidayToDate;

        }

        public String getHolidayName() {
            return holidayName;
        }

        public void setHolidayName(String holidayName) {
            this.holidayName = holidayName;
        }


        public String getHolidayFromDate() {
            return holidayFromDate;
        }

        public void setHolidayFromDate(String holidayFromDate) {
            this.holidayFromDate = holidayFromDate;
        }

        public String getHolidayToDate() {
            return holidayToDate;
        }

        public void setHolidayToDate(String holidayToDate) {
            this.holidayToDate = holidayToDate;
        }

        public Integer getLeaveFlag() {
            return leaveFlag;
        }

        public void setLeaveFlag(Integer leaveFlag) {
            this.leaveFlag = leaveFlag;
        }

        public String getLeaveId() {
            return leaveId;
        }

        public void setLeaveId(String leaveId) {
            this.leaveId = leaveId;
        }

        public String getLeaveReason() {
            return leaveReason;
        }

        public void setLeaveReason(String leaveReason) {
            this.leaveReason = leaveReason;
        }

        public String getReason() {
            return reason;
        }

        public void setReason(String reason) {
            this.reason = reason;
        }

        public String getLeaveStatus() {
            return leaveStatus;
        }

        public void setLeaveStatus(String leaveStatus) {
            this.leaveStatus = leaveStatus;
        }

        public String getAddedDate() {
            return addedDate;
        }

        public void setAddedDate(String addedDate) {
            this.addedDate = addedDate;
        }

        public String getLeaveStartDate() {
            return leaveStartDate;
        }

        public void setLeaveStartDate(String leaveStartDate) {
            this.leaveStartDate = leaveStartDate;
        }

        public String getLeaveEndDate() {
            return leaveEndDate;
        }

        public void setLeaveEndDate(String leaveEndDate) {
            this.leaveEndDate = leaveEndDate;
        }
    }

    public class Holiday extends CalendarEvent {
        @SerializedName("holiday_id")
        @Expose
        private String holidayId;
        @SerializedName("name")
        @Expose
        private String name;
        @SerializedName("from_date")
        @Expose
        private String fromDate;
        @SerializedName("to_date")
        @Expose
        private String toDate;
        @SerializedName("status")
        @Expose
        private String status;


        public Holiday(long startTimeInMillis, int indicatorColor, String holidayId, String name, String fromDate, String toDate, String status) {
            super(startTimeInMillis, indicatorColor);
            this.holidayId = holidayId;
            this.name = name;
            this.fromDate = fromDate;
            this.toDate = toDate;
            this.status = status;
        }

        public String getHolidayId() {
            return holidayId;
        }

        public void setHolidayId(String holidayId) {
            this.holidayId = holidayId;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getFromDate() {
            return fromDate;
        }

        public void setFromDate(String fromDate) {
            this.fromDate = fromDate;
        }

        public String getToDate() {
            return toDate;
        }

        public void setToDate(String toDate) {
            this.toDate = toDate;
        }
    }
}
