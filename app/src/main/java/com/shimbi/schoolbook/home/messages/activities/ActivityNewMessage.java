package com.shimbi.schoolbook.home.messages.activities;

import android.Manifest;
import android.app.Dialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.appbar.MaterialToolbar;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import com.google.android.material.textfield.TextInputEditText;
import com.shimbi.schoolbook.R;
import com.shimbi.schoolbook.home.adapters.AttachmentAdapter;
import com.shimbi.schoolbook.home.messages.adapters.SelectMultipleAdapter;
import com.shimbi.schoolbook.home.messages.models.MessageDropDownModel;
import com.shimbi.schoolbook.home.models.AttachmentModel;
import com.shimbi.schoolbook.home.models.SuccessResponseModel;
import com.shimbi.schoolbook.utils.Alerts;
import com.shimbi.schoolbook.utils.AuthModel;
import com.shimbi.schoolbook.utils.FileUtils;
import com.shimbi.schoolbook.utils.RetrofitClient;
import com.shimbi.schoolbook.utils.SaveSharedPreference;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ActivityNewMessage extends AppCompatActivity {


    private static final String TAG = "ActivityNewMessage";
    public static MaterialButton btSelectStudent;
    public static TextView tvSelectedStudent;
    public static String username;
    public static String userEmail;
    public static String userType;
    public static String userId;
    public static Dialog dialog;
    public String eventId;
    public Integer studentCount;
    @BindView(R.id.linearlayout)
    LinearLayout linearLayout;
    @BindView(R.id.selectClass)
    AutoCompleteTextView tvSelectClass;
    @BindView(R.id.selectUser)
    AutoCompleteTextView tvSelectUser;
    @BindView(R.id.et_title)
    TextInputEditText etTitle;
    @BindView(R.id.et_details)
    TextInputEditText etDetails;
    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;
    @BindView(R.id.bt_save)
    MaterialButton btSave;
    @BindView(R.id.llProgressBar)
    View llProgressBar;
    private List<MessageDropDownModel.UserClass> classList = new ArrayList<>();
    private List<MessageDropDownModel.User> userList = new ArrayList<>();
    private List<MessageDropDownModel.Student> studentList = new ArrayList<>();
    private Map<String, String> classListMap = new HashMap<>();
    private Map<String, String> userListMap = new HashMap<>();
    private Map<String, String> studentListMap = new HashMap<>();
    private String filePath;
    private String fileName;
    private List<AttachmentModel> attachmentModelList = new ArrayList<>();
    private AttachmentAdapter attachmentAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_message);
        ButterKnife.bind(this);

        btSelectStudent = findViewById(R.id.bt_select_student);
        tvSelectedStudent = findViewById(R.id.student_name);
        init();
        getDropDownDetails();

        tvSelectClass.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                String _class = tvSelectClass.getText().toString();
                if (classListMap.get(_class) != null) {
                    loadStudents(Integer.valueOf(classListMap.get(_class)));
                    btSelectStudent.setEnabled(true);
                } else {
                    btSelectStudent.setEnabled(false);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        btSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String _class = tvSelectClass.getText().toString();
                String user = tvSelectUser.getText().toString();
                String title = etTitle.getText().toString();
                String details = etDetails.getText().toString();
                String student = tvSelectedStudent.getText().toString();

                if (!validate(_class, user, title, details, student)) {
                    return;
                }

                attachmentModelList = attachmentAdapter.getAll();
                sendNewMessage(_class, user, title, details, student, attachmentModelList);
            }
        });

        btSelectStudent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showDialog();
            }
        });
    }


    private void init() {

        try {
            Intent intent = getIntent();
            eventId = intent.getExtras().getString("eventId");
            etTitle.setText(intent.getExtras().getString("title"));
            etDetails.setText(intent.getExtras().getString("details"));
        } catch (NullPointerException n) {
            Log.e(TAG, "init: " + n);
        }

        AuthModel authModel = SaveSharedPreference.getData();
        userId = authModel.uid;
        username = authModel.username;
        userEmail = authModel.userEmail;
        userType = authModel.userType;
        studentCount = authModel.studentCount;

        MaterialToolbar materialToolbar = findViewById(R.id.appbar);
        setSupportActionBar(materialToolbar);
        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        if (userType.equals("3")) {
            linearLayout.setVisibility(View.GONE);
        }
        if (eventId != null) {
            getSupportActionBar().setTitle("Edit Message");
        }

        attachmentAdapter = new AttachmentAdapter();
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(attachmentAdapter);

        attachmentAdapter.setUploadStatusChangeListner(isUploading -> {
            if (isUploading) {
                btSave.setEnabled(false);
            } else {
                btSave.setEnabled(true);
            }
        });

    }

    private void getDropDownDetails() {

        progressVisibility(true);

        Call<MessageDropDownModel> apicall = RetrofitClient.get().getMessageDropDownDetails(userId, userType, 1, 0, 0, 1, 0, null);
        apicall.enqueue(new Callback<MessageDropDownModel>() {
            @Override
            public void onResponse(Call<MessageDropDownModel> call, Response<MessageDropDownModel> response) {
                try {
                    if (response.body() != null) {
                        MessageDropDownModel model = response.body();
                        classList = model.getUserclass();
                        userList = model.getUser();
                        String[] classes = new String[classList.size()];
                        String[] users = new String[userList.size()];
                        for (int i = 0; i < classList.size(); i++) {
                            classes[i] = classList.get(i).getValue();
                            classListMap.put(classList.get(i).getValue(), classList.get(i).getKey());
                        }

                        for (int i = 0; i < userList.size(); i++) {
                            users[i] = userList.get(i).getValue();
                            userListMap.put(userList.get(i).getValue(), userList.get(i).getKey());
                        }
                        ArrayAdapter<String> adapter1 = new ArrayAdapter<String>
                                (ActivityNewMessage.this, android.R.layout.select_dialog_item, classes);
                        tvSelectClass.setThreshold(1);
                        tvSelectClass.setAdapter(adapter1);

                        ArrayAdapter<String> adapter3 = new ArrayAdapter<String>
                                (ActivityNewMessage.this, android.R.layout.select_dialog_item, users);
                        tvSelectUser.setThreshold(1);
                        tvSelectUser.setAdapter(adapter3);

                        progressVisibility(false);

                    } else {
                        Alerts.displayToast(getApplicationContext(), "Loading Failed");
                        progressVisibility(false);
                    }
                } catch (Exception e) {
                    Log.e(TAG, "onResponse: ", e);
                    Alerts.displayToast(getApplicationContext(), "Loading Failed");
                    progressVisibility(false);
                }
            }

            @Override
            public void onFailure(Call<MessageDropDownModel> call, Throwable t) {
                Log.e(TAG, "onFailure: ", t);
                Alerts.displayToast(getApplicationContext(), "Loading Failed");
                progressVisibility(false);
            }
        });

    }

    private void loadStudents(Integer classId) {
        studentList.clear();
        progressVisibility(true);
        Call<MessageDropDownModel> apicall = RetrofitClient.get().getMessageDropDownDetails(userId, userType,
                0, 0, 0, 0, 1, classId);

        apicall.enqueue(new Callback<MessageDropDownModel>() {
            @Override
            public void onResponse(Call<MessageDropDownModel> call, Response<MessageDropDownModel> response) {

                if (response.body() != null) {
                    MessageDropDownModel model = response.body();
                    studentList = model.getStudent();
                    String[] students = new String[studentList.size()];
                    for (int i = 0; i < studentList.size(); i++) {
                        students[i] = studentList.get(i).getValue();
                        studentListMap.put(studentList.get(i).getValue(), studentList.get(i).getKey());
                    }
                    progressVisibility(false);

                } else {
                    Alerts.displayToast(getApplicationContext(), "Loading Failed");
                    progressVisibility(false);
                }
            }

            @Override
            public void onFailure(Call<MessageDropDownModel> call, Throwable t) {
                Log.e(TAG, "onFailure: ", t);
                Alerts.displayToast(getApplicationContext(), "Loading Failed");
                progressVisibility(false);
            }
        });
    }

    private void showDialog() {

        dialog = new Dialog(ActivityNewMessage.this, R.style.DialogStyle);
        dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        dialog.setContentView(R.layout.dialog_select_multiple);
        dialog.setCanceledOnTouchOutside(true);
        dialog.setCancelable(true);
        dialog.show();

        RecyclerView recyclerView = dialog.findViewById(R.id.recycler_view);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext()));

        SelectMultipleAdapter adapter = new SelectMultipleAdapter(studentList);
        recyclerView.setAdapter(adapter);
        adapter.notifyDataSetChanged();


    }


    private boolean validate(String _class, String user, String title, String details, String student) {

        tvSelectClass.setError(null);
        tvSelectUser.setError(null);

        if (!userType.equals("3")) {
            if (!_class.isEmpty() && !user.isEmpty()) {
                new MaterialAlertDialogBuilder(ActivityNewMessage.this, R.style.DialogStyle)
                        .setTitle("Please select either a Class or an User.")
                        .setPositiveButton("Okay", null)
                        .setCancelable(false)
                        .show();
                return false;
            }
            if (_class.isEmpty() && user.isEmpty()) {
                tvSelectClass.setError("Please Enter Class");
                tvSelectUser.setError("Or Please Enter User");
                return false;
            }

            if (classListMap.get(_class) == null && user.isEmpty()) {
                tvSelectClass.setError("Please Select A Valid Class");
                return false;
            }

            if (userListMap.get(user) == null && _class.isEmpty()) {
                tvSelectUser.setError("Please Select A Valid User");
                return false;
            }

            if (!_class.isEmpty() && student.isEmpty()) {
                new MaterialAlertDialogBuilder(ActivityNewMessage.this, R.style.DialogStyle)
                        .setTitle("Please select a student")
                        .setPositiveButton("Okay", null)
                        .setCancelable(false)
                        .show();
                return false;
            }


        } else {
            if (user.isEmpty()) {
                tvSelectUser.setError("Please Enter User");
                return false;
            }
            if (userListMap.get(user) == null) {
                tvSelectUser.setError("Please Select A Valid User");
                return false;
            }
        }
        if (title.isEmpty()) {
            etTitle.setError("Please Add Title");
            return false;
        }
        if (details.isEmpty()) {
            etDetails.setError("Please Add Assignment Details");
            return false;
        }
        return true;
    }

    private void sendNewMessage(String _class, String user, String title, String details, String student, List<AttachmentModel> list) {

        progressVisibility(true);


        try {

            String uploadedFileName = null;
            String originalFileName = null;
            if (!list.isEmpty()) {
                originalFileName = list.get(0).getFilename();
                uploadedFileName = list.get(0).getFinalFileName();
            }

            Integer classId = null;
            Integer receiverUserId = null;
            Integer studentId = null;
            if (classListMap.get(_class) != null) {

                classId = Integer.parseInt(classListMap.get(_class));
            }
            if (studentListMap.get(student) != null) {

                studentId = Integer.parseInt(studentListMap.get(student));
            }
            if (userListMap.get(user) != null) {

                receiverUserId = Integer.parseInt(userListMap.get(user));
            }


            Call<SuccessResponseModel> apicall = RetrofitClient.get().addMessage(userId, userType, title, details, classId, studentId, receiverUserId, eventId, uploadedFileName, originalFileName);

            apicall.enqueue(new Callback<SuccessResponseModel>() {
                @Override
                public void onResponse(Call<SuccessResponseModel> call, Response<SuccessResponseModel> response) {

                    if (response.body() != null) {

                        SuccessResponseModel model = response.body();
                        if (model.getSuccess().equals("1")) {
                            Alerts.displayToast(getApplicationContext(), "Success!");
                            finish();
                        } else {
                            Alerts.displayToast(getApplicationContext(), "Addition/Updating Failed!");
                        }

                    } else {
                        Alerts.displayToast(getApplicationContext(), "Addition/Updating Failed!");
                    }
                    progressVisibility(false);

                }

                @Override
                public void onFailure(Call<SuccessResponseModel> call, Throwable t) {
                    Log.e(TAG, "onFailure: ", t);
                    Alerts.displayToast(getApplicationContext(), "Addition/Updating Failed!");
                    progressVisibility(false);
                }
            });
        } catch (Exception e) {
            Log.e(TAG, "sendNewMessage: ", e);
            progressVisibility(false);
        }
    }


    private void attachFile() {


        if (attachmentAdapter.getItemCount() == 1) {
            Alerts.displayToast(getApplicationContext(), "Only one image is allowed");
            return;
        }
        Intent mRequestFileIntent = new Intent(Intent.ACTION_GET_CONTENT);
        mRequestFileIntent.setType("image/*");
        startActivityForResult(Intent.createChooser(mRequestFileIntent, "Choose File to Upload.."), 0);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {


            try {
                if (data != null) {

                    Log.e(TAG, "onActivityResult: 1");
                    Uri returnUri = data.getData();
                    processAttachmentUri(returnUri);
                }
            } catch (Exception e) {
                Log.e(TAG, "onActivityResult: ", e);
            }


        }
    }

    public void processAttachmentUri(Uri returnUri) {

        //filePath = PathUtil.getPath(getApplicationContext(), returnUri);
        filePath = FileUtils.getPath(getApplicationContext(), returnUri);
        fileName = FileUtils.getFileNameFromUri(getApplicationContext(), returnUri);
        if (filePath != null) {
            Log.e(TAG, "onActivityResult: " + fileName);
            Log.e(TAG, "onActivityResult: " + filePath);
            Log.e(TAG, "onActivityResult: " + FileUtils.getStringFile(filePath));
            attachmentAdapter.add(fileName, filePath, "message_communication");
        }

    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == 100) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                //attachFile();
            } else {
                new MaterialAlertDialogBuilder(ActivityNewMessage.this, R.style.DialogStyle)
                        .setTitle("Storage Access Permission Not Granted")
                        .setMessage("Please Grant Storage Permission To attach files.")
                        .setPositiveButton("Okay", null)
                        .setCancelable(false)
                        .show();
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.attach_file, menu);
        menu.findItem(R.id.menu_delete).setVisible(false);
        menu.findItem(R.id.menu_edit).setVisible(false);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle presses on the action bar items
        int id = item.getItemId();

        if (id == R.id.menu_attachment) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if (checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                    requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 100);
                } else {
                    attachFile();
                }
            }

        }

        if (id == android.R.id.home) {
            finish();
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * Display progress bar and uses TOUCHABLE flags
     *
     * @param visibility Give True to display dialog, False to remove
     */
    public void progressVisibility(boolean visibility) {
        if (visibility) {
            llProgressBar.setVisibility(View.VISIBLE);
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                    WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
        } else {
            llProgressBar.setVisibility(View.GONE);
            getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
        }
    }
}
