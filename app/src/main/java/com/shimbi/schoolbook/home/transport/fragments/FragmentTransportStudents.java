package com.shimbi.schoolbook.home.transport.fragments;


import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.shimbi.schoolbook.R;
import com.shimbi.schoolbook.home.activities.ActivityHome;
import com.shimbi.schoolbook.home.transport.models.StudentJourneyStatusModel;
import com.shimbi.schoolbook.utils.Alerts;
import com.shimbi.schoolbook.utils.AuthModel;
import com.shimbi.schoolbook.utils.RetrofitClient;
import com.shimbi.schoolbook.utils.SaveSharedPreference;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class FragmentTransportStudents extends Fragment {

    private static final String TAG = "FragmentTransportStuden";

    @BindView(R.id.tv_title1)
    TextView tvTitle1;
    @BindView(R.id.tv_title2)
    TextView tvTitle2;
    @BindView(R.id.tv_title3)
    TextView tvTitle3;
    @BindView(R.id.tv_title4)
    TextView tvTitle4;
    @BindView(R.id.tv_desc1)
    TextView tvDesc1;
    @BindView(R.id.tv_desc2)
    TextView tvDesc2;
    @BindView(R.id.tv_desc3)
    TextView tvDesc3;
    @BindView(R.id.tv_desc4)
    TextView tvDesc4;
    @BindView(R.id.tv_time1)
    TextView tvTime1;
    @BindView(R.id.tv_am1)
    TextView tvAm1;
    @BindView(R.id.tv_time2)
    TextView tvTime2;
    @BindView(R.id.tv_am2)
    TextView tvAm2;
    @BindView(R.id.tv_time3)
    TextView tvTime3;
    @BindView(R.id.tv_am3)
    TextView tvAm3;
    @BindView(R.id.tv_time4)
    TextView tvTime4;
    @BindView(R.id.tv_am4)
    TextView tvAm4;
    @BindView(R.id.circle_1)
    ImageView circle1;
    @BindView(R.id.circle_2)
    ImageView circle2;
    @BindView(R.id.circle_3)
    ImageView circle3;
    @BindView(R.id.circle_4)
    ImageView circle4;
    @BindView(R.id.view1)
    View line1;
    @BindView(R.id.view2)
    View line2;
    @BindView(R.id.view3)
    View line3;

    private String userType;
    private String userId;
    public ProgressBar syncProgress;
    public View llProgressBar;
    private ImageView noData;


    public FragmentTransportStudents() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        ActivityHome activityHome = (ActivityHome) getActivity();
        activityHome.setTitleAndLogo("Transport", R.drawable.ic_transport_black_24dp);
        ActivityHome.fab.setVisibility(View.GONE);
        if (getActivity() != null) {
            syncProgress = getActivity().findViewById(R.id.progress_sync);
            syncProgress.setVisibility(View.GONE);
            noData = getActivity().findViewById(R.id.no_data);
            noData.setVisibility(View.GONE);
        }

        return inflater.inflate(R.layout.fragment_transport_students, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);

        init();
        loadStatus();


    }

    private void init() {

        AuthModel authModel = SaveSharedPreference.getData();
        userId = authModel.uid;
        userType = authModel.userType;
        if (getActivity() != null) {
            llProgressBar = getActivity().findViewById(R.id.llProgressBar);
        }
    }

    private void loadStatus() {
        progressVisibility(true);

        Call<StudentJourneyStatusModel> call = RetrofitClient.get().getStudentJourneyStatus(userId, userType);
        call.enqueue(new Callback<StudentJourneyStatusModel>() {
            @Override
            public void onResponse(Call<StudentJourneyStatusModel> call, Response<StudentJourneyStatusModel> response) {
                progressVisibility(false);
                if (response.body() != null) {

                    StudentJourneyStatusModel model = response.body();
                    if (model.getStatus() == 1) {
                        List<StudentJourneyStatusModel.Result> statusList = model.getResult();
                        if (statusList.size() != 0) {

                            for (int i = 0; i < statusList.size(); i++) {

                                setViews(statusList.get(i));
                            }


                        } else {
                            Alerts.displayToast(getContext(), "Journey not started yet");
                        }
                    }
                } else {
                    Alerts.displayToast(getContext(), "Loading Failed!");
                }

            }

            @Override
            public void onFailure(Call<StudentJourneyStatusModel> call, Throwable t) {
                progressVisibility(false);
                Log.e(TAG, "onFailure: ", t);
                Alerts.displayToast(getContext(), "Loading Failed!");
            }
        });

    }

    public void setViews(StudentJourneyStatusModel.Result statusList) {


        String[] time;
        switch (statusList.getFlag()) {

            case "Embark":
                tvTitle1.setVisibility(View.VISIBLE);
                tvDesc1.setVisibility(View.VISIBLE);
                circle1.setVisibility(View.VISIBLE);
                tvTime1.setVisibility(View.VISIBLE);
                tvAm1.setVisibility(View.VISIBLE);

                time = getTimeInFormat(statusList.getFormattedDate());
                tvTime1.setText(time[0]);
                tvAm1.setText(time[1]);
                break;

            case "disEmbark":
                tvTitle2.setVisibility(View.VISIBLE);
                tvDesc2.setVisibility(View.VISIBLE);
                line1.setVisibility(View.VISIBLE);
                circle2.setVisibility(View.VISIBLE);
                tvTime2.setVisibility(View.VISIBLE);
                tvAm2.setVisibility(View.VISIBLE);

                time = getTimeInFormat(statusList.getFormattedDate());
                tvTime2.setText(time[0]);
                tvAm2.setText(time[1]);

                circle1.setImageResource(R.drawable.circle_border);
                circle2.setImageResource(R.drawable.circle);
                break;

            case "Return_Embark":
                tvTitle3.setVisibility(View.VISIBLE);
                tvDesc3.setVisibility(View.VISIBLE);
                line2.setVisibility(View.VISIBLE);
                circle3.setVisibility(View.VISIBLE);
                tvTime3.setVisibility(View.VISIBLE);
                tvAm3.setVisibility(View.VISIBLE);

                time = getTimeInFormat(statusList.getFormattedDate());
                tvTime3.setText(time[0]);
                tvAm3.setText(time[1]);

                circle1.setImageResource(R.drawable.circle_border);
                circle2.setImageResource(R.drawable.circle_border);
                circle3.setImageResource(R.drawable.circle);
                break;

            case "Return_DisEmbark":

                tvTitle4.setVisibility(View.VISIBLE);
                tvDesc4.setVisibility(View.VISIBLE);
                line3.setVisibility(View.VISIBLE);
                circle4.setVisibility(View.VISIBLE);
                tvTime4.setVisibility(View.VISIBLE);
                tvAm4.setVisibility(View.VISIBLE);

                time = getTimeInFormat(statusList.getFormattedDate());
                tvTime4.setText(time[0]);
                tvAm4.setText(time[1]);

                circle1.setImageResource(R.drawable.circle_border);
                circle2.setImageResource(R.drawable.circle_border);
                circle3.setImageResource(R.drawable.circle_border);
                circle4.setImageResource(R.drawable.circle);
                break;
        }

    }

    public String[] getTimeInFormat(String time) {
        String[] timeArray = new String[2];
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("hh:mm a");
            Date date = sdf.parse(time);
            Calendar calendar = GregorianCalendar.getInstance();
            calendar.setTime(date);
            int hour = calendar.get(Calendar.HOUR);
            int min = calendar.get(Calendar.MINUTE);
            int ampm = calendar.get(Calendar.AM_PM);
            timeArray[0] = (hour < 10 ? "0" + hour : hour) + ":" + ((min < 10) ? "0" + min : min);
            timeArray[1] = ampm == 1 ? "PM" : "AM";
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return timeArray;
    }

    public void progressVisibility(boolean visibility) {

        if (getActivity() != null) {

            if (visibility) {
                llProgressBar.setVisibility(View.VISIBLE);
                getActivity().getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                        WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);

            } else {
                llProgressBar.setVisibility(View.GONE);
                getActivity().getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
            }
        }
    }
}
