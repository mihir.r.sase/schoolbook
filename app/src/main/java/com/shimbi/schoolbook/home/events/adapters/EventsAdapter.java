package com.shimbi.schoolbook.home.events.adapters;

import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.card.MaterialCardView;
import com.shimbi.schoolbook.R;
import com.shimbi.schoolbook.home.events.activities.ActivityEventDetails;
import com.shimbi.schoolbook.home.events.models.EventsResponseModel;
import com.shimbi.schoolbook.utils.calendar.TimeFormatter;

import java.util.ArrayList;
import java.util.List;

public class EventsAdapter extends RecyclerView.Adapter {

    private List<EventsResponseModel.MyEvent> mEventsList;

    private TimeFormatter mTimeFormatter = new TimeFormatter();

    public EventsAdapter() {
        mEventsList = new ArrayList<>();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_event, parent, false);
        return new EventHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        EventsResponseModel.MyEvent event = mEventsList.get(position);

        ((EventHolder) holder).tvTitle.setText(event.getEventTitle());
        ((EventHolder) holder).tvDesc.setText(event.getDescription());
        ((EventHolder) holder).tvStartDate.setText(event.getEventDate());
        //((EventHolder) holder).cardView.setCardBackgroundColor(mEventsList.get(position).getColor());


        ((EventHolder) holder).cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(view.getContext(), ActivityEventDetails.class);
                intent.putExtra("eventId", event.getEventId());
                view.getContext().startActivity(intent);
            }
        });

    }

    @Override
    public int getItemCount() {
        return mEventsList.size();
    }

    class EventHolder extends RecyclerView.ViewHolder {

        TextView tvStartDate, tvTitle, tvDesc;
        MaterialCardView cardView;

        EventHolder(View itemView) {
            super(itemView);

            cardView = itemView.findViewById(R.id.cardView);
            tvStartDate = itemView.findViewById(R.id.tv_start_date);
            tvTitle = itemView.findViewById(R.id.tv_title);
            tvDesc = itemView.findViewById(R.id.tv_desc);

        }
    }

    public void addEvents(List<EventsResponseModel.MyEvent> events) {
        for (int i = 0; i < events.size(); i++) {
            mEventsList.add(events.get(i));
            notifyItemInserted(i);
        }
    }

    public void addEvent(EventsResponseModel.MyEvent event) {
        mEventsList.add(event);
        notifyItemInserted(mEventsList.size());
    }

    public void clearList() {
        mEventsList.clear();
        notifyDataSetChanged();
    }
}
