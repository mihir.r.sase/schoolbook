package com.shimbi.schoolbook.home.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AddAttachmentResponseModel {
    @SerializedName("image_name")
    @Expose
    private String imageName;
    @SerializedName("result")
    @Expose
    private String result;

    public String getImageName() {
        return imageName;
    }

    public void setImageName(String imageName) {
        this.imageName = imageName;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }
}
