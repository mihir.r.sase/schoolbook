package com.shimbi.schoolbook.home.leaves.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DeleteLeaveResponseModel {

    @SerializedName("success")
    @Expose
    private Integer success;
    @SerializedName("leave_id")
    @Expose
    private String leaveId;

    @Override
    public String toString() {
        return "DeleteLeaveResponseModel{" +
                "success=" + success +
                ", leaveId='" + leaveId + '\'' +
                '}';
    }

    public Integer getSuccess() {
        return success;
    }

    public void setSuccess(Integer success) {
        this.success = success;
    }

    public String getLeaveId() {
        return leaveId;
    }

    public void setLeaveId(String leaveId) {
        this.leaveId = leaveId;
    }
}
