package com.shimbi.schoolbook.home.activities;

import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;

import androidx.appcompat.app.AppCompatActivity;

import com.google.android.material.appbar.MaterialToolbar;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.textfield.TextInputEditText;
import com.shimbi.schoolbook.R;
import com.shimbi.schoolbook.utils.Alerts;
import com.shimbi.schoolbook.utils.AuthModel;
import com.shimbi.schoolbook.utils.RetrofitClient;
import com.shimbi.schoolbook.utils.SaveSharedPreference;

import org.jetbrains.annotations.NotNull;

import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ActivityChangePassword extends AppCompatActivity {


    private static final String TAG = "ActivityChangePassword";
    @BindView(R.id.et_old_password)
    TextInputEditText etOldPassword;
    @BindView(R.id.et_new_password)
    TextInputEditText etNewPassword;
    @BindView(R.id.bt_Save)
    MaterialButton btSave;
    @BindView(R.id.llProgressBar)
    View llProgressBar;
    private String username;
    private String userEmail;
    private String userType;
    private String userId;
    private Integer studentCount;

    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_password);
        ButterKnife.bind(this);
        init();

        btSave.setOnClickListener(view -> {

            String oldPassword = etOldPassword.getText().toString();
            String newPassword = etNewPassword.getText().toString();

            if (oldPassword.isEmpty()) {
                etOldPassword.setError("Please enter the old password");
                return;
            }

            if (newPassword.isEmpty()) {
                etNewPassword.setError("Please enter the new password");
                return;
            }

            changePassword(oldPassword, newPassword);

        });

    }

    private void init() {

        MaterialToolbar materialToolbar = findViewById(R.id.appbar);
        setSupportActionBar(materialToolbar);
        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);


        AuthModel authModel = SaveSharedPreference.getData();
        userId = authModel.uid;
        username = authModel.username;
        userEmail = authModel.userEmail;
        userType = authModel.userType;
        studentCount = authModel.studentCount;


    }

    private void changePassword(String oldPassword, String newPassword) {

        progressVisibility(true);

        Call<Integer> apicall = RetrofitClient.get().changePassword(userId, userType, newPassword, oldPassword);
        apicall.enqueue(new Callback<Integer>() {
            @Override
            public void onResponse(@NotNull Call<Integer> call, @NotNull Response<Integer> response) {

                if (response.body() != null) {

                    if (response.body() == 1) {
                        Alerts.displayToast(getApplicationContext(), "Success!");
                        finish();
                    } else {
                        Alerts.displayToast(getApplicationContext(), "Failed! Old password is incorrect");
                    }
                } else {
                    Alerts.displayToast(getApplicationContext(), "Operation Failed!");
                }
                progressVisibility(false);
            }

            @Override
            public void onFailure(@NotNull Call<Integer> call, @NotNull Throwable t) {
                Log.e(TAG, "onFailure: ", t);
                Alerts.displayToast(getApplicationContext(), "Operation Failed!");
                progressVisibility(false);
            }
        });
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle presses on the action bar items
        int id = item.getItemId();

        if (id == android.R.id.home) {
            finish();
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * Display progress bar and uses TOUCHABLE flags
     *
     * @param visibility Give True to display dialog, False to remove
     */
    public void progressVisibility(boolean visibility) {
        if (visibility) {
            llProgressBar.setVisibility(View.VISIBLE);
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                    WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
        } else {
            llProgressBar.setVisibility(View.GONE);
            getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
        }
    }
}
