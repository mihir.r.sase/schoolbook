package com.shimbi.schoolbook.home.events.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class EventDetailsResponseModel {
    @SerializedName("syncdate")
    @Expose
    private String syncdate;
    @SerializedName("result")
    @Expose
    private Result result;
    @SerializedName("post_data")
    @Expose
    private PostData postData;

    public PostData getPostData() {
        return postData;
    }

    public void setPostData(PostData postData) {
        this.postData = postData;
    }

    public String getSyncdate() {
        return syncdate;
    }

    public void setSyncdate(String syncdate) {
        this.syncdate = syncdate;
    }

    public Result getResult() {
        return result;
    }

    public void setResult(Result result) {
        this.result = result;
    }

    public class Result {
        @SerializedName("post")
        @Expose
        private List<Post> post = null;
        @SerializedName("attachment")
        @Expose
        private List<Attachment> attachment = null;

        public List<Post> getPost() {
            return post;
        }

        public void setPost(List<Post> post) {
            this.post = post;
        }

        public List<Attachment> getAttachment() {
            return attachment;
        }

        public void setAttachment(List<Attachment> attachment) {
            this.attachment = attachment;
        }
    }

    public class Post {
        @SerializedName("name")
        @Expose
        private String name;
        @SerializedName("date")
        @Expose
        private String date;
        @SerializedName("id")
        @Expose
        private String id;
        @SerializedName("type")
        @Expose
        private String type;
        @SerializedName("comment")
        @Expose
        private String comment;
        @SerializedName("image")
        @Expose
        private String image;

        private String title, eventDate, attachment;

        public Post(String name, String image, String date, String comment, String title, String eventDate, String attachment) {
            this.name = name;
            this.image = image;
            this.date = date;
            this.comment = comment;
            this.title = title;
            this.eventDate = eventDate;
            this.attachment = attachment;
        }

        public Post(String name, String image, String date, String comment) {
            this.name = name;
            this.image = image;
            this.date = date;
            this.comment = comment;
        }


        public String getAttachment() {
            return attachment;
        }

        public void setAttachment(String attachment) {
            this.attachment = attachment;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getEventDate() {
            return eventDate;
        }

        public void setEventDate(String eventDate) {
            this.eventDate = eventDate;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getDate() {
            return date;
        }

        public void setDate(String date) {
            this.date = date;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public String getComment() {
            return comment;
        }

        public void setComment(String comment) {
            this.comment = comment;
        }

        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }
    }

    public class Attachment {
        @SerializedName("download_link")
        @Expose
        private String downloadLink;
        @SerializedName("filename")
        @Expose
        private String filename;

        public String getDownloadLink() {
            return downloadLink;
        }

        public void setDownloadLink(String downloadLink) {
            this.downloadLink = downloadLink;
        }

        public String getFilename() {
            return filename;
        }

        public void setFilename(String filename) {
            this.filename = filename;
        }
    }

    public class PostData {

        @SerializedName("username")
        @Expose
        private String username;
        @SerializedName("event_id")
        @Expose
        private String eventId;
        @SerializedName("no_reply")
        @Expose
        private Integer noReply;
        @SerializedName("event_title")
        @Expose
        private String eventTitle;
        @SerializedName("description")
        @Expose
        private String description;
        @SerializedName("added_date")
        @Expose
        private String addedDate;
        @SerializedName("formatted_date")
        @Expose
        private String formattedDate;
        @SerializedName("picture")
        @Expose
        private String picture;
        @SerializedName("userimage")
        @Expose
        private String userimage;
        @SerializedName("class_key")
        @Expose
        private String classKey;
        @SerializedName("class_value")
        @Expose
        private Object classValue;
        @SerializedName("startdate")
        @Expose
        private String startdate;
        @SerializedName("enddate")
        @Expose
        private String enddate;
        @SerializedName("starttime")
        @Expose
        private String starttime;
        @SerializedName("endtime")
        @Expose
        private Object endtime;
        @SerializedName("eventstatue")
        @Expose
        private String eventstatue;
        @SerializedName("group_name")
        @Expose
        private Object groupName;
        @SerializedName("notice_sms")
        @Expose
        private String noticeSms;
        @SerializedName("send_sms_flag")
        @Expose
        private Integer sendSmsFlag;
        @SerializedName("submiton_date")
        @Expose
        private String submitonDate;
        @SerializedName("flag")
        @Expose
        private String flag;
        @SerializedName("event_date")
        @Expose
        private String eventDate;
        @SerializedName("added_user_id")
        @Expose
        private String addedUserId;
        @SerializedName("added_user_type")
        @Expose
        private String addedUserType;

        public String getAddedUserId() {
            return addedUserId;
        }

        public void setAddedUserId(String addedUserId) {
            this.addedUserId = addedUserId;
        }

        public String getAddedUserType() {
            return addedUserType;
        }

        public void setAddedUserType(String addedUserType) {
            this.addedUserType = addedUserType;
        }

        public String getUsername() {
            return username;
        }

        public void setUsername(String username) {
            this.username = username;
        }

        public String getEventId() {
            return eventId;
        }

        public void setEventId(String eventId) {
            this.eventId = eventId;
        }

        public Integer getNoReply() {
            return noReply;
        }

        public void setNoReply(Integer noReply) {
            this.noReply = noReply;
        }

        public String getEventTitle() {
            return eventTitle;
        }

        public void setEventTitle(String eventTitle) {
            this.eventTitle = eventTitle;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public String getAddedDate() {
            return addedDate;
        }

        public void setAddedDate(String addedDate) {
            this.addedDate = addedDate;
        }

        public String getFormattedDate() {
            return formattedDate;
        }

        public void setFormattedDate(String formattedDate) {
            this.formattedDate = formattedDate;
        }

        public String getPicture() {
            return picture;
        }

        public void setPicture(String picture) {
            this.picture = picture;
        }

        public String getUserimage() {
            return userimage;
        }

        public void setUserimage(String userimage) {
            this.userimage = userimage;
        }

        public String getClassKey() {
            return classKey;
        }

        public void setClassKey(String classKey) {
            this.classKey = classKey;
        }

        public Object getClassValue() {
            return classValue;
        }

        public void setClassValue(Object classValue) {
            this.classValue = classValue;
        }

        public String getStartdate() {
            return startdate;
        }

        public void setStartdate(String startdate) {
            this.startdate = startdate;
        }

        public String getEnddate() {
            return enddate;
        }

        public void setEnddate(String enddate) {
            this.enddate = enddate;
        }

        public String getStarttime() {
            return starttime;
        }

        public void setStarttime(String starttime) {
            this.starttime = starttime;
        }

        public Object getEndtime() {
            return endtime;
        }

        public void setEndtime(Object endtime) {
            this.endtime = endtime;
        }

        public String getEventstatue() {
            return eventstatue;
        }

        public void setEventstatue(String eventstatue) {
            this.eventstatue = eventstatue;
        }

        public Object getGroupName() {
            return groupName;
        }

        public void setGroupName(Object groupName) {
            this.groupName = groupName;
        }

        public String getNoticeSms() {
            return noticeSms;
        }

        public void setNoticeSms(String noticeSms) {
            this.noticeSms = noticeSms;
        }

        public Integer getSendSmsFlag() {
            return sendSmsFlag;
        }

        public void setSendSmsFlag(Integer sendSmsFlag) {
            this.sendSmsFlag = sendSmsFlag;
        }

        public String getSubmitonDate() {
            return submitonDate;
        }

        public void setSubmitonDate(String submitonDate) {
            this.submitonDate = submitonDate;
        }

        public String getFlag() {
            return flag;
        }

        public void setFlag(String flag) {
            this.flag = flag;
        }

        public String getEventDate() {
            return eventDate;
        }

        public void setEventDate(String eventDate) {
            this.eventDate = eventDate;
        }
    }
}
