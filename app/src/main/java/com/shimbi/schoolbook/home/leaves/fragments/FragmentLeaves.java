package com.shimbi.schoolbook.home.leaves.fragments;


import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.work.Constraints;
import androidx.work.OneTimeWorkRequest;
import androidx.work.WorkInfo;
import androidx.work.WorkManager;

import com.github.florent37.tutoshowcase.TutoShowcase;
import com.google.android.material.button.MaterialButton;
import com.shimbi.schoolbook.R;
import com.shimbi.schoolbook.db.helper.MyDataBase;
import com.shimbi.schoolbook.db.tables.DbLeaveHoliday;
import com.shimbi.schoolbook.db.tables.DbLeaves;
import com.shimbi.schoolbook.home.activities.ActivityHome;
import com.shimbi.schoolbook.home.leaves.activities.ActivityApproveLeaves;
import com.shimbi.schoolbook.home.leaves.activities.ActivityNewLeave;
import com.shimbi.schoolbook.home.leaves.adapters.LeavesAdapter;
import com.shimbi.schoolbook.home.leaves.models.LeaveListResponseModel;
import com.shimbi.schoolbook.home.leaves.workers.LeaveWorker;
import com.shimbi.schoolbook.utils.AuthModel;
import com.shimbi.schoolbook.utils.Constants;
import com.shimbi.schoolbook.utils.NetworkStatusChecker;
import com.shimbi.schoolbook.utils.SaveSharedPreference;
import com.shimbi.schoolbook.utils.calendar.CalendarEvent;
import com.shimbi.schoolbook.utils.calendar.CalendarMonthNameFormatter;
import com.shimbi.schoolbook.utils.calendar.CalendarView;
import com.shimbi.schoolbook.utils.calendar.OnDateSelectedListener;
import com.shimbi.schoolbook.utils.calendar.OnLoadEventsListener;
import com.shimbi.schoolbook.utils.calendar.OnMonthChangedListener;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * A simple {@link Fragment} subclass.
 */
public class FragmentLeaves extends Fragment implements OnMonthChangedListener,
        OnDateSelectedListener, OnLoadEventsListener {

    private static final String TAG = "FragmentLeaves";
    //    List<LeaveListResponseModel.Leave> leaveList = new ArrayList<>();
//    List<LeaveListResponseModel.Holiday> holidayList = new ArrayList<>();
    List<DbLeaves> leaveList = new ArrayList<>();
    List<DbLeaveHoliday> holidayList = new ArrayList<>();
    private String username;
    private String userEmail;
    private String userType;
    private String userId;
    private Integer studentCount;

    private CalendarMonthNameFormatter mFormatter = new CalendarMonthNameFormatter();
    private LeavesAdapter leavesAdapter = new LeavesAdapter();

    private RecyclerView mRecyclerView;
    private CalendarView mCalendarView;
    private TextView tvMonth;
    private MaterialButton btApproveDeny;
    public ProgressBar syncProgress;
    private ImageView noData;


    public FragmentLeaves() {
        // Required empty public constructor
    }

    /**
     * Used to find the next date
     *
     * @param date current date
     * @return next date
     */
    public static Date getNextDate(Date date) {

        try {
            Calendar today = Calendar.getInstance();
            today.setTime(date);
            today.add(Calendar.DAY_OF_YEAR, 1);
            return today.getTime();
        } catch (Exception e) {
            return null;
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        ActivityHome activityHome = (ActivityHome) getActivity();
        activityHome.setTitleAndLogo("Leaves", R.drawable.ic_leaves_black_24dp);
        ActivityHome.fab.setVisibility(View.GONE);
        if (getActivity() != null) {
            syncProgress = getActivity().findViewById(R.id.progress_sync);
            syncProgress.setVisibility(View.GONE);
            noData = getActivity().findViewById(R.id.no_data);
            noData.setVisibility(View.GONE);
        }

        View v = inflater.inflate(R.layout.fragment_leaves, container, false);
        AuthModel authModel = SaveSharedPreference.getData();
        userId = authModel.uid;
        username = authModel.username;
        userEmail = authModel.userEmail;
        userType = authModel.userType;
        studentCount = authModel.studentCount;
        btApproveDeny = v.findViewById(R.id.bt_approve_deny);

        if (authModel.leaveApproveFlag == null) {
            btApproveDeny.setVisibility(View.GONE);
        }

        loadDataFromLocal();
        loadDataOnline();


        return v;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {

        super.onViewCreated(view, savedInstanceState);

        init(view);

        ActivityHome.fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                startActivity(new Intent(getContext(), ActivityNewLeave.class));
            }
        });

        btApproveDeny.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getContext(), ActivityApproveLeaves.class));
            }
        });
    }

    private void init(View view) {

        TutoShowcase.from(getActivity())
                .setContentView(R.layout.toto_calendar)
                .on(R.id.drawerLayout)
                .displaySwipableLeft()
                .show();

        ActivityHome.fab.setVisibility(View.VISIBLE);

        mRecyclerView = view.findViewById(R.id.recycler_view);
        mCalendarView = view.findViewById(R.id.calendarView);
        tvMonth = view.findViewById(R.id.tv_month);

        mCalendarView.setOnMonthChangedListener(this);
        mCalendarView.setOnLoadEventsListener(this);
        mCalendarView.setOnDateSelectedListener(this);
        Calendar calendar = Calendar.getInstance();

        calendar.set(2015, Calendar.JANUARY, 1);
        mCalendarView.setMinimumDate(calendar.getTimeInMillis());

        calendar.set(2020, Calendar.OCTOBER, 1);
        mCalendarView.setMaximumDate(calendar.getTimeInMillis());


        mRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        mRecyclerView.setAdapter(leavesAdapter);
    }

    private void loadDataFromLocal() {

        MyDataBase.db.getLeavesDataSync().observe(this, new Observer<List<DbLeaves>>() {
            @Override
            public void onChanged(List<DbLeaves> dbLeaves) {
                try {
                    leaveList = dbLeaves;
                    Log.e(TAG, "onChanged: " + leaveList.size());
                } catch (Exception e) {
                    Log.e(TAG, "onChanged: ", e);
                }
            }
        });

        MyDataBase.db.getLeaveHolidayDataSync().observe(this, new Observer<List<DbLeaveHoliday>>() {
            @Override
            public void onChanged(List<DbLeaveHoliday> dbLeaveHolidays) {
                try {
                    holidayList = dbLeaveHolidays;

                } catch (Exception e) {
                    Log.e(TAG, "onChanged: ", e);
                }
            }
        });

    }

    private void loadDataOnline() {

        Constraints constraints = NetworkStatusChecker.getNetWorkConstraints();
        if (!NetworkStatusChecker.isNetworkConnected(getContext())) {

            syncProgress.setVisibility(View.VISIBLE);
            OneTimeWorkRequest leaves = new OneTimeWorkRequest.Builder(LeaveWorker.class)
                    .addTag(Constants.TASK_SYNC_LEAVES)
                    .setConstraints(constraints)
                    .build();
            WorkManager manager = WorkManager.getInstance();
            manager.beginWith(leaves).enqueue();
            WorkManager.getInstance().getWorkInfoByIdLiveData(leaves.getId()).observe(this, new Observer<WorkInfo>() {
                @Override
                public void onChanged(WorkInfo workInfo) {
                    if (workInfo != null && workInfo.getState() == WorkInfo.State.SUCCEEDED) {
                        FragmentLeaves fragmentLeaves = (FragmentLeaves) getFragmentManager().findFragmentById(R.id.flContent);
                        getFragmentManager().beginTransaction().detach(fragmentLeaves).attach(fragmentLeaves).commit();
                        syncProgress.setVisibility(View.GONE);
                    } else {
                        syncProgress.setVisibility(View.GONE);
                    }
                }
            });
        }
    }


    @Override
    public void onDateSelected(Calendar dayCalendar, @Nullable List<CalendarEvent> events) {

        leavesAdapter.clearList();
        if (events == null) {
            return;
        }
        for (CalendarEvent event : events) {
            if (event instanceof LeaveListResponseModel.Leave) {
                leavesAdapter.addEvent((LeaveListResponseModel.Leave) event);
            }
        }
    }

    @Override
    public void onMonthChanged(Calendar monthCalendar) {
        tvMonth.setText(mFormatter.format(monthCalendar) + ", " + monthCalendar.get(Calendar.YEAR));
    }

    @Override
    public List<? extends CalendarEvent> onLoadEvents(int year, int month) {

        List<LeaveListResponseModel.Leave> leaves = new ArrayList<>();

        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.YEAR, year);
        calendar.set(Calendar.MONTH, month);

        leaveList = MyDataBase.db.getLeavesData();
        holidayList = MyDataBase.db.getLeaveHolidayData();
        try {

            if (leaveList != null) {

                LeaveListResponseModel model = new LeaveListResponseModel();
                for (int i = 0; i < leaveList.size(); i++) {
                    SimpleDateFormat formatter = new SimpleDateFormat("MMM dd, yyyy");
                    String start = leaveList.get(i).getLeaveStartDate();
                    String end = leaveList.get(i).getLeaveEndDate();
                    Date date1 = new SimpleDateFormat("yyyy-MM-dd").parse(start);
                    Date date2 = new SimpleDateFormat("yyyy-MM-dd").parse(end);

                    String[] dateParts = start.split("-");
                    String startyear = dateParts[0];
                    String startmonth = dateParts[1];
                    String startday = dateParts[2];

                    String[] dateParts2 = end.split("-");
                    String endyear = dateParts2[0];
                    String endmonth = dateParts2[1];
                    String endday = dateParts2[2];

                    if (start.equals(end)) {

                        calendar.set(Integer.valueOf(startyear), Integer.valueOf(startmonth) - 1, Integer.valueOf(startday));

                        leaves.add(model.new Leave(
                                calendar.getTimeInMillis(),
                                getColor(leaveList.get(i).getLeaveStatus()),
                                leaveList.get(i).getLeaveFlag(),
                                String.valueOf(leaveList.get(i).getLeaveId()),
                                leaveList.get(i).getLeaveReason(),
                                leaveList.get(i).getReason(),
                                leaveList.get(i).getLeaveStatus(),
                                leaveList.get(i).getAddedDate(),
                                leaveList.get(i).getLeaveStartDate(),
                                leaveList.get(i).getLeaveEndDate()
                        ));
                    } else {

                        long diff = date2.getTime() - date1.getTime();
                        // days = number of days of ongoing event
                        long days = TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);
                        calendar.set(Integer.valueOf(startyear), Integer.valueOf(startmonth) - 1, Integer.valueOf(startday));
                        leaves.add(model.new Leave(
                                calendar.getTimeInMillis(),
                                getColor(leaveList.get(i).getLeaveStatus()),
                                leaveList.get(i).getLeaveFlag(),
                                String.valueOf(leaveList.get(i).getLeaveId()),
                                leaveList.get(i).getLeaveReason(),
                                leaveList.get(i).getReason(),
                                leaveList.get(i).getLeaveStatus(),
                                leaveList.get(i).getAddedDate(),
                                leaveList.get(i).getLeaveStartDate(),
                                leaveList.get(i).getLeaveEndDate()
                        ));

                        Date prev = date1;
                        for (int j = 0; j < days; j++) {
                            Date next = getNextDate(prev);
                            Calendar calendar1 = new GregorianCalendar();
                            calendar1.setTime(next);
                            calendar.set(calendar1.get(Calendar.YEAR), calendar1.get(Calendar.MONTH), calendar1.get(Calendar.DAY_OF_MONTH));
                            //add remaining days
                            leaves.add(model.new Leave(
                                    calendar.getTimeInMillis(),
                                    getColor(leaveList.get(i).getLeaveStatus()),
                                    leaveList.get(i).getLeaveFlag(),
                                    String.valueOf(leaveList.get(i).getLeaveId()),
                                    leaveList.get(i).getLeaveReason(),
                                    leaveList.get(i).getReason(),
                                    leaveList.get(i).getLeaveStatus(),
                                    leaveList.get(i).getAddedDate(),
                                    leaveList.get(i).getLeaveStartDate(),
                                    leaveList.get(i).getLeaveEndDate()
                            ));
                            prev = next;
                        }

                    }
                }

            }


            if (holidayList != null) {
                LeaveListResponseModel model = new LeaveListResponseModel();
                for (int i = 0; i < holidayList.size(); i++) {
                    SimpleDateFormat formatter = new SimpleDateFormat("MMM dd, yyyy");
                    String start = holidayList.get(i).getHolidayFromDate();
                    String end = holidayList.get(i).getHolidayToDate();
                    Date date1 = new SimpleDateFormat("yyyy-MM-dd").parse(start);
                    Date date2 = new SimpleDateFormat("yyyy-MM-dd").parse(end);


                    String[] dateParts = start.split("-");
                    String startyear = dateParts[0];
                    String startmonth = dateParts[1];
                    String startday = dateParts[2];

                    String[] dateParts2 = end.split("-");
                    String endyear = dateParts2[0];
                    String endmonth = dateParts2[1];
                    String endday = dateParts2[2];


                    if (start.equals(end)) {

                        calendar.set(Integer.valueOf(startyear), Integer.valueOf(startmonth) - 1, Integer.valueOf(startday));
                        leaves.add(model.new Leave(
                                calendar.getTimeInMillis(),
                                getColor(holidayList.get(i).getStatus()),
                                holidayList.get(i).getStatus(),
                                holidayList.get(i).getHolidayName(),
                                holidayList.get(i).getHolidayFromDate(),
                                holidayList.get(i).getHolidayToDate()
                        ));
                    } else {
                        long diff = date2.getTime() - date1.getTime();
                        // days = number of days of ongoing event
                        long days = TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);

                        calendar.set(Integer.valueOf(startyear), Integer.valueOf(startmonth) - 1, Integer.valueOf(startday));
                        leaves.add(model.new Leave(
                                calendar.getTimeInMillis(),
                                getColor(holidayList.get(i).getStatus()),
                                holidayList.get(i).getStatus(),
                                holidayList.get(i).getHolidayName(),
                                holidayList.get(i).getHolidayFromDate(),
                                holidayList.get(i).getHolidayToDate()
                        ));
                        Date prev = date1;
                        for (int j = 0; j < days; j++) {
                            Date next = getNextDate(prev);
                            Calendar calendar1 = new GregorianCalendar();
                            calendar1.setTime(next);
                            calendar.set(calendar1.get(Calendar.YEAR), calendar1.get(Calendar.MONTH), calendar1.get(Calendar.DAY_OF_MONTH));
                            //add remaining days
                            leaves.add(model.new Leave(
                                    calendar.getTimeInMillis(),
                                    getColor(holidayList.get(i).getStatus()),
                                    holidayList.get(i).getStatus(),
                                    holidayList.get(i).getHolidayName(),
                                    holidayList.get(i).getHolidayFromDate(),
                                    holidayList.get(i).getHolidayToDate()
                            ));
                            prev = next;
                        }
                    }
                }


            }


        } catch (
                Exception e) {
            Log.e(TAG, "onLoadEvents: ", e);
        }

        return leaves;
    }

    private int getColor(String status) {
        Resources resources = getResources();


        switch (status) {
            case "Hold":
                //hold
                return resources.getColor(R.color.colorHold);
            case "Approve":
                //approved
                return resources.getColor(R.color.colorApprove);
            case "Deny":
                //denied
                return resources.getColor(R.color.colorDeny);
            case "holiday":
                //holiday
                return resources.getColor(R.color.colorHoliday);
        }

        return 0x000000;
    }
}
