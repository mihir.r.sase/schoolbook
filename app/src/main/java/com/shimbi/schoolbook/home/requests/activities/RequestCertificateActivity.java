package com.shimbi.schoolbook.home.requests.activities;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;

import com.google.android.material.appbar.MaterialToolbar;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import com.google.android.material.textfield.TextInputEditText;
import com.shimbi.schoolbook.R;
import com.shimbi.schoolbook.home.requests.models.RequestSuccessModel;
import com.shimbi.schoolbook.home.requests.models.TcDropdownModel;
import com.shimbi.schoolbook.utils.Alerts;
import com.shimbi.schoolbook.utils.AuthModel;
import com.shimbi.schoolbook.utils.RetrofitClient;
import com.shimbi.schoolbook.utils.SaveSharedPreference;

import java.util.ArrayList;
import java.util.List;

import androidx.appcompat.app.AppCompatActivity;
import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RequestCertificateActivity extends AppCompatActivity {


    @BindView(R.id.ll_tc_certificate)
    LinearLayout llTcCertificate;
    @BindView(R.id.ll_bonafide_certificate)
    LinearLayout llBonafideCertificate;
    @BindView(R.id.radio_group)
    RadioGroup radioGroup;
    @BindView(R.id.radio1)
    RadioButton rbEndTerm;
    private static final String TAG = "RequestCertificateActiv";
    @BindView(R.id.bt_tc_certificate)
    MaterialButton btTcCertificate;
    @BindView(R.id.bt_bonafide_certificate)
    MaterialButton btBonafideCertificate;
    @BindView(R.id.et_reason)
    TextInputEditText etReason;
    @BindView(R.id.radio2)
    RadioButton rbMidTerm;
    @BindView(R.id.llProgressBar)
    View llProgressBar;
    private String username;
    private String userEmail;
    private String userType;
    private String userId;
    @BindView(R.id.spinner)
    Spinner spinner;
    boolean isTcCertificate = true;
    String type = "End Term";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_request_certificate);

        ButterKnife.bind(this);
        init();


        if (isTcCertificate) {
            //loadDropDown();
            List<String> reasons = new ArrayList<>();
            reasons.add("Transfer");
            reasons.add("Relocation");
            reasons.add("Parents Wish");
            ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, reasons);
            dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            spinner.setAdapter(dataAdapter);
        }

        btTcCertificate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addTcRequest();
            }
        });

        btBonafideCertificate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String reason = etReason.getText().toString();
                if (reason.isEmpty()) {
                    etReason.setError("Please enter a reason.");
                    etReason.requestFocus();
                    return;
                }

                addBonafideRequest(reason);
            }
        });

        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                if (i == 0) {
                    type = "End Term";
                } else {
                    type = "Mid Term";
                }
            }
        });


    }

    private void addTcRequest() {

        progressVisibility(true);

        Call<RequestSuccessModel> call = RetrofitClient.get().saveTc(userId, userType, type, spinner.getSelectedItem().toString());
        call.enqueue(new Callback<RequestSuccessModel>() {
            @Override
            public void onResponse(Call<RequestSuccessModel> call, Response<RequestSuccessModel> response) {

                if (response.body() != null) {
                    RequestSuccessModel model = response.body();
                    Alerts.displayToast(getApplicationContext(), model.getMsg());
                    if (model.getSuccess() == 1) {
                    }

                } else {
                    Alerts.displayToast(getApplicationContext(), "Operation Failed");
                }
                progressVisibility(false);
            }

            @Override
            public void onFailure(Call<RequestSuccessModel> call, Throwable t) {
                Log.e(TAG, "onFailure: ", t);
                Alerts.displayToast(getApplicationContext(), "Operation Failed");
                progressVisibility(false);
            }
        });
    }

    private void addBonafideRequest(String reason) {
        progressVisibility(true);

        Call<RequestSuccessModel> call = RetrofitClient.get().saveBonafide(userId, userType, reason);
        call.enqueue(new Callback<RequestSuccessModel>() {
            @Override
            public void onResponse(Call<RequestSuccessModel> call, Response<RequestSuccessModel> response) {

                if (response.body() != null) {
                    RequestSuccessModel model = response.body();
                    Alerts.displayToast(getApplicationContext(), model.getMsg());

                } else {
                    Alerts.displayToast(getApplicationContext(), "Operation Failed");
                }
                progressVisibility(false);
            }

            @Override
            public void onFailure(Call<RequestSuccessModel> call, Throwable t) {
                Log.e(TAG, "onFailure: ", t);
                Alerts.displayToast(getApplicationContext(), "Operation Failed");
                progressVisibility(false);
            }
        });
    }

    private void init() {
        AuthModel authModel = SaveSharedPreference.getData();
        userId = authModel.uid;
        username = authModel.username;
        userEmail = authModel.userEmail;
        userType = authModel.userType;

        Intent intent = getIntent();
        if (intent.getExtras().getInt("certificate") == 1) {
            llTcCertificate.setVisibility(View.VISIBLE);
            llBonafideCertificate.setVisibility(View.GONE);
            isTcCertificate = true;
        } else {
            llTcCertificate.setVisibility(View.GONE);
            llBonafideCertificate.setVisibility(View.VISIBLE);
            isTcCertificate = false;
        }

        MaterialToolbar materialToolbar = findViewById(R.id.appbar);
        setSupportActionBar(materialToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
    }

    private void loadDropDown() {

        progressVisibility(true);

        Call<TcDropdownModel> call = RetrofitClient.get().getTcDropdownDetails();
        call.enqueue(new Callback<TcDropdownModel>() {
            @Override
            public void onResponse(Call<TcDropdownModel> call, Response<TcDropdownModel> response) {

                if (response.body() != null) {
                    TcDropdownModel model = response.body();
                    TcDropdownModel.Reason reason = model.getReason();


                } else {
                    Alerts.displayToast(getApplicationContext(), "Loading Failed");
                    progressVisibility(false);
                }

            }

            @Override
            public void onFailure(Call<TcDropdownModel> call, Throwable t) {
                Log.e(TAG, "onFailure: ", t);
                Alerts.displayToast(getApplicationContext(), "Loading Failed");
                progressVisibility(false);
            }
        });

    }

    private void showStatus() {

        Call<RequestSuccessModel> call = null;
        if (isTcCertificate) {
            call = RetrofitClient.get().getTcStatus(userId, userType);

        } else {
            call = RetrofitClient.get().getBonafideStatus(userId, userType);
        }

        if (call != null) {
            progressVisibility(true);
            call.enqueue(new Callback<RequestSuccessModel>() {
                @Override
                public void onResponse(Call<RequestSuccessModel> call, Response<RequestSuccessModel> response) {

                    if (response.body() != null) {
                        RequestSuccessModel model = response.body();
                        if (model.getSuccess() == 1) {
                            new MaterialAlertDialogBuilder(RequestCertificateActivity.this, R.style.DialogStyle)
                                    .setTitle("Request Status")
                                    .setMessage(model.getMsg())
                                    .setPositiveButton("Okay", null)
                                    .setCancelable(true)
                                    .show();
                        } else {
                            Alerts.displayToast(getApplicationContext(), "Loading Failed!");
                        }
                    } else {
                        Alerts.displayToast(getApplicationContext(), "Loading Failed");
                    }
                    progressVisibility(false);
                }

                @Override
                public void onFailure(Call<RequestSuccessModel> call, Throwable t) {
                    Log.e(TAG, "onFailure: ", t);
                    Alerts.displayToast(getApplicationContext(), "Loading Failed");
                    progressVisibility(false);
                }
            });
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_home, menu);
        menu.findItem(R.id.menu_notification).setVisible(false);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle presses on the action bar items
        int id = item.getItemId();
        if (id == android.R.id.home) {
            finish();
        } else if (id == R.id.menu_status) {

            showStatus();
        }


        return super.onOptionsItemSelected(item);
    }

    /**
     * Display progress bar and uses TOUCHABLE flags
     *
     * @param visibility Give True to display dialog, False to remove
     */
    private void progressVisibility(boolean visibility) {
        if (visibility) {
            llProgressBar.setVisibility(View.VISIBLE);
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                    WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
        } else {
            llProgressBar.setVisibility(View.GONE);
            getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
        }
    }
}
