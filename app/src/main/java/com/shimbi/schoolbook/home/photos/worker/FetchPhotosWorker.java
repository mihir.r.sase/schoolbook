package com.shimbi.schoolbook.home.photos.worker;

import android.content.Context;
import android.util.Log;

import com.shimbi.schoolbook.db.helper.MyDataBase;
import com.shimbi.schoolbook.db.tables.DbAlbum;
import com.shimbi.schoolbook.db.tables.DbPhoto;
import com.shimbi.schoolbook.home.photos.models.PhotosListResponseModel;
import com.shimbi.schoolbook.utils.AuthModel;
import com.shimbi.schoolbook.utils.RetrofitClient;
import com.shimbi.schoolbook.utils.SaveSharedPreference;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.work.Worker;
import androidx.work.WorkerParameters;
import retrofit2.Call;
import retrofit2.Response;

public class FetchPhotosWorker extends Worker {

    private static final String TAG = "FetchPhotosWorker";
    private String username;
    private String userEmail;
    private String userType;
    private String userId;
    private Context context;

    public FetchPhotosWorker(@NonNull Context context, @NonNull WorkerParameters workerParams) {
        super(context, workerParams);
        this.context = context;
    }

    @NonNull
    @Override
    public Result doWork() {
        AuthModel authModel = SaveSharedPreference.getData();
        userId = authModel.uid;
        username = authModel.username;
        userEmail = authModel.userEmail;
        userType = authModel.userType;


        Call<PhotosListResponseModel> call = RetrofitClient.get().getPhotos(userId, userType, SaveSharedPreference.getPhotosSyncDate()
                , String.valueOf(SaveSharedPreference.getphotosLatestID()));

        try {

            Response<PhotosListResponseModel> response = call.execute();
            if (response.body() != null) {
                PhotosListResponseModel model = response.body();

                List<Integer> photoIds = new ArrayList<>();


                if (model.getPhotos().size() != 0 || model.getAlbum().size() != 0) {
                    SaveSharedPreference.setPhotosSyncDate(model.getSyncDate());


                    for (int i = 0; i < model.getPhotos().size(); i++) {
                        DbPhoto dbPhoto = new DbPhoto(model.getPhotos().get(i));
                        photoIds.add(Integer.valueOf(model.getPhotos().get(i).getPhotoId()));
                        if (MyDataBase.db.checkIfPresentInPhotos(Integer.valueOf(model.getPhotos().get(i).getPhotoId())) == 0) {
                            MyDataBase.db.insertToPhotos(dbPhoto);
                            Log.e(TAG, "doWork: Insertion Successful(Photos)");
                        } else {
                            MyDataBase.db.updatePhotos(dbPhoto);
                            Log.e(TAG, "doWork: Updating Successful(Photos)");
                        }
                    }


                    for (int j = 0; j < model.getAlbum().size(); j++) {
                        DbAlbum dbAlbum = new DbAlbum(model.getAlbum().get(j));

                        if (MyDataBase.db.checkIfPresentInAlbum(Integer.valueOf(model.getAlbum().get(j).getPhotoCatId())) == 0) {
                            MyDataBase.db.insertToAlbum(dbAlbum);
                            Log.e(TAG, "doWork: Insertion Successful(Album)");
                        } else {
                            MyDataBase.db.updateAlbum(dbAlbum);
                            Log.e(TAG, "doWork: Updating Successful(Album)");
                        }


                        for (int i = 0; i < model.getAlbum().get(j).getAlbumPhotos().size(); i++) {
                            DbPhoto dbPhoto = new DbPhoto(model.getAlbum().get(j).getAlbumPhotos().get(i), model.getAlbum().get(j).getPhotoCatId());
                            photoIds.add(Integer.valueOf(model.getAlbum().get(j).getAlbumPhotos().get(i).getPhotoId()));
                            if (MyDataBase.db.checkIfPresentInPhotos(Integer.valueOf(model.getAlbum().get(j).getAlbumPhotos().get(i).getPhotoId())) == 0) {
                                MyDataBase.db.insertToPhotos(dbPhoto);
                                Log.e(TAG, "doWork: Insertion Successful(Photos,A)");
                            } else {
                                MyDataBase.db.updatePhotos(dbPhoto);
                                Log.e(TAG, "doWork: Updating Successful(Photos,A)");
                            }
                        }
                    }

                    SaveSharedPreference.setPhotosLatestId(Collections.max(photoIds));
                } else {
                    Log.e(TAG, "doWork: No New Data");
                }


            } else {
                Log.e(TAG, "doWork: Failed");
                return Result.failure();
            }

        } catch (Exception e) {
            Log.e(TAG, "doWork: ", e);
            return Result.failure();
        }

        return Result.success();
    }
}
