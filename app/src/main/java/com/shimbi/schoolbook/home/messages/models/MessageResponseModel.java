package com.shimbi.schoolbook.home.messages.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class MessageResponseModel {


    @SerializedName("syncdate")
    @Expose
    private String syncdate;
    @SerializedName("no_of_record")
    @Expose
    private Integer noOfRecord;
    @SerializedName("total_pages")
    @Expose
    private Integer totalPages;
    @SerializedName("result")
    @Expose
    private List<MessageResultModel> result = null;

    public String getSyncdate() {
        return syncdate;
    }

    public void setSyncdate(String syncdate) {
        this.syncdate = syncdate;
    }

    public Integer getNoOfRecord() {
        return noOfRecord;
    }

    public void setNoOfRecord(Integer noOfRecord) {
        this.noOfRecord = noOfRecord;
    }

    public Integer getTotalPages() {
        return totalPages;
    }

    public void setTotalPages(Integer totalPages) {
        this.totalPages = totalPages;
    }

    public List<MessageResultModel> getResult() {
        return result;
    }

    public void setResult(List<MessageResultModel> result) {
        this.result = result;
    }


    public class MessageResultModel{

        @SerializedName("username")
        @Expose
        private String username;
        @SerializedName("user_profile_img")
        @Expose
        private String userProfileImg;
        @SerializedName("event_id")
        @Expose
        private Integer eventId;
        @SerializedName("event_title")
        @Expose
        private String eventTitle;
        @SerializedName("description")
        @Expose
        private String description;
        @SerializedName("picture")
        @Expose
        private String picture;
        @SerializedName("userimage")
        @Expose
        private Object userimage;
        @SerializedName("added_date")
        @Expose
        private String addedDate;
        @SerializedName("flag")
        @Expose
        private String flag;


        public MessageResultModel(String username, String userProfileImg, Integer eventId, String eventTitle, String description, String addedDate) {
            this.username = username;
            this.userProfileImg = userProfileImg;
            this.eventId = eventId;
            this.eventTitle = eventTitle;
            this.description = description;
            this.addedDate = addedDate;
        }

        public String getUsername() {
            return username;
        }

        public void setUsername(String username) {
            this.username = username;
        }

        public String getUserProfileImg() {
            return userProfileImg;
        }

        public void setUserProfileImg(String userProfileImg) {
            this.userProfileImg = userProfileImg;
        }

        public Integer getEventId() {
            return eventId;
        }

        public void setEventId(Integer eventId) {
            this.eventId = eventId;
        }

        public String getEventTitle() {
            return eventTitle;
        }

        public void setEventTitle(String eventTitle) {
            this.eventTitle = eventTitle;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public String getPicture() {
            return picture;
        }

        public void setPicture(String picture) {
            this.picture = picture;
        }

        public Object getUserimage() {
            return userimage;
        }

        public void setUserimage(Object userimage) {
            this.userimage = userimage;
        }

        public String getAddedDate() {
            return addedDate;
        }

        public void setAddedDate(String addedDate) {
            this.addedDate = addedDate;
        }

        public String getFlag() {
            return flag;
        }

        public void setFlag(String flag) {
            this.flag = flag;
        }
    }
}
