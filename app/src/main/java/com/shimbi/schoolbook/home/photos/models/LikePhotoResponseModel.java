package com.shimbi.schoolbook.home.photos.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class LikePhotoResponseModel {

    @SerializedName("success")
    @Expose
    private Integer success;
    @SerializedName("like_users")
    @Expose
    private String likeUsers;
    @SerializedName("like_count")
    @Expose
    private Integer likeCount;

    public Integer getSuccess() {
        return success;
    }

    public void setSuccess(Integer success) {
        this.success = success;
    }

    public String getLikeUsers() {
        return likeUsers;
    }

    public void setLikeUsers(String likeUsers) {
        this.likeUsers = likeUsers;
    }

    public Integer getLikeCount() {
        return likeCount;
    }

    public void setLikeCount(Integer likeCount) {
        this.likeCount = likeCount;
    }
}
