package com.shimbi.schoolbook.home.notice.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.shimbi.schoolbook.R;
import com.shimbi.schoolbook.home.notice.models.NoticeDetailsResponseModel;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

public class NoticeDetailsAdapter extends RecyclerView.Adapter<NoticeDetailsAdapter.MyViewHolder> {
    private List<NoticeDetailsResponseModel.NoticeDetailsResultModel.NoticeDetailsPostModel> noticeDetailList;
    private static final String TAG = "NoticeDetailsAdapter";
    Context context;
    AttachmentButtonClickListener listener;

    public boolean isAssignmentEmpty = false;

    public void setIsAssignmentEmpty(boolean isAssignmentEmpty) {
        this.isAssignmentEmpty = isAssignmentEmpty;
    }

    public NoticeDetailsAdapter(List<NoticeDetailsResponseModel.NoticeDetailsResultModel.NoticeDetailsPostModel> noticeDetailList) {
        this.noticeDetailList = noticeDetailList;
    }

    public void setAttachmnetButtonClickListner(AttachmentButtonClickListener listener) {
        this.listener = listener;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_notice_list_detail, parent, false);
        context = parent.getContext();
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        int pos = holder.getAdapterPosition();
        NoticeDetailsResponseModel.NoticeDetailsResultModel.NoticeDetailsPostModel noticeComment = noticeDetailList.get(pos);
        holder.tvUsername.setText(noticeComment.getName());
        holder.tvDate.setText(noticeComment.getDate());
        holder.tvComment.setText(noticeComment.getComment());

        Glide.with(context).load(noticeComment.getImage()).centerCrop()
                .placeholder(R.drawable.default_profile)
                .into(holder.profileImage);

        if (pos == (noticeDetailList.size() - 1)) {
            holder.lineConnector.setVisibility(View.GONE);
        } else {
            holder.lineConnector.setVisibility(View.VISIBLE);
        }
        if (pos == 0) {
            holder.tvTitle.setVisibility(View.VISIBLE);
            holder.tvTitle.setText(noticeDetailList.get(pos).getTitle());
            holder.relativeLayoutOfTimeline.setVisibility(View.GONE);
            holder.ivAttachment.setVisibility(View.VISIBLE);
        } else {
            holder.tvTitle.setVisibility(View.GONE);
            holder.relativeLayoutOfTimeline.setVisibility(View.VISIBLE);
            holder.ivAttachment.setVisibility(View.GONE);
        }

        holder.ivAttachment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (listener != null) {
                    listener.onClick();
                }
            }
        });

        if (isAssignmentEmpty) {
            holder.ivAttachment.setVisibility(View.GONE);
        }
    }

    @Override
    public int getItemCount() {
        return noticeDetailList.size();
    }

    public interface AttachmentButtonClickListener {
        void onClick();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tv_username)
        TextView tvUsername;
        @BindView(R.id.tv_date)

        TextView tvDate;
        @BindView(R.id.tv_comment)
        TextView tvComment;
        @BindView(R.id.layout_of_timeline)
        RelativeLayout relativeLayoutOfTimeline;
        @BindView(R.id.line_connector)
        View lineConnector;
        @BindView(R.id.tv_title)
        TextView tvTitle;
        @BindView(R.id.profile_image)
        CircleImageView profileImage;
        @BindView(R.id.iv_attachment)
        ImageView ivAttachment;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
