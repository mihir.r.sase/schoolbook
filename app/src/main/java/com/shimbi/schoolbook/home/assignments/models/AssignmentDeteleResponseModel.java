package com.shimbi.schoolbook.home.assignments.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AssignmentDeteleResponseModel {


    @SerializedName("result")
    @Expose
    private String result;
    @SerializedName("event_id")
    @Expose
    private Integer eventId;

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public Integer getEventId() {
        return eventId;
    }

    public void setEventId(Integer eventId) {
        this.eventId = eventId;
    }
}
