package com.shimbi.schoolbook.home.holidays.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.shimbi.schoolbook.R;
import com.shimbi.schoolbook.home.holidays.models.HolidayResponseModel;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;

public class HolidaysAdapter extends RecyclerView.Adapter<HolidaysAdapter.MyViewHolder> {


    List<HolidayResponseModel> holidays;

    public HolidaysAdapter(List<HolidayResponseModel> holidays) {
        this.holidays = holidays;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_holiday_list, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        HolidayResponseModel model = holidays.get(position);
        SimpleDateFormat formatter = new SimpleDateFormat("MMM dd, yyyy");
        try {
            Date date1 = new SimpleDateFormat("yyyy-MM-dd").parse(model.getFromDate());
            Date date2 = new SimpleDateFormat("yyyy-MM-dd").parse(model.getToDate());
            holder.tvHoliday.setText(model.getHolidayName());

            if (model.getFromDate().equals(model.getToDate())) {

                holder.tvTo.setVisibility(View.GONE);
                holder.tvToDate.setVisibility(View.GONE);
                holder.tvFromDate.setText(formatter.format(date1));
            } else {
                holder.tvTo.setVisibility(View.VISIBLE);
                holder.tvToDate.setVisibility(View.VISIBLE);
                holder.tvFromDate.setText(formatter.format(date1));
                holder.tvToDate.setText(formatter.format(date2));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return holidays.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {


        @BindView(R.id.tv_from_date)
        TextView tvFromDate;
        @BindView(R.id.tv_to)
        TextView tvTo;
        @BindView(R.id.tv_to_date)
        TextView tvToDate;
        @BindView(R.id.tv_holiday)
        TextView tvHoliday;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
