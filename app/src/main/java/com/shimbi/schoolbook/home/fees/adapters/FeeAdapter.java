package com.shimbi.schoolbook.home.fees.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.shimbi.schoolbook.R;
import com.shimbi.schoolbook.home.fees.models.FeesResponseModel;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;

public class FeeAdapter extends RecyclerView.Adapter<FeeAdapter.MyViewHolder> {

    public List<FeesResponseModel.Upcoming> feeList;
    private static final String TAG = "FeeAdapter";
    public FeeAdapter(List<FeesResponseModel.Upcoming> feeList) {
        this.feeList = feeList;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_fees, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {

        FeesResponseModel.Upcoming model = feeList.get(position);
        holder.tvFeeName.setText(model.getFeeName());
        holder.tvAmount.setText(String.valueOf(model.getAmount()));
        holder.tvPenalty.setText(model.getPenaltyAmount());
        holder.tvPortal.setText(model.getPayLink());
        if (model.getType().equals("u")) {
            holder.tvEndDate.setText(model.getDueDate());
            holder.tvDueDate.setText("Due Date : ");
        } else {
            holder.tvEndDate.setText(model.getEndDate());
            holder.tvDueDate.setText("End Date : ");
        }
    }

    @Override
    public int getItemCount() {
        return feeList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tv_fee_name)
        TextView tvFeeName;
        @BindView(R.id.tv_end_date)
        TextView tvEndDate;
        @BindView(R.id.tv_penalty)
        TextView tvPenalty;
        @BindView(R.id.tv_portal)
        TextView tvPortal;
        @BindView(R.id.tv_amount)
        TextView tvAmount;
        @BindView(R.id.tv_due_date)
        TextView tvDueDate;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
