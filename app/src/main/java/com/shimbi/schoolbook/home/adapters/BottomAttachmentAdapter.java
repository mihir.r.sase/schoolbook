package com.shimbi.schoolbook.home.adapters;

import android.app.DownloadManager;
import android.content.Context;
import android.net.Uri;
import android.os.Environment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.shimbi.schoolbook.R;
import com.shimbi.schoolbook.home.models.AttachmentDetailsModel;
import com.shimbi.schoolbook.utils.Alerts;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;

public class BottomAttachmentAdapter extends RecyclerView.Adapter<BottomAttachmentAdapter.MyViewHolder> {

    private static final String TAG = "BottomAttachmentAdapter";
    List<AttachmentDetailsModel> list;
    Context context;

    public BottomAttachmentAdapter(List<AttachmentDetailsModel> list) {
        this.list = list;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_attachment_bottom, parent, false);
        context = parent.getContext();
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        AttachmentDetailsModel model = list.get(position);

        holder.tvFileName.setText(model.getFileName());
        holder.tvInfo.setText(model.getFileInfo());

        try {
            String extention = model.getFileName().substring(model.getFileName().lastIndexOf(".") + 1);
            if (extention.equalsIgnoreCase("jpg") || extention.equalsIgnoreCase("jpeg") || extention.equalsIgnoreCase("png")) {
                holder.ivFileType.setBackgroundResource(R.drawable.ic_image);
            } else if (extention.equalsIgnoreCase("pdf")) {
                holder.ivFileType.setBackgroundResource(R.drawable.ic_pdf);
            } else if (extention.equalsIgnoreCase("docx")) {
                holder.ivFileType.setBackgroundResource(R.drawable.ic_word);
            } else if (extention.equalsIgnoreCase("pptx") || extention.equalsIgnoreCase("ppt")) {
                holder.ivFileType.setBackgroundResource(R.drawable.ic_ppt);
            } else if (extention.equalsIgnoreCase("xlsx")) {
                holder.ivFileType.setBackgroundResource(R.drawable.ic_excel);
            } else {
                holder.ivFileType.setBackgroundResource(R.drawable.ic_file);
            }


            holder.ivDownload.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Alerts.displayToast(context, "Downloading");
                    DownloadManager downloadManager = (DownloadManager) context.getSystemService(Context.DOWNLOAD_SERVICE);
                    Uri dowmloadUri = Uri.parse(model.getDownloadLink());

                    DownloadManager.Request request = new DownloadManager.Request(dowmloadUri);
                    request.setAllowedNetworkTypes(DownloadManager.Request.NETWORK_WIFI | DownloadManager.Request.NETWORK_MOBILE);
                    request.setAllowedOverRoaming(false);
                    request.setTitle("Downloading " + model.getFileName());
                    request.setVisibleInDownloadsUi(true);
                    request.allowScanningByMediaScanner();
                    request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);
                    request.setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS, model.getFileName());
                    downloadManager.enqueue(request);
                }
            });

        } catch (Exception e) {
            Log.e(TAG, "onBindViewHolder: ", e);
        }


    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {


        @BindView(R.id.iv_file_type)
        ImageView ivFileType;
        @BindView(R.id.tv_file_name)
        TextView tvFileName;
        @BindView(R.id.tv_file_info)
        TextView tvInfo;
        @BindView(R.id.iv_download)
        ImageView ivDownload;


        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
