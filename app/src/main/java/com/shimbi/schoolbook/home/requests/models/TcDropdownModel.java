package com.shimbi.schoolbook.home.requests.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class TcDropdownModel {

    @SerializedName("reason")
    @Expose
    private Reason reason;
    @SerializedName("type")
    @Expose
    private Type type;

    public Reason getReason() {
        return reason;
    }

    public void setReason(Reason reason) {
        this.reason = reason;
    }

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }

    public class Reason {

        @SerializedName("Transfer")
        @Expose
        private String transfer;
        @SerializedName("Relocation")
        @Expose
        private String relocation;
        @SerializedName("Parent wish")
        @Expose
        private String parentWish;

        public String getTransfer() {
            return transfer;
        }

        public void setTransfer(String transfer) {
            this.transfer = transfer;
        }

        public String getRelocation() {
            return relocation;
        }

        public void setRelocation(String relocation) {
            this.relocation = relocation;
        }

        public String getParentWish() {
            return parentWish;
        }

        public void setParentWish(String parentWish) {
            this.parentWish = parentWish;
        }
    }

    public class Type {

        @SerializedName("year_end")
        @Expose
        private String yearEnd;
        @SerializedName("mid_term")
        @Expose
        private String midTerm;

        public String getYearEnd() {
            return yearEnd;
        }

        public void setYearEnd(String yearEnd) {
            this.yearEnd = yearEnd;
        }

        public String getMidTerm() {
            return midTerm;
        }

        public void setMidTerm(String midTerm) {
            this.midTerm = midTerm;
        }
    }
}
