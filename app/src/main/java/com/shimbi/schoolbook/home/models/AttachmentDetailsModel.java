package com.shimbi.schoolbook.home.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.shimbi.schoolbook.home.assignments.models.AssignmentDetailsResponseModel;
import com.shimbi.schoolbook.home.events.models.EventDetailsResponseModel;
import com.shimbi.schoolbook.home.messages.models.MessageDetailsResponseModel;
import com.shimbi.schoolbook.home.notice.models.NoticeDetailsResponseModel;

public class AttachmentDetailsModel {

    @SerializedName("assignment_add_more_id")
    @Expose
    private String assignmentAddMoreId;
    @SerializedName("assignment_id")
    @Expose
    private String assignmentId;
    @SerializedName("file_original_name")
    @Expose
    private String fileOriginalName;
    @SerializedName("file_name")
    @Expose
    private String fileName;
    @SerializedName("file_info")
    @Expose
    private String fileInfo;
    @SerializedName("added_from")
    @Expose
    private String addedFrom;
    @SerializedName("download_link")
    @Expose
    private String downloadLink;

    private String tempFileName;

    public AttachmentDetailsModel(AssignmentDetailsResponseModel.AssignmentDetailsResultModel.Attachment attachment) {
        this.assignmentAddMoreId = attachment.getAssignmentAddMoreId();
        this.assignmentId = attachment.getAssignmentId();
        this.fileOriginalName = attachment.getFileOriginalName();
        this.fileName = attachment.getFileOriginalName();
        this.fileInfo = attachment.getFileInfo();
        this.addedFrom = attachment.getAddedFrom();
        this.downloadLink = attachment.getDownloadLink();
        this.tempFileName = attachment.getFileName();
    }

    public AttachmentDetailsModel(NoticeDetailsResponseModel.NoticeDetailsResultModel.Attachment attachment) {
        this.downloadLink = attachment.getDownloadLink();
        this.fileOriginalName = attachment.getFilename();
        this.fileInfo = attachment.getFileInfo();
        this.fileName = attachment.getFilename();
        this.tempFileName = attachment.getFileName();
    }

    public AttachmentDetailsModel(MessageDetailsResponseModel.Attachment attachment) {
        this.downloadLink = attachment.getDownloadLink();
        this.fileOriginalName = attachment.getFileOriginalName();
        this.fileInfo = attachment.getFileInfo();
        this.fileName = attachment.getFilename();
    }

    public AttachmentDetailsModel(EventDetailsResponseModel.Attachment attachment) {
        this.downloadLink = attachment.getDownloadLink();
        this.fileName = attachment.getFilename();
    }


    public String getTempFileName() {
        return tempFileName;
    }

    public void setTempFileName(String tempFileName) {
        this.tempFileName = tempFileName;
    }

    public String getAssignmentAddMoreId() {
        return assignmentAddMoreId;
    }

    public void setAssignmentAddMoreId(String assignmentAddMoreId) {
        this.assignmentAddMoreId = assignmentAddMoreId;
    }

    public String getAssignmentId() {
        return assignmentId;
    }

    public void setAssignmentId(String assignmentId) {
        this.assignmentId = assignmentId;
    }

    public String getFileOriginalName() {
        return fileOriginalName;
    }

    public void setFileOriginalName(String fileOriginalName) {
        this.fileOriginalName = fileOriginalName;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getFileInfo() {
        return fileInfo;
    }

    public void setFileInfo(String fileInfo) {
        this.fileInfo = fileInfo;
    }

    public String getAddedFrom() {
        return addedFrom;
    }

    public void setAddedFrom(String addedFrom) {
        this.addedFrom = addedFrom;
    }

    public String getDownloadLink() {
        return downloadLink;
    }

    public void setDownloadLink(String downloadLink) {
        this.downloadLink = downloadLink;
    }

    @Override
    public String toString() {
        return "AttachmentDetailsModel{" +
                "assignmentAddMoreId='" + assignmentAddMoreId + '\'' +
                ", assignmentId='" + assignmentId + '\'' +
                ", fileOriginalName='" + fileOriginalName + '\'' +
                ", fileName='" + fileName + '\'' +
                ", fileInfo='" + fileInfo + '\'' +
                ", addedFrom='" + addedFrom + '\'' +
                ", downloadLink='" + downloadLink + '\'' +
                ", tempFileName='" + tempFileName + '\'' +
                '}';
    }
}
