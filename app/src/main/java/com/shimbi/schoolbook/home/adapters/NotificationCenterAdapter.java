package com.shimbi.schoolbook.home.adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.card.MaterialCardView;
import com.shimbi.schoolbook.R;
import com.shimbi.schoolbook.db.tables.DbNotification;
import com.shimbi.schoolbook.home.assignments.activities.ActivityAssignmentDetails;
import com.shimbi.schoolbook.home.events.activities.ActivityEventDetails;
import com.shimbi.schoolbook.home.messages.activities.ActivityMessageDetails;
import com.shimbi.schoolbook.home.notice.activities.ActivityNoticeDetails;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class NotificationCenterAdapter extends RecyclerView.Adapter<NotificationCenterAdapter.MyViewHolder> {


    List<DbNotification> notifications = new ArrayList<>();
    Context context;

    public void addAll(List<DbNotification> notifications) {
        this.notifications = notifications;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_notification, parent, false);
        context = parent.getContext();
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {

        DbNotification model = notifications.get(position);

        DateFormat dateFormat = new SimpleDateFormat("MMM dd,yyyy HH:mm a");
        holder.tvDate.setText(dateFormat.format(model.getDateTime()));
        holder.tvTitle.setText(model.getTitle());
        holder.tvMessage.setText(model.getMessage());
        switch (model.getModule()) {
            case "event":
                holder.moduleIcon.setImageResource(R.drawable.ic_event_black_24dp);
                break;

            case "notice":
                holder.moduleIcon.setImageResource(R.drawable.ic_notice_black_24dp);
                break;

            case "assignment":
                holder.moduleIcon.setImageResource(R.drawable.ic_assignment_black_24dp);
                break;

            case "message_communication":
                holder.moduleIcon.setImageResource(R.drawable.ic_message_black_24dp);
                break;

            case "photo":
                holder.moduleIcon.setImageResource(R.drawable.ic_photo_black_24dp);
                break;

            case "leave":

            case "student_approve_leave":

            case "student_reject_leave":

            case "teacher_approve_leave":

            case "teacher_reject_leave":
                holder.moduleIcon.setImageResource(R.drawable.ic_leaves_black_24dp);
                break;

            case "fee_structure":

            case "fee_link_generation":
                holder.moduleIcon.setImageResource(R.drawable.ic_fee);
                break;
        }

        holder.card.setOnClickListener(v -> {
            handleClick(model.getEventId(), model.getModule());
        });
    }

    @Override
    public int getItemCount() {
        return notifications.size();
    }

    private void handleClick(Integer eventId, String module) {

        Intent intent;
        switch (module) {
            case "event":
                intent = new Intent(context, ActivityEventDetails.class);
                intent.putExtra("eventId", eventId);
                context.startActivity(intent);
                break;

            case "notice":
                intent = new Intent(context, ActivityNoticeDetails.class);
                intent.putExtra("eventId", eventId);
                context.startActivity(intent);
                break;

            case "assignment":
                intent = new Intent(context, ActivityAssignmentDetails.class);
                intent.putExtra("eventId", eventId);
                context.startActivity(intent);
                break;

            case "message_communication":
                intent = new Intent(context, ActivityMessageDetails.class);
                intent.putExtra("eventId", eventId);
                context.startActivity(intent);
                break;


        }
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tv_date)
        TextView tvDate;
        @BindView(R.id.image)
        ImageView moduleIcon;
        @BindView(R.id.tv_title)
        TextView tvTitle;
        @BindView(R.id.tv_message)
        TextView tvMessage;
        @BindView(R.id.notification_card)
        MaterialCardView card;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
