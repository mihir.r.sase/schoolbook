package com.shimbi.schoolbook.home.fragments;


import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.appbar.MaterialToolbar;
import com.google.android.material.card.MaterialCardView;
import com.shimbi.schoolbook.R;
import com.shimbi.schoolbook.home.activities.ActivityHome;
import com.shimbi.schoolbook.home.assignments.fragments.FragmentAssignments;
import com.shimbi.schoolbook.home.attendance.FragmentAttendance;
import com.shimbi.schoolbook.home.events.fragments.FragmentEvents;
import com.shimbi.schoolbook.home.messages.fragments.FragmentMessages;
import com.shimbi.schoolbook.home.notice.fragments.FragmentNotice;
import com.shimbi.schoolbook.home.time_table.FragmentTimeTable;
import com.shimbi.schoolbook.utils.AuthModel;
import com.shimbi.schoolbook.utils.SaveSharedPreference;

import java.text.SimpleDateFormat;
import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * A simple {@link Fragment} subclass.
 */
public class FragmentHome extends Fragment {

    private static final String TAG = "FragmentHome";
    public ProgressBar syncProgress;
    @BindView(R.id.card_assignments)
    MaterialCardView cardAssignments;
    @BindView(R.id.card_attendance)
    MaterialCardView cardAttendance;
    @BindView(R.id.card_time_table)
    MaterialCardView cardTimeTable;
    @BindView(R.id.card_notices)
    MaterialCardView cardNotices;
    @BindView(R.id.card_events)
    MaterialCardView cardEvents;
    @BindView(R.id.card_messages)
    MaterialCardView cardMessages;
    @BindView(R.id.tv_username)
    TextView tvUsername;
    @BindView(R.id.tv_date)
    TextView tvDate;
    private String username;
    private String userId;
    private String userType;
    private ImageView noData;

    public FragmentHome() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        ActivityHome activityHome = (ActivityHome) getActivity();
        activityHome.setTitleAndLogo("Home", R.drawable.ic_home_black_24dp);
        ActivityHome.fab.setVisibility(View.GONE);
        if (getActivity() != null) {
            syncProgress = getActivity().findViewById(R.id.progress_sync);
            syncProgress.setVisibility(View.GONE);
            noData = getActivity().findViewById(R.id.no_data);
            noData.setVisibility(View.GONE);
        }


        return inflater.inflate(R.layout.fragment_home, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
        init();


        ActivityHome.fab.setVisibility(View.GONE);


        FragmentManager fragmentManager = getFragmentManager();
        cardAssignments.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    fragmentManager.beginTransaction()
                            .replace(R.id.flContent, FragmentAssignments.class.newInstance(), FragmentAssignments.class.newInstance().getTag())
                            .addToBackStack(FragmentAssignments.class.newInstance().getTag())
                            .commit();
                } catch (Exception e) {
                    Log.e(TAG, "onClick: ", e);
                }
            }
        });

        cardAttendance.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    fragmentManager.beginTransaction()
                            .replace(R.id.flContent, FragmentAttendance.class.newInstance(), FragmentAttendance.class.newInstance().getTag())
                            .addToBackStack(FragmentAttendance.class.newInstance().getTag())
                            .commit();
                } catch (Exception e) {
                    Log.e(TAG, "onClick: ", e);
                }
            }
        });
        cardTimeTable.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    fragmentManager.beginTransaction()
                            .replace(R.id.flContent, FragmentTimeTable.class.newInstance(), FragmentTimeTable.class.newInstance().getTag())
                            .addToBackStack(FragmentTimeTable.class.newInstance().getTag())
                            .commit();
                } catch (Exception e) {
                    Log.e(TAG, "onClick: ", e);
                }
            }
        });
        cardNotices.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    fragmentManager.beginTransaction()
                            .replace(R.id.flContent, FragmentNotice.class.newInstance(), FragmentNotice.class.newInstance().getTag())
                            .addToBackStack(FragmentNotice.class.newInstance().getTag())
                            .commit();
                } catch (Exception e) {
                    Log.e(TAG, "onClick: ", e);
                }
            }
        });
        cardEvents.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    fragmentManager.beginTransaction()
                            .replace(R.id.flContent, FragmentEvents.class.newInstance(), FragmentEvents.class.newInstance().getTag())
                            .addToBackStack(FragmentEvents.class.newInstance().getTag())
                            .commit();
                } catch (Exception e) {
                    Log.e(TAG, "onClick: ", e);
                }
            }
        });
        cardMessages.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    fragmentManager.beginTransaction()
                            .replace(R.id.flContent, FragmentMessages.class.newInstance(), FragmentMessages.class.newInstance().getTag())
                            .addToBackStack(FragmentMessages.class.newInstance().getTag())
                            .commit();
                } catch (Exception e) {
                    Log.e(TAG, "onClick: ", e);
                }
            }
        });


    }

    private void init() {
        AuthModel authModel = SaveSharedPreference.getData();
        userId = authModel.uid;
        userType = authModel.userType;
        username = authModel.username;

        String firstName = "";
        if (username.split("\\w+").length > 1) {
            firstName = username.substring(0, username.lastIndexOf(' '));
        } else {
            firstName = username;
        }
        tvUsername.setText(firstName);

        if (getActivity() != null) {
            MaterialToolbar materialToolbar = getActivity().findViewById(R.id.appbar);
            AppBarLayout.LayoutParams p = (AppBarLayout.LayoutParams) materialToolbar.getLayoutParams();
            p.setScrollFlags(0);
            materialToolbar.setLayoutParams(p);
        }

        SimpleDateFormat format = new SimpleDateFormat("EEEE, MMM d");
        tvDate.setText(format.format(new Date()));
    }


    @Override
    public void onResume() {
        super.onResume();
        if (getActivity() != null) {
            getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        if (getActivity() != null) {
            getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_FULL_SENSOR);
        }
    }
}
