package com.shimbi.schoolbook.home.events.activities;

import android.Manifest;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TimePicker;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.appbar.MaterialToolbar;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import com.google.android.material.textfield.TextInputEditText;
import com.shimbi.schoolbook.R;
import com.shimbi.schoolbook.home.adapters.AttachmentAdapter;
import com.shimbi.schoolbook.home.models.AttachmentModel;
import com.shimbi.schoolbook.home.models.DropDownModel;
import com.shimbi.schoolbook.utils.Alerts;
import com.shimbi.schoolbook.utils.AuthModel;
import com.shimbi.schoolbook.utils.FileUtils;
import com.shimbi.schoolbook.utils.RetrofitClient;
import com.shimbi.schoolbook.utils.SaveSharedPreference;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ActivityNewEvent extends AppCompatActivity implements DatePickerDialog.OnDateSetListener, TimePickerDialog.OnTimeSetListener {

    private static final String TAG = "ActivityNewEvent";
    public static String username;
    public static String userEmail;
    public static String userType;
    public static String userId;
    public Integer studentCount;
    @BindView(R.id.selectClass)
    AutoCompleteTextView tvSelectClass;
    @BindView(R.id.et_title)
    TextInputEditText etTitle;
    @BindView(R.id.et_details)
    TextInputEditText etDetails;
    @BindView(R.id.bt_save)
    MaterialButton btSave;
    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;
    @BindView(R.id.check_reply)
    CheckBox checkReply;
    @BindView(R.id.et_start_date)
    TextInputEditText etStartDate;
    @BindView(R.id.et_end_date)
    TextInputEditText etEndDate;
    @BindView(R.id.et_start_time)
    TextInputEditText etStartTime;
    @BindView(R.id.et_end_time)
    TextInputEditText etEndTime;
    @BindView(R.id.radio_group)
    RadioGroup radioGroup;
    @BindView(R.id.radio_activate)
    RadioButton radioActive;
    @BindView(R.id.radio_deactivate)
    RadioButton radioDeactive;
    @BindView(R.id.llProgressBar)
    View llProgressBar;
    private List<DropDownModel.ClassModel> classList = new ArrayList<>();
    private Map<String, String> classListMap = new HashMap<>();
    private DatePickerDialog datePickerDialog;
    private boolean flag;
    private TimePickerDialog timePickerDialog;
    private int noReply = 1;
    private int eventStatue = 1;
    private String filePath;
    private String fileName;
    private List<AttachmentModel> attachmentModelList = new ArrayList<>();
    private AttachmentAdapter attachmentAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_event);
        ButterKnife.bind(this);

        init();
        getClassDropDownDetails();

        etStartDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                flag = true;
                datePickerDialog.show();
            }
        });
        etEndDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                flag = false;
                datePickerDialog.show();
            }
        });

        etStartTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                flag = true;
                timePickerDialog.show();
            }
        });

        etEndTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                flag = false;
                timePickerDialog.show();
            }
        });

        checkReply.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    noReply = 0;
                } else {
                    noReply = 1;
                }
            }
        });


        btSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String _class = tvSelectClass.getText().toString();
                String title = etTitle.getText().toString();
                String details = etDetails.getText().toString();
                String startDate = etStartDate.getText().toString();
                String endDate = etEndDate.getText().toString();
                String startTime = etStartTime.getText().toString();
                String endTime = etEndTime.getText().toString();

                if (_class.isEmpty()) {
                    tvSelectClass.setError("Please Select Class");
                    return;
                }
                if (classListMap.get(_class) == null) {
                    tvSelectClass.setError("Please Select A Valid Class");
                    return;
                }
                if (title.isEmpty()) {
                    etTitle.setError("Please Add Title");
                    return;
                }
                if (details.isEmpty()) {
                    etDetails.setError("Please Add Assignment Details");
                    return;
                }
                if (startDate.isEmpty()) {
                    etStartDate.setError("Please Add Event Start Date");
                    return;
                }
                if (startTime.isEmpty()) {
                    etStartTime.setError("Please Add Event Start Time");
                    return;
                }

                if (radioActive.isChecked()) {
                    eventStatue = 1;
                }
                if (radioDeactive.isChecked()) {
                    eventStatue = 0;
                }

                attachmentModelList = attachmentAdapter.getAll();
                addEvent(_class, title, details, startDate, startTime, endDate, endTime, attachmentModelList);
            }
        });


    }

    private void init() {

        AuthModel authModel = SaveSharedPreference.getData();
        userId = authModel.uid;
        username = authModel.username;
        userEmail = authModel.userEmail;
        userType = authModel.userType;
        studentCount = authModel.studentCount;

        MaterialToolbar materialToolbar = findViewById(R.id.appbar);
        setSupportActionBar(materialToolbar);
        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);


        final Calendar c = Calendar.getInstance();
        int year = c.get(Calendar.YEAR);
        int month = c.get(Calendar.MONTH);
        int day = c.get(Calendar.DAY_OF_MONTH);
        int mHour = c.get(Calendar.HOUR_OF_DAY);
        int mMinute = c.get(Calendar.MINUTE);
        datePickerDialog = new DatePickerDialog(ActivityNewEvent.this, this, year, month, day);
        timePickerDialog = new TimePickerDialog(ActivityNewEvent.this, this, mHour, mMinute, false);

        try {
            Intent intent = getIntent();


            if (intent.getIntExtra("eventId", -1) != -1) {
                getSupportActionBar().setTitle("Edit Event");
            }
            if (intent.getIntExtra("noReply", -1) == 0) {
                checkReply.setChecked(true);
            } else {
                checkReply.setChecked(false);
            }
            etTitle.setText(intent.getStringExtra("title"));
            etDetails.setText(intent.getStringExtra("desc"));
            etStartDate.setText(intent.getStringExtra("startDate"));
            etEndDate.setText(intent.getStringExtra("endDate"));

        } catch (Exception e) {
            Log.e(TAG, "init: ", e);
        }

        attachmentAdapter = new AttachmentAdapter();
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(attachmentAdapter);

        attachmentAdapter.setUploadStatusChangeListner(isUploading -> {
            if (isUploading) {
                btSave.setEnabled(false);
            } else {
                btSave.setEnabled(true);
            }
        });
    }

    private void getClassDropDownDetails() {
        progressVisibility(true);

        Call<DropDownModel> apicall = RetrofitClient.get().getDropDownDetails(userId, userType, 1, 0, 0, 0);
        apicall.enqueue(new Callback<DropDownModel>() {
            @Override
            public void onResponse(Call<DropDownModel> call, Response<DropDownModel> response) {
                if (response.body() != null) {
                    classList = response.body().getClasslist();
                    String[] classes = new String[classList.size()];
                    for (int i = 0; i < classList.size(); i++) {
                        classes[i] = classList.get(i).getValue();
                        classListMap.put(classList.get(i).getValue(), classList.get(i).getKey());
                    }
                    ArrayAdapter<String> adapter = new ArrayAdapter<String>
                            (ActivityNewEvent.this, android.R.layout.select_dialog_item, classes);
                    tvSelectClass.setThreshold(1);
                    tvSelectClass.setAdapter(adapter);
                    progressVisibility(false);

                } else {
                    progressVisibility(false);
                    Alerts.displayToast(getApplicationContext(), "Loading Failed");
                }
            }

            @Override
            public void onFailure(Call<DropDownModel> call, Throwable t) {
                Log.e(TAG, "onFailure: ", t);
                Alerts.displayToast(getApplicationContext(), "Loading Failed");
                progressVisibility(false);
            }
        });
    }

    private void addEvent(String aClass, String title, String details, String startDate, String startTime, String endDate, String endTime, List<AttachmentModel> list) {

        progressVisibility(true);
        try {

            String uploadedFileName = null;
            String originalFileName = null;
            if (!list.isEmpty()) {
                originalFileName = list.get(0).getFilename();
                uploadedFileName = list.get(0).getFinalFileName();
            }


            if (endDate.isEmpty()) {
                endDate = null;
            }
            if (endTime.isEmpty()) {
                endTime = null;
            }

            Call<Integer> apicall = RetrofitClient.get().addEvent(userId, userType, Integer.valueOf(classListMap.get(aClass)),
                    noReply, title, details, startDate, startTime, endDate, endTime, eventStatue, null, uploadedFileName, originalFileName);

            apicall.enqueue(new Callback<Integer>() {
                @Override
                public void onResponse(Call<Integer> call, Response<Integer> response) {

                    if (response.body() != null) {

                        if (response.body() == 1) {
                            Alerts.displayToast(getApplicationContext(), "Success");
                            finish();
                        } else {
                            Alerts.displayToast(getApplicationContext(), "Addition/Updating Failed");
                        }

                    } else {
                        Alerts.displayToast(getApplicationContext(), "Addition/Updating Failed");
                    }
                    progressVisibility(false);
                }

                @Override
                public void onFailure(Call<Integer> call, Throwable t) {
                    Log.e(TAG, "onFailure: ", t);
                    Alerts.displayToast(getApplicationContext(), "Addition/Updating Failed");
                    progressVisibility(false);
                }
            });
        } catch (Exception e) {
            Log.e(TAG, "addEvent: ", e);
            progressVisibility(false);
        }

    }


    private void attachFile() {


        if (attachmentAdapter.getItemCount() == 1) {
            Alerts.displayToast(getApplicationContext(), "Only one image is allowed");
            return;
        }
        Intent mRequestFileIntent = new Intent(Intent.ACTION_GET_CONTENT);
        mRequestFileIntent.setType("image/*");
        startActivityForResult(Intent.createChooser(mRequestFileIntent, "Choose File to Upload.."), 0);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {


            try {
                if (data != null) {

                    Log.e(TAG, "onActivityResult: 1");
                    Uri returnUri = data.getData();
                    processAttachmentUri(returnUri);
                }
            } catch (Exception e) {
                Log.e(TAG, "onActivityResult: ", e);
            }


        }
    }

    public void processAttachmentUri(Uri returnUri) {

        //filePath = PathUtil.getPath(getApplicationContext(), returnUri);
        filePath = FileUtils.getPath(getApplicationContext(), returnUri);
        fileName = FileUtils.getFileNameFromUri(getApplicationContext(), returnUri);
        if (filePath != null) {
            Log.e(TAG, "onActivityResult: " + fileName);
            Log.e(TAG, "onActivityResult: " + filePath);
            Log.e(TAG, "onActivityResult: " + FileUtils.getStringFile(filePath));
            attachmentAdapter.add(fileName, filePath, "event");
        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == 100) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                attachFile();
            } else {
                new MaterialAlertDialogBuilder(ActivityNewEvent.this, R.style.DialogStyle)
                        .setTitle("Storage Access Permission Not Granted")
                        .setMessage("Please Grant Storage Permission To attach files.")
                        .setPositiveButton("Okay", null)
                        .setCancelable(false)
                        .show();
            }
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.attach_file, menu);
        menu.findItem(R.id.menu_delete).setVisible(false);
        menu.findItem(R.id.menu_edit).setVisible(false);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle presses on the action bar items
        int id = item.getItemId();

        if (id == R.id.menu_attachment) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if (checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                    requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 100);
                } else {
                    attachFile();
                }
            }

        }

        if (id == android.R.id.home) {
            finish();
        }

        return super.onOptionsItemSelected(item);
    }


    /**
     * Display progress bar and uses TOUCHABLE flags
     *
     * @param visibility Give True to display dialog, False to remove
     */
    public void progressVisibility(boolean visibility) {
        if (visibility) {
            llProgressBar.setVisibility(View.VISIBLE);
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                    WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
        } else {
            llProgressBar.setVisibility(View.GONE);
            getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
        }
    }

    @Override
    public void onDateSet(DatePicker datePicker, int i, int i1, int i2) {
        i1 = i1 + 1;
        if (flag) {
            etStartDate.setText(String.valueOf(i2).concat("-").concat(String.valueOf(i1)).concat("-").concat(String.valueOf(i)));
        } else {
            etEndDate.setText(String.valueOf(i2).concat("-").concat(String.valueOf(i1)).concat("-").concat(String.valueOf(i)));
        }
    }

    @Override
    public void onTimeSet(TimePicker timePicker, int i, int i1) {
        if (flag) {
            etStartTime.setText(String.valueOf(i).concat(":").concat(String.valueOf(i1)));
        } else {
            etEndTime.setText(String.valueOf(i).concat(":").concat(String.valueOf(i1)));
        }
    }
}
