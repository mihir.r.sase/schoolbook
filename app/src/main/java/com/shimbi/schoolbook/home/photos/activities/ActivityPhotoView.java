package com.shimbi.schoolbook.home.photos.activities;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import com.google.gson.Gson;
import com.jsibbold.zoomage.ZoomageView;
import com.shimbi.schoolbook.R;
import com.shimbi.schoolbook.db.helper.MyDataBase;
import com.shimbi.schoolbook.db.tables.DbAlbum;
import com.shimbi.schoolbook.home.photos.adapters.LikedUsersAdapter;
import com.shimbi.schoolbook.home.photos.models.DeletePhotoResponseModel;
import com.shimbi.schoolbook.home.photos.models.GetLikesResponseModel;
import com.shimbi.schoolbook.home.photos.models.LikePhotoResponseModel;
import com.shimbi.schoolbook.home.photos.models.PhotosListResponseModel;
import com.shimbi.schoolbook.utils.Alerts;
import com.shimbi.schoolbook.utils.AuthModel;
import com.shimbi.schoolbook.utils.RetrofitClient;
import com.shimbi.schoolbook.utils.SaveSharedPreference;

import java.util.ArrayList;
import java.util.List;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ActivityPhotoView extends AppCompatActivity {


    private static final String TAG = "ActivityPhotoView";
    @BindView(R.id.iv_image)
    ZoomageView imageView;
    @BindView(R.id.back)
    ImageView iv_back;
    @BindView(R.id.delete)
    ImageView iv_delete;
    @BindView(R.id.tv_date)
    TextView tvDate;
    @BindView(R.id.tv_title)
    TextView tvTitle;
    @BindView(R.id.tv_description)
    TextView tvDescription;
    @BindView(R.id.tv_likes)
    TextView tvLikes;
    @BindView(R.id.like_enabled)
    ImageView ivLikeEnabled;
    @BindView(R.id.like_disabled)
    ImageView ivLikeDisabled;
    @BindView(R.id.llProgressBar)
    View llProgressBar;
    private String username;
    private String userEmail;
    private String userType;
    private String userId;
    private Integer photoId, albumId;
    private List<String> userList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setStatusBarColor(ContextCompat.getColor(this, R.color.my_statusbar_color));
        }
        setContentView(R.layout.activity_photo_view);

        ButterKnife.bind(this);

        init();

        getLikes();


        iv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        iv_delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (photoId != null) {


                    new MaterialAlertDialogBuilder(ActivityPhotoView.this, R.style.DialogStyle)
                            .setTitle("Do you want to delete this photo?")
                            .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    deletePhoto();
                                }
                            })
                            .setNegativeButton("No", null)
                            .setCancelable(false)
                            .show();

                }
            }
        });

        ivLikeDisabled.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                likePhoto();
            }
        });

        tvLikes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showLikeUsersDialog();
            }
        });
    }

    private void init() {
        AuthModel authModel = SaveSharedPreference.getData();
        userId = authModel.uid;
        username = authModel.username;
        userEmail = authModel.userEmail;
        userType = authModel.userType;

        try {

            PhotosListResponseModel model = new PhotosListResponseModel();
            PhotosListResponseModel.Photo photoModel = new Gson().fromJson(getIntent().getExtras().getString("photos"), PhotosListResponseModel.Photo.class);
            Glide.with(getApplicationContext()).load(photoModel.getPhotoFile())
//                .centerCrop()
                    .placeholder(R.drawable.default_profile)
                    .into(imageView);

            tvDate.setText(photoModel.getAddedDate());
            tvTitle.setText(photoModel.getPhotoTitle());
            tvDescription.setText(photoModel.getDescription());
            photoId = Integer.valueOf(photoModel.getPhotoId());
            albumId = Integer.valueOf(photoModel.getAlbumId());

            if (photoModel.getLikeCount().equals("1")) {
                tvLikes.setText(photoModel.getLikeCount() + " like");
            } else {
                tvLikes.setText(photoModel.getLikeCount() + " likes");
            }

            if (photoModel.getDeleteFlag() != 1) {
                iv_delete.setVisibility(View.GONE);
            }

        } catch (Exception e) {
            Log.e(TAG, "init: ", e);
        }
    }

    private void getLikes() {
        //  progressVisibility(true);

        Call<GetLikesResponseModel> call = RetrofitClient.get().getPhotoLikes(userId, userType, photoId);
        call.enqueue(new Callback<GetLikesResponseModel>() {
            @Override
            public void onResponse(Call<GetLikesResponseModel> call, Response<GetLikesResponseModel> response) {

                if (response.body() != null) {
                    GetLikesResponseModel model = response.body();
                    tvLikes.setText(model.getLikeCount() + " likes");
                    if (model.getLikeLoginUser() == 1) {
                        ivLikeEnabled.setVisibility(View.VISIBLE);
                        ivLikeDisabled.setVisibility(View.GONE);
                    } else {
                        ivLikeEnabled.setVisibility(View.GONE);
                        ivLikeDisabled.setVisibility(View.VISIBLE);
                    }

                    String users = model.getLikeUsers();
                    if (users != null) {
                        String[] arr = users.split(",");
                        for (int i = 0; i < Integer.parseInt(model.getLikeCount()); i++) {
                            userList.add(arr[i]);
                        }
                    }


                } else {
                    Alerts.displayToast(getApplicationContext(), "Loading Failed");
                }
                progressVisibility(false);
            }

            @Override
            public void onFailure(Call<GetLikesResponseModel> call, Throwable t) {
                Log.e(TAG, "onFailure: ", t);
                Alerts.displayToast(getApplicationContext(), "Loading Failed");
                progressVisibility(false);
            }
        });
    }

    private void likePhoto() {

        progressVisibility(true);

        Call<LikePhotoResponseModel> call = RetrofitClient.get().likePhoto(userId, userType, photoId);
        call.enqueue(new Callback<LikePhotoResponseModel>() {
            @Override
            public void onResponse(Call<LikePhotoResponseModel> call, Response<LikePhotoResponseModel> response) {
                if (response.body() != null) {
                    LikePhotoResponseModel model = response.body();
                    Log.e(TAG, "onResponse: " + model.getSuccess());
                    if (model.getSuccess() == 1) {
                        ivLikeEnabled.setVisibility(View.VISIBLE);
                        ivLikeDisabled.setVisibility(View.GONE);
                        tvLikes.setText(model.getLikeCount() + " likes");
                    } else {
                        Alerts.displayToast(getApplicationContext(), "Operation Failed");
                    }
                } else {
                    Alerts.displayToast(getApplicationContext(), "Operation Failed");
                }

                progressVisibility(false);
            }

            @Override
            public void onFailure(Call<LikePhotoResponseModel> call, Throwable t) {
                Log.e(TAG, "onFailure: ", t);
                Alerts.displayToast(getApplicationContext(), "Operation Failed");
                progressVisibility(false);
            }
        });
    }

    private void showLikeUsersDialog() {
        Dialog dialog = new Dialog(ActivityPhotoView.this, R.style.DialogStyle);
        dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        dialog.setContentView(R.layout.dialog_select_multiple);
        dialog.setCanceledOnTouchOutside(true);
        dialog.setCancelable(true);
        dialog.show();

        RecyclerView recyclerView = dialog.findViewById(R.id.recycler_view);
        TextView title = dialog.findViewById(R.id.tv_title);
        title.setText("Liked by");
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext()));

        LikedUsersAdapter adapter = new LikedUsersAdapter(userList);
        recyclerView.setAdapter(adapter);
        adapter.notifyDataSetChanged();
    }

    private void deletePhoto() {

        progressVisibility(true);
        Call<DeletePhotoResponseModel> call = RetrofitClient.get().deletePhoto(userId, userType, photoId);
        call.enqueue(new Callback<DeletePhotoResponseModel>() {
            @Override
            public void onResponse(Call<DeletePhotoResponseModel> call, Response<DeletePhotoResponseModel> response) {

                if (response.body() != null) {
                    if (response.body().getSuccess() == 1) {
                        Alerts.displayToast(getApplicationContext(), "Success");

                        if (MyDataBase.db.getPhotoCountInAlbum(albumId) == 1) {
                            MyDataBase.db.deleteAlbum(albumId);
                            MyDataBase.db.deletePhoto(photoId);
                            finish();
                        } else {
                            MyDataBase.db.deletePhoto(photoId);
                            DbAlbum dbAlbum = new DbAlbum(response.body().getAlbum().get(0));
                            MyDataBase.db.updateAlbum(dbAlbum);
                            finish();
                        }


                    }

                } else {
                    Alerts.displayToast(getApplicationContext(), "Operation Failed");
                }
                progressVisibility(false);

            }

            @Override
            public void onFailure(Call<DeletePhotoResponseModel> call, Throwable t) {
                Log.e(TAG, "onFailure: ", t);
                Alerts.displayToast(getApplicationContext(), "Operation Failed!");
                progressVisibility(false);
            }
        });
    }

    /**
     * Display progress bar and uses TOUCHABLE flags
     *
     * @param visibility Give True to display dialog, False to remove
     */
    public void progressVisibility(boolean visibility) {
        if (visibility) {
            llProgressBar.setVisibility(View.VISIBLE);
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                    WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
        } else {
            llProgressBar.setVisibility(View.GONE);
            getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
        }
    }

}
