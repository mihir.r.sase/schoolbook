package com.shimbi.schoolbook.home.leaves.adapters;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.Resources;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.android.material.button.MaterialButton;
import com.google.android.material.chip.Chip;
import com.google.android.material.textfield.TextInputEditText;
import com.shimbi.schoolbook.R;
import com.shimbi.schoolbook.home.leaves.models.LeaveAproveListResponseModel;
import com.shimbi.schoolbook.home.models.SuccessResponseModel;
import com.shimbi.schoolbook.utils.Alerts;
import com.shimbi.schoolbook.utils.AuthModel;
import com.shimbi.schoolbook.utils.RetrofitClient;
import com.shimbi.schoolbook.utils.SaveSharedPreference;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ApproveLeaveAdapter extends RecyclerView.Adapter<ApproveLeaveAdapter.MyViewHolder> {

    private static final String TAG = "ApproveLeaveAdapter";
    List<LeaveAproveListResponseModel> leaveList;
    Context context;
    Activity activity;

    public ApproveLeaveAdapter(List<LeaveAproveListResponseModel> leaveList, Activity activity) {
        this.leaveList = leaveList;
        this.activity = activity;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_approve_leave, parent, false);
        context = parent.getContext();
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        LeaveAproveListResponseModel model = leaveList.get(position);
        holder.username.setText(model.getUserName());
        holder.startDate.setText(model.getLeaveStartDate());
        holder.endDate.setText(model.getLeaveEndDate());
        holder.reason.setText(model.getLeaveReason());
        Resources resources = context.getResources();
        if (model.getLeaveStatus().equals("Hold")) {
            holder.btAccept.setVisibility(View.VISIBLE);
            holder.btDecline.setVisibility(View.VISIBLE);
            holder.chip.setText("On Hold");
            holder.chip.setChipBackgroundColor(ColorStateList.valueOf(resources.getColor(R.color.colorHold)));
        } else if (model.getLeaveStatus().equals("Deny")) {
            holder.btAccept.setVisibility(View.GONE);
            holder.btDecline.setVisibility(View.GONE);
            holder.chip.setText("Denied");
            holder.chip.setChipBackgroundColor(ColorStateList.valueOf(resources.getColor(R.color.colorDeny)));
        } else if (model.getLeaveStatus().equals("Approve")) {
            holder.btAccept.setVisibility(View.GONE);
            holder.btDecline.setVisibility(View.GONE);
            holder.chip.setText("Approved");
            holder.chip.setChipBackgroundColor(ColorStateList.valueOf(resources.getColor(R.color.colorApprove)));
        }

        holder.btDecline.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Dialog dialog = new Dialog(activity, R.style.DialogStyle);
                dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                dialog.setContentView(R.layout.dialog_add_leave_reason);
                dialog.setCanceledOnTouchOutside(true);
                dialog.setCancelable(true);
                dialog.show();

                TextInputEditText etReason = dialog.findViewById(R.id.et_reason);
                MaterialButton btSubmit = dialog.findViewById(R.id.bt_save);

                btSubmit.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        String reason = etReason.getText().toString();
                        if (reason.isEmpty()) {
                            etReason.setError("Please enter reason");
                            etReason.requestFocus();
                            return;
                        }
                        dialog.dismiss();
                        apicall(model.getLeaveId(), "Deny", reason);

                    }
                });


            }
        });

        holder.btAccept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                apicall(model.getLeaveId(), "Approve", null);
            }
        });
    }

    private void apicall(String leaveId, String type, String reason) {
        AuthModel authModel = SaveSharedPreference.getData();
        String userId = authModel.uid;
        String userType = authModel.userType;
        Call<SuccessResponseModel> call = RetrofitClient.get().approveRejectLeave(userId, userType, leaveId, type, reason);
        call.enqueue(new Callback<SuccessResponseModel>() {
            @Override
            public void onResponse(Call<SuccessResponseModel> call, Response<SuccessResponseModel> response) {

                SuccessResponseModel model = response.body();
                if (model.getSuccess().equals("1")) {
                    Alerts.displayToast(context, "Success");
                    activity.finish();
                    context.startActivity(activity.getIntent());

                } else {
                    Alerts.displayToast(context, "Failed");
                }
            }

            @Override
            public void onFailure(Call<SuccessResponseModel> call, Throwable t) {
                Log.e(TAG, "onFailure: ", t);
            }
        });

    }

    @Override
    public int getItemCount() {
        return leaveList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {


        @BindView(R.id.tv_title)
        TextView username;
        @BindView(R.id.tv_start_date)
        TextView startDate;
        @BindView(R.id.tv_end_date)
        TextView endDate;
        @BindView(R.id.tv_desc)
        TextView reason;
        @BindView(R.id.bt_decline)
        MaterialButton btDecline;
        @BindView(R.id.bt_accept)
        MaterialButton btAccept;
        @BindView(R.id.chip)
        Chip chip;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
