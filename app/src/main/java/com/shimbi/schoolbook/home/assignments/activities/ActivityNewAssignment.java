package com.shimbi.schoolbook.home.assignments.activities;

import android.Manifest;
import android.app.DatePickerDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.DatePicker;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.appbar.MaterialToolbar;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import com.google.android.material.textfield.TextInputEditText;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.shimbi.schoolbook.R;
import com.shimbi.schoolbook.home.adapters.AttachmentAdapter;
import com.shimbi.schoolbook.home.adapters.AttachmentEditAdapter;
import com.shimbi.schoolbook.home.models.AttachmentDetailsModel;
import com.shimbi.schoolbook.home.models.AttachmentModel;
import com.shimbi.schoolbook.home.models.DropDownModel;
import com.shimbi.schoolbook.utils.Alerts;
import com.shimbi.schoolbook.utils.AuthModel;
import com.shimbi.schoolbook.utils.FileUtils;
import com.shimbi.schoolbook.utils.RetrofitClient;
import com.shimbi.schoolbook.utils.SaveSharedPreference;

import org.jetbrains.annotations.NotNull;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ActivityNewAssignment extends AppCompatActivity implements DatePickerDialog.OnDateSetListener {

    private static final String TAG = "ActivityNewAssignment";
    public static String username;
    public static String userEmail;
    public static String userType;
    public static String userId;
    public Integer studentCount;
    @BindView(R.id.selectClass)
    AutoCompleteTextView tvSelectClass;
    @BindView(R.id.et_title)
    TextInputEditText etTitle;
    @BindView(R.id.et_submission_date)
    TextInputEditText etSubmissionDate;
    @BindView(R.id.et_details)
    TextInputEditText etDetails;
    @BindView(R.id.bt_save)
    MaterialButton btSave;
    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;
    @BindView(R.id.recycler_view_edit)
    RecyclerView recyclerViewEdit;
    String eventId = null;
    @BindView(R.id.llProgressBar)
    View llProgressBar;
    List<AttachmentModel> attachmentEditList = new ArrayList<>();
    AttachmentEditAdapter attachmentEditAdapter;
    private DatePickerDialog datePickerDialog;
    private String filePath;
    private String fileName;
    private List<AttachmentModel> attachmentModelList = new ArrayList<>();
    private List<DropDownModel.ClassModel> classList = new ArrayList<>();
    private Map<String, String> classListMap = new HashMap<>();
    private AttachmentAdapter attachmentAdapter;
    private List<AttachmentDetailsModel> attachments = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_aassignment);
        ButterKnife.bind(this);

        init();

        //get class details from server
        getClassDropDownDetails();

        etSubmissionDate.setOnClickListener(view -> datePickerDialog.show());

        btSave.setOnClickListener(view -> {

            String _class = tvSelectClass.getText().toString();
            String title = etTitle.getText().toString();
            String submissionDate = etSubmissionDate.getText().toString();
            String details = etDetails.getText().toString();

            if (_class.isEmpty()) {
                tvSelectClass.setError("Please Select Class");
                return;
            }
            if (title.isEmpty()) {
                etTitle.setError("Please Add Title");
                return;
            }
            if (submissionDate.isEmpty()) {
                etSubmissionDate.setError("Please Add Submission Date");
                return;
            }
            if (details.isEmpty()) {
                etDetails.setError("Please Add Assignment Details");
                return;
            }
            if (classListMap.get(_class) == null) {
                tvSelectClass.setError("Please Select A Valid Class");
                return;
            }

            attachmentModelList = attachmentAdapter.getAll();

            addAssignment(_class, title, submissionDate, details, attachmentModelList, attachmentEditAdapter.getDeleteList());

        });

    }

    private void init() {


        try {

            AuthModel authModel = SaveSharedPreference.getData();
            userId = authModel.uid;
            username = authModel.username;
            userEmail = authModel.userEmail;
            userType = authModel.userType;
            studentCount = authModel.studentCount;


            MaterialToolbar materialToolbar = findViewById(R.id.appbar);
            setSupportActionBar(materialToolbar);
            Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);

            final Calendar c = Calendar.getInstance();
            int year = c.get(Calendar.YEAR);
            int month = c.get(Calendar.MONTH);
            int day = c.get(Calendar.DAY_OF_MONTH);
            datePickerDialog = new DatePickerDialog(ActivityNewAssignment.this, this, year, month, day);

            attachmentAdapter = new AttachmentAdapter();
            RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
            recyclerView.setLayoutManager(layoutManager);
            recyclerView.setItemAnimator(new DefaultItemAnimator());
            recyclerView.setAdapter(attachmentAdapter);

            attachmentEditAdapter = new AttachmentEditAdapter(attachmentEditList);
            RecyclerView.LayoutManager layoutManager1 = new LinearLayoutManager(getApplicationContext());
            recyclerViewEdit.setLayoutManager(layoutManager1);
            recyclerViewEdit.setItemAnimator(new DefaultItemAnimator());
            recyclerViewEdit.setAdapter(attachmentEditAdapter);


            attachmentAdapter.setUploadStatusChangeListner(isUploading -> {
                if (isUploading) {
                    btSave.setEnabled(false);
                } else {
                    btSave.setEnabled(true);
                }
            });

        } catch (Exception e) {
            e.printStackTrace();
            Log.e(TAG, "init: ", e);
        }

        try {
            Intent intent = getIntent();
            tvSelectClass.setText(intent.getExtras().getString("class"));
            etTitle.setText(intent.getExtras().getString("title"));
            etSubmissionDate.setText(intent.getExtras().getString("subDate"));
            etDetails.setText(intent.getExtras().getString("details"));
            eventId = intent.getExtras().getString("eventId");
            attachments = new Gson().fromJson(intent.getExtras().getString("attachments"), new TypeToken<List<AttachmentDetailsModel>>() {
            }.getType());

            for (int i = 0; i < attachments.size(); i++) {
                attachmentEditList.add(new AttachmentModel(attachments.get(i)));
            }
            attachmentEditAdapter.notifyDataSetChanged();

            if (eventId != null) {
                getSupportActionBar().setTitle("Edit Assignment");
            }
        } catch (NullPointerException n) {
            Log.e(TAG, "init: " + n);
        }
    }

    /**
     * Retrieves Class list for the user
     */
    private void getClassDropDownDetails() {
        progressVisibility(true);

        Call<DropDownModel> apicall = RetrofitClient.get().getDropDownDetails(userId, userType, 1, 0, 0, 0);
        apicall.enqueue(new Callback<DropDownModel>() {
            @Override
            public void onResponse(@NotNull Call<DropDownModel> call, @NotNull Response<DropDownModel> response) {
                if (response.body() != null) {
                    classList = response.body().getClasslist();
                    String[] classes = new String[classList.size()];
                    for (int i = 0; i < classList.size(); i++) {
                        classes[i] = classList.get(i).getValue();
                        classListMap.put(classList.get(i).getValue(), classList.get(i).getKey());
                    }
                    ArrayAdapter<String> adapter = new ArrayAdapter<String>
                            (ActivityNewAssignment.this, android.R.layout.select_dialog_item, classes);
                    tvSelectClass.setThreshold(1);
                    tvSelectClass.setAdapter(adapter);
                    progressVisibility(false);

                } else {
                    progressVisibility(false);
                    Alerts.displayToast(getApplicationContext(), "Loading Failed");
                }
            }

            @Override
            public void onFailure(@NotNull Call<DropDownModel> call, @NotNull Throwable t) {
                Log.e(TAG, "onFailure: ", t);
                Alerts.displayToast(getApplicationContext(), "Loading Failed");
                progressVisibility(false);
            }
        });
    }


    /**
     * open request file activity
     */
    private void attachFile() {

        Intent mRequestFileIntent = new Intent(Intent.ACTION_GET_CONTENT);
        mRequestFileIntent.setType("*/*");
        mRequestFileIntent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
        startActivityForResult(Intent.createChooser(mRequestFileIntent, "Choose File to Upload.."), 0);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {

            try {
                if (data != null) {
                    if (data.getClipData() != null) {
                        for (int i = 0; i < data.getClipData().getItemCount(); i++) {
                            Uri returnUri = data.getClipData().getItemAt(i).getUri();
                            processAttachmentUri(returnUri);

                        }
                    } else {
                        Log.e(TAG, "onActivityResult: 1");
                        Uri returnUri = data.getData();
                        processAttachmentUri(returnUri);
                    }
                }


            } catch (Exception e) {
                Log.e(TAG, "onActivityResult: ", e);
            }


        }
    }


    /**
     * process the selected file uri
     *
     * @param returnUri uri of the selected file
     */
    public void processAttachmentUri(Uri returnUri) {

        //filePath = PathUtil.getPath(getApplicationContext(), returnUri);
        filePath = FileUtils.getPath(getApplicationContext(), returnUri);
        fileName = FileUtils.getFileNameFromUri(getApplicationContext(), returnUri);
        if (filePath != null) {
            Log.e(TAG, "onActivityResult: " + fileName);
            Log.e(TAG, "onActivityResult: " + filePath);
            Log.e(TAG, "onActivityResult: " + FileUtils.getStringFile(filePath));
            attachmentAdapter.add(fileName, filePath, "assignment");
        }

    }

    /**
     * add/edit assignment
     *
     * @param aClass         selected class
     * @param title          title for assignment
     * @param submissionDate submission date for assignment
     * @param details        assignment details
     * @param list           attachment list
     * @param deleteFileList deletion attachment list
     */
    private void addAssignment(String aClass, String title, String submissionDate, String details, List<AttachmentModel> list, List<String> deleteFileList) {


        progressVisibility(true);
        Integer id;


        try {
            JSONArray mainArray = new JSONArray();

            for (int i = 0; i < list.size(); i++) {
                JSONObject object = new JSONObject();
                AttachmentModel model = list.get(i);
                object.put("filepath", "0");
                object.put("filename", model.getFilename());
                object.put("file_information", model.getFileInformation());
                object.put("uploaded_filename", model.getFinalFileName());
                mainArray.put(object);
            }

            Log.e(TAG, "addAssignment: " + mainArray);

            JSONArray deleteArray = new JSONArray();
            for (int i = 0; i < deleteFileList.size(); i++) {
                JSONObject object = new JSONObject();
                String filename = deleteFileList.get(i);
                object.put("filename", filename);
                deleteArray.put(object);
            }

            Log.e(TAG, "addAssignment: " + deleteArray);


            if (eventId != null) {
                //edit assignment
                id = Integer.valueOf(eventId);
            } else {
                //add assignment
                id = null;
            }

            Call<Integer> apicall = RetrofitClient.get().addAssignment(
                    Integer.valueOf(userId), Integer.valueOf(userType), Integer.valueOf(classListMap.get(aClass)),
                    submissionDate, title, details, id, mainArray, deleteArray
            );
            apicall.enqueue(new Callback<Integer>() {
                @Override
                public void onResponse(@NotNull Call<Integer> call, @NotNull Response<Integer> response) {

                    if (response.body() != null) {
                        if (response.body() == 1) {
                            Alerts.displayToast(getApplicationContext(), "Success");
                            finish();
                        } else {
                            Alerts.displayToast(getApplicationContext(), "Adding/Editing Failed!!!");
                        }

                    } else {
                        Alerts.displayToast(getApplicationContext(), "Adding/Editing Failed!!");
                    }
                    progressVisibility(false);
                }

                @Override
                public void onFailure(@NotNull Call<Integer> call, @NotNull Throwable t) {
                    Log.e(TAG, "onFailure: ", t);
                    Alerts.displayToast(getApplicationContext(), "Adding/Editing Failed!");
                    progressVisibility(false);
                }
            });
        } catch (Exception e) {
            Log.e(TAG, "addAssignment: ", e);
            progressVisibility(false);
        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == 100) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                attachFile();
            } else {
                new MaterialAlertDialogBuilder(ActivityNewAssignment.this, R.style.DialogStyle)
                        .setTitle("Storage Access Permission Not Granted")
                        .setMessage("Please Grant Storage Permission To attach files.")
                        .setPositiveButton("Okay", null)
                        .setCancelable(false)
                        .show();
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.attach_file, menu);
        menu.findItem(R.id.menu_delete).setVisible(false);
        menu.findItem(R.id.menu_edit).setVisible(false);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle presses on the action bar items
        int id = item.getItemId();

        if (id == R.id.menu_attachment) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if (checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                    requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 100);
                } else {
                    attachFile();
                }
            }

        }

        if (id == android.R.id.home) {
            finish();
        }

        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onDateSet(DatePicker datePicker, int i, int i1, int i2) {
        i1 = i1 + 1;
        etSubmissionDate.setText(String.valueOf(i2).concat("-").concat(String.valueOf(i1)).concat("-").concat(String.valueOf(i)));
    }

    /**
     * Display progress bar and uses TOUCHABLE flags
     *
     * @param visibility Give True to display dialog, False to remove
     */
    public void progressVisibility(boolean visibility) {
        if (visibility) {
            llProgressBar.setVisibility(View.VISIBLE);
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                    WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
        } else {
            llProgressBar.setVisibility(View.GONE);
            getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
        }
    }
}
