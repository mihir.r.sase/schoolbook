package com.shimbi.schoolbook.home.messages.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class MessageDetailsResponseModel {

    @SerializedName("syncdate")
    @Expose
    private String syncdate;
    @SerializedName("result")
    @Expose
    private Result result;
    @SerializedName("post_data")
    @Expose
    private PostData postData;

    public PostData getPostData() {
        return postData;
    }

    public void setPostData(PostData postData) {
        this.postData = postData;
    }

    public String getSyncdate() {
        return syncdate;
    }

    public void setSyncdate(String syncdate) {
        this.syncdate = syncdate;
    }

    public Result getResult() {
        return result;
    }

    public void setResult(Result result) {
        this.result = result;
    }

    public class Result {
        @SerializedName("post")
        @Expose
        private List<Post> post = null;
        @SerializedName("attachment")
        @Expose
        private List<Attachment> attachment;

        public List<Post> getPost() {
            return post;
        }

        public void setPost(List<Post> post) {
            this.post = post;
        }

        public List<Attachment> getAttachment() {
            return attachment;
        }

        public void setAttachment(List<Attachment> attachment) {
            this.attachment = attachment;
        }
    }

    public class Post {


        @SerializedName("date")
        @Expose
        private String date;
        @SerializedName("name")
        @Expose
        private String name;
        @SerializedName("image")
        @Expose
        private String image;
        @SerializedName("comment")
        @Expose
        private String comment;
        @SerializedName("id")
        @Expose
        private String id;
        @SerializedName("type")
        @Expose
        private String type;
        @SerializedName("comment_id")
        @Expose
        private String commentId;

        private String title;

        public Post(String cardUsername, String cardDate, String cardDesc, String cardTitle, String image) {
            name = cardUsername;
            date = cardDate;
            comment = cardDesc;
            title = cardTitle;
            this.image = image;
        }

        public Post(String name, String date, String comment, String image) {
            this.name = name;
            this.date = date;
            this.comment = comment;
            this.image = image;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getDate() {
            return date;
        }

        public void setDate(String date) {
            this.date = date;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }

        public String getComment() {
            return comment;
        }

        public void setComment(String comment) {
            this.comment = comment;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public String getCommentId() {
            return commentId;
        }

        public void setCommentId(String commentId) {
            this.commentId = commentId;
        }
    }

    public class Attachment {
        @SerializedName("download_link")
        @Expose
        private String downloadLink;
        @SerializedName("file_info")
        @Expose
        private String fileInfo;
        @SerializedName("file_original_name")
        @Expose
        private String fileOriginalName;
        @SerializedName("filename")
        @Expose
        private String filename;

        public String getFilename() {
            return filename;
        }

        public void setFilename(String filename) {
            this.filename = filename;
        }

        public String getDownloadLink() {
            return downloadLink;
        }

        public void setDownloadLink(String downloadLink) {
            this.downloadLink = downloadLink;
        }

        public String getFileInfo() {
            return fileInfo;
        }

        public void setFileInfo(String fileInfo) {
            this.fileInfo = fileInfo;
        }

        public String getFileOriginalName() {
            return fileOriginalName;
        }

        public void setFileOriginalName(String fileOriginalName) {
            this.fileOriginalName = fileOriginalName;
        }
    }

    public class PostData {

        @SerializedName("username")
        @Expose
        private String username;
        @SerializedName("user_profile_img")
        @Expose
        private String userProfileImg;
        @SerializedName("event_id")
        @Expose
        private String eventId;
        @SerializedName("event_title")
        @Expose
        private String eventTitle;
        @SerializedName("description")
        @Expose
        private String description;
        @SerializedName("picture")
        @Expose
        private String picture;
        @SerializedName("userimage")
        @Expose
        private String userimage;
        @SerializedName("added_date")
        @Expose
        private String addedDate;
        @SerializedName("flag")
        @Expose
        private String flag;
        @SerializedName("added_user_id")
        @Expose
        private String addedUserId;
        @SerializedName("added_user_type")
        @Expose
        private String addedUserType;

        public String getAddedUserId() {
            return addedUserId;
        }

        public void setAddedUserId(String addedUserId) {
            this.addedUserId = addedUserId;
        }

        public String getAddedUserType() {
            return addedUserType;
        }

        public void setAddedUserType(String addedUserType) {
            this.addedUserType = addedUserType;
        }

        public String getUsername() {
            return username;
        }

        public void setUsername(String username) {
            this.username = username;
        }

        public String getUserProfileImg() {
            return userProfileImg;
        }

        public void setUserProfileImg(String userProfileImg) {
            this.userProfileImg = userProfileImg;
        }

        public String getEventId() {
            return eventId;
        }

        public void setEventId(String eventId) {
            this.eventId = eventId;
        }

        public String getEventTitle() {
            return eventTitle;
        }

        public void setEventTitle(String eventTitle) {
            this.eventTitle = eventTitle;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public String getPicture() {
            return picture;
        }

        public void setPicture(String picture) {
            this.picture = picture;
        }

        public String getUserimage() {
            return userimage;
        }

        public void setUserimage(String userimage) {
            this.userimage = userimage;
        }

        public String getAddedDate() {
            return addedDate;
        }

        public void setAddedDate(String addedDate) {
            this.addedDate = addedDate;
        }

        public String getFlag() {
            return flag;
        }

        public void setFlag(String flag) {
            this.flag = flag;
        }
    }
}
