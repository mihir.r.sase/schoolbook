package com.shimbi.schoolbook.home.leaves.workers;

import android.content.Context;
import android.util.Log;

import com.shimbi.schoolbook.db.helper.MyDataBase;
import com.shimbi.schoolbook.db.tables.DbLeaveHoliday;
import com.shimbi.schoolbook.db.tables.DbLeaves;
import com.shimbi.schoolbook.home.leaves.models.LeaveListResponseModel;
import com.shimbi.schoolbook.utils.AuthModel;
import com.shimbi.schoolbook.utils.RetrofitClient;
import com.shimbi.schoolbook.utils.SaveSharedPreference;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.work.Worker;
import androidx.work.WorkerParameters;
import retrofit2.Call;
import retrofit2.Response;

public class LeaveWorker extends Worker {

    private static final String TAG = "LeaveWorker";
    private String username;
    private String userEmail;
    private String userType;
    private String userId;

    private Context context;

    public LeaveWorker(@NonNull Context context, @NonNull WorkerParameters workerParams) {
        super(context, workerParams);
        this.context = context;
    }

    @NonNull
    @Override
    public Result doWork() {
        AuthModel authModel = SaveSharedPreference.getData();
        userId = authModel.uid;
        username = authModel.username;
        userEmail = authModel.userEmail;
        userType = authModel.userType;
        int flag1 = 0;
        int flag2 = 0;
        Call<LeaveListResponseModel> apicall = RetrofitClient.get().getLeaveList(userId, userType, SaveSharedPreference.getLeavesSyncDate());

        try {
            Response<LeaveListResponseModel> response = apicall.execute();
            if (response.body() != null) {
                LeaveListResponseModel model = response.body();
                List<LeaveListResponseModel.Leave> leaves = model.getLeave();
                List<LeaveListResponseModel.Holiday> holidays = model.getHoliday();

                if (leaves.size() != 0) {
                    SaveSharedPreference.setLeavesSyncDate(model.getSyncdate());
                    for (int i = 0; i < leaves.size(); i++) {
                        DbLeaves dbLeaves = new DbLeaves(leaves.get(i));
                        if (MyDataBase.db.checkIfPresentInLeaves(Integer.valueOf(leaves.get(i).getLeaveId())) == 0) {
                            MyDataBase.db.insertToLeaves(dbLeaves);
                            Log.e(TAG, "doWork: Insertion Successful");
                        } else {

                            MyDataBase.db.updateLeaves(dbLeaves);
                            Log.e(TAG, "doWork: Updating Successful" + dbLeaves.getLeaveId());
                        }
                    }
                    flag1 = 0;
                } else {
                    Log.e(TAG, "doWork: No New Leaves");
                    flag1 = 1;
                }

                if (holidays.size() != 0) {
                    for (int i = 0; i < holidays.size(); i++) {
                        DbLeaveHoliday dbLeaveHoliday = new DbLeaveHoliday(holidays.get(i));
                        if (MyDataBase.db.checkIfPresentInLeaves(Integer.valueOf(holidays.get(i).getHolidayId())) == 0) {
                            MyDataBase.db.insertToLeaveHoliday(dbLeaveHoliday);
                            Log.e(TAG, "doWork: Insertion Successful");
                        } else {
                            MyDataBase.db.updateLeaveHoliday(dbLeaveHoliday);
                            Log.e(TAG, "doWork: Updating Successful" + dbLeaveHoliday.getHolidayId());
                        }
                    }
                    flag2 = 0;
                } else {

                    Log.e(TAG, "doWork: No New Holidays");
                    flag2 = 1;
                }

                if (flag1 == 0 || flag2 == 0) {
                    return Result.success();
                } else {
                    return Result.failure();
                }


            } else {
                return Result.failure();
            }

        } catch (Exception e) {
            Log.e(TAG, "doWork: ", e);
            return Result.failure();
        }
    }
}
