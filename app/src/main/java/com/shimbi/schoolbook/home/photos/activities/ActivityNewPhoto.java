package com.shimbi.schoolbook.home.photos.activities;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.LinearLayout;

import com.google.android.material.appbar.MaterialToolbar;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import com.google.android.material.textfield.TextInputEditText;
import com.shimbi.schoolbook.R;
import com.shimbi.schoolbook.home.adapters.AttachmentAdapter;
import com.shimbi.schoolbook.home.models.AttachmentModel;
import com.shimbi.schoolbook.home.models.SuccessResponseModel;
import com.shimbi.schoolbook.utils.Alerts;
import com.shimbi.schoolbook.utils.AuthModel;
import com.shimbi.schoolbook.utils.FileUtils;
import com.shimbi.schoolbook.utils.RetrofitClient;
import com.shimbi.schoolbook.utils.SaveSharedPreference;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ActivityNewPhoto extends AppCompatActivity {

    private static final String TAG = "ActivityNewPhoto";
    public static String username;
    public static String userEmail;
    public static String userType;
    public static String userId;

    int albumFlag = 0;
    @BindView(R.id.check_album)
    CheckBox checkAlbum;
    @BindView(R.id.container)
    LinearLayout albumContainer;
    @BindView(R.id.et_album_title)
    TextInputEditText etAlbumTitle;
    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;
    @BindView(R.id.bt_save)
    MaterialButton btSave;
    @BindView(R.id.llProgressBar)
    View llProgressBar;
    @BindView(R.id.et_photo_title)
    TextInputEditText etPhotoTitle;
    @BindView(R.id.et_photo_details)
    TextInputEditText etPhotoDetails;
    private String filePath;
    private String fileName;
    private List<AttachmentModel> attachmentModelList = new ArrayList<>();
    private AttachmentAdapter attachmentAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_photo);
        ButterKnife.bind(this);
        init();

        checkAlbum.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    albumContainer.setVisibility(View.VISIBLE);
                    albumFlag = 1;
                } else {
                    albumContainer.setVisibility(View.GONE);
                    albumFlag = 0;
                }
            }
        });

        btSave.setOnClickListener(view -> {
            String photoTitle = etPhotoTitle.getText().toString();
            String photoDetails = etPhotoDetails.getText().toString();
            String albumTitle = etAlbumTitle.getText().toString();

            if (photoTitle.isEmpty()) {
                etPhotoTitle.setError("Title cannot be empty");
                etPhotoTitle.requestFocus();
                return;
            }
            attachmentModelList = attachmentAdapter.getAll();
            addPhoto(photoTitle, photoDetails, albumTitle, attachmentModelList);
        });

    }

    private void init() {

        AuthModel authModel = SaveSharedPreference.getData();
        userId = authModel.uid;
        username = authModel.username;
        userEmail = authModel.userEmail;
        userType = authModel.userType;


        MaterialToolbar materialToolbar = findViewById(R.id.appbar);
        setSupportActionBar(materialToolbar);
        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        attachmentAdapter = new AttachmentAdapter();
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(attachmentAdapter);

        attachmentAdapter.setUploadStatusChangeListner(isUploading -> {
            if (isUploading) {
                btSave.setEnabled(false);
            } else {
                btSave.setEnabled(true);
            }
        });

    }


    private void addPhoto(String photoTitle, String photoDetails, String albumTitle, List<AttachmentModel> list) {

        progressVisibility(true);
        try {

            String uploadedFileName = null;
            String originalFileName = null;
            if (!list.isEmpty()) {
                originalFileName = list.get(0).getFilename();
                uploadedFileName = list.get(0).getFinalFileName();
            }

            Call<SuccessResponseModel> call = RetrofitClient.get().addPhoto(userId, userType, albumTitle, photoTitle, photoDetails, uploadedFileName, originalFileName);
            call.enqueue(new Callback<SuccessResponseModel>() {
                @Override
                public void onResponse(Call<SuccessResponseModel> call, Response<SuccessResponseModel> response) {
                    if (response.body() != null) {

                        SuccessResponseModel model = response.body();
                        if (model.getSuccess().equals("1")) {
                            Alerts.displayToast(getApplicationContext(), "Success!");
                            finish();
                        } else {
                            Alerts.displayToast(getApplicationContext(), "Operation Failed");
                        }

                    } else {
                        Alerts.displayToast(getApplicationContext(), "Operation Failed");
                    }
                    progressVisibility(false);
                }

                @Override
                public void onFailure(Call<SuccessResponseModel> call, Throwable t) {
                    Log.e(TAG, "onFailure: ", t);
                    Alerts.displayToast(getApplicationContext(), "Operation Failed");
                    progressVisibility(false);
                }
            });


        } catch (Exception e) {
            Log.e(TAG, "addPhoto: ", e);
            progressVisibility(false);
        }
    }


    private void attachFile() {


        if (attachmentAdapter.getItemCount() == 1) {
            Alerts.displayToast(getApplicationContext(), "Only one image is allowed");
            return;
        }
        Intent mRequestFileIntent = new Intent(Intent.ACTION_GET_CONTENT);
        mRequestFileIntent.setType("image/*");
        startActivityForResult(Intent.createChooser(mRequestFileIntent, "Choose File to Upload.."), 0);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {


            try {
                if (data != null) {

                    Log.e(TAG, "onActivityResult: 1");
                    Uri returnUri = data.getData();
                    processAttachmentUri(returnUri);
                }
            } catch (Exception e) {
                Log.e(TAG, "onActivityResult: ", e);
            }


        }
    }

    public void processAttachmentUri(Uri returnUri) {

        //filePath = PathUtil.getPath(getApplicationContext(), returnUri);
        filePath = FileUtils.getPath(getApplicationContext(), returnUri);
        fileName = FileUtils.getFileNameFromUri(getApplicationContext(), returnUri);
        if (filePath != null) {
            Log.e(TAG, "onActivityResult: " + fileName);
            Log.e(TAG, "onActivityResult: " + filePath);
            Log.e(TAG, "onActivityResult: " + FileUtils.getStringFile(filePath));
            attachmentAdapter.add(fileName, filePath, "photo");
        }

    }




    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.attach_file, menu);
        menu.findItem(R.id.menu_delete).setVisible(false);
        menu.findItem(R.id.menu_edit).setVisible(false);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle presses on the action bar items
        int id = item.getItemId();

        if (id == R.id.menu_attachment) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if (checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                    requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 100);
                } else {
                    attachFile();
                }
            }

        }

        if (id == android.R.id.home) {
            finish();
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == 100) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                attachFile();
            } else {
                new MaterialAlertDialogBuilder(ActivityNewPhoto.this, R.style.DialogStyle)
                        .setTitle("Storage Access Permission Not Granted")
                        .setMessage("Please Grant Storage Permission To attach files.")
                        .setPositiveButton("Okay", null)
                        .setCancelable(false)
                        .show();
            }
        }
    }

    /**
     * Display progress bar and uses TOUCHABLE flags
     *
     * @param visibility Give True to display dialog, False to remove
     */
    public void progressVisibility(boolean visibility) {
        if (visibility) {
            llProgressBar.setVisibility(View.VISIBLE);
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                    WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
        } else {
            llProgressBar.setVisibility(View.GONE);
            getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
        }
    }


}
