package com.shimbi.schoolbook.home.activities;

import android.os.Bundle;
import android.view.MenuItem;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.appbar.MaterialToolbar;
import com.shimbi.schoolbook.R;
import com.shimbi.schoolbook.db.helper.MyDataBase;
import com.shimbi.schoolbook.home.adapters.NotificationCenterAdapter;

import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ActivityNotificationCenter extends AppCompatActivity {


    private static final String TAG = "ActivityNotificationCen";
    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;

    NotificationCenterAdapter adapter = new NotificationCenterAdapter();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notification_center);
        ButterKnife.bind(this);
        init();

        loadData();
    }

    private void init() {
        MaterialToolbar materialToolbar = findViewById(R.id.appbar);
        setSupportActionBar(materialToolbar);
        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        adapter = new NotificationCenterAdapter();
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(adapter);
    }

    private void loadData() {
        adapter.addAll(MyDataBase.db.getNotifications());
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle presses on the action bar items
        int id = item.getItemId();

        if (id == android.R.id.home) {
            finish();
        }

        return super.onOptionsItemSelected(item);
    }
}
