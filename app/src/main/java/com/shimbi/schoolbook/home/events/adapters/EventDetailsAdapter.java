package com.shimbi.schoolbook.home.events.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.shimbi.schoolbook.R;
import com.shimbi.schoolbook.home.events.models.EventDetailsResponseModel;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

public class EventDetailsAdapter extends RecyclerView.Adapter<EventDetailsAdapter.MyViewHolder> {

    List<EventDetailsResponseModel.Post> events;
    private static final String TAG = "EventDetailsAdapter";
    Context context;
    AttachmentButtonClickListener listener;

    public boolean isAssignmentEmpty = false;

    public void setIsAssignmentEmpty(boolean isAssignmentEmpty) {
        this.isAssignmentEmpty = isAssignmentEmpty;
    }



    public EventDetailsAdapter(List<EventDetailsResponseModel.Post> events) {
        this.events = events;
    }


    public void setAttachmnetButtonClickListner(AttachmentButtonClickListener listener) {
        this.listener = listener;
    }
    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_event_details, parent, false);
        context = parent.getContext();
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {

        int pos = holder.getAdapterPosition();
        EventDetailsResponseModel.Post comment = events.get(pos);
        holder.tvUsername.setText(comment.getName());
        holder.tvDate.setText(comment.getDate());
        holder.tvComment.setText(comment.getComment());

        Glide.with(context).load(comment.getImage()).centerCrop()
                .placeholder(R.drawable.default_profile)
                .into(holder.profileImage);

        if (pos == (events.size() - 1)) {
            holder.lineConnector.setVisibility(View.GONE);
        } else {
            holder.lineConnector.setVisibility(View.VISIBLE);
        }
        if (pos == 0) {
            holder.tvTitle.setVisibility(View.VISIBLE);
            holder.tvTitle.setText(comment.getTitle());
            holder.tvEventDate.setVisibility(View.VISIBLE);
            holder.tvEventDate.setText(comment.getEventDate());
            holder.relativeLayoutOfTimeline.setVisibility(View.GONE);
            holder.ivAttachment.setVisibility(View.VISIBLE);
            if (comment.getAttachment().isEmpty()) {
                holder.image.setVisibility(View.GONE);
            } else {
                Glide.with(context).load(comment.getAttachment()).centerCrop()
                        .placeholder(R.drawable.placeholder)
                        .into(holder.image);
            }
        } else {
            holder.tvTitle.setVisibility(View.GONE);
            holder.tvEventDate.setVisibility(View.GONE);
            holder.relativeLayoutOfTimeline.setVisibility(View.VISIBLE);
            holder.ivAttachment.setVisibility(View.GONE);
            holder.image.setVisibility(View.GONE);
        }

        holder.ivAttachment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (listener != null) {
                    listener.onClick();
                }
            }
        });

        if (isAssignmentEmpty) {
            holder.ivAttachment.setVisibility(View.GONE);
        }
    }

    @Override
    public int getItemCount() {
        return events.size();
    }


    public interface AttachmentButtonClickListener {
        void onClick();
    }
    class MyViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tv_username)
        TextView tvUsername;
        @BindView(R.id.tv_date)
        TextView tvDate;
        @BindView(R.id.tv_comment)
        TextView tvComment;
        @BindView(R.id.layout_of_timeline)
        RelativeLayout relativeLayoutOfTimeline;
        @BindView(R.id.line_connector)
        View lineConnector;
        @BindView(R.id.tv_title)
        TextView tvTitle;
        @BindView(R.id.profile_image)
        CircleImageView profileImage;
        @BindView(R.id.tv_eventdate)
        TextView tvEventDate;
        @BindView(R.id.iv_attachment)
        ImageView ivAttachment;
        @BindView(R.id.image)
        ImageView image;


        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
