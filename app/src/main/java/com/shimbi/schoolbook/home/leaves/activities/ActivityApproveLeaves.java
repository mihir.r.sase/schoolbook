package com.shimbi.schoolbook.home.leaves.activities;

import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;

import com.google.android.material.appbar.MaterialToolbar;
import com.shimbi.schoolbook.R;
import com.shimbi.schoolbook.home.leaves.adapters.ApproveLeaveAdapter;
import com.shimbi.schoolbook.home.leaves.models.LeaveAproveListResponseModel;
import com.shimbi.schoolbook.utils.Alerts;
import com.shimbi.schoolbook.utils.AuthModel;
import com.shimbi.schoolbook.utils.RetrofitClient;
import com.shimbi.schoolbook.utils.SaveSharedPreference;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ActivityApproveLeaves extends AppCompatActivity {

    private static final String TAG = "ActivityApproveLeaves";
    List<LeaveAproveListResponseModel> leaveList = new ArrayList<>();
    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;
    @BindView(R.id.llProgressBar)
    View llProgressBar;
    private String username;
    private String userEmail;
    private String userType;
    private String userId;
    private ApproveLeaveAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_approve_leaves);
        ButterKnife.bind(this);


        init();
        loaddata();
    }

    private void init() {


        AuthModel authModel = SaveSharedPreference.getData();
        userId = authModel.uid;
        username = authModel.username;
        userEmail = authModel.userEmail;
        userType = authModel.userType;


        adapter = new ApproveLeaveAdapter(leaveList, ActivityApproveLeaves.this);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(adapter);

        MaterialToolbar materialToolbar = findViewById(R.id.appbar);
        setSupportActionBar(materialToolbar);
        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
    }

    private void loaddata() {

        progressVisibility(true);
        Call<List<LeaveAproveListResponseModel>> call = RetrofitClient.get().getLeaveApporvalList(userId, userType);
        call.enqueue(new Callback<List<LeaveAproveListResponseModel>>() {
            @Override
            public void onResponse(Call<List<LeaveAproveListResponseModel>> call, Response<List<LeaveAproveListResponseModel>> response) {

                if (response.body() != null) {

                    List<LeaveAproveListResponseModel> list = response.body();
                    for (int i = 0; i < list.size(); i++) {
                        leaveList.add(list.get(i));
                    }
                    adapter.notifyDataSetChanged();

                } else {
                    Alerts.displayToast(getApplicationContext(), "Loading Failed");
                }
                progressVisibility(false);
            }

            @Override
            public void onFailure(Call<List<LeaveAproveListResponseModel>> call, Throwable t) {
                Log.e(TAG, "onFailure: ", t);
                progressVisibility(false);
                Alerts.displayToast(getApplicationContext(), "Loading Failed");
            }
        });
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle presses on the action bar items
        int id = item.getItemId();
        switch (id) {
            case android.R.id.home:
                finish();
                break;


        }


        return super.onOptionsItemSelected(item);
    }

    /**
     * Display progress bar and uses TOUCHABLE flags
     *
     * @param visibility Give True to display dialog, False to remove
     */
    private void progressVisibility(boolean visibility) {
        if (visibility) {
            llProgressBar.setVisibility(View.VISIBLE);
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                    WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
        } else {
            llProgressBar.setVisibility(View.GONE);
            getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
        }
    }
}
