package com.shimbi.schoolbook.home.report_card;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.ProgressBar;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.shimbi.schoolbook.BuildConfig;
import com.shimbi.schoolbook.R;
import com.shimbi.schoolbook.home.activities.ActivityHome;
import com.shimbi.schoolbook.utils.Alerts;
import com.shimbi.schoolbook.utils.AuthModel;
import com.shimbi.schoolbook.utils.Constants;
import com.shimbi.schoolbook.utils.NetworkStatusChecker;
import com.shimbi.schoolbook.utils.SaveSharedPreference;

/**
 * A simple {@link Fragment} subclass.
 */
public class FragmentReportCard extends Fragment {

    private String userType;
    private String userId;
    public ProgressBar syncProgress;
    public View llProgressBar;
    private ImageView noData;

    public FragmentReportCard() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        ActivityHome activityHome = (ActivityHome) getActivity();
        activityHome.setTitleAndLogo("Report Card", R.drawable.ic_report);
        ActivityHome.fab.setVisibility(View.GONE);
        if (getActivity() != null) {
            syncProgress = getActivity().findViewById(R.id.progress_sync);
            syncProgress.setVisibility(View.GONE);
            noData = getActivity().findViewById(R.id.no_data);
            noData.setVisibility(View.GONE);
        }

        return inflater.inflate(R.layout.fragment_report_card, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        AuthModel authModel = SaveSharedPreference.getData();
        userId = authModel.uid;
        userType = authModel.userType;
        if (getActivity() != null) {
            llProgressBar = getActivity().findViewById(R.id.llProgressBar);
        }
        WebView webView = view.findViewById(R.id.web_view);

        webView.getSettings().setJavaScriptEnabled(true);
        if (!NetworkStatusChecker.isNetworkConnected(getContext())) {
            webView.getSettings().setCacheMode(WebSettings.LOAD_DEFAULT);
            webView.getSettings().setAppCacheEnabled(true);
            progressVisibility(true);
            webView.setWebViewClient(new WebViewClient() {
                @Override
                public boolean shouldOverrideUrlLoading(WebView view, String url) {
                    view.loadUrl(url);
                    return true;
                }

                @Override
                public void onPageFinished(WebView view, String url) {
                    progressVisibility(false);

                }

                @Override
                public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
                    Alerts.displayToast(getContext(), "Loading Failed!");

                }
            });
            String baseUrl;
            if (BuildConfig.DEBUG) {

                if (SaveSharedPreference.getBaseUrl()) {
                    baseUrl = Constants.BASE_URL_RELEASE;
                } else {
                    baseUrl = Constants.BASE_URL_DEBUG;
                }
            } else {
                baseUrl = Constants.BASE_URL_RELEASE;
            }
            webView.loadUrl(baseUrl + "reportcard/show_reportcard_view/" + userId + "/" + userType);
        } else {
            Alerts.displayToast(getContext(), "Please check your connection and try again.");
        }
    }

    public void progressVisibility(boolean visibility) {

        if (getActivity() != null) {

            if (visibility) {
                llProgressBar.setVisibility(View.VISIBLE);
                getActivity().getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                        WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);

            } else {
                llProgressBar.setVisibility(View.GONE);
                getActivity().getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
            }
        }
    }
}
