package com.shimbi.schoolbook.home.assignments.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class AssignmentDetailsResponseModel {

    @SerializedName("syncdate")
    @Expose
    private String syncdate;
    @SerializedName("result")
    @Expose
    private AssignmentDetailsResultModel result;
    @SerializedName("post_data")
    @Expose
    private PostData postData;

    public PostData getPostData() {
        return postData;
    }

    public void setPostData(PostData postData) {
        this.postData = postData;
    }

    public String getSyncdate() {
        return syncdate;
    }

    public void setSyncdate(String syncdate) {
        this.syncdate = syncdate;
    }

    public AssignmentDetailsResultModel getResult() {
        return result;
    }

    public void setResult(AssignmentDetailsResultModel result) {
        this.result = result;
    }


    public class AssignmentDetailsResultModel {

        @SerializedName("post")
        @Expose
        private List<AssignmentDetailsPostModel> post = null;
        @SerializedName("attachment")
        @Expose
        private List<Attachment> attachment = null;

        public List<AssignmentDetailsPostModel> getPost() {
            return post;
        }

        public void setPost(List<AssignmentDetailsPostModel> post) {
            this.post = post;
        }

        public List<Attachment> getAttachment() {
            return attachment;
        }

        public void setAttachment(List<Attachment> attachment) {
            this.attachment = attachment;
        }


        public class AssignmentDetailsPostModel {


            @SerializedName("name")
            @Expose
            private String name;
            @SerializedName("date")
            @Expose
            private String date;
            @SerializedName("id")
            @Expose
            private String id;
            @SerializedName("type")
            @Expose
            private String type;
            @SerializedName("comment")
            @Expose
            private String comment;
            @SerializedName("image")
            @Expose
            private String image;

            private String title;

            private String submissionDate;

            public AssignmentDetailsPostModel(String cardUsername, String image, String cardDate, String cardDesc, String cardTitle, String cardSubmissionDate) {
                this.name = cardUsername;
                this.image = image;
                this.date = cardDate;
                this.comment = cardDesc;
                this.title = cardTitle;
                this.submissionDate = cardSubmissionDate;
            }

            public AssignmentDetailsPostModel(String name, String image, String date, String comment) {
                this.name = name;
                this.image = image;
                this.date = date;
                this.comment = comment;
            }

            public String getSubmissionDate() {
                return submissionDate;
            }

            public void setSubmissionDate(String submissionDate) {
                this.submissionDate = submissionDate;
            }

            public String getTitle() {
                return title;
            }

            public void setTitle(String title) {
                this.title = title;
            }

            @Override
            public String toString() {
                return "AssignmentDetailsPostModel{" +
                        "name='" + name + '\'' +
                        ", date='" + date + '\'' +
                        ", id='" + id + '\'' +
                        ", type='" + type + '\'' +
                        ", comment='" + comment + '\'' +
                        ", image='" + image + '\'' +
                        '}';
            }

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            public String getDate() {
                return date;
            }

            public void setDate(String date) {
                this.date = date;
            }

            public String getId() {
                return id;
            }

            public void setId(String id) {
                this.id = id;
            }

            public String getType() {
                return type;
            }

            public void setType(String type) {
                this.type = type;
            }

            public String getComment() {
                return comment;
            }

            public void setComment(String comment) {
                this.comment = comment;
            }

            public String getImage() {
                return image;
            }

            public void setImage(String image) {
                this.image = image;
            }
        }


        public class Attachment {

            @SerializedName("assignment_add_more_id")
            @Expose
            private String assignmentAddMoreId;
            @SerializedName("assignment_id")
            @Expose
            private String assignmentId;
            @SerializedName("file_original_name")
            @Expose
            private String fileOriginalName;
            @SerializedName("file_name")
            @Expose
            private String fileName;
            @SerializedName("file_info")
            @Expose
            private String fileInfo;
            @SerializedName("added_from")
            @Expose
            private String addedFrom;
            @SerializedName("download_link")
            @Expose
            private String downloadLink;

            public Attachment(Attachment attachment) {
                this.assignmentAddMoreId = attachment.getAssignmentAddMoreId();
                this.assignmentId = attachment.getAssignmentId();
                this.fileOriginalName = attachment.getFileOriginalName();
                this.fileName = attachment.getFileName();
                this.fileInfo = attachment.getFileInfo();
                this.addedFrom = attachment.getAddedFrom();
                this.downloadLink = attachment.getDownloadLink();
            }


            public String getAssignmentAddMoreId() {
                return assignmentAddMoreId;
            }

            public void setAssignmentAddMoreId(String assignmentAddMoreId) {
                this.assignmentAddMoreId = assignmentAddMoreId;
            }

            public String getAssignmentId() {
                return assignmentId;
            }

            public void setAssignmentId(String assignmentId) {
                this.assignmentId = assignmentId;
            }

            public String getFileOriginalName() {
                return fileOriginalName;
            }

            public void setFileOriginalName(String fileOriginalName) {
                this.fileOriginalName = fileOriginalName;
            }

            public String getFileName() {
                return fileName;
            }

            public void setFileName(String fileName) {
                this.fileName = fileName;
            }

            public String getFileInfo() {
                return fileInfo;
            }

            public void setFileInfo(String fileInfo) {
                this.fileInfo = fileInfo;
            }

            public String getAddedFrom() {
                return addedFrom;
            }

            public void setAddedFrom(String addedFrom) {
                this.addedFrom = addedFrom;
            }

            public String getDownloadLink() {
                return downloadLink;
            }

            public void setDownloadLink(String downloadLink) {
                this.downloadLink = downloadLink;
            }

            @Override
            public String toString() {
                return "Attachment{" +
                        "assignmentAddMoreId='" + assignmentAddMoreId + '\'' +
                        ", assignmentId='" + assignmentId + '\'' +
                        ", fileOriginalName='" + fileOriginalName + '\'' +
                        ", fileName='" + fileName + '\'' +
                        ", fileInfo='" + fileInfo + '\'' +
                        ", addedFrom='" + addedFrom + '\'' +
                        ", downloadLink='" + downloadLink + '\'' +
                        '}';
            }
        }

    }

    public class PostData {

        @SerializedName("no_reply")
        @Expose
        private Integer noReply;
        @SerializedName("event_title")
        @Expose
        private String eventTitle;
        @SerializedName("username")
        @Expose
        private String username;
        @SerializedName("description")
        @Expose
        private String description;
        @SerializedName("added_date")
        @Expose
        private String addedDate;
        @SerializedName("formatted_date")
        @Expose
        private String formattedDate;
        @SerializedName("submiton_date")
        @Expose
        private String submitonDate;
        @SerializedName("picture")
        @Expose
        private String picture;
        @SerializedName("userimage")
        @Expose
        private String userimage;
        @SerializedName("class_key")
        @Expose
        private String classKey;
        @SerializedName("class_value")
        @Expose
        private String classValue;
        @SerializedName("userid")
        @Expose
        private String userid;
        @SerializedName("event_date")
        @Expose
        private String eventDate;
        @SerializedName("added_user_id")
        @Expose
        private String addedUserId;
        @SerializedName("added_user_type")
        @Expose
        private String addedUserType;

        public String getAddedUserId() {
            return addedUserId;
        }

        public void setAddedUserId(String addedUserId) {
            this.addedUserId = addedUserId;
        }

        public String getAddedUserType() {
            return addedUserType;
        }

        public void setAddedUserType(String addedUserType) {
            this.addedUserType = addedUserType;
        }

        public String getEventDate() {
            return eventDate;
        }

        public void setEventDate(String eventDate) {
            this.eventDate = eventDate;
        }

        public Integer getNoReply() {
            return noReply;
        }

        public void setNoReply(Integer noReply) {
            this.noReply = noReply;
        }

        public String getEventTitle() {
            return eventTitle;
        }

        public void setEventTitle(String eventTitle) {
            this.eventTitle = eventTitle;
        }

        public String getUsername() {
            return username;
        }

        public void setUsername(String username) {
            this.username = username;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public String getAddedDate() {
            return addedDate;
        }

        public void setAddedDate(String addedDate) {
            this.addedDate = addedDate;
        }

        public String getFormattedDate() {
            return formattedDate;
        }

        public void setFormattedDate(String formattedDate) {
            this.formattedDate = formattedDate;
        }

        public String getSubmitonDate() {
            return submitonDate;
        }

        public void setSubmitonDate(String submitonDate) {
            this.submitonDate = submitonDate;
        }

        public String getPicture() {
            return picture;
        }

        public void setPicture(String picture) {
            this.picture = picture;
        }

        public String getUserimage() {
            return userimage;
        }

        public void setUserimage(String userimage) {
            this.userimage = userimage;
        }

        public String getClassKey() {
            return classKey;
        }

        public void setClassKey(String classKey) {
            this.classKey = classKey;
        }

        public String getClassValue() {
            return classValue;
        }

        public void setClassValue(String classValue) {
            this.classValue = classValue;
        }

        public String getUserid() {
            return userid;
        }

        public void setUserid(String userid) {
            this.userid = userid;
        }
    }

}
