package com.shimbi.schoolbook.home.notice.adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.shimbi.schoolbook.R;
import com.shimbi.schoolbook.home.notice.activities.ActivityNoticeDetails;
import com.shimbi.schoolbook.home.notice.models.NoticeResponseModel;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

public class NoticeAdapter extends RecyclerView.Adapter<NoticeAdapter.MyViewHolder> {

    private static final String TAG = "NoticeAdapter";
    Context context;
    private List<NoticeResponseModel.NoticeResultModel> noticeList;

    public NoticeAdapter(List<NoticeResponseModel.NoticeResultModel> noticeList) {
        this.noticeList = noticeList;
    }


    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_notice_list, parent, false);
        context = parent.getContext();
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        NoticeResponseModel.NoticeResultModel noticeDetails = noticeList.get(position);
        holder.tvUsername.setText(noticeDetails.getUsername());
        holder.tvDate.setText(noticeDetails.getAddedDate());
        holder.tvTitle.setText(noticeDetails.getEventTitle());
        holder.tvDesc.setText(noticeDetails.getDescription());


        Glide.with(context).load(noticeDetails.getUserimage()).centerCrop()
                .placeholder(R.drawable.default_profile)
                .into(holder.profileImage);

        holder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(view.getContext(), ActivityNoticeDetails.class);


                intent.putExtra("eventId", noticeDetails.getEventId());
                view.getContext().startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return noticeList.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tv_username)
        TextView tvUsername;
        @BindView(R.id.tv_date)
        TextView tvDate;
        @BindView(R.id.tv_title)
        TextView tvTitle;
        @BindView(R.id.tv_desc)
        TextView tvDesc;
        @BindView(R.id.notice_card)
        CardView cardView;
        @BindView(R.id.profile_image)
        CircleImageView profileImage;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
