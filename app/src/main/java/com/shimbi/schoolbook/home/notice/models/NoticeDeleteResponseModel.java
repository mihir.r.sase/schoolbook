package com.shimbi.schoolbook.home.notice.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class NoticeDeleteResponseModel {

    @SerializedName("event_id")
    @Expose
    private String eventId;
    @SerializedName("result")
    @Expose
    private Integer result;

    public String getEventId() {
        return eventId;
    }

    public void setEventId(String eventId) {
        this.eventId = eventId;
    }

    public Integer getResult() {
        return result;
    }

    public void setResult(Integer result) {
        this.result = result;
    }
}
