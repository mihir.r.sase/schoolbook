package com.shimbi.schoolbook.home.transport.fragments;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.ProgressBar;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.appbar.MaterialToolbar;
import com.shimbi.schoolbook.R;
import com.shimbi.schoolbook.home.activities.ActivityHome;
import com.shimbi.schoolbook.utils.AuthModel;
import com.shimbi.schoolbook.utils.SaveSharedPreference;

/**
 * A simple {@link Fragment} subclass.
 */
public class FragmentTransport extends Fragment {


    static MyPagerAdapter adapter;
    private String userType;
    private String userId;
    static ViewPager mViewPager;
    public ProgressBar syncProgress;
    private ImageView noData;

    public FragmentTransport() {
        // Required empty public constructor
    }

    public static void changeFragment(int pos) {

        mViewPager.setCurrentItem(pos);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        ActivityHome activityHome = (ActivityHome) getActivity();
        activityHome.setTitleAndLogo("Transport", R.drawable.ic_transport_black_24dp);
        ActivityHome.fab.setVisibility(View.GONE);
        if (getActivity() != null) {
            syncProgress = getActivity().findViewById(R.id.progress_sync);
            syncProgress.setVisibility(View.GONE);
            noData = getActivity().findViewById(R.id.no_data);
            noData.setVisibility(View.GONE);
        }

        if (getActivity() != null) {
            getActivity().getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        }

        return inflater.inflate(R.layout.fragment_transport, container, false);
    }

    private class MyPagerAdapter extends FragmentPagerAdapter {


        public MyPagerAdapter(@NonNull FragmentManager fm, int behavior) {
            super(fm, behavior);
        }

        @Override
        public Fragment getItem(int pos) {
            switch (pos) {

                case 0:
                    return new FragmentAdvance();
                case 1:
                    return new FragmentReturn();
                default:
                    return new FragmentAdvance();
            }
        }

        @Override
        public int getCount() {
            return 2;
        }
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (getActivity() != null) {
            MaterialToolbar materialToolbar = getActivity().findViewById(R.id.appbar);
            AppBarLayout.LayoutParams p = (AppBarLayout.LayoutParams) materialToolbar.getLayoutParams();
            p.setScrollFlags(0);
            materialToolbar.setLayoutParams(p);
        }
        ActivityHome.fab.setVisibility(View.GONE);
        mViewPager = view.findViewById(R.id.view_pager);

        AuthModel authModel = SaveSharedPreference.getData();
        userId = authModel.uid;
        userType = authModel.userType;

        adapter = new MyPagerAdapter(getChildFragmentManager(), FragmentPagerAdapter.BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT);
        if (!userType.equals("3")) {
            mViewPager.setAdapter(adapter);
        } else {
            Fragment fragment = null;
            try {
                fragment = FragmentTransportStudents.class.newInstance();
                ((FragmentActivity) getContext()).getSupportFragmentManager().beginTransaction()
                        .replace(R.id.flContent, fragment, fragment.getTag())
                        .commit();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }


    }

    @Override
    public void onStop() {
        super.onStop();
        if (getActivity() != null) {
            getActivity().getWindow().clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        }
    }
}
