package com.shimbi.schoolbook.home.transport.fragments;


import android.Manifest;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.location.Location;
import android.media.MediaPlayer;
import android.os.Build;
import android.os.Bundle;
import android.os.Looper;
import android.os.VibrationEffect;
import android.os.Vibrator;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.ResolvableApiException;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResponse;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.tasks.Task;
import com.google.android.material.button.MaterialButton;
import com.shimbi.schoolbook.R;
import com.shimbi.schoolbook.db.helper.MyDataBase;
import com.shimbi.schoolbook.db.tables.DbEmbarkedUsers;
import com.shimbi.schoolbook.db.tables.DbPickupLocations;
import com.shimbi.schoolbook.db.tables.DbRouteDetails;
import com.shimbi.schoolbook.db.tables.DbTransportSync;
import com.shimbi.schoolbook.db.tables.DbTransportUsers;
import com.shimbi.schoolbook.db.tables.DbYetToBeEmbarked;
import com.shimbi.schoolbook.home.transport.adapters.TransportDialogAdapter;
import com.shimbi.schoolbook.home.transport.models.NotificationSuccessModel;
import com.shimbi.schoolbook.services.TransportService;
import com.shimbi.schoolbook.utils.Alerts;
import com.shimbi.schoolbook.utils.AuthModel;
import com.shimbi.schoolbook.utils.ConnectionStateMonitor;
import com.shimbi.schoolbook.utils.RetrofitClient;
import com.shimbi.schoolbook.utils.SaveSharedPreference;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class FragmentReturn extends Fragment {


    private static final String TAG = "FragmentReturn";
    private static final int RADIUS = 200;

    @BindView(R.id.bt_end_journey)
    MaterialButton btEndJourney;
    @BindView(R.id.bt_embark)
    MaterialButton btEmbark;
    @BindView(R.id.bt_disembark)
    MaterialButton btDisembark;
    @BindView(R.id.tv_time1)
    TextView tvTime1;
    @BindView(R.id.tv_am1)
    TextView tvAm1;
    @BindView(R.id.tv_time3)
    TextView tvTime3;
    @BindView(R.id.tv_am3)
    TextView tvAm3;
    @BindView(R.id.circle_1)
    ImageView circle1;
    @BindView(R.id.circle_2)
    ImageView circle2;
    @BindView(R.id.circle_3)
    ImageView circle3;
    SimpleDateFormat formatter1 = new SimpleDateFormat("hh:mm");
    SimpleDateFormat formatter2 = new SimpleDateFormat("a");
    private List<DbRouteDetails> routeDetailsList = new ArrayList<>();
    private List<DbPickupLocations> pickupLocationsList = new ArrayList<>();
    private List<DbTransportUsers> transportUsersList = new ArrayList<>();
    private List<DbEmbarkedUsers> embarkedUsersList = new ArrayList<>();
    private List<DbYetToBeEmbarked> yetToBeEmbarkedList = new ArrayList<>();
    private int startPickupId, endPickupId;
    private int currentPickupId;
    private String markerTitle;
    private String username;
    private String userEmail;
    private String userType;
    private String userId;
    private Integer journeyStatus;
    private Integer routeId;
    private Integer busId;
    private MediaPlayer mediaPlayer;
    private Vibrator vibrator;
    private FusedLocationProviderClient mFusedLocationClient;
    private LocationRequest locationRequest;
    private LocationCallback locationCallback;
    public View llProgressBar;

    public FragmentReturn() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_return, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);

        init();

        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(getActivity());
        locationRequest = LocationRequest.create();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationRequest.setInterval(8 * 1000);
        locationCallback = new LocationCallback() {
            @Override
            public void onLocationResult(LocationResult locationResult) {

                if (locationResult == null) {
                    return;
                }
                for (Location location : locationResult.getLocations()) {
                    if (location != null) {
                        onLocationChanged(location.getLatitude(), location.getLongitude(), location);
                    }
                }
            }
        };

        //checkForLocationPermission();


//        getRouteDetailsFromLocal();
//        getTransportDataFromLocal();
//        getEmbarkStatusDataFromLocal();

        btEmbark.setOnClickListener(v -> embarkStudents());

        btDisembark.setOnClickListener(v -> disembarkStudents());
        btEndJourney.setOnClickListener(v -> endJourney());

        if (SaveSharedPreference.getJorneyStatus() >= 4) {

            ConnectionStateMonitor connectionStateMonitor = new ConnectionStateMonitor(getActivity());
            connectionStateMonitor.observe(this, new Observer<Boolean>() {
                @Override
                public void onChanged(Boolean aBoolean) {
                    if (aBoolean) {
                        Alerts.displayToast(getContext(), "Connected");
                        List<DbTransportSync> list = MyDataBase.db.getTransportSync();
                        if (!list.isEmpty()) {
                            for (int i = 0; i < list.size(); i++) {
                                sendNotificationSync(list.get(i));
                            }
                        }
                    } else {
                        // network lost
                        Alerts.displayToast(getContext(), "Disconnected");
                    }
                }
            });
        }
    }

    private void init() {

        AuthModel authModel = SaveSharedPreference.getData();
        userId = authModel.uid;
        username = authModel.username;
        userEmail = authModel.userEmail;
        userType = authModel.userType;

        mediaPlayer = MediaPlayer.create(getContext(), R.raw.notification);
        vibrator = (Vibrator) getActivity().getSystemService(Context.VIBRATOR_SERVICE);

        if (getActivity() != null) {
            llProgressBar = getActivity().findViewById(R.id.llProgressBar);
        }

        journeyStatus = SaveSharedPreference.getJorneyStatus();
        if (journeyStatus == 4) {
            btEndJourney.setEnabled(false);
            btEmbark.setEnabled(true);
            btDisembark.setEnabled(false);
            circle1.setImageResource(R.drawable.circle);
            circle2.setImageResource(R.drawable.circle_border);
            circle3.setImageResource(R.drawable.circle_border);
        } else if (journeyStatus == 5) {
            btEndJourney.setEnabled(false);
            btEmbark.setEnabled(false);
            btDisembark.setEnabled(false);
            circle1.setImageResource(R.drawable.circle_border);
            circle2.setImageResource(R.drawable.circle);
            circle3.setImageResource(R.drawable.circle_border);
        } else if (journeyStatus == 6) {
            btEndJourney.setEnabled(false);
            btEmbark.setEnabled(false);
            btDisembark.setEnabled(false);
            circle1.setImageResource(R.drawable.circle_border);
            circle2.setImageResource(R.drawable.circle_border);
            circle3.setImageResource(R.drawable.circle);
        } else {
            btEndJourney.setEnabled(false);
            btEmbark.setEnabled(false);
            btDisembark.setEnabled(false);
            circle1.setImageResource(R.drawable.circle_border);
            circle2.setImageResource(R.drawable.circle_border);
            circle3.setImageResource(R.drawable.circle_border);
        }
    }


    private void onLocationChanged(double latitude, double longitude, Location location) {

        float distance;
        Log.e(TAG, "onLocationChanged: " + embarkedUsersList.size());
        for (int i = 0; i < pickupLocationsList.size(); i++) {
            if (pickupLocationsList.get(i).getLatitude() == null || pickupLocationsList.get(i).getLongitude() == null) {
                break;
            }
            Location location1 = new Location("");
            location1.setLatitude(Double.parseDouble(pickupLocationsList.get(i).getLatitude()));
            location1.setLongitude(Double.parseDouble(pickupLocationsList.get(i).getLongitude()));
            distance = location.distanceTo(location1);

            if (distance < RADIUS && SaveSharedPreference.getJorneyStatus() > 3 && SaveSharedPreference.getJorneyStatus() != 6) {
                currentPickupId = pickupLocationsList.get(i).getPickupId();
                markerTitle = pickupLocationsList.get(i).getPickupName();
                if (currentPickupId == endPickupId) {
                    // Radius entry of end stop
                    if (embarkedUsersList.isEmpty() && SaveSharedPreference.getJorneyStatus() == 4) {
                        btEndJourney.setEnabled(false);
                        btEmbark.setEnabled(true);
                        btDisembark.setEnabled(false);
                        circle1.setImageResource(R.drawable.circle);
                        circle2.setImageResource(R.drawable.circle_border);
                        circle3.setImageResource(R.drawable.circle_border);
                    }
                } else {
                    // Radius entry of pickup points other than end stop

                    //handle radius entry operations
                    if (!MyDataBase.db.hasVisitedBusStop(currentPickupId)) {
                        MyDataBase.db.updateVisitedBusStop(currentPickupId, true);
                        mediaPlayer.start();
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                            vibrator.vibrate(VibrationEffect.createOneShot(3000, VibrationEffect.DEFAULT_AMPLITUDE));
                        } else {
                            vibrator.vibrate(3000);
                        }
                        sendRadiusEntryNotification(currentPickupId, markerTitle, "transport");
                    }

                    List<DbYetToBeEmbarked> currentStudentList = MyDataBase.db.getYetToBeEmbarked(currentPickupId, routeId);
                    if (currentStudentList.isEmpty())
                        btDisembark.setEnabled(false);
                    else
                        btDisembark.setEnabled(true);
                    btEndJourney.setEnabled(false);
                    btEmbark.setEnabled(false);
                    circle1.setImageResource(R.drawable.circle_border);
                    circle2.setImageResource(R.drawable.circle);
                    circle3.setImageResource(R.drawable.circle_border);

                }
                break;
            } else {
                btEmbark.setEnabled(false);
                btDisembark.setEnabled(false);
                currentPickupId = -1;
                markerTitle = "";

            }
        }
        if (SaveSharedPreference.getJorneyStatus() > 4 && !embarkedUsersList.isEmpty() && SaveSharedPreference.getJorneyStatus() != 6) {
            btEndJourney.setEnabled(true);
        } else {
            btEndJourney.setEnabled(false);
        }
    }

    private void embarkStudents() {

        List<DbYetToBeEmbarked> studentList = new ArrayList<>();
        for (int i = 0; i < transportUsersList.size(); i++) {
            studentList.add(new DbYetToBeEmbarked(transportUsersList.get(i)));
        }

        Dialog dialog = new Dialog(getActivity(), R.style.DialogStyle);
        dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        dialog.setContentView(R.layout.dialog_transport_select_students);
        dialog.setCanceledOnTouchOutside(true);
        dialog.setCancelable(true);
        dialog.show();

        TransportDialogAdapter adapter = new TransportDialogAdapter(studentList);
        adapter.setSelected(true);
        RecyclerView recyclerView = dialog.findViewById(R.id.recycler_view);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.setAdapter(adapter);

        MaterialButton btSelect = dialog.findViewById(R.id.bt_select);
        MaterialButton btCancel = dialog.findViewById(R.id.bt_cancel);
        btSelect.setOnClickListener(v -> {
            List<DbYetToBeEmbarked> selectedStudentList = adapter.getSelectedStudentList();
            if (selectedStudentList.isEmpty()) {
                Alerts.displayToast(getContext(), "Student List cannot be empty");
                return;
            }


            for (int i = 0; i < selectedStudentList.size(); i++) {
                MyDataBase.db.insertToYetToBeEmbarked(new DbYetToBeEmbarked(selectedStudentList.get(i)));
                yetToBeEmbarkedList = MyDataBase.db.getYetToBeEmbarkedRouteId(routeId);
            }
            if (markerTitle != null) {
                sendEmbarkDisEmbarkNotification(selectedStudentList, markerTitle, "Return_Embark");
            } else {
                dialog.dismiss();
                return;
            }
            SaveSharedPreference.setJourneyStatus(5);
            MyDataBase.db.updateJourneyStatuse(5, routeId);
            btEndJourney.setEnabled(false);
            btEmbark.setEnabled(false);
            btDisembark.setEnabled(false);
            circle1.setImageResource(R.drawable.circle_border);
            circle2.setImageResource(R.drawable.circle);
            circle3.setImageResource(R.drawable.circle_border);

            Date date = new Date();
            tvTime1.setVisibility(View.VISIBLE);
            tvAm1.setVisibility(View.VISIBLE);
            tvTime1.setText(formatter1.format(date));
            tvAm1.setText(formatter2.format(date));
            MyDataBase.db.updateSchoolEmbarkTime(date, routeId);

            dialog.dismiss();
        });

        btCancel.setOnClickListener(v -> dialog.dismiss());
    }

    private void disembarkStudents() {

        List<DbYetToBeEmbarked> currentStudentList = MyDataBase.db.getYetToBeEmbarked(currentPickupId, routeId);

        Dialog dialog = new Dialog(getActivity(), R.style.DialogStyle);
        dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        dialog.setContentView(R.layout.dialog_transport_select_students);
        dialog.setCanceledOnTouchOutside(true);
        dialog.setCancelable(true);
        dialog.show();

        TransportDialogAdapter adapter = new TransportDialogAdapter(currentStudentList);
        RecyclerView recyclerView = dialog.findViewById(R.id.recycler_view);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.setAdapter(adapter);
        MaterialButton btSelect = dialog.findViewById(R.id.bt_select);
        MaterialButton btCancel = dialog.findViewById(R.id.bt_cancel);
        btSelect.setOnClickListener(v -> {
            List<DbYetToBeEmbarked> selectedStudentList = adapter.getSelectedStudentList();
            for (int i = 0; i < selectedStudentList.size(); i++) {
                MyDataBase.db.insertToEmbarkedUsers(new DbEmbarkedUsers(selectedStudentList.get(i)));
                MyDataBase.db.deleteFromYetToBeEmbarked(selectedStudentList.get(i).getStudentId(), selectedStudentList.get(i).getUserType());
                embarkedUsersList = MyDataBase.db.getEmbarkedUsers(routeId);
                yetToBeEmbarkedList = MyDataBase.db.getYetToBeEmbarkedRouteId(routeId);
            }
            sendEmbarkDisEmbarkNotification(selectedStudentList, markerTitle, "Return_DisEmbark");
            dialog.dismiss();
        });
        btCancel.setOnClickListener(v -> dialog.dismiss());
    }

    private void endJourney() {

        Dialog dialog = new Dialog(getActivity(), R.style.DialogStyle);
        dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        dialog.setContentView(R.layout.dialog_transport_select_students);
        dialog.setCanceledOnTouchOutside(false);
        dialog.setCancelable(false);
        dialog.show();

        TransportDialogAdapter adapter = new TransportDialogAdapter(yetToBeEmbarkedList);
        RecyclerView recyclerView = dialog.findViewById(R.id.recycler_view);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.setAdapter(adapter);

        MaterialButton btSelect = dialog.findViewById(R.id.bt_select);
        MaterialButton btCancel = dialog.findViewById(R.id.bt_cancel);
        TextView tvTitle = dialog.findViewById(R.id.tv_title);
        tvTitle.setText("Remaining Students");
        btSelect.setText("End");
        btSelect.setOnClickListener(v -> {
            List<DbYetToBeEmbarked> selectedStudentList = adapter.getSelectedStudentList();
            for (int i = 0; i < selectedStudentList.size(); i++) {
                MyDataBase.db.insertToEmbarkedUsers(new DbEmbarkedUsers(selectedStudentList.get(i)));
                MyDataBase.db.deleteFromYetToBeEmbarked(selectedStudentList.get(i).getStudentId(), selectedStudentList.get(i).getUserType());
                embarkedUsersList = MyDataBase.db.getEmbarkedUsers(routeId);
                yetToBeEmbarkedList = MyDataBase.db.getYetToBeEmbarkedRouteId(routeId);
            }
            sendEmbarkDisEmbarkNotification(selectedStudentList, "Your Location", "Return_DisEmbark");
            dialog.dismiss();
            SaveSharedPreference.setJourneyStatus(6);
            MyDataBase.db.updateJourneyStatuse(6, routeId);
            btEndJourney.setEnabled(false);
            btEmbark.setEnabled(false);
            btDisembark.setEnabled(false);
            circle1.setImageResource(R.drawable.circle_border);
            circle2.setImageResource(R.drawable.circle_border);
            circle3.setImageResource(R.drawable.circle);

            Date date = new Date();
            tvTime3.setVisibility(View.VISIBLE);
            tvAm3.setVisibility(View.VISIBLE);
            tvTime3.setText(formatter1.format(date));
            tvAm3.setText(formatter2.format(date));
            MyDataBase.db.updateJourneyEndTime(date, routeId);

        });
        btCancel.setOnClickListener(v -> dialog.dismiss());

    }


    private void sendRadiusEntryNotification(int pickupId, String markerTitle, String module) {

        progressVisibility(true);

        DateFormat df = new SimpleDateFormat("yyyy-mm-dd HH:mm:ss");
        Date date = new Date();
        Call<NotificationSuccessModel> call = RetrofitClient.get().sendNotificationFromDriver(pickupId, markerTitle, routeId, busId, module, df.format(date));
        call.enqueue(new Callback<NotificationSuccessModel>() {
            @Override
            public void onResponse(Call<NotificationSuccessModel> call, Response<NotificationSuccessModel> response) {

                if (response.body() != null) {
                    NotificationSuccessModel model = response.body();
                    Alerts.displayToast(getContext(), model.getMsg());
                } else {
                    Alerts.displayToast(getContext(), "Notification sent successfully!");
                }
                progressVisibility(false);
            }

            @Override
            public void onFailure(Call<NotificationSuccessModel> call, Throwable t) {
                progressVisibility(false);
                Alerts.displayToast(getContext(), "Notification Sending Failed!");
                Log.e(TAG, "onFailure: ", t);
            }
        });

    }

    private void sendEmbarkDisEmbarkNotification(List<DbYetToBeEmbarked> selectedStudentList, String markerTitle, String flag) {


        DateFormat df = new SimpleDateFormat("yyyy-mm-dd HH:mm:ss");
        Date date = new Date();

        StringBuilder attend = new StringBuilder();
        StringBuilder absent = new StringBuilder();
        String delimeter = "";

        for (int i = 0; i < selectedStudentList.size(); i++) {
            if (selectedStudentList.get(i).getUserType() == 3) {
                attend.append(delimeter);
                attend.append(selectedStudentList.get(i).getStudentId());
                delimeter = ",";
            }
        }

        if (markerTitle.equals("Your Location")) {
            List<DbYetToBeEmbarked> remainingStudentList = MyDataBase.db.getYetToBeEmbarkedRouteId(routeId);
            delimeter = "";
            for (int i = 0; i < remainingStudentList.size(); i++) {
                if (remainingStudentList.get(i).getUserType() == 3) {
                    absent.append(delimeter);
                    absent.append(remainingStudentList.get(i).getStudentId());
                    delimeter = ",";
                }
            }

        }

        MyDataBase.db.insertToTransportSync(new DbTransportSync(markerTitle, routeId, busId, attend.toString(), absent.toString(), flag, df.format(date)));
        progressVisibility(true);
        Call<NotificationSuccessModel> call = RetrofitClient.get().sendStudentNotification(markerTitle, routeId, busId, attend.toString(), absent.toString(), flag, df.format(date));
        call.enqueue(new Callback<NotificationSuccessModel>() {
            @Override
            public void onResponse(Call<NotificationSuccessModel> call, Response<NotificationSuccessModel> response) {
                if (response.body() != null) {
                    NotificationSuccessModel model = response.body();
                    Alerts.displayToast(getContext(), model.getMsg());
                } else {
                    Alerts.displayToast(getContext(), "Notification Sending Failed!");
                }
                try {
                    MyDataBase.db.deleteFromTransportSync(df.format(date));
                } catch (Exception e) {
                    Log.e(TAG, "onResponse: ", e);
                }
                progressVisibility(false);
            }

            @Override
            public void onFailure(Call<NotificationSuccessModel> call, Throwable t) {
                progressVisibility(false);
                Alerts.displayToast(getContext(), "Notification Sending Failed!");
                Log.e(TAG, "onFailure: ", t);
            }
        });
    }

    private void sendNotificationSync(DbTransportSync dbTransportSync) {

        progressVisibility(true);
        Call<NotificationSuccessModel> call = RetrofitClient.get().sendStudentNotification(dbTransportSync.getMarkerTitle(), dbTransportSync.getRouteId()
                , dbTransportSync.getBusId(), dbTransportSync.getAttend(), dbTransportSync.getAbsent(), dbTransportSync.getFlag(), dbTransportSync.getDateTime());
        call.enqueue(new Callback<NotificationSuccessModel>() {
            @Override
            public void onResponse(Call<NotificationSuccessModel> call, Response<NotificationSuccessModel> response) {
                if (response.body() != null) {
                    NotificationSuccessModel model = response.body();
                    Alerts.displayToast(getContext(), model.getMsg());
                } else {
                    Alerts.displayToast(getContext(), "Notification Sending Failed!");
                }
                try {
                    MyDataBase.db.deleteFromTransportSync(dbTransportSync.getDateTime());
                } catch (Exception e) {
                    Log.e(TAG, "onResponse: ", e);
                }
                progressVisibility(false);
            }

            @Override
            public void onFailure(Call<NotificationSuccessModel> call, Throwable t) {
                progressVisibility(false);
                Alerts.displayToast(getContext(), "Notification Sending Failed!");
                Log.e(TAG, "onFailure: ", t);
            }
        });
    }

    private void getRouteDetailsFromLocal() {

        routeDetailsList = MyDataBase.db.getRouteDetails();
        routeId = SaveSharedPreference.getRouteId();
        busId = SaveSharedPreference.getBusId();
        int status = SaveSharedPreference.getJorneyStatus();
        if (status > 4) {
            Date startTime = MyDataBase.db.getSchoolEmbarkTime(routeId);
            tvTime1.setVisibility(View.VISIBLE);
            tvAm1.setVisibility(View.VISIBLE);
            tvTime1.setText(formatter1.format(startTime));
            tvAm1.setText(formatter2.format(startTime));
        }
        if (status == 6) {
            Date endTime = MyDataBase.db.getJourneyEndTime(routeId);
            tvTime3.setVisibility(View.VISIBLE);
            tvAm3.setVisibility(View.VISIBLE);
            tvTime3.setText(formatter1.format(endTime));
            tvAm3.setText(formatter2.format(endTime));
        }
    }

    private void getTransportDataFromLocal() {

        pickupLocationsList = MyDataBase.db.getPickupLocations(routeId);
        transportUsersList = MyDataBase.db.getTransportUsers(routeId);
        startPickupId = SaveSharedPreference.getStartPickupId();
        endPickupId = SaveSharedPreference.getEndPickupId();


    }

    private void getEmbarkStatusDataFromLocal() {

        embarkedUsersList = MyDataBase.db.getEmbarkedUsers(routeId);
        yetToBeEmbarkedList = MyDataBase.db.getYetToBeEmbarkedRouteId(routeId);
    }

    private void checkForLocationPermission() {

        if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            // requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION}, 1000);
        } else {
            if (journeyStatus != 1) {
                turnOnGps();
                mFusedLocationClient.requestLocationUpdates(locationRequest, locationCallback, Looper.getMainLooper());
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        switch (requestCode) {
            case 1000: {
                // If request is cancelled, the result arrays are empty.

                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if (journeyStatus != 1) {
                        turnOnGps();
                        mFusedLocationClient.requestLocationUpdates(locationRequest, locationCallback, Looper.getMainLooper());
                    }
                } else {
                    Alerts.displayToast(getContext(), "Permission Denied");
                    getActivity().finish();
                }
                break;
            }
        }
    }

    private void turnOnGps() {
        LocationSettingsRequest.Builder settingsBuilder = new LocationSettingsRequest.Builder()
                .addLocationRequest(locationRequest);
        settingsBuilder.setAlwaysShow(true);
        Task<LocationSettingsResponse> result = LocationServices.getSettingsClient(getActivity())
                .checkLocationSettings(settingsBuilder.build());


        result.addOnCompleteListener(task -> {
            try {
                LocationSettingsResponse response =
                        task.getResult(ApiException.class);
            } catch (ApiException ex) {
                switch (ex.getStatusCode()) {
                    case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                        try {
                            ResolvableApiException resolvableApiException =
                                    (ResolvableApiException) ex;
                            resolvableApiException
                                    .startResolutionForResult(getActivity(),
                                            200);
                            //onActivity result to candle gps is in ActivityHome.java
                        } catch (IntentSender.SendIntentException e) {
                            Log.e(TAG, "turnOnGps: ", e);
                        }
                        break;
                    case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:

                        break;
                }
            }
        });
    }

    public void progressVisibility(boolean visibility) {

        if (getActivity() != null) {

            if (visibility) {
                llProgressBar.setVisibility(View.VISIBLE);
                getActivity().getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                        WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);

            } else {
                llProgressBar.setVisibility(View.GONE);
                getActivity().getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
            }
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        journeyStatus = SaveSharedPreference.getJorneyStatus();
        init();
        if (journeyStatus != 1) {
            turnOnGps();
            mFusedLocationClient.requestLocationUpdates(locationRequest, locationCallback, Looper.getMainLooper());
        }
        startPickupId = SaveSharedPreference.getStartPickupId();
        endPickupId = SaveSharedPreference.getEndPickupId();
        getRouteDetailsFromLocal();
        getTransportDataFromLocal();
        getEmbarkStatusDataFromLocal();

        if (getActivity() != null) {
            Intent serviceIntent = new Intent(getContext(), TransportService.class);
            getActivity().stopService(serviceIntent);
        }

    }

    @Override
    public void onStart() {
        super.onStart();
        journeyStatus = SaveSharedPreference.getJorneyStatus();
        init();
        if (journeyStatus != 1) {
            turnOnGps();
            mFusedLocationClient.requestLocationUpdates(locationRequest, locationCallback, Looper.getMainLooper());
        }
        startPickupId = SaveSharedPreference.getStartPickupId();
        endPickupId = SaveSharedPreference.getEndPickupId();
        getRouteDetailsFromLocal();
        getTransportDataFromLocal();
        getEmbarkStatusDataFromLocal();
    }

    @Override
    public void onStop() {
        super.onStop();
        int js = SaveSharedPreference.getJorneyStatus();
        if (js == 5) {
            if (getContext() != null) {
                mFusedLocationClient.removeLocationUpdates(locationCallback);
                Intent serviceIntent = new Intent(getContext(), TransportService.class);
                serviceIntent.putExtra("journeyStatus", js);
                serviceIntent.putExtra("routeId", SaveSharedPreference.getRouteId());
                ContextCompat.startForegroundService(getContext(), serviceIntent);
            }
        }

    }
}
