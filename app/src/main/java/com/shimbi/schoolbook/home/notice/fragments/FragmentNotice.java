package com.shimbi.schoolbook.home.notice.fragments;


import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;

import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.work.Constraints;
import androidx.work.Data;
import androidx.work.OneTimeWorkRequest;
import androidx.work.WorkInfo;
import androidx.work.WorkManager;

import com.shimbi.schoolbook.R;
import com.shimbi.schoolbook.db.helper.MyDataBase;
import com.shimbi.schoolbook.db.tables.DbNotice;
import com.shimbi.schoolbook.home.activities.ActivityHome;
import com.shimbi.schoolbook.home.notice.activities.ActivityNewNotice;
import com.shimbi.schoolbook.home.notice.adapters.NoticeAdapter;
import com.shimbi.schoolbook.home.notice.models.NoticeResponseModel;
import com.shimbi.schoolbook.utils.AuthModel;
import com.shimbi.schoolbook.utils.Constants;
import com.shimbi.schoolbook.utils.NetworkStatusChecker;
import com.shimbi.schoolbook.utils.SaveSharedPreference;
import com.shimbi.schoolbook.utils.workers.FetchNoticesWorker;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * A simple {@link Fragment} subclass.
 */
public class FragmentNotice extends Fragment {


    private static final String TAG = "FragmentNotice";
    View v;
    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;
    @BindView(R.id.swipe_container)
    SwipeRefreshLayout swipeRefreshLayout;
    private String username;
    private String userEmail;
    private String userType;
    private String userId;
    private Integer studentCount;
    private NoticeAdapter noticeAdapter;
    private LinearLayoutManager layoutManager;
    private List<NoticeResponseModel.NoticeResultModel> noticeList = new ArrayList<>();
    public ProgressBar syncProgress;
    public ImageView noData;

    public FragmentNotice() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        v = inflater.inflate(R.layout.fragment_notice, container, false);

        ActivityHome activityHome = (ActivityHome) getActivity();
        activityHome.setTitleAndLogo("Notices", R.drawable.ic_notice_black_24dp);
        ActivityHome.fab.setVisibility(View.GONE);

        if (getActivity() != null) {
            syncProgress = getActivity().findViewById(R.id.progress_sync);
            syncProgress.setVisibility(View.GONE);
            noData = getActivity().findViewById(R.id.no_data);
            noData.setVisibility(View.GONE);
        }

        return inflater.inflate(R.layout.fragment_notice, container, false);
    }


    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {

        ButterKnife.bind(this, view);
        init();


        loadDataFromLocal();


        loadDataOnline();


        ActivityHome.fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getContext(), ActivityNewNotice.class));
            }
        });

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                loadDataOnline();
                swipeRefreshLayout.setRefreshing(false);
            }
        });


    }


    private void init() {
        AuthModel authModel = SaveSharedPreference.getData();
        userId = authModel.uid;
        username = authModel.username;
        userEmail = authModel.userEmail;
        userType = authModel.userType;
        studentCount = authModel.studentCount;
        Log.e(TAG, "init: " + userId + " " + userType);

        noticeAdapter = new NoticeAdapter(noticeList);
        layoutManager = new LinearLayoutManager(this.getActivity());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(noticeAdapter);


        if (ActivityHome.noticeAddFlag == 1) {
            ActivityHome.fab.setVisibility(View.VISIBLE);
        } else {
            ActivityHome.fab.setVisibility(View.GONE);
        }

    }


    private void loadDataFromLocal() {


        MyDataBase.db.getNoticeData().observe(this, new Observer<List<DbNotice>>() {
            @Override
            public void onChanged(List<DbNotice> dbNotices) {
                try {
                    NoticeResponseModel model = new NoticeResponseModel();
                    noticeList.clear();
                    for (int i = 0; i < dbNotices.size(); i++) {

                        noticeList.add(model.new NoticeResultModel(
                                dbNotices.get(i).getUserName(),
                                dbNotices.get(i).getUserImage(),
                                dbNotices.get(i).getEventTitle(),
                                dbNotices.get(i).getDescription(),
                                dbNotices.get(i).getAddedDate(),
                                dbNotices.get(i).getEventId(),
                                dbNotices.get(i).getNoReply(),
                                dbNotices.get(i).getClassValue(),
                                dbNotices.get(i).getSendSmsFlag(),
                                dbNotices.get(i).getNoticeSms()
                        ));

                    }
                    if (noticeList.size() == 0) {
                        noData.setVisibility(View.VISIBLE);
                    } else {
                        noData.setVisibility(View.GONE);
                    }
                    noticeAdapter.notifyDataSetChanged();
                } catch (Exception e) {
                    Log.e(TAG, "onChanged: ", e);
                }
            }
        });

    }

    private void loadDataOnline() {
        if (!NetworkStatusChecker.isNetworkConnected(getContext())) {
            Constraints constraints = NetworkStatusChecker.getNetWorkConstraints();
            syncProgress.setVisibility(View.VISIBLE);
            OneTimeWorkRequest notices = new OneTimeWorkRequest.Builder(FetchNoticesWorker.class)
                    .addTag(Constants.TASK_SYNC_NOTICE)
                    .setConstraints(constraints)
                    .setInputData(new Data.Builder().putString("From", "Sync").build())
                    .build();
            WorkManager manager = WorkManager.getInstance();
            manager.beginWith(notices).enqueue();
            WorkManager.getInstance().getWorkInfoByIdLiveData(notices.getId()).observe(this, new Observer<WorkInfo>() {
                @Override
                public void onChanged(WorkInfo workInfo) {
                    if (workInfo != null && workInfo.getState() == WorkInfo.State.SUCCEEDED) {
                        syncProgress.setVisibility(View.GONE);
                    } else if (workInfo != null && workInfo.getState() == WorkInfo.State.FAILED) {
                        syncProgress.setVisibility(View.GONE);
                    }
                }
            });
        }
    }


}
