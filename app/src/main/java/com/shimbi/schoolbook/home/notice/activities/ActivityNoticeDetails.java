package com.shimbi.schoolbook.home.notice.activities;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Rect;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.appbar.MaterialToolbar;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.card.MaterialCardView;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.gson.Gson;
import com.shimbi.schoolbook.R;
import com.shimbi.schoolbook.db.helper.MyDataBase;
import com.shimbi.schoolbook.home.adapters.BottomAttachmentAdapter;
import com.shimbi.schoolbook.home.models.AttachmentDetailsModel;
import com.shimbi.schoolbook.home.notice.adapters.NoticeDetailsAdapter;
import com.shimbi.schoolbook.home.notice.models.NoticeDeleteResponseModel;
import com.shimbi.schoolbook.home.notice.models.NoticeDetailsResponseModel;
import com.shimbi.schoolbook.utils.Alerts;
import com.shimbi.schoolbook.utils.AuthModel;
import com.shimbi.schoolbook.utils.NetworkStatusChecker;
import com.shimbi.schoolbook.utils.RetrofitClient;
import com.shimbi.schoolbook.utils.SaveSharedPreference;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ActivityNoticeDetails extends AppCompatActivity {

    private static final String TAG = "ActivityNoticeDetails";
    @BindView(R.id.comment_card)
    CardView commentCard;
    BottomSheetBehavior sheetBehavior;
    @BindView(R.id.bottom_sheet)
    MaterialCardView bottomSheet;
    @BindView(R.id.recycler_view_attachment)
    RecyclerView recyclerViewAttachment;
    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;
    @BindView(R.id.llProgressBar)
    View llProgressBar;
    @BindView(R.id.et_comment)
    EditText editTextComment;
    @BindView(R.id.send)
    FloatingActionButton buttonSend;
    @BindView(R.id.blur_Background)
    View blurBackground;
    private String username;
    private String userEmail;
    private String userType;
    private String userId;
    private Integer eventID;
    private Integer studentCount;
    private Integer noReply;
    private String noticeClass;
    private String smsDetails;
    private String addedUserId;
    private String addedUserType;
    private NoticeDetailsAdapter noticeAdapter;
    private List<NoticeDetailsResponseModel.NoticeDetailsResultModel.NoticeDetailsPostModel> noticeDetailsList = new ArrayList<>();
    private String cardTitle;
    private String cardDesc;
    private BottomAttachmentAdapter bottomAttachmentAdapter;
    private List<AttachmentDetailsModel> attachments = new ArrayList<>();
    private Integer smsFlag;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notice_details);
        ButterKnife.bind(this);

        sheetBehavior = BottomSheetBehavior.from(bottomSheet);
        init();
        loadData("");

        bottomSheet.setBackgroundResource(R.drawable.card_background);

        buttonSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String comment = editTextComment.getText().toString();
                if (comment.isEmpty()) {
                    return;
                }
                if (!NetworkStatusChecker.isNetworkConnected(getApplicationContext())) {
                    sendComment(comment);
                    editTextComment.setText("");
                } else {
                    Alerts.displayToast(getApplicationContext(), "Check Your Connection");
                }

            }
        });
    }

    private void init() {

        AuthModel authModel = SaveSharedPreference.getData();
        userId = authModel.uid;
        username = authModel.username;
        userEmail = authModel.userEmail;
        userType = authModel.userType;
        studentCount = authModel.studentCount;

        Intent intent = getIntent();
        eventID = intent.getIntExtra("eventId", -1);


        Log.e(TAG, "init: " + eventID);

        noticeAdapter = new NoticeDetailsAdapter(noticeDetailsList);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(noticeAdapter);
        noticeAdapter.setAttachmnetButtonClickListner(new NoticeDetailsAdapter.AttachmentButtonClickListener() {
            @Override
            public void onClick() {


                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    if (checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                        requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 100);
                    } else {
                        toggleBottomDrawer();
                    }
                }

            }
        });

        sheetBehavior.addBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(@NonNull View bottomSheet, int newState) {
                if (newState == BottomSheetBehavior.STATE_COLLAPSED) {
                    blurBackground.setVisibility(View.GONE);
                }

            }

            @Override
            public void onSlide(@NonNull View bottomSheet, float slideOffset) {

            }
        });

        bottomAttachmentAdapter = new BottomAttachmentAdapter(attachments);
        RecyclerView.LayoutManager layoutManager1 = new LinearLayoutManager(getApplicationContext());
        recyclerViewAttachment.setLayoutManager(layoutManager1);
        recyclerViewAttachment.setItemAnimator(new DefaultItemAnimator());
        recyclerViewAttachment.setAdapter(bottomAttachmentAdapter);


    }


    /**
     * load comments
     *
     * @param syncdate syncdate param
     */
    private void loadData(String syncdate) {
        progressVisibility(true);


        Call<NoticeDetailsResponseModel> noticeDetailsApiCall = RetrofitClient.get().getNoticeDetails(String.valueOf(eventID), "");
        noticeDetailsApiCall.enqueue(new Callback<NoticeDetailsResponseModel>() {
            @Override
            public void onResponse(Call<NoticeDetailsResponseModel> call, Response<NoticeDetailsResponseModel> response) {
                NoticeDetailsResponseModel noticeResponseModel = response.body();
                NoticeDetailsResponseModel.NoticeDetailsResultModel noticeDetailsResultModel = noticeResponseModel.getResult();
                List<NoticeDetailsResponseModel.NoticeDetailsResultModel.NoticeDetailsPostModel> noticeDetailsPostModelList = noticeDetailsResultModel.getPost();

                NoticeDetailsResponseModel.PostData postData = noticeResponseModel.getPostData();

                cardTitle = postData.getEventTitle();
                cardDesc = postData.getDescription();
                noReply = postData.getNoReply();
                noticeClass = postData.getClassValue();
                smsFlag = postData.getSendSmsFlag();
                smsDetails = postData.getNoticeSms();
                addedUserId = postData.getAddedUserId();
                addedUserType = postData.getAddedUserType();
                if (noReply == 1) {
                    editTextComment.setVisibility(View.GONE);
                    buttonSend.setVisibility(View.GONE);
                    commentCard.setVisibility(View.GONE);
                }

                MaterialToolbar materialToolbar = findViewById(R.id.appbar);
                setSupportActionBar(materialToolbar);
                getSupportActionBar().setDisplayHomeAsUpEnabled(true);
                getSupportActionBar().setDisplayShowHomeEnabled(true);

                noticeDetailsList.add(noticeDetailsResultModel.new NoticeDetailsPostModel(postData.getUsername(), postData.getUserimage(), postData.getAddedDate(), cardDesc, cardTitle));
                for (int i = 0; i < noticeDetailsPostModelList.size(); i++) {
                    noticeDetailsList.add(noticeDetailsResultModel.new NoticeDetailsPostModel(
                            noticeDetailsPostModelList.get(i).getName(),
                            noticeDetailsPostModelList.get(i).getImage(),
                            noticeDetailsPostModelList.get(i).getDate(),
                            noticeDetailsPostModelList.get(i).getComment()));
                }
                noticeAdapter.notifyDataSetChanged();


                List<NoticeDetailsResponseModel.NoticeDetailsResultModel.Attachment> attachmentList = noticeDetailsResultModel.getAttachment();
                if (attachmentList.size() == 0) {
                    noticeAdapter.setIsAssignmentEmpty(true);
                }

                for (int i = 0; i < attachmentList.size(); i++) {
                    attachments.add(new AttachmentDetailsModel(attachmentList.get(i)));
                }
                bottomAttachmentAdapter.notifyDataSetChanged();
                progressVisibility(false);

            }

            @Override
            public void onFailure(Call<NoticeDetailsResponseModel> call, Throwable t) {
                Log.e(TAG, "onFailure: ", t);
                Alerts.displayToast(getApplicationContext(), "Loading Failed!");
                progressVisibility(false);
            }
        });

    }


    /**
     * Used to send the comment
     *
     * @param comment comment to send
     */
    private void sendComment(String comment) {
        progressVisibility(true);
        Call<Integer> sendComment = RetrofitClient.get().sendCommentOnNotice(String.valueOf(eventID), userId, userType, comment);
        sendComment.enqueue(new Callback<Integer>() {
            @Override
            public void onResponse(@Nullable Call<Integer> call, @Nullable Response<Integer> response) {
                if (response != null && response.body() != null) {
                    if (response.body() == 1) {
                        Alerts.displayToast(getApplicationContext(), "Message Sent");
                        noticeDetailsList.clear();
                        loadData("");
                    }
                }
                progressVisibility(false);
            }

            @Override
            public void onFailure(Call<Integer> call, Throwable t) {
                Log.e(TAG, "onFailure: ", t);
                Alerts.displayToast(getApplicationContext(), "Sending Failed!");
                progressVisibility(false);
            }
        });
    }


    /**
     * Deletes the notice if the user has rights
     */
    private void deleteNotice() {

        progressVisibility(true);

        Call<NoticeDeleteResponseModel> apicall = RetrofitClient.get().deleteNotice(userId, userType, String.valueOf(eventID));
        apicall.enqueue(new Callback<NoticeDeleteResponseModel>() {
            @Override
            public void onResponse(Call<NoticeDeleteResponseModel> call, Response<NoticeDeleteResponseModel> response) {
                if (response.body() != null) {
                    NoticeDeleteResponseModel model = response.body();
                    if (model.getResult() == 1) {
                        Alerts.displayToast(getApplicationContext(), "Deletion Successful");
//                        startActivity(new Intent(ActivityNoticeDetails.this, FragmentNotice.class));
                        MyDataBase.db.deleteNotice(eventID);
                        finish();
                        progressVisibility(false);
                    } else {
                        Alerts.displayToast(getApplicationContext(), "Deletion Failed");
                        progressVisibility(false);
                    }
                } else {
                    Alerts.displayToast(getApplicationContext(), "Deletion Failed");
                    progressVisibility(false);
                }
            }

            @Override
            public void onFailure(Call<NoticeDeleteResponseModel> call, Throwable t) {
                Log.e(TAG, "onFailure: ", t);
                Alerts.displayToast(getApplicationContext(), "Deletion Failed");
                progressVisibility(false);
            }
        });
    }

    private void toggleBottomDrawer() {
        if (sheetBehavior.getState() != BottomSheetBehavior.STATE_EXPANDED) {
            blurBackground.setVisibility(View.VISIBLE);
            sheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
        } else {
            sheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.attach_file, menu);


        if (!(userId.equals(addedUserId) && userType.equals(addedUserType))) {
            menu.findItem(R.id.menu_delete).setVisible(false);
            menu.findItem(R.id.menu_edit).setVisible(false);
        }
        menu.findItem(R.id.menu_attachment).setVisible(false);


        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle presses on the action bar items
        int id = item.getItemId();
        switch (id) {
            case android.R.id.home:
                finish();
                break;
            case R.id.menu_edit:
                // send data to new notice activity
                Intent intent = new Intent(ActivityNoticeDetails.this, ActivityNewNotice.class);
                Bundle mBundle = new Bundle();
                mBundle.putString("class", noticeClass);
                mBundle.putString("title", cardTitle);
                mBundle.putString("details", cardDesc);
                mBundle.putInt("noReply", noReply);
                mBundle.putInt("smsFlag", smsFlag);
                mBundle.putString("smsDetails", smsDetails);
                mBundle.putString("eventId", String.valueOf(eventID));
                mBundle.putString("attachments", new Gson().toJson(attachments));
                intent.putExtras(mBundle);
                startActivity(intent);
                break;
            case R.id.menu_delete:
                new MaterialAlertDialogBuilder(ActivityNoticeDetails.this, R.style.DialogStyle)
                        .setTitle("Do you want to delete this notice?")
                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                deleteNotice();
                            }
                        })
                        .setNegativeButton("No", null)
                        .setCancelable(false)
                        .show();
                break;

        }


        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == 100) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                toggleBottomDrawer();
            } else {
                new MaterialAlertDialogBuilder(ActivityNoticeDetails.this, R.style.DialogStyle)
                        .setTitle("Storage Access Permission Not Granted")
                        .setMessage("Please grant storage permission to download attached files.")
                        .setPositiveButton("Okay", null)
                        .setCancelable(false)
                        .show();
            }
        }
    }


    /**
     * Display progress bar and uses TOUCHABLE flags
     *
     * @param visibility Give True to display dialog, False to remove
     */
    private void progressVisibility(boolean visibility) {
        if (visibility) {
            llProgressBar.setVisibility(View.VISIBLE);
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                    WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
        } else {
            llProgressBar.setVisibility(View.GONE);
            getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
        }
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent event) {
        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            if (sheetBehavior.getState() == BottomSheetBehavior.STATE_EXPANDED) {

                Rect outRect = new Rect();
                bottomSheet.getGlobalVisibleRect(outRect);

                if (!outRect.contains((int) event.getRawX(), (int) event.getRawY()))
                    sheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
            }
        }
        return super.dispatchTouchEvent(event);
    }


}
