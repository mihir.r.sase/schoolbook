package com.shimbi.schoolbook.home.messages.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.shimbi.schoolbook.R;
import com.shimbi.schoolbook.home.messages.activities.ActivityNewMessage;
import com.shimbi.schoolbook.home.messages.models.MessageDropDownModel;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SelectMultipleAdapter extends RecyclerView.Adapter<SelectMultipleAdapter.MyViewHolder> {

    List<MessageDropDownModel.Student> studentList;
    Map<String, String> studentListMap = new HashMap<>();

    public SelectMultipleAdapter(List<MessageDropDownModel.Student> studentList) {
        this.studentList = studentList;
    }



    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_select_multiple, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        MessageDropDownModel.Student model = studentList.get(position);
        String name = model.getValue();
        holder.tvName.setText(name);
        holder.checkBox.setVisibility(View.INVISIBLE);
        holder.checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    studentListMap.put(model.getValue(), model.getKey());
                } else {
                    studentListMap.remove(name);
                }
            }
        });

        holder.tvName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ActivityNewMessage.tvSelectedStudent.setText(name);
                ActivityNewMessage.tvSelectedStudent.setVisibility(View.VISIBLE);
                ActivityNewMessage.dialog.dismiss();
            }
        });
    }


    @Override
    public int getItemCount() {
        return studentList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {


        @BindView(R.id.tv_name)
        TextView tvName;
        @BindView(R.id.checkbox)
        CheckBox checkBox;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
