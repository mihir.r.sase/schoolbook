package com.shimbi.schoolbook.home.fees.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class FeesResponseModel {

    @SerializedName("paid")
    @Expose
    public List<Paid> paid = null;
    @SerializedName("upcoming")
    @Expose
    public List<Upcoming> upcoming = null;
    @SerializedName("overdue")
    @Expose
    public List<Overdue> overdue = null;

    public List<Paid> getPaid() {
        return paid;
    }

    public void setPaid(List<Paid> paid) {
        this.paid = paid;
    }

    public List<Upcoming> getUpcoming() {
        return upcoming;
    }

    public void setUpcoming(List<Upcoming> upcoming) {
        this.upcoming = upcoming;
    }

    public List<Overdue> getOverdue() {
        return overdue;
    }

    public void setOverdue(List<Overdue> overdue) {
        this.overdue = overdue;
    }

    public class Paid {

        @SerializedName("amount")
        @Expose
        public Integer amount;

        public Integer getAmount() {
            return amount;
        }

        public void setAmount(Integer amount) {
            this.amount = amount;
        }
    }

    public class Overdue {

        @SerializedName("fee_name")
        @Expose
        public String feeName;
        @SerializedName("amount")
        @Expose
        public Integer amount;
        @SerializedName("end_date")
        @Expose
        public String endDate;
        @SerializedName("id")
        @Expose
        public String id;
        @SerializedName("key")
        @Expose
        public String key;
        @SerializedName("MID")
        @Expose
        public String mID;
        @SerializedName("phone")
        @Expose
        public String phone;
        @SerializedName("show_payment_button")
        @Expose
        public Integer showPaymentButton;
        @SerializedName("penalty_amount")
        @Expose
        public String penaltyAmount;
        @SerializedName("pay_link")
        @Expose
        public String payLink;
        @SerializedName("payment_from")
        @Expose
        public Integer paymentFrom;


        public String getFeeName() {
            return feeName;
        }

        public void setFeeName(String feeName) {
            this.feeName = feeName;
        }

        public Integer getAmount() {
            return amount;
        }

        public void setAmount(Integer amount) {
            this.amount = amount;
        }

        public String getEndDate() {
            return endDate;
        }

        public void setEndDate(String endDate) {
            this.endDate = endDate;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getKey() {
            return key;
        }

        public void setKey(String key) {
            this.key = key;
        }

        public String getmID() {
            return mID;
        }

        public void setmID(String mID) {
            this.mID = mID;
        }

        public String getPhone() {
            return phone;
        }

        public void setPhone(String phone) {
            this.phone = phone;
        }

        public Integer getShowPaymentButton() {
            return showPaymentButton;
        }

        public void setShowPaymentButton(Integer showPaymentButton) {
            this.showPaymentButton = showPaymentButton;
        }

        public String getPenaltyAmount() {
            return penaltyAmount;
        }

        public void setPenaltyAmount(String penaltyAmount) {
            this.penaltyAmount = penaltyAmount;
        }

        public String getPayLink() {
            return payLink;
        }

        public void setPayLink(String payLink) {
            this.payLink = payLink;
        }

        public Integer getPaymentFrom() {
            return paymentFrom;
        }

        public void setPaymentFrom(Integer paymentFrom) {
            this.paymentFrom = paymentFrom;
        }
    }

    public class Upcoming {
        @SerializedName("fee_name")
        @Expose
        public String feeName;
        @SerializedName("amount")
        @Expose
        public Integer amount;
        @SerializedName("end_date")
        @Expose
        public String endDate;
        @SerializedName("due_date")
        @Expose
        public String dueDate;
        @SerializedName("id")
        @Expose
        public String id;
        @SerializedName("key")
        @Expose
        public String key;
        @SerializedName("MID")
        @Expose
        public String mID;
        @SerializedName("phone")
        @Expose
        public String phone;
        @SerializedName("show_payment_button")
        @Expose
        public Integer showPaymentButton;
        @SerializedName("penalty_amount")
        @Expose
        public String penaltyAmount;
        @SerializedName("pay_link")
        @Expose
        public String payLink;
        @SerializedName("payment_from")
        @Expose
        public Integer paymentFrom;

        public String type;

        public Upcoming(Upcoming upcoming) {
            this.feeName = upcoming.getFeeName();
            this.amount = upcoming.getAmount();
            this.endDate = upcoming.getEndDate();
            this.id = upcoming.getId();
            this.key = upcoming.getKey();
            this.mID = upcoming.getmID();
            this.phone = upcoming.getPhone();
            this.showPaymentButton = upcoming.getShowPaymentButton();
            this.penaltyAmount = upcoming.getPenaltyAmount();
            this.payLink = upcoming.getPayLink();
            this.paymentFrom = upcoming.getPaymentFrom();
            this.dueDate = upcoming.getDueDate();
            this.type = "u";
        }


        public Upcoming(String feeName, Integer amount, String endDate, String id, String key, String mID, String phone, Integer showPaymentButton, String penaltyAmount, String payLink, Integer paymentFrom) {
            this.feeName = feeName;
            this.amount = amount;
            this.endDate = endDate;
            this.id = id;
            this.key = key;
            this.mID = mID;
            this.phone = phone;
            this.showPaymentButton = showPaymentButton;
            this.penaltyAmount = penaltyAmount;
            this.payLink = payLink;
            this.paymentFrom = paymentFrom;
        }

        public Upcoming(Overdue upcoming) {
            this.feeName = upcoming.getFeeName();
            this.amount = upcoming.getAmount();
            this.endDate = upcoming.getEndDate();
            this.id = upcoming.getId();
            this.key = upcoming.getKey();
            this.mID = upcoming.getmID();
            this.phone = upcoming.getPhone();
            this.showPaymentButton = upcoming.getShowPaymentButton();
            this.penaltyAmount = upcoming.getPenaltyAmount();
            this.payLink = upcoming.getPayLink();
            this.paymentFrom = upcoming.getPaymentFrom();
            this.type = "o";
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public String getDueDate() {
            return dueDate;
        }

        public void setDueDate(String dueDate) {
            this.dueDate = dueDate;
        }
        public String getFeeName() {
            return feeName;
        }

        public void setFeeName(String feeName) {
            this.feeName = feeName;
        }

        public Integer getAmount() {
            return amount;
        }

        public void setAmount(Integer amount) {
            this.amount = amount;
        }

        public String getEndDate() {
            return endDate;
        }

        public void setEndDate(String endDate) {
            this.endDate = endDate;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getKey() {
            return key;
        }

        public void setKey(String key) {
            this.key = key;
        }

        public String getmID() {
            return mID;
        }

        public void setmID(String mID) {
            this.mID = mID;
        }

        public String getPhone() {
            return phone;
        }

        public void setPhone(String phone) {
            this.phone = phone;
        }

        public Integer getShowPaymentButton() {
            return showPaymentButton;
        }

        public void setShowPaymentButton(Integer showPaymentButton) {
            this.showPaymentButton = showPaymentButton;
        }

        public String getPenaltyAmount() {
            return penaltyAmount;
        }

        public void setPenaltyAmount(String penaltyAmount) {
            this.penaltyAmount = penaltyAmount;
        }

        public String getPayLink() {
            return payLink;
        }

        public void setPayLink(String payLink) {
            this.payLink = payLink;
        }

        public Integer getPaymentFrom() {
            return paymentFrom;
        }

        public void setPaymentFrom(Integer paymentFrom) {
            this.paymentFrom = paymentFrom;
        }
    }
}
