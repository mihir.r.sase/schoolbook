package com.shimbi.schoolbook.home.leaves.activities;

import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.DatePicker;

import com.google.android.material.appbar.MaterialToolbar;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import com.google.android.material.textfield.TextInputEditText;
import com.google.gson.Gson;
import com.shimbi.schoolbook.R;
import com.shimbi.schoolbook.db.helper.MyDataBase;
import com.shimbi.schoolbook.home.leaves.models.DeleteLeaveResponseModel;
import com.shimbi.schoolbook.home.leaves.models.LeaveListResponseModel;
import com.shimbi.schoolbook.home.models.SuccessResponseModel;
import com.shimbi.schoolbook.utils.Alerts;
import com.shimbi.schoolbook.utils.AuthModel;
import com.shimbi.schoolbook.utils.RetrofitClient;
import com.shimbi.schoolbook.utils.SaveSharedPreference;

import java.util.Calendar;
import java.util.Objects;

import androidx.appcompat.app.AppCompatActivity;
import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ActivityNewLeave extends AppCompatActivity implements DatePickerDialog.OnDateSetListener {


    private static final String TAG = "ActivityNewLeave";
    public static String username;
    public static String userEmail;
    public static String userType;
    public static String userId;
    public Integer studentCount;
    LeaveListResponseModel.Leave currentLeave = null;
    @BindView(R.id.et_start_date)
    TextInputEditText etStartDate;
    @BindView(R.id.et_end_date)
    TextInputEditText etEndDate;
    @BindView(R.id.et_reason)
    TextInputEditText etReason;
    @BindView(R.id.bt_save)
    MaterialButton btSave;
    @BindView(R.id.llProgressBar)
    View llProgressBar;
    private DatePickerDialog datePickerDialog;
    private boolean flag;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_leave);
        ButterKnife.bind(this);
        init();

        etStartDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                flag = true;
                datePickerDialog.show();
            }
        });
        etEndDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                flag = false;
                datePickerDialog.show();
            }
        });


        btSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String start = etStartDate.getText().toString();
                String end = etEndDate.getText().toString();
                String reason = etReason.getText().toString();

                if (start.isEmpty()) {
                    etStartDate.setError("Please enter leave start date");
                    etStartDate.requestFocus();
                    return;
                }
                if (end.isEmpty()) {
                    etEndDate.setError("please enter leave end date");
                    etEndDate.requestFocus();
                    return;
                }
                if (reason.isEmpty()) {
                    etReason.setError("Please enter reason for taking leave");
                    etReason.requestFocus();
                    return;
                }

                if (currentLeave == null) {
                    save(start, end, reason);
                } else {
                    edit(start, end, reason);
                }

            }
        });
    }

    private void init() {
        AuthModel authModel = SaveSharedPreference.getData();
        userId = authModel.uid;
        username = authModel.username;
        userEmail = authModel.userEmail;
        userType = authModel.userType;
        studentCount = authModel.studentCount;

        MaterialToolbar materialToolbar = findViewById(R.id.appbar);
        setSupportActionBar(materialToolbar);
        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);


        final Calendar c = Calendar.getInstance();
        int year = c.get(Calendar.YEAR);
        int month = c.get(Calendar.MONTH);
        int day = c.get(Calendar.DAY_OF_MONTH);

        datePickerDialog = new DatePickerDialog(ActivityNewLeave.this, this, year, month, day);

        try {
            currentLeave = new Gson().fromJson(getIntent().getExtras().getString("leave"), LeaveListResponseModel.Leave.class);
            etStartDate.setText(currentLeave.getLeaveStartDate());
            etEndDate.setText(currentLeave.getLeaveEndDate());
            etReason.setText(currentLeave.getLeaveReason());
            getSupportActionBar().setTitle("Edit Leave");
        } catch (Exception e) {
            Log.e(TAG, "init: ", e);
        }
    }

    private void save(String start, String end, String reason) {


        progressVisibility(true);
        Call<SuccessResponseModel> call = RetrofitClient.get().addLeave(userId, userType, start, end, reason);

        call.enqueue(new Callback<SuccessResponseModel>() {
            @Override
            public void onResponse(Call<SuccessResponseModel> call, Response<SuccessResponseModel> response) {
                if (response.body() != null) {
                    SuccessResponseModel model = response.body();
                    if (model.getSuccess().equals("1")) {
                        Alerts.displayToast(getApplicationContext(), "Success!");
                        finish();
                    } else {
                        Alerts.displayToast(getApplicationContext(), "Operation Failed!");
                    }
                } else {
                    Alerts.displayToast(getApplicationContext(), "Operation Failed!");
                }
                progressVisibility(false);
            }

            @Override
            public void onFailure(Call<SuccessResponseModel> call, Throwable t) {
                Log.e(TAG, "onFailure: ", t);
                progressVisibility(false);
                Alerts.displayToast(getApplicationContext(), "Operation Failed!");
            }
        });

    }

    private void edit(String start, String end, String reason) {

        progressVisibility(true);

        Call<SuccessResponseModel> apicall = RetrofitClient.get().editLeave(userId, userType, currentLeave.getLeaveId(), start, end, reason);
        apicall.enqueue(new Callback<SuccessResponseModel>() {
            @Override
            public void onResponse(Call<SuccessResponseModel> call, Response<SuccessResponseModel> response) {
                if (response.body() != null) {
                    SuccessResponseModel model = response.body();
                    if (model.getSuccess().equals("1")) {
                        Alerts.displayToast(getApplicationContext(), "Success!");
                        finish();
                    } else {
                        Alerts.displayToast(getApplicationContext(), "Operation Failed!");
                    }
                } else {
                    Alerts.displayToast(getApplicationContext(), "Operation Failed!");
                }
                progressVisibility(false);
            }

            @Override
            public void onFailure(Call<SuccessResponseModel> call, Throwable t) {
                Log.e(TAG, "onFailure: ", t);
                Alerts.displayToast(getApplicationContext(), "Operation Failed!");
                progressVisibility(false);
            }
        });
    }

    private void deleteLeave() {

        progressVisibility(true);

        Call<DeleteLeaveResponseModel> call = RetrofitClient.get().deleteLeave(Integer.valueOf(userId), Integer.valueOf(userType), Integer.valueOf(currentLeave.getLeaveId()));
        call.enqueue(new Callback<DeleteLeaveResponseModel>() {
            @Override
            public void onResponse(Call<DeleteLeaveResponseModel> call, Response<DeleteLeaveResponseModel> response) {
                DeleteLeaveResponseModel model = response.body();
                if (model.getSuccess() == 1) {
                    Alerts.displayToast(getApplicationContext(), "Success");
                    MyDataBase.db.deleteLeave(Integer.valueOf(currentLeave.getLeaveId()));
                    finish();
                } else {
                    Alerts.displayToast(getApplicationContext(), "Operation Failed");
                }
                progressVisibility(false);
            }

            @Override
            public void onFailure(Call<DeleteLeaveResponseModel> call, Throwable t) {
                Log.e(TAG, "onFailure: ", t);
                Alerts.displayToast(getApplicationContext(), "Operation Failed!");
                progressVisibility(false);
            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.attach_file, menu);
        menu.findItem(R.id.menu_attachment).setVisible(false);
        menu.findItem(R.id.menu_delete).setVisible(false);
        menu.findItem(R.id.menu_edit).setVisible(false);
        if (currentLeave != null) {
            menu.findItem(R.id.menu_delete).setVisible(true);
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle presses on the action bar items
        int id = item.getItemId();


        if (id == android.R.id.home) {
            finish();
        }
        if (id == R.id.menu_delete) {
            new MaterialAlertDialogBuilder(ActivityNewLeave.this, R.style.DialogStyle)
                    .setTitle("Do you want to delete this leave?")
                    .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            deleteLeave();
                        }
                    })
                    .setNegativeButton("No", null)
                    .setCancelable(false)
                    .show();
        }

        return super.onOptionsItemSelected(item);
    }


    /**
     * Display progress bar and uses TOUCHABLE flags
     *
     * @param visibility Give True to display dialog, False to remove
     */
    public void progressVisibility(boolean visibility) {
        if (visibility) {
            llProgressBar.setVisibility(View.VISIBLE);
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                    WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
        } else {
            llProgressBar.setVisibility(View.GONE);
            getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
        }
    }

    @Override
    public void onDateSet(DatePicker datePicker, int i, int i1, int i2) {
        i1 = i1 + 1;
        if (flag) {
            etStartDate.setText(String.valueOf(i).concat("-").concat(String.valueOf(i1)).concat("-").concat(String.valueOf(i2)));
        } else {
            etEndDate.setText(String.valueOf(i).concat("-").concat(String.valueOf(i1)).concat("-").concat(String.valueOf(i2)));
        }
    }
}
