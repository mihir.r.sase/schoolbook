package com.shimbi.schoolbook.home.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class DropDownModel {

    @SerializedName("class")
    @Expose
    private List<ClassModel> classlist = null;
    @SerializedName("teacher")
    @Expose
    private List<TeacherModel> teacherlist = null;
    @SerializedName("group")
    @Expose
    private List<GroupModel> groupList = null;
    @SerializedName("department")
    @Expose
    private List<DepartmentModel> departmentlist = null;


    public List<ClassModel> getClasslist() {
        return classlist;
    }

    public void setClasslist(List<ClassModel> classlist) {
        this.classlist = classlist;
    }

    public List<TeacherModel> getTeacherlist() {
        return teacherlist;
    }

    public void setTeacherlist(List<TeacherModel> teacherlist) {
        this.teacherlist = teacherlist;
    }

    public List<GroupModel> getGroupList() {
        return groupList;
    }

    public void setGroupList(List<GroupModel> groupList) {
        this.groupList = groupList;
    }

    public List<DepartmentModel> getDepartmentlist() {
        return departmentlist;
    }

    public void setDepartmentlist(List<DepartmentModel> departmentlist) {
        this.departmentlist = departmentlist;
    }

    public class ClassModel {
        @SerializedName("key")
        @Expose
        private String key;
        @SerializedName("value")
        @Expose
        private String value;

        public String getKey() {
            return key;
        }

        public void setKey(String key) {
            this.key = key;
        }

        public String getValue() {
            return value;
        }

        public void setValue(String value) {
            this.value = value;
        }
    }

    public class TeacherModel {
        @SerializedName("key")
        @Expose
        private String key;
        @SerializedName("value")
        @Expose
        private String value;

        public String getKey() {
            return key;
        }

        public void setKey(String key) {
            this.key = key;
        }

        public String getValue() {
            return value;
        }

        public void setValue(String value) {
            this.value = value;
        }
    }

    public class GroupModel {
        @SerializedName("key")
        @Expose
        private String key;
        @SerializedName("value")
        @Expose
        private String value;

        public String getKey() {
            return key;
        }

        public void setKey(String key) {
            this.key = key;
        }

        public String getValue() {
            return value;
        }

        public void setValue(String value) {
            this.value = value;
        }
    }

    public class DepartmentModel {
        @SerializedName("key")
        @Expose
        private String key;
        @SerializedName("value")
        @Expose
        private String value;

        public String getKey() {
            return key;
        }

        public void setKey(String key) {
            this.key = key;
        }

        public String getValue() {
            return value;
        }

        public void setValue(String value) {
            this.value = value;
        }
    }


}
