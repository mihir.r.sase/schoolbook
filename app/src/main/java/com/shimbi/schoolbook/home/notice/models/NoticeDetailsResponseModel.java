package com.shimbi.schoolbook.home.notice.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class NoticeDetailsResponseModel {

    @SerializedName("syncdate")
    @Expose
    private String syncdate;
    @SerializedName("result")
    @Expose
    private NoticeDetailsResultModel result;
    @SerializedName("post_data")
    @Expose
    private PostData postData;

    public PostData getPostData() {
        return postData;
    }

    public void setPostData(PostData postData) {
        this.postData = postData;
    }

    public String getSyncdate() {
        return syncdate;
    }

    public void setSyncdate(String syncdate) {
        this.syncdate = syncdate;
    }

    public NoticeDetailsResultModel getResult() {
        return result;
    }

    public void setResult(NoticeDetailsResultModel result) {
        this.result = result;
    }


    public class NoticeDetailsResultModel {

        @SerializedName("post")
        @Expose
        private List<NoticeDetailsPostModel> post = null;
        @SerializedName("attachment")
        @Expose
        private List<Attachment> attachment = null;

        public List<NoticeDetailsPostModel> getPost() {
            return post;
        }

        public void setPost(List<NoticeDetailsPostModel> post) {
            this.post = post;
        }

        public List<Attachment> getAttachment() {
            return attachment;
        }

        public void setAttachment(List<Attachment> attachment) {
            this.attachment = attachment;
        }

        public class NoticeDetailsPostModel {

            @SerializedName("name")
            @Expose
            private String name;
            @SerializedName("date")
            @Expose
            private String date;
            @SerializedName("id")
            @Expose
            private String id;
            @SerializedName("type")
            @Expose
            private String type;
            @SerializedName("image")
            @Expose
            private String image;
            @SerializedName("comment")
            @Expose
            private String comment;

            private String title;

            public NoticeDetailsPostModel(String name, String image, String date, String comment) {
                this.name = name;
                this.image = image;
                this.date = date;
                this.comment = comment;
            }

            public NoticeDetailsPostModel(String name, String image, String date, String comment, String title) {
                this.name = name;
                this.image = image;
                this.date = date;
                this.comment = comment;
                this.title = title;
            }

            public String getTitle() {
                return title;
            }

            public void setTitle(String title) {
                this.title = title;
            }

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            public String getDate() {
                return date;
            }

            public void setDate(String date) {
                this.date = date;
            }

            public String getId() {
                return id;
            }

            public void setId(String id) {
                this.id = id;
            }

            public String getType() {
                return type;
            }

            public void setType(String type) {
                this.type = type;
            }

            public String getImage() {
                return image;
            }

            public void setImage(String image) {
                this.image = image;
            }

            public String getComment() {
                return comment;
            }

            public void setComment(String comment) {
                this.comment = comment;
            }

            @Override
            public String toString() {
                return "NoticeDetailsPostModel{" +
                        "name='" + name + '\'' +
                        ", date='" + date + '\'' +
                        ", id='" + id + '\'' +
                        ", type='" + type + '\'' +
                        ", image='" + image + '\'' +
                        ", comment='" + comment + '\'' +
                        '}';
            }
        }

        public class Attachment {


            @SerializedName("download_link")
            @Expose
            private String downloadLink;
            @SerializedName("file_info")
            @Expose
            private String fileInfo;
            @SerializedName("file_original_name")
            @Expose
            private String fileOriginalName;
            @SerializedName("filename")
            @Expose
            private String filename;
            @SerializedName("file_name")
            @Expose
            private String fileName;

            public String getFileName() {
                return fileName;
            }

            public void setFileName(String fileName) {
                this.fileName = fileName;
            }

            public String getFilename() {
                return filename;
            }

            public void setFilename(String filename) {
                this.filename = filename;
            }

            public String getDownloadLink() {
                return downloadLink;
            }

            public void setDownloadLink(String downloadLink) {
                this.downloadLink = downloadLink;
            }

            public String getFileInfo() {
                return fileInfo;
            }

            public void setFileInfo(String fileInfo) {
                this.fileInfo = fileInfo;
            }

            public String getFileOriginalName() {
                return fileOriginalName;
            }

            public void setFileOriginalName(String fileOriginalName) {
                this.fileOriginalName = fileOriginalName;
            }
        }
    }

    public class PostData {

        @SerializedName("username")
        @Expose
        private String username;
        @SerializedName("user_profile_img")
        @Expose
        private String userProfileImg;
        @SerializedName("event_id")
        @Expose
        private String eventId;
        @SerializedName("no_reply")
        @Expose
        private Integer noReply;
        @SerializedName("event_title")
        @Expose
        private String eventTitle;
        @SerializedName("description")
        @Expose
        private String description;
        @SerializedName("added_date")
        @Expose
        private String addedDate;
        @SerializedName("formatted_date")
        @Expose
        private String formattedDate;
        @SerializedName("picture")
        @Expose
        private String picture;
        @SerializedName("userimage")
        @Expose
        private String userimage;
        @SerializedName("class_key")
        @Expose
        private String classKey;
        @SerializedName("class_value")
        @Expose
        private String classValue;
        @SerializedName("startdate")
        @Expose
        private String startdate;
        @SerializedName("enddate")
        @Expose
        private String enddate;
        @SerializedName("starttime")
        @Expose
        private String starttime;
        @SerializedName("endtime")
        @Expose
        private String endtime;
        @SerializedName("eventstatue")
        @Expose
        private String eventstatue;
        @SerializedName("group_name")
        @Expose
        private String groupName;
        @SerializedName("notice_sms")
        @Expose
        private String noticeSms;
        @SerializedName("send_sms_flag")
        @Expose
        private Integer sendSmsFlag;
        @SerializedName("submiton_date")
        @Expose
        private String submitonDate;
        @SerializedName("event_date")
        @Expose
        private String eventDate;
        @SerializedName("added_user_id")
        @Expose
        private String addedUserId;
        @SerializedName("added_user_type")
        @Expose
        private String addedUserType;

        public String getAddedUserId() {
            return addedUserId;
        }

        public void setAddedUserId(String addedUserId) {
            this.addedUserId = addedUserId;
        }

        public String getAddedUserType() {
            return addedUserType;
        }

        public void setAddedUserType(String addedUserType) {
            this.addedUserType = addedUserType;
        }

        public String getClassValue() {
            return classValue;
        }

        public void setClassValue(String classValue) {
            this.classValue = classValue;
        }

        public String getUsername() {
            return username;
        }

        public void setUsername(String username) {
            this.username = username;
        }

        public String getUserProfileImg() {
            return userProfileImg;
        }

        public void setUserProfileImg(String userProfileImg) {
            this.userProfileImg = userProfileImg;
        }

        public String getEventId() {
            return eventId;
        }

        public void setEventId(String eventId) {
            this.eventId = eventId;
        }

        public Integer getNoReply() {
            return noReply;
        }

        public void setNoReply(Integer noReply) {
            this.noReply = noReply;
        }

        public String getEventTitle() {
            return eventTitle;
        }

        public void setEventTitle(String eventTitle) {
            this.eventTitle = eventTitle;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public String getAddedDate() {
            return addedDate;
        }

        public void setAddedDate(String addedDate) {
            this.addedDate = addedDate;
        }

        public String getFormattedDate() {
            return formattedDate;
        }

        public void setFormattedDate(String formattedDate) {
            this.formattedDate = formattedDate;
        }

        public String getPicture() {
            return picture;
        }

        public void setPicture(String picture) {
            this.picture = picture;
        }

        public String getUserimage() {
            return userimage;
        }

        public void setUserimage(String userimage) {
            this.userimage = userimage;
        }

        public String getClassKey() {
            return classKey;
        }

        public void setClassKey(String classKey) {
            this.classKey = classKey;
        }


        public String getStartdate() {
            return startdate;
        }

        public void setStartdate(String startdate) {
            this.startdate = startdate;
        }

        public String getEnddate() {
            return enddate;
        }

        public void setEnddate(String enddate) {
            this.enddate = enddate;
        }

        public String getStarttime() {
            return starttime;
        }

        public void setStarttime(String starttime) {
            this.starttime = starttime;
        }

        public String getEndtime() {
            return endtime;
        }

        public void setEndtime(String endtime) {
            this.endtime = endtime;
        }

        public String getEventstatue() {
            return eventstatue;
        }

        public void setEventstatue(String eventstatue) {
            this.eventstatue = eventstatue;
        }

        public String getGroupName() {
            return groupName;
        }

        public void setGroupName(String groupName) {
            this.groupName = groupName;
        }

        public String getNoticeSms() {
            return noticeSms;
        }

        public void setNoticeSms(String noticeSms) {
            this.noticeSms = noticeSms;
        }

        public Integer getSendSmsFlag() {
            return sendSmsFlag;
        }

        public void setSendSmsFlag(Integer sendSmsFlag) {
            this.sendSmsFlag = sendSmsFlag;
        }

        public String getSubmitonDate() {
            return submitonDate;
        }

        public void setSubmitonDate(String submitonDate) {
            this.submitonDate = submitonDate;
        }

        public String getEventDate() {
            return eventDate;
        }

        public void setEventDate(String eventDate) {
            this.eventDate = eventDate;
        }
    }
}
