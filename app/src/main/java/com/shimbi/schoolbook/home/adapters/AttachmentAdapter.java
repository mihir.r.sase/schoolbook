package com.shimbi.schoolbook.home.adapters;

import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.android.material.textfield.TextInputEditText;
import com.shimbi.schoolbook.R;
import com.shimbi.schoolbook.home.models.AddAttachmentResponseModel;
import com.shimbi.schoolbook.home.models.AttachmentModel;
import com.shimbi.schoolbook.utils.Alerts;
import com.shimbi.schoolbook.utils.AuthModel;
import com.shimbi.schoolbook.utils.FileUtils;
import com.shimbi.schoolbook.utils.RetrofitClient;
import com.shimbi.schoolbook.utils.SaveSharedPreference;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AttachmentAdapter extends RecyclerView.Adapter<AttachmentAdapter.MyViewHolder> {

    private static final String TAG = "AttachmentAdapter";
    public static String userType;
    private List<String> pathList;
    private Map<Integer, String> infoList;
    public static String userId;
    private Context context;
    private List<String> attachmentList;
    private List<String> filenameList;

    private String module;

    private UploadStatusChangeListner uploadStatusChangeListner;

    public AttachmentAdapter() {
        attachmentList = new ArrayList<>();
        pathList = new ArrayList<>();
        infoList = new HashMap<>();
        filenameList = new ArrayList<>();
        AuthModel authModel = SaveSharedPreference.getData();
        userId = authModel.uid;
        userType = authModel.userType;
    }

    public void add(String file, String filePath, String module) {
        attachmentList.add(file);
        pathList.add(filePath);
        infoList.put(attachmentList.size() - 1, "");
        this.module = module;
        notifyItemInserted(attachmentList.size() - 1);
    }

    public void setUploadStatusChangeListner(UploadStatusChangeListner uploadStatusChangeListner) {
        this.uploadStatusChangeListner = uploadStatusChangeListner;
    }

    public List<AttachmentModel> getAll() {
        List<AttachmentModel> list = new ArrayList<>();
        for (int i = 0; i < getItemCount(); i++) {
            list.add(new AttachmentModel(attachmentList.get(i), infoList.get(i) == null ? "" : infoList.get(i), pathList.get(i), filenameList.get(i)));
        }
        return list;
    }


    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_attach_file, parent, false);
        context = parent.getContext();
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {

        try {
            String fileName = attachmentList.get(position);
            holder.tvFileName.setText(fileName);
            holder.etInformation.setText(infoList.get(position));

            if (uploadStatusChangeListner != null) {
                uploadStatusChangeListner.onStatusChanged(true);
            }

            holder.progressBar.setVisibility(View.VISIBLE);
            Call<AddAttachmentResponseModel> call = RetrofitClient.get().addAttachment(userId, userType, FileUtils.getStringFile(pathList.get(position)), fileName, module);
            call.enqueue(new Callback<AddAttachmentResponseModel>() {
                @Override
                public void onResponse(Call<AddAttachmentResponseModel> call, Response<AddAttachmentResponseModel> response) {
                    if (response.body() != null) {
                        AddAttachmentResponseModel model = response.body();
                        if (model.getResult().equals("1")) {
                            filenameList.add(model.getImageName());
                        } else {
                            Alerts.displayToast(context, "Something went wrong!");
                            attachmentList.remove(holder.getAdapterPosition());
                            infoList.remove(holder.getAdapterPosition());
                            pathList.remove(holder.getAdapterPosition());
                            notifyDataSetChanged();
                        }
                    } else {
                        Log.e(TAG, "onResponse: Error");
                        Alerts.displayToast(context, "Upload Failed!");
                        attachmentList.remove(holder.getAdapterPosition());
                        infoList.remove(holder.getAdapterPosition());
                        pathList.remove(holder.getAdapterPosition());
                        notifyDataSetChanged();
                    }
                    holder.progressBar.setVisibility(View.GONE);
                    if (uploadStatusChangeListner != null) {
                        uploadStatusChangeListner.onStatusChanged(false);
                    }
                }

                @Override
                public void onFailure(Call<AddAttachmentResponseModel> call, Throwable t) {
                    Log.e(TAG, "onFailure: ", t);
                    holder.progressBar.setVisibility(View.GONE);
                    Alerts.displayToast(context, "Upload Failed!");
                    attachmentList.remove(holder.getAdapterPosition());
                    infoList.remove(holder.getAdapterPosition());
                    pathList.remove(holder.getAdapterPosition());
                    notifyDataSetChanged();
                    if (uploadStatusChangeListner != null) {
                        uploadStatusChangeListner.onStatusChanged(false);
                    }
                }
            });


            holder.cancel_button.setOnClickListener(view -> {
                attachmentList.remove(holder.getAdapterPosition());
                infoList.remove(holder.getAdapterPosition());
                pathList.remove(holder.getAdapterPosition());
                filenameList.remove(holder.getAdapterPosition());
                notifyItemRemoved(holder.getAdapterPosition());
            });
            holder.etInformation.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                    infoList.put(position, charSequence.toString());
                }

                @Override
                public void afterTextChanged(Editable editable) {

                }
            });

        } catch (Exception e) {
            e.printStackTrace();
            Log.e(TAG, "onBindViewHolder: ", e);
        }
    }

    @Override
    public int getItemCount() {
        return attachmentList.size();
    }


    public class MyViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.et_information)
        public TextInputEditText etInformation;
        @BindView(R.id.tv_file_name)
        TextView tvFileName;
        @BindView(R.id.cancel_button)
        ImageView cancel_button;
        @BindView(R.id.progress_upload)
        ProgressBar progressBar;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

    }


    public interface UploadStatusChangeListner {
        void onStatusChanged(boolean isUploading);
    }

}
