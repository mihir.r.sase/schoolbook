package com.shimbi.schoolbook.home.about_us;

import android.content.pm.PackageInfo;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.shimbi.schoolbook.R;
import com.shimbi.schoolbook.home.activities.ActivityHome;
import com.shimbi.schoolbook.utils.AuthModel;
import com.shimbi.schoolbook.utils.SaveSharedPreference;

import butterknife.BindView;
import butterknife.ButterKnife;

public class FragmentAboutUs extends Fragment {
    private static final String TAG = "FragmentAboutUs";
    @BindView(R.id.name)
    TextView tvName;
    @BindView(R.id.email)
    TextView tvEmail;
    @BindView(R.id.installed_version)
    TextView tvInstalledVersion;
    @BindView(R.id.latest_version)
    TextView tvLatestVersion;
    @BindView(R.id.os_version)
    TextView tvOsVersion;
    @BindView(R.id.os_api_level)
    TextView tvOsApiLevel;
    @BindView(R.id.android_version)
    TextView tvAndroidVersion;
    @BindView(R.id.device)
    TextView tvDevice;
    @BindView(R.id.model)
    TextView tvModel;
    private String username;
    private String userEmail;
    private String userType;
    private String userId;
    private Integer studentCount;
    public ProgressBar syncProgress;
    private ImageView noData;

    public FragmentAboutUs() {

    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        ActivityHome activityHome = (ActivityHome) getActivity();
        activityHome.setTitleAndLogo("About Us", R.drawable.ic_info_black_24dp);
        ActivityHome.fab.setVisibility(View.GONE);
        if (getActivity() != null) {
            syncProgress = getActivity().findViewById(R.id.progress_sync);
            syncProgress.setVisibility(View.GONE);
            noData = getActivity().findViewById(R.id.no_data);
            noData.setVisibility(View.GONE);
        }

        return inflater.inflate(R.layout.fragment_about_us, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
        init();

        PackageInfo packageInfo = null;
        try {
            tvName.setText(username);
            tvEmail.setText(userEmail);
            packageInfo = getActivity().getPackageManager().getPackageInfo(getActivity().getPackageName(), 0);
            tvInstalledVersion.setText(packageInfo.versionName);
            // TODO: 30-12-2019 Latest Version
            tvLatestVersion.setText("");
            tvOsVersion.setText(System.getProperty("os.name").concat("(").concat(System.getProperty("os.version")) + ")");
            tvOsApiLevel.setText(String.valueOf(Build.VERSION.SDK_INT));
            tvAndroidVersion.setText("android : " + Build.VERSION.RELEASE);
            tvDevice.setText(android.os.Build.DEVICE);
            tvModel.setText(android.os.Build.MODEL + " (" + android.os.Build.BRAND + ") " + android.os.Build.PRODUCT);
        } catch (Exception e) {
            Log.e(TAG, "onCreate: ", e);
        }
    }

    private void init() {


        AuthModel authModel = SaveSharedPreference.getData();
        userId = authModel.uid;
        username = authModel.username;
        userEmail = authModel.userEmail;
        userType = authModel.userType;
        studentCount = authModel.studentCount;


    }

}
