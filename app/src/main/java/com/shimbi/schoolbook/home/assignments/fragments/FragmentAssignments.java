package com.shimbi.schoolbook.home.assignments.fragments;


import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;

import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.work.Constraints;
import androidx.work.Data;
import androidx.work.OneTimeWorkRequest;
import androidx.work.WorkInfo;
import androidx.work.WorkManager;

import com.shimbi.schoolbook.R;
import com.shimbi.schoolbook.db.helper.MyDataBase;
import com.shimbi.schoolbook.db.tables.DbAssignment;
import com.shimbi.schoolbook.home.activities.ActivityHome;
import com.shimbi.schoolbook.home.assignments.activities.ActivityNewAssignment;
import com.shimbi.schoolbook.home.assignments.adapters.AssignmentAdapter;
import com.shimbi.schoolbook.home.assignments.models.AssignmentResponseModel;
import com.shimbi.schoolbook.utils.AuthModel;
import com.shimbi.schoolbook.utils.Constants;
import com.shimbi.schoolbook.utils.NetworkStatusChecker;
import com.shimbi.schoolbook.utils.SaveSharedPreference;
import com.shimbi.schoolbook.utils.workers.FetchAssignmentsWorker;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * A simple {@link Fragment} subclass.
 */
public class FragmentAssignments extends Fragment {


    private static final String TAG = "ActivityAssignments";
    public ProgressBar syncProgress;
    public ImageView noData;
    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;
    @BindView(R.id.swipe_container)
    SwipeRefreshLayout swipeRefreshLayout;
    private String username;
    private String userEmail;
    private String userType;
    private String userId;
    private Integer studentCount;
    private AssignmentAdapter assignmentAdapter;
    private LinearLayoutManager layoutManager;
    private List<AssignmentResponseModel.AssignmentResultModel> assignmentList = new ArrayList<>();
    private String SYNC_DATE;

    public FragmentAssignments() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        ActivityHome activityHome = (ActivityHome) getActivity();
        activityHome.setTitleAndLogo("Assignments", R.drawable.ic_assignment_black_24dp);
        ActivityHome.fab.setVisibility(View.GONE);

        if (getActivity() != null) {
            syncProgress = getActivity().findViewById(R.id.progress_sync);
            syncProgress.setVisibility(View.GONE);
            noData = getActivity().findViewById(R.id.no_data);
            noData.setVisibility(View.GONE);
        }

        return inflater.inflate(R.layout.fragment_assignments, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {

        ButterKnife.bind(this, view);
        init();


        //initially load data from sqlite
        displayFromLocal();

        //check for updates online and update sqlite
        loadDataFromOnline();


        ActivityHome.fab.setOnClickListener(view1 -> {
            Intent intent = new Intent(getContext(), ActivityNewAssignment.class);
            startActivity(intent);
        });


        swipeRefreshLayout.setOnRefreshListener(() -> {
            loadDataFromOnline();
            swipeRefreshLayout.setRefreshing(false);
        });

    }


    private void init() {
        AuthModel authModel = SaveSharedPreference.getData();
        userId = authModel.uid;
        username = authModel.username;
        userEmail = authModel.userEmail;
        userType = authModel.userType;
        studentCount = authModel.studentCount;
        Log.e(TAG, "init: " + userId + " " + userType);

        assignmentAdapter = new AssignmentAdapter(assignmentList);
        layoutManager = new LinearLayoutManager(this.getActivity());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(assignmentAdapter);

        if (ActivityHome.assignmentAddFlag.equals("1")) {
            ActivityHome.fab.setVisibility(View.VISIBLE);
        } else {
            ActivityHome.fab.setVisibility(View.GONE);
        }

        SYNC_DATE = SaveSharedPreference.getAssignmentsSyncDate();
        if (SYNC_DATE.equals("Default")) {
            SYNC_DATE = "";
        }
        Log.e(TAG, "init: syncdate : " + SYNC_DATE);
    }


    /**
     * initially load data from sqlite
     */
    private void displayFromLocal() {

        MyDataBase.db.getAssignmentData().observe(this, new Observer<List<DbAssignment>>() {
            @Override
            public void onChanged(List<DbAssignment> dbAssignments) {
                try {
                    AssignmentResponseModel model = new AssignmentResponseModel();
                    assignmentList.clear();
                    for (int i = 0; i < dbAssignments.size(); i++) {

                        assignmentList.add(model.new AssignmentResultModel(
                                dbAssignments.get(i).getUsername(),
                                dbAssignments.get(i).getUserImage(),
                                dbAssignments.get(i).getEventTitle(),
                                dbAssignments.get(i).getDescription(),
                                dbAssignments.get(i).getAddedDate(),
                                dbAssignments.get(i).getEventId(),
                                dbAssignments.get(i).getEventDate(),
                                dbAssignments.get(i).getUserid(),
                                dbAssignments.get(i).getSubmitonDate(),
                                dbAssignments.get(i).getClassValue(),
                                dbAssignments.get(i).getNoReply()
                        ));
                    }
                    if (assignmentList.size() == 0) {
                        noData.setVisibility(View.VISIBLE);
                    } else {
                        noData.setVisibility(View.GONE);
                    }
                    assignmentAdapter.notifyDataSetChanged();
                } catch (Exception e) {
                    Log.e(TAG, "onChanged: ", e);
                }
            }
        });


    }

    /**
     * check for updates online and update sqlite
     */
    private void loadDataFromOnline() {
        Constraints constraints = NetworkStatusChecker.getNetWorkConstraints();
        syncProgress.setVisibility(View.VISIBLE);
        if (!NetworkStatusChecker.isNetworkConnected(getContext())) {
            OneTimeWorkRequest assignments = new OneTimeWorkRequest.Builder(FetchAssignmentsWorker.class)
                    .addTag(Constants.TASK_SYNC_ASSIGNMENT)
                    .setConstraints(constraints)
                    .setInputData(new Data.Builder().putString("From", "Sync").build())
                    .build();
            WorkManager manager = WorkManager.getInstance();
            manager.beginWith(assignments).enqueue();
            WorkManager.getInstance().getWorkInfoByIdLiveData(assignments.getId()).observe(this, new Observer<WorkInfo>() {
                @Override
                public void onChanged(WorkInfo workInfo) {
                    if (workInfo != null && workInfo.getState() == WorkInfo.State.SUCCEEDED) {
                        syncProgress.setVisibility(View.GONE);
                    } else if (workInfo != null && workInfo.getState() == WorkInfo.State.FAILED) {
                        syncProgress.setVisibility(View.GONE);
                    }
                }
            });
        }

    }


}
