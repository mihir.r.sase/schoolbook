package com.shimbi.schoolbook.home.transport.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class TransportDataModel {

    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("msg")
    @Expose
    private String msg;
    @SerializedName("pickupLocArr")
    @Expose
    private List<PickupLocArr> pickupLocArr = null;
    @SerializedName("route_details")
    @Expose
    private List<RouteDetail> routeDetails = null;
    @SerializedName("busRec")
    @Expose
    private List<BusRec> busRec = null;
    @SerializedName("userRec")
    @Expose
    private List<UserRec> userRec = null;


    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public List<PickupLocArr> getPickupLocArr() {
        return pickupLocArr;
    }

    public void setPickupLocArr(List<PickupLocArr> pickupLocArr) {
        this.pickupLocArr = pickupLocArr;
    }

    public List<RouteDetail> getRouteDetails() {
        return routeDetails;
    }

    public void setRouteDetails(List<RouteDetail> routeDetails) {
        this.routeDetails = routeDetails;
    }

    public List<BusRec> getBusRec() {
        return busRec;
    }

    public void setBusRec(List<BusRec> busRec) {
        this.busRec = busRec;
    }

    public List<UserRec> getUserRec() {
        return userRec;
    }

    public void setUserRec(List<UserRec> userRec) {
        this.userRec = userRec;
    }

    public class BusRec {
        @SerializedName("bus_no")
        @Expose
        private String busNo;
        @SerializedName("driver_name")
        @Expose
        private String driverName;
        @SerializedName("driver_contact1")
        @Expose
        private String driverContact1;
        @SerializedName("vendor_name")
        @Expose
        private String vendorName;
        @SerializedName("vendor_contact1")
        @Expose
        private String vendorContact1;
        @SerializedName("route_name")
        @Expose
        private String routeName;
        @SerializedName("start_point")
        @Expose
        private String startPoint;
        @SerializedName("end_point")
        @Expose
        private String endPoint;
        @SerializedName("capacity")
        @Expose
        private String capacity;
        @SerializedName("route_id")
        @Expose
        private String routeId;
        @SerializedName("bus_id")
        @Expose
        private String busId;
        @SerializedName("driver_id")
        @Expose
        private String driverId;
        @SerializedName("start_place")
        @Expose
        private String startPlace;
        @SerializedName("start_latitude")
        @Expose
        private String startLatitude;
        @SerializedName("start_longitude")
        @Expose
        private String startLongitude;
        @SerializedName("end_place")
        @Expose
        private String endPlace;
        @SerializedName("end_latitude")
        @Expose
        private String endLatitude;
        @SerializedName("end_longitude")
        @Expose
        private String endLongitude;

        public String getBusNo() {
            return busNo;
        }

        public void setBusNo(String busNo) {
            this.busNo = busNo;
        }

        public String getDriverName() {
            return driverName;
        }

        public void setDriverName(String driverName) {
            this.driverName = driverName;
        }

        public String getDriverContact1() {
            return driverContact1;
        }

        public void setDriverContact1(String driverContact1) {
            this.driverContact1 = driverContact1;
        }

        public String getVendorName() {
            return vendorName;
        }

        public void setVendorName(String vendorName) {
            this.vendorName = vendorName;
        }

        public String getVendorContact1() {
            return vendorContact1;
        }

        public void setVendorContact1(String vendorContact1) {
            this.vendorContact1 = vendorContact1;
        }

        public String getRouteName() {
            return routeName;
        }

        public void setRouteName(String routeName) {
            this.routeName = routeName;
        }

        public String getStartPoint() {
            return startPoint;
        }

        public void setStartPoint(String startPoint) {
            this.startPoint = startPoint;
        }

        public String getEndPoint() {
            return endPoint;
        }

        public void setEndPoint(String endPoint) {
            this.endPoint = endPoint;
        }

        public String getCapacity() {
            return capacity;
        }

        public void setCapacity(String capacity) {
            this.capacity = capacity;
        }

        public String getRouteId() {
            return routeId;
        }

        public void setRouteId(String routeId) {
            this.routeId = routeId;
        }

        public String getBusId() {
            return busId;
        }

        public void setBusId(String busId) {
            this.busId = busId;
        }

        public String getDriverId() {
            return driverId;
        }

        public void setDriverId(String driverId) {
            this.driverId = driverId;
        }

        public String getStartPlace() {
            return startPlace;
        }

        public void setStartPlace(String startPlace) {
            this.startPlace = startPlace;
        }

        public String getStartLatitude() {
            return startLatitude;
        }

        public void setStartLatitude(String startLatitude) {
            this.startLatitude = startLatitude;
        }

        public String getStartLongitude() {
            return startLongitude;
        }

        public void setStartLongitude(String startLongitude) {
            this.startLongitude = startLongitude;
        }

        public String getEndPlace() {
            return endPlace;
        }

        public void setEndPlace(String endPlace) {
            this.endPlace = endPlace;
        }

        public String getEndLatitude() {
            return endLatitude;
        }

        public void setEndLatitude(String endLatitude) {
            this.endLatitude = endLatitude;
        }

        public String getEndLongitude() {
            return endLongitude;
        }

        public void setEndLongitude(String endLongitude) {
            this.endLongitude = endLongitude;
        }
    }

    public class PickupLocArr {
        @SerializedName("pickup_id")
        @Expose
        private String pickupId;
        @SerializedName("pickup_name")
        @Expose
        private String pickupName;
        @SerializedName("pickup_area")
        @Expose
        private String pickupArea;
        @SerializedName("latitude")
        @Expose
        private String latitude;
        @SerializedName("longitude")
        @Expose
        private String longitude;
        @SerializedName("route_id")
        @Expose
        private String routeId;

        public String getPickupId() {
            return pickupId;
        }

        public void setPickupId(String pickupId) {
            this.pickupId = pickupId;
        }

        public String getPickupName() {
            return pickupName;
        }

        public void setPickupName(String pickupName) {
            this.pickupName = pickupName;
        }

        public String getPickupArea() {
            return pickupArea;
        }

        public void setPickupArea(String pickupArea) {
            this.pickupArea = pickupArea;
        }

        public String getLatitude() {
            return latitude;
        }

        public void setLatitude(String latitude) {
            this.latitude = latitude;
        }

        public String getLongitude() {
            return longitude;
        }

        public void setLongitude(String longitude) {
            this.longitude = longitude;
        }

        public String getRouteId() {
            return routeId;
        }

        public void setRouteId(String routeId) {
            this.routeId = routeId;
        }
    }

    public class RouteDetail {

        @SerializedName("bus_id")
        @Expose
        private String busId;
        @SerializedName("route_id")
        @Expose
        private String routeId;
        @SerializedName("route_name")
        @Expose
        private String routeName;

        public String getBusId() {
            return busId;
        }

        public void setBusId(String busId) {
            this.busId = busId;
        }

        public String getRouteId() {
            return routeId;
        }

        public void setRouteId(String routeId) {
            this.routeId = routeId;
        }

        public String getRouteName() {
            return routeName;
        }

        public void setRouteName(String routeName) {
            this.routeName = routeName;
        }
    }

    public class UserRec {

        @SerializedName("student_name")
        @Expose
        private String studentName;
        @SerializedName("route_id")
        @Expose
        private String routeId;
        @SerializedName("payment_date")
        @Expose
        private String paymentDate;
        @SerializedName("user_order")
        @Expose
        private String userOrder;
        @SerializedName("mobile")
        @Expose
        private String mobile;
        @SerializedName("update_flag")
        @Expose
        private String updateFlag;
        @SerializedName("update_drop_flag")
        @Expose
        private String updateDropFlag;
        @SerializedName("class_name")
        @Expose
        private String className;
        @SerializedName("student_id")
        @Expose
        private String studentId;
        @SerializedName("address")
        @Expose
        private String address;
        @SerializedName("pickup_name")
        @Expose
        private String pickupName;
        @SerializedName("pickup_area")
        @Expose
        private String pickupArea;
        @SerializedName("latitude")
        @Expose
        private String latitude;
        @SerializedName("longitude")
        @Expose
        private String longitude;
        @SerializedName("type")
        @Expose
        private String type;
        @SerializedName("pickup_id")
        @Expose
        private String pickupId;
        @SerializedName("user_type")
        @Expose
        private String userType;

        public String getStudentName() {
            return studentName;
        }

        public void setStudentName(String studentName) {
            this.studentName = studentName;
        }

        public String getRouteId() {
            return routeId;
        }

        public void setRouteId(String routeId) {
            this.routeId = routeId;
        }

        public String getPaymentDate() {
            return paymentDate;
        }

        public void setPaymentDate(String paymentDate) {
            this.paymentDate = paymentDate;
        }

        public String getUserOrder() {
            return userOrder;
        }

        public void setUserOrder(String userOrder) {
            this.userOrder = userOrder;
        }

        public String getMobile() {
            return mobile;
        }

        public void setMobile(String mobile) {
            this.mobile = mobile;
        }

        public String getUpdateFlag() {
            return updateFlag;
        }

        public void setUpdateFlag(String updateFlag) {
            this.updateFlag = updateFlag;
        }

        public String getUpdateDropFlag() {
            return updateDropFlag;
        }

        public void setUpdateDropFlag(String updateDropFlag) {
            this.updateDropFlag = updateDropFlag;
        }

        public String getClassName() {
            return className;
        }

        public void setClassName(String className) {
            this.className = className;
        }

        public String getStudentId() {
            return studentId;
        }

        public void setStudentId(String studentId) {
            this.studentId = studentId;
        }

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        public String getPickupName() {
            return pickupName;
        }

        public void setPickupName(String pickupName) {
            this.pickupName = pickupName;
        }

        public String getPickupArea() {
            return pickupArea;
        }

        public void setPickupArea(String pickupArea) {
            this.pickupArea = pickupArea;
        }

        public String getLatitude() {
            return latitude;
        }

        public void setLatitude(String latitude) {
            this.latitude = latitude;
        }

        public String getLongitude() {
            return longitude;
        }

        public void setLongitude(String longitude) {
            this.longitude = longitude;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public String getPickupId() {
            return pickupId;
        }

        public void setPickupId(String pickupId) {
            this.pickupId = pickupId;
        }

        public String getUserType() {
            return userType;
        }

        public void setUserType(String userType) {
            this.userType = userType;
        }
    }
}
