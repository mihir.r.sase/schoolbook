package com.shimbi.schoolbook.home.transport.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.shimbi.schoolbook.R;
import com.shimbi.schoolbook.db.tables.DbYetToBeEmbarked;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class TransportDialogAdapter extends RecyclerView.Adapter<TransportDialogAdapter.MyViewHolder> {

    List<DbYetToBeEmbarked> list;
    List<DbYetToBeEmbarked> selectedStudentList = new ArrayList<>();
    boolean isSelected = false;

    public TransportDialogAdapter(List<DbYetToBeEmbarked> list) {
        this.list = list;

    }

    public void setSelected(boolean selected) {
        isSelected = selected;
        selectedStudentList.addAll(list);
    }

    public List<DbYetToBeEmbarked> getSelectedStudentList() {
        return selectedStudentList;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_select_multiple, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {

        DbYetToBeEmbarked model = list.get(position);
        holder.tvName.setText(model.getStudentName());
        holder.checkBox.setVisibility(View.VISIBLE);
        holder.checkBox.setEnabled(true);

        if (isSelected) {
            holder.checkBox.setChecked(true);
        } else {
            holder.checkBox.setChecked(false);
        }

        holder.checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    selectedStudentList.add(list.get(holder.getAdapterPosition()));
                } else {
                    selectedStudentList.remove(list.get(holder.getAdapterPosition()));
                }
            }
        });

    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {


        @BindView(R.id.tv_name)
        TextView tvName;
        @BindView(R.id.checkbox)
        CheckBox checkBox;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

        }
    }
}
