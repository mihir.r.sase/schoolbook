package com.shimbi.schoolbook.home.assignments.activities;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Rect;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.appbar.MaterialToolbar;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.card.MaterialCardView;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.gson.Gson;
import com.shimbi.schoolbook.R;
import com.shimbi.schoolbook.db.helper.MyDataBase;
import com.shimbi.schoolbook.home.adapters.BottomAttachmentAdapter;
import com.shimbi.schoolbook.home.assignments.adapters.AssignmentDetailsAdapter;
import com.shimbi.schoolbook.home.assignments.models.AssignmentDetailsResponseModel;
import com.shimbi.schoolbook.home.assignments.models.AssignmentDeteleResponseModel;
import com.shimbi.schoolbook.home.models.AttachmentDetailsModel;
import com.shimbi.schoolbook.utils.Alerts;
import com.shimbi.schoolbook.utils.AuthModel;
import com.shimbi.schoolbook.utils.RetrofitClient;
import com.shimbi.schoolbook.utils.SaveSharedPreference;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ActivityAssignmentDetails extends AppCompatActivity {

    private static final String TAG = "ActivityAssignmentDetai";
    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;
    @BindView(R.id.llProgressBar)
    View llProgressBar;
    @BindView(R.id.et_comment)
    EditText editTextComment;
    @BindView(R.id.send)
    FloatingActionButton buttonSend;
    @BindView(R.id.comment_card)
    CardView commentCard;
    @BindView(R.id.bottom_sheet)
    MaterialCardView bottomSheet;
    @BindView(R.id.recycler_view_attachment)
    RecyclerView recyclerViewAttachment;
    @BindView(R.id.blur_Background)
    View blurBackground;


    BottomSheetBehavior sheetBehavior;
    private String username;
    private String userEmail;
    private String userType;
    private String userId;
    private Integer eventID;
    private String addedUserId;
    private String addedUserType;
    private Integer studentCount;
    private String subDate;
    private String assClass;
    private Integer noReply;
    private AssignmentDetailsAdapter assignmentAdapter;
    private List<AssignmentDetailsResponseModel.AssignmentDetailsResultModel.AssignmentDetailsPostModel> assignmentDetailsList = new ArrayList<>();
    private BottomAttachmentAdapter bottomAttachmentAdapter;

    private String cardTitle, cardDesc;

    private List<AttachmentDetailsModel> attachments = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_assignment_details);
        ButterKnife.bind(this);

        sheetBehavior = BottomSheetBehavior.from(bottomSheet);

        init();

        //load data from server
        loadData("");

        bottomSheet.setBackgroundResource(R.drawable.card_background);


        buttonSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String comment = editTextComment.getText().toString();
                if (comment.isEmpty()) {
                    return;
                }
                sendComment(comment);
                editTextComment.setText("");
            }
        });


    }

    private void init() {
        AuthModel authModel = SaveSharedPreference.getData();
        userId = authModel.uid;
        username = authModel.username;
        userEmail = authModel.userEmail;
        userType = authModel.userType;
        studentCount = authModel.studentCount;

        try {

            //get event id from previous activities
            Intent intent = getIntent();
            eventID = intent.getIntExtra("eventId", -1);


        } catch (NullPointerException n) {
            Log.e(TAG, "init: " + n);
        }


        assignmentAdapter = new AssignmentDetailsAdapter(assignmentDetailsList);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(assignmentAdapter);
        assignmentAdapter.setAttachmnetButtonClickListner(new AssignmentDetailsAdapter.AttachmentButtonClickListener() {
            @Override
            public void onClick() {

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    if (checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                        requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 100);
                    } else {
                        toggleBottomDrawer();
                    }
                }


            }
        });

        sheetBehavior.addBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(@NonNull View bottomSheet, int newState) {
                if (newState == BottomSheetBehavior.STATE_COLLAPSED) {
                    blurBackground.setVisibility(View.GONE);
                }

            }

            @Override
            public void onSlide(@NonNull View bottomSheet, float slideOffset) {

            }
        });


        bottomAttachmentAdapter = new BottomAttachmentAdapter(attachments);
        RecyclerView.LayoutManager layoutManager1 = new LinearLayoutManager(getApplicationContext());
        recyclerViewAttachment.setLayoutManager(layoutManager1);
        recyclerViewAttachment.setItemAnimator(new DefaultItemAnimator());
        recyclerViewAttachment.setAdapter(bottomAttachmentAdapter);


    }


    /**
     * load comments
     *
     * @param syncdate syncdate param
     */
    private void loadData(String syncdate) {

        progressVisibility(true);


        Call<AssignmentDetailsResponseModel> assigmentDetailsApiCall = RetrofitClient.get().getAssignmentDetails(String.valueOf(eventID), "");
        assigmentDetailsApiCall.enqueue(new Callback<AssignmentDetailsResponseModel>() {
            @Override
            public void onResponse(@NotNull Call<AssignmentDetailsResponseModel> call, @NotNull Response<AssignmentDetailsResponseModel> response) {
                AssignmentDetailsResponseModel assignmentResponseModel = response.body();
                AssignmentDetailsResponseModel.AssignmentDetailsResultModel assignmentDetailsResultModel = assignmentResponseModel.getResult();
                List<AssignmentDetailsResponseModel.AssignmentDetailsResultModel.AssignmentDetailsPostModel> model =
                        assignmentDetailsResultModel.getPost();

                // get original thread data
                AssignmentDetailsResponseModel.PostData postData = assignmentResponseModel.getPostData();
                cardTitle = postData.getEventTitle();
                cardDesc = postData.getDescription();
                noReply = postData.getNoReply();
                assClass = postData.getClassValue();
                subDate = postData.getSubmitonDate();
                addedUserId = postData.getAddedUserId();
                addedUserType = postData.getAddedUserType();


                MaterialToolbar materialToolbar = findViewById(R.id.appbar);
                setSupportActionBar(materialToolbar);
                getSupportActionBar().setDisplayHomeAsUpEnabled(true);
                getSupportActionBar().setDisplayShowHomeEnabled(true);
                if (noReply == 1) {
                    editTextComment.setVisibility(View.GONE);
                    buttonSend.setVisibility(View.GONE);
                    commentCard.setVisibility(View.GONE);
                }
                //add original thread data to recycler view
                assignmentDetailsList.add(assignmentDetailsResultModel.new AssignmentDetailsPostModel(postData.getUsername(), postData.getUserimage(), postData.getAddedDate(), postData.getDescription(), postData.getEventTitle(), postData.getEventDate()));

                //get comments for the thread
                for (int i = 0; i < model.size(); i++) {
                    assignmentDetailsList.add(assignmentDetailsResultModel.new AssignmentDetailsPostModel(
                            model.get(i).getName(),
                            model.get(i).getImage(),
                            model.get(i).getDate(),
                            model.get(i).getComment()
                    ));
                }
                assignmentAdapter.notifyDataSetChanged();


                //get attachments for the thread
                List<AssignmentDetailsResponseModel.AssignmentDetailsResultModel.Attachment> attachmentList = assignmentDetailsResultModel.getAttachment();
                if (attachmentList.size() == 0) {
                    assignmentAdapter.setIsAssignmentEmpty(true);
                }

                for (int i = 0; i < attachmentList.size(); i++) {
                    attachments.add(new AttachmentDetailsModel(attachmentList.get(i)));
                }
                bottomAttachmentAdapter.notifyDataSetChanged();

                progressVisibility(false);
            }

            @Override
            public void onFailure(Call<AssignmentDetailsResponseModel> call, Throwable t) {
                Log.e(TAG, "onFailure: ", t);
                Alerts.displayToast(getApplicationContext(), "Loading Failed!");
                progressVisibility(false);
            }
        });
    }


    /**
     * used to send a comment
     *
     * @param comment comment to be sent
     */
    private void sendComment(String comment) {
        progressVisibility(true);

        Call<Integer> sendComment = RetrofitClient.get().sendCommentOnAssignment(String.valueOf(eventID), userId, userType, comment);
        sendComment.enqueue(new Callback<Integer>() {
            @Override
            public void onResponse(@NotNull Call<Integer> call, @NotNull Response<Integer> response) {
                if (response.body() != null) {

                    if (response.body() == 1) {
                        Alerts.displayToast(getApplicationContext(), "Message Sent");
                        assignmentDetailsList.clear();
                        loadData("");
                    } else {
                        Log.e(TAG, "onFailure: Response = 0");
                        Alerts.displayToast(getApplicationContext(), "Sending Failed!");
                    }
                } else {
                    Alerts.displayToast(getApplicationContext(), "Operation Failed!");
                }
                progressVisibility(false);
            }

            @Override
            public void onFailure(Call<Integer> call, Throwable t) {
                Log.e(TAG, "onFailure: ", t);
                Alerts.displayToast(getApplicationContext(), "Sending Failed!");
                progressVisibility(false);
            }
        });
    }


    /**
     * delete's the assignment if user has rights
     */
    public void deleteAssignment() {
        progressVisibility(true);

        Call<AssignmentDeteleResponseModel> apicall = RetrofitClient.get().deleteAssignment(userId, userType, String.valueOf(eventID));
        apicall.enqueue(new Callback<AssignmentDeteleResponseModel>() {
            @Override
            public void onResponse(@NotNull Call<AssignmentDeteleResponseModel> call, @NotNull Response<AssignmentDeteleResponseModel> response) {
                if (response.body() != null) {
                    AssignmentDeteleResponseModel model = response.body();
                    if (model.getResult().equals("1")) {
                        Alerts.displayToast(getApplicationContext(), "Deletion Successful");
                        MyDataBase.db.deleteAssignment(eventID);
                        finish();
                        progressVisibility(false);
                    } else {
                        Alerts.displayToast(getApplicationContext(), "Deletion Failed");
                        progressVisibility(false);
                    }
                } else {
                    Alerts.displayToast(getApplicationContext(), "Deletion Failed");
                    progressVisibility(false);
                }
            }

            @Override
            public void onFailure(@NotNull Call<AssignmentDeteleResponseModel> call, @NotNull Throwable t) {
                Log.e(TAG, "onFailure: ", t);
                Alerts.displayToast(getApplicationContext(), "Deletion Failed");
                progressVisibility(false);
            }
        });
    }

    /**
     * open/close bottom drawer
     */
    private void toggleBottomDrawer() {
        if (sheetBehavior.getState() != BottomSheetBehavior.STATE_EXPANDED) {
            blurBackground.setVisibility(View.VISIBLE);
            sheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
        } else {
            sheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.attach_file, menu);

        //check if the user id matches with user id of the thread owner
        Log.e(TAG, "onCreateOptionsMenu: " + addedUserId + " " + addedUserType);
        if (!(userId.equals(addedUserId) && userType.equals(addedUserType))) {
            menu.findItem(R.id.menu_delete).setVisible(false);
            menu.findItem(R.id.menu_edit).setVisible(false);
        }
        menu.findItem(R.id.menu_attachment).setVisible(false);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle presses on the action bar items
        int id = item.getItemId();
        switch (id) {
            case android.R.id.home:
                finish();
                break;
            case R.id.menu_edit:
                Intent intent = new Intent(ActivityAssignmentDetails.this, ActivityNewAssignment.class);
                Bundle mBundle = new Bundle();
                mBundle.putString("class", assClass);
                mBundle.putString("title", cardTitle);
                mBundle.putString("subDate", subDate);
                mBundle.putString("details", cardDesc);
                mBundle.putString("eventId", String.valueOf(eventID));
                mBundle.putString("attachments", new Gson().toJson(attachments));
                intent.putExtras(mBundle);
                startActivity(intent);
                break;
            case R.id.menu_delete:
                new MaterialAlertDialogBuilder(ActivityAssignmentDetails.this, R.style.DialogStyle)
                        .setTitle("Do you want to delete this assignment?")
                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                deleteAssignment();
                            }
                        })
                        .setNegativeButton("No", null)
                        .setCancelable(false)
                        .show();
                break;

        }


        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == 100) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                toggleBottomDrawer();
            } else {
                new MaterialAlertDialogBuilder(ActivityAssignmentDetails.this, R.style.DialogStyle)
                        .setTitle("Storage Access Permission Not Granted")
                        .setMessage("Please grant storage permission to download attached files.")
                        .setPositiveButton("Okay", null)
                        .setCancelable(false)
                        .show();
            }
        }
    }


    /**
     * Display progress bar and uses TOUCHABLE flags
     *
     * @param visibility Give True to display dialog, False to remove
     */
    private void progressVisibility(boolean visibility) {
        if (visibility) {
            llProgressBar.setVisibility(View.VISIBLE);
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                    WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
        } else {
            llProgressBar.setVisibility(View.GONE);
            getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
        }
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent event) {
        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            if (sheetBehavior.getState() == BottomSheetBehavior.STATE_EXPANDED) {

                Rect outRect = new Rect();
                bottomSheet.getGlobalVisibleRect(outRect);

                if (!outRect.contains((int) event.getRawX(), (int) event.getRawY()))
                    sheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
            }
        }
        return super.dispatchTouchEvent(event);
    }
}
