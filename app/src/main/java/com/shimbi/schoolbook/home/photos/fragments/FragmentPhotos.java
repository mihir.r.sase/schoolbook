package com.shimbi.schoolbook.home.photos.fragments;


import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.work.Constraints;
import androidx.work.Data;
import androidx.work.OneTimeWorkRequest;
import androidx.work.WorkInfo;
import androidx.work.WorkManager;

import com.shimbi.schoolbook.R;
import com.shimbi.schoolbook.db.helper.MyDataBase;
import com.shimbi.schoolbook.home.activities.ActivityHome;
import com.shimbi.schoolbook.home.photos.activities.ActivityNewPhoto;
import com.shimbi.schoolbook.home.photos.adapters.PhotosAdapter;
import com.shimbi.schoolbook.home.photos.models.PhotosListResponseModel;
import com.shimbi.schoolbook.home.photos.worker.FetchPhotosWorker;
import com.shimbi.schoolbook.utils.AuthModel;
import com.shimbi.schoolbook.utils.Constants;
import com.shimbi.schoolbook.utils.NetworkStatusChecker;
import com.shimbi.schoolbook.utils.SaveSharedPreference;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * A simple {@link Fragment} subclass.
 */
public class FragmentPhotos extends Fragment {


    private static final String TAG = "FragmentPhotos";
    public static String username;
    public static String userEmail;
    public static String userType;
    public static String userId;
    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;
    @BindView(R.id.swipe_container)
    SwipeRefreshLayout swipeRefreshLayout;
    private PhotosAdapter adapter;
    private GridLayoutManager gridLayoutManager;
    public ProgressBar syncProgress;
    private ImageView noData;

    public FragmentPhotos() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        ActivityHome activityHome = (ActivityHome) getActivity();
        activityHome.setTitleAndLogo("Photos", R.drawable.ic_photo_library_black_24dp);
        ActivityHome.fab.setVisibility(View.GONE);
        if (getActivity() != null) {
            syncProgress = getActivity().findViewById(R.id.progress_sync);
            syncProgress.setVisibility(View.GONE);
            noData = getActivity().findViewById(R.id.no_data);
            noData.setVisibility(View.GONE);
        }

        return inflater.inflate(R.layout.fragment_photos, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);

        init();


        loadDataFromLocal();
        loadOnline();

        ActivityHome.fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getContext(), ActivityNewPhoto.class));
            }
        });
    }

    private void init() {

        AuthModel authModel = SaveSharedPreference.getData();
        userId = authModel.uid;
        username = authModel.username;
        userEmail = authModel.userEmail;
        userType = authModel.userType;

        if (authModel.photoAddFlag.equals("1")) {
            ActivityHome.fab.setVisibility(View.VISIBLE);
        }

        adapter = new PhotosAdapter(getActivity());
        gridLayoutManager = new GridLayoutManager(this.getActivity(), 3);
        recyclerView.setLayoutManager(gridLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(adapter);


    }

    private void loadDataFromLocal() {
        MyDataBase.db.getAlbumData().observe(this, dbAlbums -> {
            if (dbAlbums != null) {
                List<PhotosListResponseModel.Photo> list = new ArrayList<>();
                PhotosListResponseModel model = new PhotosListResponseModel();
                for (int i = 0; i < dbAlbums.size(); i++) {
                    list.add(model.new Photo(dbAlbums.get(i)));
                }
                adapter.addAllAlbums(list);
            }
        });
        MyDataBase.db.getPhotosData().observe(this, dbPhotos -> {
            if (dbPhotos != null) {

                List<PhotosListResponseModel.Photo> list = new ArrayList<>();
                PhotosListResponseModel model = new PhotosListResponseModel();
                for (int i = 0; i < dbPhotos.size(); i++) {
                    if (dbPhotos.get(i).getAlbumId() == null) {
                        list.add(model.new Photo(dbPhotos.get(i)));
                    }
                }
                adapter.addAllPhotos(list);
            }

        });

    }

    private void loadOnline() {

        if (!NetworkStatusChecker.isNetworkConnected(getContext())) {
            Constraints constraints = NetworkStatusChecker.getNetWorkConstraints();
            syncProgress.setVisibility(View.VISIBLE);
            OneTimeWorkRequest photos = new OneTimeWorkRequest.Builder(FetchPhotosWorker.class)
                    .addTag(Constants.TASK_SYNC_PhOTOS)
                    .setConstraints(constraints)
                    .setInputData(new Data.Builder().putString("From", "Sync").build())
                    .build();
            WorkManager manager = WorkManager.getInstance();
            manager.beginWith(photos).enqueue();
            WorkManager.getInstance().getWorkInfoByIdLiveData(photos.getId()).observe(this, new Observer<WorkInfo>() {
                @Override
                public void onChanged(WorkInfo workInfo) {
                    if (workInfo != null && workInfo.getState() == WorkInfo.State.SUCCEEDED) {
                        syncProgress.setVisibility(View.GONE);
                    } else if (workInfo != null && workInfo.getState() == WorkInfo.State.FAILED) {
                        syncProgress.setVisibility(View.GONE);
                    }
                }
            });
        }


//        myPhotoList.clear();
//        ActivityHome.progressVisibility(true,getActivity());
//
//        Call<PhotosListResponseModel> call = retrofitApiCalls.getPhotos(userId,userType);
//
//        call.enqueue(new Callback<PhotosListResponseModel>() {
//            @Override
//            public void onResponse(Call<PhotosListResponseModel> call, Response<PhotosListResponseModel> response) {
//
//                if(response.body()!=null){
//
//                    try{
//                        PhotosListResponseModel model = response.body();
//                        List<PhotosListResponseModel.Photo> photoList = model.getPhotos();
//                        List<PhotosListResponseModel.Album> albumList = model.getAlbum();
//
//                        for(int i=0;i<albumList.size();i++){
//                            myPhotoList.add(model.new Photo(albumList.get(i)));
//                        }
//
//                        for(int i=0;i<photoList.size();i++){
//                            myPhotoList.add(model.new Photo(photoList.get(i)));
//                        }
//                        adapter.notifyDataSetChanged();
//                    }catch (Exception e){
//                        Log.e(TAG, "onResponse: ",e );
//                    }
//
//
//                }else{
//                    Alerts.displayToast(getContext(),"Loading Failed");
//                }
//                ActivityHome.progressVisibility(false,getActivity());
//            }
//
//            @Override
//            public void onFailure(Call<PhotosListResponseModel> call, Throwable t) {
//                Log.e(TAG, "onFailure: ",t );
//                Alerts.displayToast(getContext(),"Loading Failed");
//                ActivityHome.progressVisibility(false,getActivity());
//            }
//        });

    }
}
