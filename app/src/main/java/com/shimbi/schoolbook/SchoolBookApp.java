package com.shimbi.schoolbook;

import android.util.Log;

import androidx.multidex.MultiDexApplication;

import com.shimbi.schoolbook.db.helper.MyDataBase;
import com.shimbi.schoolbook.utils.SaveSharedPreference;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class SchoolBookApp extends MultiDexApplication {
    private static final String TAG = "SchoolBookApp";

    @Override
    public void onCreate() {
        super.onCreate();
        // Required initialization logic here!
        SaveSharedPreference.init(this);

        //init Room Database
        MyDataBase.init(getApplicationContext());

        Calendar dueDate = Calendar.getInstance();

//        dueDate.set(Calendar.HOUR_OF_DAY, 13);
//        dueDate.set(Calendar.MINUTE, 26);
//        dueDate.set(Calendar.SECOND, 0);

//        if (dueDate.before(currentDate)) {
//            dueDate.add(Calendar.HOUR_OF_DAY, 24);
//        }
//
//        long timeDiff = dueDate.getTimeInMillis() - currentDate.getTimeInMillis();
//        Log.e(TAG, "doWork: " + timeDiff / 1000);
//        Constraints.Builder builder = new Constraints.Builder();
//        builder.setRequiresCharging(true);
//        OneTimeWorkRequest clearTableRequest = new OneTimeWorkRequest.Builder(ClearTablesWorker.class)
//                .setConstraints(builder.build())
//                // .setInitialDelay(2,TimeUnit.SECONDS)
//                .addTag(Constants.TASK_CLEAR_TABLES).build();
//        WorkManager.getInstance().enqueue(clearTableRequest);

//        AlarmManager am = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
//        Intent i = new Intent(this, MyAlarm.class);
//        PendingIntent pi = PendingIntent.getBroadcast(this, 0, i, 0);
//        am.setRepeating(AlarmManager.RTC, dueDate.getTimeInMillis(), AlarmManager.INTERVAL_DAY, pi);

        //setupAlarm();
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
        Date currentDates = new Date();
        String currentDate = formatter.format(currentDates);
        String lastAccessDate = SaveSharedPreference.getLastAccessDate();
        SaveSharedPreference.setLastAccessDate(currentDate);
        Log.e(TAG, "onCreate: " + currentDate);
        Log.e(TAG, "onCreate: " + lastAccessDate);
        try {
            if (!lastAccessDate.isEmpty()) {

                if (!currentDate.equals(lastAccessDate)) {
                    MyDataBase.db.deleteRouteDetails();
                    MyDataBase.db.deletePickupLocations();
                    MyDataBase.db.deleteTransportUsers();
                    MyDataBase.db.deleteAllFromEmbarkedUsers();
                    MyDataBase.db.deleteAllFromYetToBeEmbarked();
                    MyDataBase.db.deleteVisitedBusStop();
                    SaveSharedPreference.setJourneyStatus(1);
                    SaveSharedPreference.setStartPickupId(-1);
                    SaveSharedPreference.setEndPickupId(-1);
                    SaveSharedPreference.setRouteId(-1);
                    SaveSharedPreference.setBusId(-1);
                }
            }
        } catch (Exception e) {
            Log.e(TAG, "onCreate: ", e);
        }
    }


}
