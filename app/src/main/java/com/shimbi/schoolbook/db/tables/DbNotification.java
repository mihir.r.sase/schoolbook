package com.shimbi.schoolbook.db.tables;


import androidx.annotation.NonNull;
import androidx.room.Entity;

import java.util.Date;

@Entity(tableName = "notification_center", primaryKeys = {"eventId", "module"})
public class DbNotification {

    @NonNull
    Integer eventId;

    @NonNull
    String module;

    String title;

    String message;

    Date dateTime;


    public DbNotification(@NonNull Integer eventId, @NonNull String module, String title, String message, Date dateTime) {
        this.eventId = eventId;
        this.module = module;
        this.title = title;
        this.message = message;
        this.dateTime = dateTime;
    }

    @NonNull
    public Integer getEventId() {
        return eventId;
    }

    public void setEventId(@NonNull Integer eventId) {
        this.eventId = eventId;
    }

    @NonNull
    public String getModule() {
        return module;
    }

    public void setModule(@NonNull String module) {
        this.module = module;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Date getDateTime() {
        return dateTime;
    }

    public void setDateTime(Date dateTime) {
        this.dateTime = dateTime;
    }
}
