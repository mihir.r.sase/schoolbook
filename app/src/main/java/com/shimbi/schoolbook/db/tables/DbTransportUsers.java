package com.shimbi.schoolbook.db.tables;

import com.shimbi.schoolbook.home.transport.models.TransportDataModel;

import androidx.annotation.NonNull;
import androidx.room.Entity;

@Entity(tableName = "transport_users", primaryKeys = {"studentId", "userType"})
public class DbTransportUsers {


    @NonNull
    Integer studentId;


    @NonNull
    Integer userType;

    String studentName;

    Integer routeId;

    String paymentDate;

    Integer userOrder;

    String mobile;

    String className;

    String address;

    String pickupName;

    String updateFlag;

    String updateDropFlag;

    String pickupArea;

    String latitude;

    String longitude;

    String type;

    Integer pickupId;


    public DbTransportUsers(@NonNull Integer studentId, @NonNull Integer userType, String studentName, Integer routeId, String paymentDate, Integer userOrder, String mobile, String className, String address, String pickupName, String updateFlag, String updateDropFlag, String pickupArea, String latitude, String longitude, String type, Integer pickupId) {
        this.studentId = studentId;
        this.userType = userType;
        this.studentName = studentName;
        this.routeId = routeId;
        this.paymentDate = paymentDate;
        this.userOrder = userOrder;
        this.mobile = mobile;
        this.className = className;
        this.address = address;
        this.pickupName = pickupName;
        this.updateFlag = updateFlag;
        this.updateDropFlag = updateDropFlag;
        this.pickupArea = pickupArea;
        this.latitude = latitude;
        this.longitude = longitude;
        this.type = type;
        this.pickupId = pickupId;
    }

    public DbTransportUsers(TransportDataModel.UserRec model) {
        this.studentId = Integer.valueOf(model.getStudentId());
        this.userType = Integer.valueOf(model.getUserType());
        this.studentName = model.getStudentName();
        this.routeId = Integer.valueOf(model.getRouteId());
        this.paymentDate = model.getPaymentDate();
        this.userOrder = Integer.valueOf(model.getUserOrder());
        this.mobile = model.getMobile();
        this.className = model.getClassName();
        this.address = model.getAddress();
        this.pickupName = model.getPickupName();
        this.updateFlag = model.getUpdateFlag();
        this.updateDropFlag = model.getUpdateDropFlag();
        this.pickupArea = model.getPickupArea();
        this.latitude = model.getLatitude();
        this.longitude = model.getLongitude();
        this.type = model.getType();
        this.pickupId = Integer.valueOf(model.getPickupId());
    }


    @Override
    public String toString() {
        return "DbTransportUsers{" +
                "studentId=" + studentId +
                ", userType=" + userType +
                ", studentName='" + studentName + '\'' +
                ", routeId=" + routeId +
                ", paymentDate='" + paymentDate + '\'' +
                ", userOrder=" + userOrder +
                ", mobile='" + mobile + '\'' +
                ", className='" + className + '\'' +
                ", address='" + address + '\'' +
                ", pickupName='" + pickupName + '\'' +
                ", updateFlag='" + updateFlag + '\'' +
                ", updateDropFlag='" + updateDropFlag + '\'' +
                ", pickupArea='" + pickupArea + '\'' +
                ", latitude='" + latitude + '\'' +
                ", longitude='" + longitude + '\'' +
                ", type='" + type + '\'' +
                ", pickupId=" + pickupId +
                '}';
    }

    @NonNull
    public Integer getStudentId() {
        return studentId;
    }

    public void setStudentId(@NonNull Integer studentId) {
        this.studentId = studentId;
    }

    @NonNull
    public Integer getUserType() {
        return userType;
    }

    public void setUserType(@NonNull Integer userType) {
        this.userType = userType;
    }

    public String getStudentName() {
        return studentName;
    }

    public void setStudentName(String studentName) {
        this.studentName = studentName;
    }

    public Integer getRouteId() {
        return routeId;
    }

    public void setRouteId(Integer routeId) {
        this.routeId = routeId;
    }

    public String getPaymentDate() {
        return paymentDate;
    }

    public void setPaymentDate(String paymentDate) {
        this.paymentDate = paymentDate;
    }

    public Integer getUserOrder() {
        return userOrder;
    }

    public void setUserOrder(Integer userOrder) {
        this.userOrder = userOrder;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPickupName() {
        return pickupName;
    }

    public void setPickupName(String pickupName) {
        this.pickupName = pickupName;
    }

    public String getUpdateFlag() {
        return updateFlag;
    }

    public void setUpdateFlag(String updateFlag) {
        this.updateFlag = updateFlag;
    }

    public String getUpdateDropFlag() {
        return updateDropFlag;
    }

    public void setUpdateDropFlag(String updateDropFlag) {
        this.updateDropFlag = updateDropFlag;
    }

    public String getPickupArea() {
        return pickupArea;
    }

    public void setPickupArea(String pickupArea) {
        this.pickupArea = pickupArea;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Integer getPickupId() {
        return pickupId;
    }

    public void setPickupId(Integer pickupId) {
        this.pickupId = pickupId;
    }
}
