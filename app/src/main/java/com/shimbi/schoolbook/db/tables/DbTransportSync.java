package com.shimbi.schoolbook.db.tables;

import androidx.annotation.NonNull;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "transport_sync")
public class DbTransportSync {

    @PrimaryKey
    @NonNull
    String dateTime;

    String markerTitle;

    Integer routeId;

    Integer busId;

    String attend;

    String absent;

    String flag;


    public DbTransportSync(String markerTitle, Integer routeId, Integer busId, String attend, String absent, String flag, String dateTime) {
        this.markerTitle = markerTitle;
        this.routeId = routeId;
        this.busId = busId;
        this.attend = attend;
        this.absent = absent;
        this.flag = flag;
        this.dateTime = dateTime;
    }


    public String getMarkerTitle() {
        return markerTitle;
    }

    public void setMarkerTitle(String markerTitle) {
        this.markerTitle = markerTitle;
    }

    public Integer getRouteId() {
        return routeId;
    }

    public void setRouteId(Integer routeId) {
        this.routeId = routeId;
    }

    public Integer getBusId() {
        return busId;
    }

    public void setBusId(Integer busId) {
        this.busId = busId;
    }

    public String getAttend() {
        return attend;
    }

    public void setAttend(String attend) {
        this.attend = attend;
    }

    public String getAbsent() {
        return absent;
    }

    public void setAbsent(String absent) {
        this.absent = absent;
    }

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }

    public String getDateTime() {
        return dateTime;
    }

    public void setDateTime(String dateTime) {
        this.dateTime = dateTime;
    }
}
