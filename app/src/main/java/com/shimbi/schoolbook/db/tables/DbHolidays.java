package com.shimbi.schoolbook.db.tables;


import com.shimbi.schoolbook.home.holidays.models.HolidayResponseModel;

import androidx.annotation.NonNull;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "holidays")
public class DbHolidays {

    @NonNull
    @PrimaryKey
    Integer holidayId;

    String userId;

    String holidayName;

    String fromDate;

    String toDate;

    public DbHolidays(HolidayResponseModel model) {

        this.holidayId = Integer.valueOf(model.getHolidayId());
        this.userId = model.getUid();
        this.holidayName = model.getHolidayName();
        this.fromDate = model.getFromDate();
        this.toDate = model.getToDate();
    }

    public DbHolidays() {

    }

    @NonNull
    public Integer getHolidayId() {
        return holidayId;
    }

    public void setHolidayId(@NonNull Integer holidayId) {
        this.holidayId = holidayId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getHolidayName() {
        return holidayName;
    }

    public void setHolidayName(String holidayName) {
        this.holidayName = holidayName;
    }

    public String getFromDate() {
        return fromDate;
    }

    public void setFromDate(String fromDate) {
        this.fromDate = fromDate;
    }

    public String getToDate() {
        return toDate;
    }

    public void setToDate(String toDate) {
        this.toDate = toDate;
    }
}
