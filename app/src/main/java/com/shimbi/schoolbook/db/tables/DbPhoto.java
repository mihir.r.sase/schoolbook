package com.shimbi.schoolbook.db.tables;


import com.shimbi.schoolbook.home.photos.models.PhotosListResponseModel;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "photos")
public class DbPhoto {


    @PrimaryKey
    @NonNull
    Integer photoId;

    @Nullable
    Integer albumId = null;

    String userId;

    String userType;

    String photoTitle;

    String photoFile;

    String addedDate;

    String description;

    String likeUsers;

    String likeCount;

    Integer deleteFlag;


    public DbPhoto() {

    }

    public DbPhoto(PhotosListResponseModel.Photo model) {
        this.photoId = Integer.valueOf(model.getPhotoId());
        this.userId = model.getUserId();
        this.userType = model.getUserType();
        this.photoTitle = model.getPhotoTitle();
        this.photoFile = model.getPhotoFile();
        this.addedDate = model.getAddedDate();
        this.description = model.getDescription();
        this.likeUsers = model.getLikeUsers();
        this.likeCount = model.getLikeCount();
        this.deleteFlag = model.getDeleteFlag();
    }


    public DbPhoto(PhotosListResponseModel.Photo model, String album) {
        this.photoId = Integer.valueOf(model.getPhotoId());
        this.albumId = Integer.valueOf(album);
        this.userId = model.getUserId();
        this.userType = model.getUserType();
        this.photoTitle = model.getPhotoTitle();
        this.photoFile = model.getPhotoFile();
        this.addedDate = model.getAddedDate();
        this.description = model.getDescription();
        this.likeUsers = model.getLikeUsers();
        this.likeCount = model.getLikeCount();
        this.deleteFlag = model.getDeleteFlag();
    }

    @NonNull
    public Integer getPhotoId() {
        return photoId;
    }

    public void setPhotoId(@NonNull Integer photoId) {
        this.photoId = photoId;
    }

    @NonNull
    public Integer getAlbumId() {
        return albumId;
    }

    public void setAlbumId(@NonNull Integer albumId) {
        this.albumId = albumId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserType() {
        return userType;
    }

    public void setUserType(String userType) {
        this.userType = userType;
    }

    public String getPhotoTitle() {
        return photoTitle;
    }

    public void setPhotoTitle(String photoTitle) {
        this.photoTitle = photoTitle;
    }

    public String getPhotoFile() {
        return photoFile;
    }

    public void setPhotoFile(String photoFile) {
        this.photoFile = photoFile;
    }

    public String getAddedDate() {
        return addedDate;
    }

    public void setAddedDate(String addedDate) {
        this.addedDate = addedDate;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getLikeUsers() {
        return likeUsers;
    }

    public void setLikeUsers(String likeUsers) {
        this.likeUsers = likeUsers;
    }

    public String getLikeCount() {
        return likeCount;
    }

    public void setLikeCount(String likeCount) {
        this.likeCount = likeCount;
    }

    public Integer getDeleteFlag() {
        return deleteFlag;
    }

    public void setDeleteFlag(Integer deleteFlag) {
        this.deleteFlag = deleteFlag;
    }

}
