package com.shimbi.schoolbook.db.tables;


import androidx.annotation.NonNull;
import androidx.room.Entity;

@Entity(tableName = "embarked_users", primaryKeys = {"studentId", "userType"})
public class DbEmbarkedUsers {


    @NonNull
    Integer studentId;

    @NonNull
    Integer userType;

    Integer pickupId;

    String studentName;

    String pickupName;

    Integer routeId;


    public DbEmbarkedUsers(@NonNull Integer studentId, @NonNull Integer userType, Integer pickupId, String studentName, String pickupName, Integer routeId) {
        this.studentId = studentId;
        this.userType = userType;
        this.pickupId = pickupId;
        this.studentName = studentName;
        this.pickupName = pickupName;
        this.routeId = routeId;
    }

    public DbEmbarkedUsers(DbYetToBeEmbarked model) {
        this.studentId = model.getStudentId();
        this.userType = model.getUserType();
        this.pickupId = model.getPickupId();
        this.studentName = model.getStudentName();
        this.pickupName = model.getPickupName();
        this.routeId = model.getRouteId();
    }

    public Integer getRouteId() {
        return routeId;
    }

    public void setRouteId(Integer routeId) {
        this.routeId = routeId;
    }

    @NonNull
    public Integer getStudentId() {
        return studentId;
    }

    public void setStudentId(@NonNull Integer studentId) {
        this.studentId = studentId;
    }

    @NonNull
    public Integer getUserType() {
        return userType;
    }

    public void setUserType(@NonNull Integer userType) {
        this.userType = userType;
    }

    public Integer getPickupId() {
        return pickupId;
    }

    public void setPickupId(Integer pickupId) {
        this.pickupId = pickupId;
    }

    public String getStudentName() {
        return studentName;
    }

    public void setStudentName(String studentName) {
        this.studentName = studentName;
    }

    public String getPickupName() {
        return pickupName;
    }

    public void setPickupName(String pickupName) {
        this.pickupName = pickupName;
    }
}
