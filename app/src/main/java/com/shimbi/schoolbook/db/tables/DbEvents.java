package com.shimbi.schoolbook.db.tables;


import com.shimbi.schoolbook.home.events.models.EventsResponseModel;

import androidx.annotation.NonNull;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "events")
public class DbEvents {


    @PrimaryKey
    @NonNull
    Integer eventId;

    String username;

    Integer noReplyFlag;

    String title;

    String description;

    String addedDate;

    String classKey;

    String classValue;

    String startDate;

    String endDate;

    String startTime;

    String endTime;

    String eventStatue;

    String groupName;

    String eventDate;

    String userImage;

    public DbEvents(@NonNull Integer eventId, String username, Integer noReplyFlag, String title, String description, String addedDate, String classKey, String classValue, String startDate, String endDate, String startTime, String endTime, String eventStatue, String groupName, String eventDate, String userImage) {
        this.eventId = eventId;
        this.username = username;
        this.noReplyFlag = noReplyFlag;
        this.title = title;
        this.description = description;
        this.addedDate = addedDate;
        this.classKey = classKey;
        this.classValue = classValue;
        this.startDate = startDate;
        this.endDate = endDate;
        this.startTime = startTime;
        this.endTime = endTime;
        this.eventStatue = eventStatue;
        this.groupName = groupName;
        this.eventDate = eventDate;
        this.userImage = userImage;
    }

    public DbEvents(EventsResponseModel.MyEvent myEvent) {

        this.eventId = myEvent.getEventId();
        this.username = myEvent.getUsername();
        this.noReplyFlag = myEvent.getNoReply();
        this.title = myEvent.getEventTitle();
        this.description = myEvent.getDescription();
        this.addedDate = myEvent.getAddedDate();
        this.classKey = myEvent.getClassKey();
        this.classValue = myEvent.getClassValue();
        this.startDate = myEvent.getStartdate();
        this.endDate = myEvent.getEnddate();
        this.startTime = myEvent.getStarttime();
        this.endTime = myEvent.getEndtime();
        this.eventStatue = myEvent.getEventstatue();
        this.groupName = myEvent.getGroupName();
        this.eventDate = myEvent.getEventDate();
        this.userImage = myEvent.getUserimage();
    }

    public String getUserImage() {
        return userImage;
    }

    public void setUserImage(String userImage) {
        this.userImage = userImage;
    }

    public String getEventDate() {
        return eventDate;
    }

    public void setEventDate(String eventDate) {
        this.eventDate = eventDate;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    @NonNull
    public Integer getEventId() {
        return eventId;
    }

    public void setEventId(@NonNull Integer eventId) {
        this.eventId = eventId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public Integer getNoReplyFlag() {
        return noReplyFlag;
    }

    public void setNoReplyFlag(Integer noReplyFlag) {
        this.noReplyFlag = noReplyFlag;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getAddedDate() {
        return addedDate;
    }

    public void setAddedDate(String addedDate) {
        this.addedDate = addedDate;
    }

    public String getClassKey() {
        return classKey;
    }

    public void setClassKey(String classKey) {
        this.classKey = classKey;
    }

    public String getClassValue() {
        return classValue;
    }

    public void setClassValue(String classValue) {
        this.classValue = classValue;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getEventStatue() {
        return eventStatue;
    }

    public void setEventStatue(String eventStatue) {
        this.eventStatue = eventStatue;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }
}
