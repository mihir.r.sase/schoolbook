package com.shimbi.schoolbook.db.tables;


import com.shimbi.schoolbook.home.transport.models.TransportDataModel;

import androidx.annotation.NonNull;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "pickup_locations")
public class DbPickupLocations {

    @PrimaryKey
    @NonNull
    Integer pickupId;

    String pickupName;

    String pickupArea;

    String latitude;

    String longitude;

    Integer routeId;

    public DbPickupLocations(@NonNull Integer pickupId, String pickupName, String pickupArea, String latitude, String longitude, Integer routeId) {
        this.pickupId = pickupId;
        this.pickupName = pickupName;
        this.pickupArea = pickupArea;
        this.latitude = latitude;
        this.longitude = longitude;
        this.routeId = routeId;
    }

    public DbPickupLocations(TransportDataModel.PickupLocArr model) {
        this.pickupId = Integer.valueOf(model.getPickupId());
        this.pickupName = model.getPickupName();
        this.pickupArea = model.getPickupArea();
        this.latitude = model.getLatitude();
        this.longitude = model.getLongitude();
        this.routeId = Integer.valueOf(model.getRouteId());
    }

    @Override
    public String toString() {
        return "DbPickupLocations{" +
                "pickupId=" + pickupId +
                ", pickupName='" + pickupName + '\'' +
                ", pickupArea='" + pickupArea + '\'' +
                ", latitude='" + latitude + '\'' +
                ", longitude='" + longitude + '\'' +
                ", routeId=" + routeId +
                '}';
    }

    @NonNull
    public Integer getPickupId() {
        return pickupId;
    }

    public void setPickupId(@NonNull Integer pickupId) {
        this.pickupId = pickupId;
    }

    public String getPickupName() {
        return pickupName;
    }

    public void setPickupName(String pickupName) {
        this.pickupName = pickupName;
    }

    public String getPickupArea() {
        return pickupArea;
    }

    public void setPickupArea(String pickupArea) {
        this.pickupArea = pickupArea;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public Integer getRouteId() {
        return routeId;
    }

    public void setRouteId(Integer routeId) {
        this.routeId = routeId;
    }
}
