package com.shimbi.schoolbook.db.tables;


import com.shimbi.schoolbook.home.messages.models.MessageResponseModel;

import androidx.annotation.NonNull;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "messages")
public class DbMessages {

    @NonNull
    @PrimaryKey
    Integer eventId;

    String userName;

    String userImage;

    String title;

    String description;

    String addedDate;

    public DbMessages(@NonNull Integer eventId, String userName, String userImage, String title, String description, String addedDate) {
        this.eventId = eventId;
        this.userName = userName;
        this.userImage = userImage;
        this.title = title;
        this.description = description;
        this.addedDate = addedDate;
    }

    public DbMessages(MessageResponseModel.MessageResultModel model) {
        this.eventId = model.getEventId();
        this.userName = model.getUsername();
        this.userImage = model.getUserProfileImg();
        this.title = model.getEventTitle();
        this.description = model.getDescription();
        this.addedDate = model.getAddedDate();
    }

    @NonNull
    public Integer getEventId() {
        return eventId;
    }

    public void setEventId(@NonNull Integer eventId) {
        this.eventId = eventId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserImage() {
        return userImage;
    }

    public void setUserImage(String userImage) {
        this.userImage = userImage;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getAddedDate() {
        return addedDate;
    }

    public void setAddedDate(String addedDate) {
        this.addedDate = addedDate;
    }
}
