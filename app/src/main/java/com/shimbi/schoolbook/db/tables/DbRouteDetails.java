package com.shimbi.schoolbook.db.tables;


import androidx.annotation.NonNull;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import com.shimbi.schoolbook.home.transport.models.AssignedRouteModel;

import java.util.Date;

@Entity(tableName = "route_details")
public class DbRouteDetails {

    @PrimaryKey
    @NonNull
    Integer routeId;

    Integer busId;

    String routeName;

    public Integer journeyStatus;

    Date timeJourneyStart;
    Date timeSchoolDisembark;
    Date timeSchoolEmbark;
    Date timeJourneyEnd;

    public DbRouteDetails(@NonNull Integer routeId, Integer busId, String routeName, Integer journeyStatus, Date timeJourneyStart, Date timeSchoolDisembark, Date timeSchoolEmbark, Date timeJourneyEnd) {
        this.routeId = routeId;
        this.busId = busId;
        this.routeName = routeName;
        this.journeyStatus = journeyStatus;
        this.timeJourneyStart = timeJourneyStart;
        this.timeSchoolDisembark = timeSchoolDisembark;
        this.timeSchoolEmbark = timeSchoolEmbark;
        this.timeJourneyEnd = timeJourneyEnd;
    }

    public DbRouteDetails(AssignedRouteModel.RouteDetail routeDetail) {
        routeId = Integer.valueOf(routeDetail.getRouteId());
        busId = Integer.valueOf(routeDetail.getBusId());
        routeName = routeDetail.getRouteName();
        journeyStatus = 1;
    }

    @Override
    public String toString() {
        return "DbRouteDetails{" +
                "routeId=" + routeId +
                ", busId=" + busId +
                ", routeName='" + routeName + '\'' +
                '}';
    }

    public Integer getJourneyStatus() {
        return journeyStatus;
    }

    public void setJourneyStatus(Integer journeyStatus) {
        this.journeyStatus = journeyStatus;
    }

    public Date getTimeJourneyStart() {
        return timeJourneyStart;
    }

    public void setTimeJourneyStart(Date timeJourneyStart) {
        this.timeJourneyStart = timeJourneyStart;
    }

    public Date getTimeSchoolDisembark() {
        return timeSchoolDisembark;
    }

    public void setTimeSchoolDisembark(Date timeSchoolDisembark) {
        this.timeSchoolDisembark = timeSchoolDisembark;
    }

    public Date getTimeSchoolEmbark() {
        return timeSchoolEmbark;
    }

    public void setTimeSchoolEmbark(Date timeSchoolEmbark) {
        this.timeSchoolEmbark = timeSchoolEmbark;
    }

    public Date getTimeJourneyEnd() {
        return timeJourneyEnd;
    }

    public void setTimeJourneyEnd(Date timeJourneyEnd) {
        this.timeJourneyEnd = timeJourneyEnd;
    }

    @NonNull
    public Integer getRouteId() {
        return routeId;
    }

    public void setRouteId(@NonNull Integer routeId) {
        this.routeId = routeId;
    }

    public Integer getBusId() {
        return busId;
    }

    public void setBusId(Integer busId) {
        this.busId = busId;
    }

    public String getRouteName() {
        return routeName;
    }

    public void setRouteName(String routeName) {
        this.routeName = routeName;
    }
}
