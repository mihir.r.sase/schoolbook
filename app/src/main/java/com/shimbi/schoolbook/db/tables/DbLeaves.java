package com.shimbi.schoolbook.db.tables;


import com.shimbi.schoolbook.home.leaves.models.LeaveListResponseModel;

import androidx.annotation.NonNull;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "leaves")
public class DbLeaves {

    @NonNull
    @PrimaryKey
    Integer leaveId;

    Integer leaveFlag;

    String leaveReason;

    String reason;

    String leaveStatus;

    String addedDate;

    String leaveStartDate;

    String leaveEndDate;


    public DbLeaves() {

    }

    public DbLeaves(LeaveListResponseModel.Leave model) {
        this.leaveId = Integer.valueOf(model.getLeaveId());
        this.leaveFlag = model.getLeaveFlag();
        this.leaveReason = model.getLeaveReason();
        this.reason = model.getReason();
        this.leaveStatus = model.getLeaveStatus();
        this.addedDate = model.getAddedDate();
        this.leaveStartDate = model.getLeaveStartDate();
        this.leaveEndDate = model.getLeaveEndDate();
    }

    @NonNull
    public Integer getLeaveId() {
        return leaveId;
    }

    public void setLeaveId(@NonNull Integer leaveId) {
        this.leaveId = leaveId;
    }

    public Integer getLeaveFlag() {
        return leaveFlag;
    }

    public void setLeaveFlag(Integer leaveFlag) {
        this.leaveFlag = leaveFlag;
    }

    public String getLeaveReason() {
        return leaveReason;
    }

    public void setLeaveReason(String leaveReason) {
        this.leaveReason = leaveReason;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public String getLeaveStatus() {
        return leaveStatus;
    }

    public void setLeaveStatus(String leaveStatus) {
        this.leaveStatus = leaveStatus;
    }

    public String getAddedDate() {
        return addedDate;
    }

    public void setAddedDate(String addedDate) {
        this.addedDate = addedDate;
    }

    public String getLeaveStartDate() {
        return leaveStartDate;
    }

    public void setLeaveStartDate(String leaveStartDate) {
        this.leaveStartDate = leaveStartDate;
    }

    public String getLeaveEndDate() {
        return leaveEndDate;
    }

    public void setLeaveEndDate(String leaveEndDate) {
        this.leaveEndDate = leaveEndDate;
    }
}
