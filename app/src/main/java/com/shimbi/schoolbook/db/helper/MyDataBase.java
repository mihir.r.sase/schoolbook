package com.shimbi.schoolbook.db.helper;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.room.TypeConverters;

import com.shimbi.schoolbook.db.tables.DbAlbum;
import com.shimbi.schoolbook.db.tables.DbAssignment;
import com.shimbi.schoolbook.db.tables.DbEmbarkedUsers;
import com.shimbi.schoolbook.db.tables.DbEvents;
import com.shimbi.schoolbook.db.tables.DbHolidays;
import com.shimbi.schoolbook.db.tables.DbLeaveHoliday;
import com.shimbi.schoolbook.db.tables.DbLeaves;
import com.shimbi.schoolbook.db.tables.DbMessages;
import com.shimbi.schoolbook.db.tables.DbNotice;
import com.shimbi.schoolbook.db.tables.DbNotification;
import com.shimbi.schoolbook.db.tables.DbPhoto;
import com.shimbi.schoolbook.db.tables.DbPickupLocations;
import com.shimbi.schoolbook.db.tables.DbRouteDetails;
import com.shimbi.schoolbook.db.tables.DbTransportSync;
import com.shimbi.schoolbook.db.tables.DbTransportUsers;
import com.shimbi.schoolbook.db.tables.DbUser;
import com.shimbi.schoolbook.db.tables.DbVisitedBusStop;
import com.shimbi.schoolbook.db.tables.DbYetToBeEmbarked;


@Database(entities = {
        DbEvents.class,
        DbNotice.class,
        DbAssignment.class,
        DbMessages.class,
        DbHolidays.class,
        DbLeaves.class,
        DbLeaveHoliday.class,
        DbPhoto.class,
        DbAlbum.class,
        DbUser.class,
        DbRouteDetails.class,
        DbPickupLocations.class,
        DbTransportUsers.class,
        DbEmbarkedUsers.class,
        DbYetToBeEmbarked.class,
        DbVisitedBusStop.class,
        DbTransportSync.class,
        DbNotification.class

}, version = 4, exportSchema = false)
@TypeConverters({Converters.class})
public abstract class MyDataBase extends RoomDatabase {

    public static MyDataBase obj;
    public static DAO db;

    public static void init(Context context) {

        obj = Room.databaseBuilder(context, MyDataBase.class, "school_book.db")
                .allowMainThreadQueries()
                .fallbackToDestructiveMigration()
                .build();
        db = obj.dao();


    }

    public abstract DAO dao();


}
