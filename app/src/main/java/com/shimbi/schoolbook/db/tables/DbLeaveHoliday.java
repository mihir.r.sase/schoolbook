package com.shimbi.schoolbook.db.tables;


import com.shimbi.schoolbook.home.leaves.models.LeaveListResponseModel;

import androidx.annotation.NonNull;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "leaveHoliday")
public class DbLeaveHoliday {


    @NonNull
    @PrimaryKey
    Integer holidayId;

    String holidayName;

    String holidayFromDate;

    String holidayToDate;

    String status;


    public DbLeaveHoliday() {

    }

    public DbLeaveHoliday(LeaveListResponseModel.Holiday model) {
        holidayId = Integer.valueOf(model.getHolidayId());
        holidayName = model.getName();
        holidayFromDate = model.getFromDate();
        holidayToDate = model.getToDate();
        status = model.getStatus();
    }

    @NonNull
    public Integer getHolidayId() {
        return holidayId;
    }

    public void setHolidayId(@NonNull Integer holidayId) {
        this.holidayId = holidayId;
    }

    public String getHolidayName() {
        return holidayName;
    }

    public void setHolidayName(String holidayName) {
        this.holidayName = holidayName;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getHolidayFromDate() {
        return holidayFromDate;
    }

    public void setHolidayFromDate(String holidayFromDate) {
        this.holidayFromDate = holidayFromDate;
    }

    public String getHolidayToDate() {
        return holidayToDate;
    }

    public void setHolidayToDate(String holidayToDate) {
        this.holidayToDate = holidayToDate;
    }
}
