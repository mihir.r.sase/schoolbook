package com.shimbi.schoolbook.db.tables;

import androidx.annotation.NonNull;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import com.shimbi.schoolbook.home.transport.models.TransportDataModel;

@Entity(tableName = "visited_bus_stop")
public class DbVisitedBusStop {

    @PrimaryKey
    @NonNull
    Integer pickupId;
    Boolean hasVisited;
    public Integer routeId;


    public DbVisitedBusStop(@NonNull Integer pickupId, Boolean hasVisited, Integer routeId) {
        this.pickupId = pickupId;
        this.hasVisited = hasVisited;
        this.routeId = routeId;
    }

    public DbVisitedBusStop(TransportDataModel.PickupLocArr model) {
        this.pickupId = Integer.valueOf(model.getPickupId());
        this.hasVisited = false;
        this.routeId = Integer.valueOf(model.getRouteId());
    }

    @NonNull
    public Integer getPickupId() {
        return pickupId;
    }

    public void setPickupId(@NonNull Integer pickupId) {
        this.pickupId = pickupId;
    }

    public Boolean getHasVisited() {
        return hasVisited;
    }

    public void setHasVisited(Boolean hasVisited) {
        this.hasVisited = hasVisited;
    }
}
