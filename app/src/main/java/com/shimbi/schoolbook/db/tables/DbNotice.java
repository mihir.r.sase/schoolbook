package com.shimbi.schoolbook.db.tables;


import com.shimbi.schoolbook.home.notice.models.NoticeResponseModel;

import androidx.annotation.NonNull;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "notice")
public class DbNotice {

    @PrimaryKey
    @NonNull
    Integer eventId;

    String userName;

    Integer noReply;

    String eventTitle;

    String description;

    String addedDate;

    String userImage;

    String classKey;

    String classValue;

    String groupName;

    String noticeSms;

    Integer sendSmsFlag;


    public DbNotice(@NonNull Integer eventId, String userName, Integer noReply, String eventTitle, String description, String addedDate, String userImage, String classKey, String classValue, String groupName, String noticeSms, Integer sendSmsFlag) {
        this.eventId = eventId;
        this.userName = userName;
        this.noReply = noReply;
        this.eventTitle = eventTitle;
        this.description = description;
        this.addedDate = addedDate;
        this.userImage = userImage;
        this.classKey = classKey;
        this.classValue = classValue;
        this.groupName = groupName;
        this.noticeSms = noticeSms;
        this.sendSmsFlag = sendSmsFlag;
    }

    public DbNotice(NoticeResponseModel.NoticeResultModel noticeResultModel) {
        this.eventId = noticeResultModel.getEventId();
        this.userName = noticeResultModel.getUsername();
        this.noReply = noticeResultModel.getNoReply();
        this.eventTitle = noticeResultModel.getEventTitle();
        this.description = noticeResultModel.getDescription();
        this.addedDate = noticeResultModel.getAddedDate();
        this.userImage = noticeResultModel.getUserimage();
        this.classKey = noticeResultModel.getClassKey();
        this.classValue = noticeResultModel.getClassValue();
        this.groupName = noticeResultModel.getGroupName();
        this.noticeSms = noticeResultModel.getNoticeSms();
        this.sendSmsFlag = noticeResultModel.getSendSmsFlag();


    }
//
//    public static void updateOrCreate(NoticeResponseModel.NoticeResultModel noticeResultModel) {
//        DbNotice notice = new DbNotice(noticeResultModel);
//         if (MyDataBase.db.checkIfPresentInNotice(notices.get(i).getEventId()) == 0) {
//                            MyDataBase.db.insertToNotice(dbNotice);
//                            Log.e(TAG, "doWork: Insertion Successful");
//                        } else {
//                            MyDataBase.db.updateNotices(dbNotice);
//                            Log.e(TAG, "doWork: Updating Successful");
//                        }
//    }

    @NonNull
    public Integer getEventId() {
        return eventId;
    }

    public void setEventId(@NonNull Integer eventId) {
        this.eventId = eventId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public Integer getNoReply() {
        return noReply;
    }

    public void setNoReply(Integer noReply) {
        this.noReply = noReply;
    }

    public String getEventTitle() {
        return eventTitle;
    }

    public void setEventTitle(String eventTitle) {
        this.eventTitle = eventTitle;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getAddedDate() {
        return addedDate;
    }

    public void setAddedDate(String addedDate) {
        this.addedDate = addedDate;
    }

    public String getUserImage() {
        return userImage;
    }

    public void setUserImage(String userImage) {
        this.userImage = userImage;
    }

    public String getClassKey() {
        return classKey;
    }

    public void setClassKey(String classKey) {
        this.classKey = classKey;
    }

    public String getClassValue() {
        return classValue;
    }

    public void setClassValue(String classValue) {
        this.classValue = classValue;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public String getNoticeSms() {
        return noticeSms;
    }

    public void setNoticeSms(String noticeSms) {
        this.noticeSms = noticeSms;
    }

    public Integer getSendSmsFlag() {
        return sendSmsFlag;
    }

    public void setSendSmsFlag(Integer sendSmsFlag) {
        this.sendSmsFlag = sendSmsFlag;
    }
}
