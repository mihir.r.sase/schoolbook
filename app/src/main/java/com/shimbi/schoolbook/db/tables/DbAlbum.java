package com.shimbi.schoolbook.db.tables;


import com.shimbi.schoolbook.home.photos.models.DeletePhotoResponseModel;
import com.shimbi.schoolbook.home.photos.models.PhotosListResponseModel;

import androidx.annotation.NonNull;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "album")
public class DbAlbum {

    @PrimaryKey
    @NonNull
    Integer albumId;

    String albumName;

    String photo;


    public DbAlbum() {

    }


    public DbAlbum(PhotosListResponseModel.Album album) {
        this.albumId = Integer.valueOf(album.getPhotoCatId());
        this.albumName = album.getAlbumName();
        this.photo = album.getPhoto();
    }

    public DbAlbum(DeletePhotoResponseModel.Album album) {
        this.albumId = Integer.valueOf(album.getPhotoCatId());
        this.albumName = album.getAlbumName();
        this.photo = album.getPhoto();
    }

    @NonNull
    public Integer getAlbumId() {
        return albumId;
    }

    public void setAlbumId(@NonNull Integer albumId) {
        this.albumId = albumId;
    }

    public String getAlbumName() {
        return albumName;
    }

    public void setAlbumName(String albumName) {
        this.albumName = albumName;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }
}
