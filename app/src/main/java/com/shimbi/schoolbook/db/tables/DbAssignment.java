package com.shimbi.schoolbook.db.tables;

import com.shimbi.schoolbook.home.assignments.models.AssignmentResponseModel;

import androidx.annotation.NonNull;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "assignments")
public class DbAssignment {
    @PrimaryKey
    @NonNull
    Integer eventId;

    Integer noReply;

    String eventTitle;

    String username;

    String userid;

    String description;

    String addedDate;

    String submitonDate;

    String userImage;

    String classKey;

    String classValue;

    String eventDate;


    public DbAssignment(@NonNull Integer eventId, Integer noReply, String eventTitle, String username, String userid, String description, String addedDate, String submitonDate, String userImage, String classKey, String classValue, String eventDate) {
        this.eventId = eventId;
        this.noReply = noReply;
        this.eventTitle = eventTitle;
        this.username = username;
        this.userid = userid;
        this.description = description;
        this.addedDate = addedDate;
        this.submitonDate = submitonDate;
        this.userImage = userImage;
        this.classKey = classKey;
        this.classValue = classValue;
        this.eventDate = eventDate;
    }

    public DbAssignment(AssignmentResponseModel.AssignmentResultModel model) {

        this.eventId = model.getEventId();
        this.noReply = model.getNoReply();
        this.eventTitle = model.getEventTitle();
        this.username = model.getUsername();
        this.userid = model.getUserId();
        this.description = model.getDescription();
        this.addedDate = model.getAddedDate();
        this.submitonDate = model.getSubmitonDate();
        this.userImage = model.getUserimage();
        this.classKey = model.getClassKey();
        this.classValue = model.getClassValue();
        this.eventDate = model.getEventDate();
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    @NonNull
    public Integer getEventId() {
        return eventId;
    }

    public void setEventId(@NonNull Integer eventId) {
        this.eventId = eventId;
    }

    public Integer getNoReply() {
        return noReply;
    }

    public void setNoReply(Integer noReply) {
        this.noReply = noReply;
    }

    public String getEventTitle() {
        return eventTitle;
    }

    public void setEventTitle(String eventTitle) {
        this.eventTitle = eventTitle;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getAddedDate() {
        return addedDate;
    }

    public void setAddedDate(String addedDate) {
        this.addedDate = addedDate;
    }

    public String getSubmitonDate() {
        return submitonDate;
    }

    public void setSubmitonDate(String submitonDate) {
        this.submitonDate = submitonDate;
    }

    public String getUserImage() {
        return userImage;
    }

    public void setUserImage(String userImage) {
        this.userImage = userImage;
    }

    public String getClassKey() {
        return classKey;
    }

    public void setClassKey(String classKey) {
        this.classKey = classKey;
    }

    public String getClassValue() {
        return classValue;
    }

    public void setClassValue(String classValue) {
        this.classValue = classValue;
    }

    public String getEventDate() {
        return eventDate;
    }

    public void setEventDate(String eventDate) {
        this.eventDate = eventDate;
    }

}
