package com.shimbi.schoolbook.db.helper;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import com.shimbi.schoolbook.db.tables.DbAlbum;
import com.shimbi.schoolbook.db.tables.DbAssignment;
import com.shimbi.schoolbook.db.tables.DbEmbarkedUsers;
import com.shimbi.schoolbook.db.tables.DbEvents;
import com.shimbi.schoolbook.db.tables.DbHolidays;
import com.shimbi.schoolbook.db.tables.DbLeaveHoliday;
import com.shimbi.schoolbook.db.tables.DbLeaves;
import com.shimbi.schoolbook.db.tables.DbMessages;
import com.shimbi.schoolbook.db.tables.DbNotice;
import com.shimbi.schoolbook.db.tables.DbNotification;
import com.shimbi.schoolbook.db.tables.DbPhoto;
import com.shimbi.schoolbook.db.tables.DbPickupLocations;
import com.shimbi.schoolbook.db.tables.DbRouteDetails;
import com.shimbi.schoolbook.db.tables.DbTransportSync;
import com.shimbi.schoolbook.db.tables.DbTransportUsers;
import com.shimbi.schoolbook.db.tables.DbUser;
import com.shimbi.schoolbook.db.tables.DbVisitedBusStop;
import com.shimbi.schoolbook.db.tables.DbYetToBeEmbarked;

import java.util.Date;
import java.util.List;

@Dao
public interface DAO {


    @Insert
    void insertToEvents(DbEvents dbEvents);

    @Insert
    void insertToNotice(DbNotice dbNotice);

    @Insert
    void insertToAssignment(DbAssignment dbAssignment);

    @Insert
    void insertToMessage(DbMessages dbMessages);

    @Insert
    void insertToHolidays(DbHolidays dbHolidays);

    @Insert
    void insertToLeaves(DbLeaves dbLeaves);

    @Insert
    void insertToLeaveHoliday(DbLeaveHoliday dbLeaveHoliday);

    @Insert
    void insertToPhotos(DbPhoto dbPhoto);

    @Insert
    void insertToAlbum(DbAlbum dbAlbum);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertToRouteDetails(DbRouteDetails dbRouteDetails);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertToPickupLocations(DbPickupLocations dbPickupLocations);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertToTransportUsers(DbTransportUsers dbTransportUsers);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertToEmbarkedUsers(DbEmbarkedUsers dbEmbarkedUsers);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertToYetToBeEmbarked(DbYetToBeEmbarked dbYetToBeEmbarked);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertToVisitedBusStop(DbVisitedBusStop dbVisitedBusStop);

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    void insertToNotification(DbNotification dbNotification);

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    void insertToTransportSync(DbTransportSync dbTransportSync);

    @Query("SELECT * FROM events ORDER BY eventId DESC")
    List<DbEvents> getEventsData();

    @Query("SELECT * FROM events ORDER BY eventId DESC")
    LiveData<List<DbEvents>> getEventsDataSync();

    @Query("select count(*) from events where eventId = :eventId")
    Integer checkIfPresentInEvents(Integer eventId);

    @Query("SELECT * FROM notice ORDER BY eventId DESC")
    LiveData<List<DbNotice>> getNoticeData();

    @Query("select count(*) from notice where eventId = :eventId")
    Integer checkIfPresentInNotice(Integer eventId);

    @Query("SELECT * FROM assignments ORDER BY eventId DESC")
    LiveData<List<DbAssignment>> getAssignmentData();

    @Query("select count(*) from assignments where eventId = :eventId")
    Integer checkIfPresentInAssignmnets(Integer eventId);

    @Query("SELECT * FROM messages ORDER BY eventId DESC")
    LiveData<List<DbMessages>> getMessagesData();

    @Query("SELECT count(*) FROM messages WHERE eventId = :eventId")
    Integer checkIfPresentInMessages(Integer eventId);

    @Query("SELECT * FROM holidays")
    LiveData<List<DbHolidays>> getHolidays();

    @Query("SELECT count(*) FROM holidays WHERE holidayId = :holidayId")
    Integer checkIfPresentInHolidays(Integer holidayId);

    @Query("SELECT * FROM leaves ORDER BY leaveId DESC")
    List<DbLeaves> getLeavesData();

    @Query("SELECT * FROM leaves ORDER BY leaveId DESC")
    LiveData<List<DbLeaves>> getLeavesDataSync();

    @Query("select count(*) from leaves where leaveId= :leaveId")
    Integer checkIfPresentInLeaves(Integer leaveId);

    @Query("SELECT * FROM leaveHoliday ORDER BY holidayId DESC")
    List<DbLeaveHoliday> getLeaveHolidayData();

    @Query("SELECT * FROM leaveHoliday ORDER BY holidayId DESC")
    LiveData<List<DbLeaveHoliday>> getLeaveHolidayDataSync();

    @Query("select count(*) from leaveHoliday where holidayId= :holidayId")
    Integer checkIfPresentInLeaveHoliday(Integer holidayId);

    @Query("SELECT * FROM photos ORDER BY photoId DESC")
    LiveData<List<DbPhoto>> getPhotosData();

    @Query("SELECT count(*) FROM photos WHERE photoId = :photoId")
    Integer checkIfPresentInPhotos(Integer photoId);


    @Query("SELECT * FROM album ORDER BY albumId DESC")
    LiveData<List<DbAlbum>> getAlbumData();

    @Query("SELECT count(*) FROM album WHERE albumId = :albumId")
    Integer checkIfPresentInAlbum(Integer albumId);

    @Query("SELECT * FROM photos WHERE albumId = :albumId ORDER BY photoId DESC ")
    LiveData<List<DbPhoto>> getPhotosFromAlbum(Integer albumId);

    @Query("SELECT count(*) FROM photos WHERE albumId = :albumId")
    Integer getPhotoCountInAlbum(Integer albumId);

    @Query("SELECT userImage FROM user WHERE userid = :userId")
    String getUserProfilePic(String userId);

    @Query("SELECT gender FROM user WHERE userid = :userId")
    String getUserGender(String userId);

    @Query("SELECT albumName FROM album WHERE albumId = :albumId")
    String getAlbumName(Integer albumId);

    @Query("SELECT * FROM route_details")
    List<DbRouteDetails> getRouteDetails();

    @Query("SELECT * FROM pickup_locations WHERE routeId =:routeId")
    List<DbPickupLocations> getPickupLocations(Integer routeId);

    @Query("SELECT * FROM transport_users WHERE routeId =:routeId")
    List<DbTransportUsers> getTransportUsers(Integer routeId);

    @Query("SELECT * FROM embarked_users  WHERE routeId = :routeId ")
    List<DbEmbarkedUsers> getEmbarkedUsers(Integer routeId);


    @Query("SELECT * FROM yet_to_be_embarked WHERE pickupId = :pickupId AND routeId =:routeId")
    List<DbYetToBeEmbarked> getYetToBeEmbarked(Integer pickupId, Integer routeId);

    @Query("SELECT * FROM yet_to_be_embarked WHERE routeId = :routeId")
    List<DbYetToBeEmbarked> getYetToBeEmbarkedRouteId(Integer routeId);

    @Query("SELECT hasVisited FROM visited_bus_stop WHERE pickupId =:pickupId")
    Boolean hasVisitedBusStop(Integer pickupId);

    @Query("UPDATE visited_bus_stop SET hasVisited = :visited WHERE pickupId= :pickupId")
    void updateVisitedBusStop(Integer pickupId, Boolean visited);

    @Query("UPDATE route_details SET journeyStatus =:journeyStatus WHERE routeId= :routeId")
    void updateJourneyStatuse(Integer journeyStatus, Integer routeId);

    @Query("UPDATE route_details SET timeJourneyStart =:time WHERE routeId= :routeId")
    void updateJourneyStartTime(Date time, Integer routeId);

    @Query("UPDATE route_details SET timeSchoolDisembark =:time WHERE routeId= :routeId")
    void updateSchoolDisembarkTime(Date time, Integer routeId);

    @Query("UPDATE route_details SET timeSchoolEmbark =:time WHERE routeId= :routeId")
    void updateSchoolEmbarkTime(Date time, Integer routeId);

    @Query("UPDATE route_details SET timeJourneyEnd =:time WHERE routeId= :routeId")
    void updateJourneyEndTime(Date time, Integer routeId);

    @Query("SELECT timeJourneyStart FROM route_details WHERE routeId= :routeId")
    Date getJourneyStartTime(Integer routeId);

    @Query("SELECT timeSchoolDisembark FROM route_details WHERE routeId= :routeId")
    Date getSchoolDisembarkTime(Integer routeId);

    @Query("SELECT timeSchoolEmbark FROM route_details WHERE routeId= :routeId")
    Date getSchoolEmbarkTime(Integer routeId);

    @Query("SELECT timeJourneyEnd FROM route_details WHERE routeId= :routeId")
    Date getJourneyEndTime(Integer routeId);

    @Query("SELECT journeyStatus FROM route_details WHERE routeId= :routeId")
    Integer getJourneyStatus(Integer routeId);

    @Query("SELECT * FROM notification_center ORDER BY dateTime DESC")
    List<DbNotification> getNotifications();

    @Query("SELECT * FROM transport_sync")
    List<DbTransportSync> getTransportSync();

    @Update
    void updateEvents(DbEvents dbEvents);

    @Update
    void updateNotices(DbNotice dbNotice);

    @Update
    void updateAssignmnets(DbAssignment dbAssignment);

    @Update
    void updateMessages(DbMessages dbMessages);

    @Update
    void updateHolidays(DbHolidays dbHolidays);

    @Update
    void updateLeaves(DbLeaves dbLeaves);

    @Update
    void updateLeaveHoliday(DbLeaveHoliday dbLeaveHoliday);

    @Update
    void updatePhotos(DbPhoto dbPhoto);

    @Update
    void updateAlbum(DbAlbum dbAlbum);

    @Update
    void updateRouteDetails(DbRouteDetails dbRouteDetails);


    @Query("DELETE FROM events WHERE eventId= :eventID")
    void deleteEvent(Integer eventID);

    @Query("DELETE FROM notice WHERE eventId= :eventID")
    void deleteNotice(Integer eventID);

    @Query("DELETE FROM assignments WHERE eventId= :eventID")
    void deleteAssignment(Integer eventID);

    @Query("DELETE FROM messages WHERE eventId= :eventID")
    void deleteMessage(Integer eventID);

    @Query("DELETE FROM leaves WHERE leaveId= :leaveId")
    void deleteLeave(Integer leaveId);

    @Query("DELETE FROM leaveHoliday WHERE holidayId= :holidayId")
    void deleteLeaveHoliday(Integer holidayId);

    @Query("DELETE FROM photos WHERE photoId= :photoId")
    void deletePhoto(Integer photoId);

    @Query("DELETE FROM album WHERE albumId= :albumId")
    void deleteAlbum(Integer albumId);

    @Query("DELETE FROM yet_to_be_embarked WHERE studentId= :studentId AND userType= :userType")
    void deleteFromYetToBeEmbarked(Integer studentId, Integer userType);

    @Query("DELETE FROM transport_sync WHERE dateTime=:dateTime")
    void deleteFromTransportSync(String dateTime);

    @Query("DELETE FROM yet_to_be_embarked")
    void deleteAllFromYetToBeEmbarked();

    @Query("DELETE FROM embarked_users")
    void deleteAllFromEmbarkedUsers();

    @Query("DELETE FROM route_details")
    void deleteRouteDetails();

    @Query("DELETE FROM pickup_locations")
    void deletePickupLocations();

    @Query("DELETE FROM transport_users")
    void deleteTransportUsers();

    @Query("DELETE FROM visited_bus_stop")
    void deleteVisitedBusStop();

    @Insert
    void insertToUser(DbUser dbUser);

    @Query("SELECT * FROM user WHERE userid = :userid")
    DbUser getUserData(String userid);

    @Query("UPDATE user SET userEmail =:email , userMobile =:mobile , useraddress = :address")
    void updateUser(String email, String mobile, String address);

}
