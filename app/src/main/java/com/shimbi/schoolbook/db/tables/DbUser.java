package com.shimbi.schoolbook.db.tables;

import androidx.annotation.NonNull;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import com.shimbi.schoolbook.home.models.GetProfileResponseModel;

@Entity(tableName = "user")
public class DbUser {

    @PrimaryKey
    @NonNull
    String userid;

    String userType;

    String username;

    String userEmail;

    String userClass;

    String userDob;

    String userMobile;

    String useraddress;

    String userImage;

    String gender;

    public DbUser(@NonNull String userid, String userType, String username, String userEmail, String userClass, String userDob, String userMobile, String useraddress, String userImage, String gender) {
        this.userid = userid;
        this.userEmail = userEmail;
        this.userClass = userClass;
        this.userDob = userDob;
        this.userMobile = userMobile;
        this.useraddress = useraddress;
        this.userType = userType;
        this.username = username;
        this.userImage = userImage;
        this.gender = gender;
    }

    public DbUser(GetProfileResponseModel model, String userid, String userType) {
        this.userid = userid;
        this.userEmail = model.getEmail();
        this.userClass = model.get_class();
        this.userDob = model.getDob();
        this.userMobile = model.getMobile();
        this.useraddress = model.getAddress();
        this.userType = userType;
        this.username = model.getName();
        this.userImage = model.getProfileImg();
        this.gender = model.getGender();
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getUserImage() {
        return userImage;
    }

    public void setUserImage(String userImage) {
        this.userImage = userImage;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getUserType() {
        return userType;
    }

    public void setUserType(String userType) {
        this.userType = userType;
    }

    @NonNull
    public String getUserid() {
        return userid;
    }

    public void setUserid(@NonNull String userid) {
        this.userid = userid;
    }

    public String getUserEmail() {
        return userEmail;
    }

    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail;
    }

    public String getUserClass() {
        return userClass;
    }

    public void setUserClass(String userClass) {
        this.userClass = userClass;
    }

    public String getUserDob() {
        return userDob;
    }

    public void setUserDob(String userDob) {
        this.userDob = userDob;
    }

    public String getUserMobile() {
        return userMobile;
    }

    public void setUserMobile(String userMobile) {
        this.userMobile = userMobile;
    }

    public String getUseraddress() {
        return useraddress;
    }

    public void setUseraddress(String useraddress) {
        this.useraddress = useraddress;
    }
}
