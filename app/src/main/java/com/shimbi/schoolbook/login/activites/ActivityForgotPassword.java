package com.shimbi.schoolbook.login.activites;

import android.os.Bundle;
import android.util.Log;
import android.util.Patterns;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;

import com.google.android.material.appbar.MaterialToolbar;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.textfield.TextInputEditText;
import com.shimbi.schoolbook.R;
import com.shimbi.schoolbook.login.models.ForgetPasswordResponseModel;
import com.shimbi.schoolbook.utils.Alerts;
import com.shimbi.schoolbook.utils.RetrofitClient;

import java.util.Objects;

import androidx.appcompat.app.AppCompatActivity;
import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ActivityForgotPassword extends AppCompatActivity {

    private static final String TAG = "ActivityForgotPassword";
    @BindView(R.id.et_email)
    TextInputEditText etEmail;
    @BindView(R.id.bt_send_email)
    MaterialButton btSendEmail;
    @BindView(R.id.llProgressBar)
    View llProgressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);
        ButterKnife.bind(this);
        init();

        btSendEmail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String email = etEmail.getText().toString();
                if (email.isEmpty() || !Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
                    etEmail.setError("Please Enter Valid Email Address");
                    etEmail.requestFocus();
                    return;
                }

                forgotPassword(email);
            }
        });

    }

    private void init() {

        MaterialToolbar materialToolbar = findViewById(R.id.appbar);
        setSupportActionBar(materialToolbar);
        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);


    }

    private void forgotPassword(String email) {

        progressVisibility(true);

        Call<ForgetPasswordResponseModel> apicall = RetrofitClient.get().forgotPassword(email);
        apicall.enqueue(new Callback<ForgetPasswordResponseModel>() {
            @Override
            public void onResponse(Call<ForgetPasswordResponseModel> call, Response<ForgetPasswordResponseModel> response) {

                if (response.body() != null) {

                    ForgetPasswordResponseModel model = response.body();
                    if (model.getSuccess().equals("1")) {
                        Alerts.displayToast(getApplicationContext(), "New password sent to your email address");
                        Log.e(TAG, "onResponse: " + model.getNewPassword());
                        finish();
                    } else {
                        Alerts.displayToast(getApplicationContext(), "Email address does not exist in our database");
                    }

                } else {
                    Alerts.displayToast(getApplicationContext(), "Operation Failed!");
                }
                progressVisibility(false);
            }

            @Override
            public void onFailure(Call<ForgetPasswordResponseModel> call, Throwable t) {
                Log.e(TAG, "onFailure: ", t);
                Alerts.displayToast(getApplicationContext(), "Operation Failed!");
                progressVisibility(false);
            }
        });

    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle presses on the action bar items
        int id = item.getItemId();

        if (id == android.R.id.home) {
            finish();
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * Display progress bar and uses TOUCHABLE flags
     *
     * @param visibility Give True to display dialog, False to remove
     */
    public void progressVisibility(boolean visibility) {
        if (visibility) {
            llProgressBar.setVisibility(View.VISIBLE);
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                    WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
        } else {
            llProgressBar.setVisibility(View.GONE);
            getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
        }
    }
}
