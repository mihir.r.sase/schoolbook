package com.shimbi.schoolbook.login.activites;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.work.Constraints;
import androidx.work.Data;
import androidx.work.OneTimeWorkRequest;
import androidx.work.WorkManager;

import com.shimbi.schoolbook.R;
import com.shimbi.schoolbook.home.activities.ActivityHome;
import com.shimbi.schoolbook.utils.AuthModel;
import com.shimbi.schoolbook.utils.Constants;
import com.shimbi.schoolbook.utils.NetworkStatusChecker;
import com.shimbi.schoolbook.utils.SaveSharedPreference;
import com.shimbi.schoolbook.utils.workers.FetchAssignmentsWorker;
import com.shimbi.schoolbook.utils.workers.FetchEventsWorker;
import com.shimbi.schoolbook.utils.workers.FetchHolidaysWorker;
import com.shimbi.schoolbook.utils.workers.FetchLeavesWorker;
import com.shimbi.schoolbook.utils.workers.FetchMessagesWorker;
import com.shimbi.schoolbook.utils.workers.FetchNoticesWorker;
import com.shimbi.schoolbook.utils.workers.FetchUserWorker;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ActivitySync extends AppCompatActivity {
    private static final String TAG = "ActivitySync";
    public static String username;
    public static String userEmail;
    public static String userType;
    public static String userId;
    public Integer studentCount;
    @BindView(R.id.tag_line)
    TextView tagLine;
    private BroadcastReceiver receiver;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sync);
        ButterKnife.bind(this);

        init();

        IntentFilter filter = new IntentFilter();
        filter.addAction(Constants.ACTION);
        receiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {

                switch (SaveSharedPreference.getSyncStep() + 1) {
                    case 0:
                    case 1:
                        tagLine.setText("Please wait while we load Events");
                        break;
                    case 2:
                        tagLine.setText("Now loading notices");
                        break;
                    case 3:
                        tagLine.setText("loading assignments");
                        break;
                    case 4:
                        tagLine.setText("loading messages");
                        break;
                    case 5:
                        tagLine.setText("loading holidays");
                        break;
                    case 6:
                        tagLine.setText("loading leaves");
                        break;

                    case 7:
                        tagLine.setText("Almost Done");
                        break;
                    case 8:
                        startActivity(new Intent(ActivitySync.this, ActivityHome.class));
                        finish();
                        break;
                }
            }
        };
        registerReceiver(receiver, filter);


        // internet connection required
        Constraints constraints = NetworkStatusChecker.getNetWorkConstraints();

        OneTimeWorkRequest events = new OneTimeWorkRequest.Builder(FetchEventsWorker.class)
                .addTag(Constants.TASK_FETCH_EVENTS)
                .setConstraints(constraints)
                .setInputData(new Data.Builder().putString("From", "Initial").build())
                .build();

        OneTimeWorkRequest notices = new OneTimeWorkRequest.Builder(FetchNoticesWorker.class)
                .addTag(Constants.TASK_FETCH_NOTICES)
                .setConstraints(constraints)
                .setInputData(new Data.Builder().putString("From", "Initial").build())
                .build();

        OneTimeWorkRequest assignments = new OneTimeWorkRequest.Builder(FetchAssignmentsWorker.class)
                .addTag(Constants.TASK_FETCH_ASSIGNMENTS)
                .setConstraints(constraints)
                .setInputData(new Data.Builder().putString("From", "Initial").build())
                .build();

        OneTimeWorkRequest messages = new OneTimeWorkRequest.Builder(FetchMessagesWorker.class)
                .addTag(Constants.TASK_FETCH_MESSAGES)
                .setConstraints(constraints)
                .setInputData(new Data.Builder().putString("From", "Initial").build())
                .build();

        OneTimeWorkRequest holidays = new OneTimeWorkRequest.Builder(FetchHolidaysWorker.class)
                .addTag(Constants.TASK_FETCH_HOLIDAYS)
                .setConstraints(constraints)
                .setInputData(new Data.Builder().putString("From", "Initial").build())
                .build();

        OneTimeWorkRequest leaves = new OneTimeWorkRequest.Builder(FetchLeavesWorker.class)
                .addTag(Constants.TASK_FETCH_LEAVES)
                .setConstraints(constraints)
                .build();

        OneTimeWorkRequest user = new OneTimeWorkRequest.Builder(FetchUserWorker.class)
                .addTag(Constants.TASK_SYNC_USERS)
                .setConstraints(constraints)
                .build();


        WorkManager manager = WorkManager.getInstance();
        switch (SaveSharedPreference.getSyncStep() + 1) {
            case 1:
                manager.beginWith(events).then(notices).then(assignments).then(messages).then(holidays).then(leaves).then(user).enqueue();
                break;
            case 2:
                manager.beginWith(notices).then(assignments).then(messages).then(holidays).then(leaves).then(user).enqueue();
                break;
            case 3:
                manager.beginWith(assignments).then(messages).then(holidays).then(leaves).then(user).enqueue();
                break;
            case 4:
                manager.beginWith(messages).then(holidays).then(leaves).then(user).enqueue();
                break;
            case 5:
                manager.beginWith(holidays).then(leaves).then(user).enqueue();
                break;
            case 6:
                manager.beginWith(leaves).then(user).enqueue();
                break;
            case 7:
                manager.beginWith(user).enqueue();

        }

        sendBroadcast(new Intent(Constants.ACTION));

    }

    private void init() {


        AuthModel authModel = SaveSharedPreference.getData();
        userId = authModel.uid;
        username = authModel.username;
        userEmail = authModel.userEmail;
        userType = authModel.userType;
        studentCount = authModel.studentCount;
    }

    @Override
    protected void onDestroy() {
        if (receiver != null) {
            unregisterReceiver(receiver);
            receiver = null;
        }
        super.onDestroy();
    }
}
