package com.shimbi.schoolbook.login.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class LoginResponseModel {
    @SerializedName("uid")
    @Expose
    private String uid;
    @SerializedName("fullname")
    @Expose
    private String fullname;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("user_type")
    @Expose
    private String userType;
    @SerializedName("login")
    @Expose
    private String login;
    @SerializedName("leave_approve")
    @Expose
    private String leaveApprove;
    @SerializedName("message_add")
    @Expose
    private String messageAdd;
    @SerializedName("assignment_add")
    @Expose
    private String assignmentAdd;
    @SerializedName("event_add")
    @Expose
    private Integer eventAdd;
    @SerializedName("notice_add")
    @Expose
    private Integer noticeAdd;
    @SerializedName("photo_add")
    @Expose
    private String photoAdd;
    @SerializedName("attendance_add")
    @Expose
    private String attendanceAdd;
    @SerializedName("rights_transport")
    @Expose
    private String rightsTransport;
    @SerializedName("student_count")
    @Expose
    private List<StudentCountModel> studentCount = null;

    public List<StudentCountModel> getStudentCount() {
        return studentCount;
    }

    public void setStudentCount(List<StudentCountModel> studentCount) {
        this.studentCount = studentCount;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getUserType() {
        return userType;
    }

    public void setUserType(String userType) {
        this.userType = userType;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getLeaveApprove() {
        return leaveApprove;
    }

    public void setLeaveApprove(String leaveApprove) {
        this.leaveApprove = leaveApprove;
    }

    public String getMessageAdd() {
        return messageAdd;
    }

    public void setMessageAdd(String messageAdd) {
        this.messageAdd = messageAdd;
    }

    public String getAssignmentAdd() {
        return assignmentAdd;
    }

    public void setAssignmentAdd(String assignmentAdd) {
        this.assignmentAdd = assignmentAdd;
    }

    public Integer getEventAdd() {
        return eventAdd;
    }

    public void setEventAdd(Integer eventAdd) {
        this.eventAdd = eventAdd;
    }

    public Integer getNoticeAdd() {
        return noticeAdd;
    }

    public void setNoticeAdd(Integer noticeAdd) {
        this.noticeAdd = noticeAdd;
    }

    public String getPhotoAdd() {
        return photoAdd;
    }

    public void setPhotoAdd(String photoAdd) {
        this.photoAdd = photoAdd;
    }

    public String getAttendanceAdd() {
        return attendanceAdd;
    }

    public void setAttendanceAdd(String attendanceAdd) {
        this.attendanceAdd = attendanceAdd;
    }

    public String getRightsTransport() {
        return rightsTransport;
    }

    public void setRightsTransport(String rightsTransport) {
        this.rightsTransport = rightsTransport;
    }
}
