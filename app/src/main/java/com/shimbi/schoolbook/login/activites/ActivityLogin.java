package com.shimbi.schoolbook.login.activites;

import android.app.Dialog;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CompoundButton;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Switch;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.textfield.TextInputEditText;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.google.gson.Gson;
import com.shimbi.schoolbook.BuildConfig;
import com.shimbi.schoolbook.R;
import com.shimbi.schoolbook.login.models.LoginResponseModel;
import com.shimbi.schoolbook.login.models.StudentCountModel;
import com.shimbi.schoolbook.utils.Alerts;
import com.shimbi.schoolbook.utils.AuthModel;
import com.shimbi.schoolbook.utils.NetworkStatusChecker;
import com.shimbi.schoolbook.utils.RetrofitClient;
import com.shimbi.schoolbook.utils.SaveSharedPreference;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ActivityLogin extends AppCompatActivity {

    private static final String TAG = "ActivityLogin";
    private MaterialButton buttonLogin;
    private TextInputEditText editTextUserName, editTextPassword;
    private TextView tvForgotPassword;
    private ProgressBar progressBar;
    private Switch releaseSwitch;
    private String FCMtoken;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        init();


        buttonLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String username = editTextUserName.getText().toString();
                String password = editTextPassword.getText().toString();

                if (NetworkStatusChecker.isNetworkConnected(ActivityLogin.this)) {
                    Alerts.displayToast(ActivityLogin.this, "Please Check Your Connection");
                    return;
                }

                Log.e(TAG, "onClick: " + SaveSharedPreference.getBaseUrl());

                FirebaseInstanceId.getInstance().getInstanceId().addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
                    @Override
                    public void onComplete(@NonNull Task<InstanceIdResult> task) {
                        if (!task.isSuccessful()) {
                            Log.e(TAG, "onComplete: ", task.getException());
                            Alerts.displayToast(getApplicationContext(), "Operation Failed!");
                        } else {

                            try {
                                FCMtoken = task.getResult().getToken();
                                Log.e(TAG, "onComplete: " + FCMtoken);
                                if (validateInput(username, password)) {
                                    performLogin(username, password);
                                }
                            } catch (Exception e) {
                                Log.e(TAG, "onComplete: ", e);
                                Alerts.displayToast(getApplicationContext(), "Operation Failed!");
                            }
                        }
                    }
                });

            }
        });

        tvForgotPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                startActivity(new Intent(ActivityLogin.this, ActivityForgotPassword.class));

            }
        });

    }


    /**
     * Initialization of objects
     */
    private void init() {

        buttonLogin = findViewById(R.id.login_button);
        editTextUserName = findViewById(R.id.editText_username);
        editTextPassword = findViewById(R.id.editText_password);
        tvForgotPassword = findViewById(R.id.tv_forgot_password);
        progressBar = findViewById(R.id.progressBar);
        releaseSwitch = findViewById(R.id.release_switch);

        RetrofitClient.updateAndroidSecurityProvider(this);

        if (BuildConfig.DEBUG) {
            releaseSwitch.setVisibility(View.VISIBLE);
        } else {
            releaseSwitch.setVisibility(View.GONE);
        }
        releaseSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    SaveSharedPreference.setBaseUrl(true);
                    Alerts.displayToast(getApplicationContext(), "App is live");
                } else {
                    SaveSharedPreference.setBaseUrl(false);
                    Alerts.displayToast(getApplicationContext(), "App is in staging");
                }
            }
        });
    }


    /**
     * Validates editText input
     *
     * @param username username
     * @param password password
     * @return bool
     */
    private boolean validateInput(String username, String password) {

        if (username.isEmpty()) {
            editTextUserName.setError("Username is Required");
            editTextUserName.requestFocus();
            return false;
        }
        if (!Patterns.EMAIL_ADDRESS.matcher(username).matches()) {
            editTextUserName.setError("Valid Email is Required");
            editTextUserName.requestFocus();
            return false;
        }
        if (password.isEmpty()) {
            editTextPassword.setError("Password is Required");
            editTextPassword.requestFocus();
            return false;
        }
        return true;
    }


    /**
     * Gives call to retrofit api and provides required
     * parameters to the api interface method
     *
     * @param username username
     * @param password password
     */
    private void performLogin(String username, String password) {

        progressBar.setVisibility(View.VISIBLE);
        buttonLogin.setVisibility(View.GONE);

        Call<LoginResponseModel> apiCall = RetrofitClient.get().performLogin(username, password, FCMtoken,
                Build.MANUFACTURER + " " + Build.MODEL, Integer.valueOf(BuildConfig.VERSION_CODE).toString(),
                BuildConfig.VERSION_NAME);

        apiCall.enqueue(new Callback<LoginResponseModel>() {
            @Override
            public void onResponse(Call<LoginResponseModel> call, Response<LoginResponseModel> response) {

                LoginResponseModel loginResponseModel = new LoginResponseModel();
                loginResponseModel = response.body();
                if (loginResponseModel.getLogin().equalsIgnoreCase("sucessfull")) {
                    successfullLogin(loginResponseModel);

                } else {
                    Alerts.displayToast(ActivityLogin.this, "Login Failed!");
                    progressBar.setVisibility(View.INVISIBLE);
                    buttonLogin.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onFailure(Call<LoginResponseModel> call, Throwable t) {
                Log.e(TAG, "onFailure: ", t);
                Alerts.displayToast(ActivityLogin.this, "Login Failed!");
                progressBar.setVisibility(View.INVISIBLE);
                buttonLogin.setVisibility(View.VISIBLE);
            }
        });
    }

    /**
     * Used to handle the events after successful login
     *
     * @param loginResponseModel response from the login api call
     */
    private void successfullLogin(LoginResponseModel loginResponseModel) {


        //teachers login
        if (loginResponseModel.getUserType().equals("1")) {
            AuthModel authModel = new AuthModel(loginResponseModel, 1);
            SaveSharedPreference.saveData(authModel);


            Intent intent = new Intent(ActivityLogin.this, ActivitySync.class)
                    .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);

            progressBar.setVisibility(View.INVISIBLE);
            buttonLogin.setVisibility(View.VISIBLE);
            return;
        }

        //student's login
        if (loginResponseModel.getStudentCount().size() > 1) { //sibling login
            List<StudentCountModel> studentCountModel = loginResponseModel.getStudentCount();
            String json = new Gson().toJson(studentCountModel);
            SaveSharedPreference.setMultipleStudentDetails(json);


            showStudentSelectionDialog(studentCountModel, loginResponseModel);
        } else { //single student login

            AuthModel authModel = new AuthModel(loginResponseModel, 1);
            SaveSharedPreference.saveData(authModel);

            Intent intent = new Intent(ActivityLogin.this, ActivitySync.class)
                    .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
        }

        progressBar.setVisibility(View.INVISIBLE);
        buttonLogin.setVisibility(View.VISIBLE);
    }

    /**
     * Used to display a dialog of sibling's
     *
     * @param studentCountModel list of siblings
     */
    private void showStudentSelectionDialog(final List<StudentCountModel> studentCountModel, LoginResponseModel loginResponseModel) {
        final Dialog dialog = new Dialog(ActivityLogin.this, R.style.DialogStyle);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.dialog_select_single);

        final String[] studentNameList = new String[studentCountModel.size()];
        for (int i = 0; i < studentCountModel.size(); i++) {
            studentNameList[i] = studentCountModel.get(i).getUserName();
        }

        ListView listView = dialog.findViewById(R.id.listview);
        ArrayAdapter arrayAdapter = new ArrayAdapter(ActivityLogin.this, R.layout.item_select_student, R.id.textview_name, studentNameList);
        listView.setAdapter(arrayAdapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Log.e(TAG, "onItemClick: " + studentCountModel.get(i).getUserName());

                AuthModel authModel = new AuthModel(studentCountModel.get(i), loginResponseModel);

                SaveSharedPreference.saveData(authModel);


                dialog.dismiss();
                Intent intent = new Intent(ActivityLogin.this, ActivitySync.class)
                        .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);

            }
        });
        dialog.show();
    }


}
