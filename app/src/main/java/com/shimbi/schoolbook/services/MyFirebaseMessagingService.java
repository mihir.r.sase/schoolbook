package com.shimbi.schoolbook.services;

import android.util.Log;

import androidx.annotation.NonNull;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.shimbi.schoolbook.db.helper.MyDataBase;
import com.shimbi.schoolbook.db.tables.DbNotification;
import com.shimbi.schoolbook.utils.MyNotificationManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Date;
import java.util.Map;

public class MyFirebaseMessagingService extends FirebaseMessagingService {

    private static final String TAG = "MyFirebaseMessaginServi";

    @Override
    public void onMessageReceived(@NonNull RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);

        if (remoteMessage.getData().size() > 0) {

            Log.e(TAG, "onMessageReceived: " + remoteMessage.getData());
            Map<String, String> params = remoteMessage.getData();
            JSONObject object = new JSONObject(params);
            try {
                String module = object.getString("module");
                String title = object.getString("title");
                String message = object.getString("message");
                String eventId = "";
                if (!module.equals("transport")) {
                    eventId = object.getString("event_id");
                    Date date = new Date();
                    MyDataBase.db.insertToNotification(new DbNotification(Integer.parseInt(eventId), module, title, message, date));
                }


                Log.e(TAG, "onMessageReceived: " + title + " " + message);
                MyNotificationManager.displayNotification(getApplicationContext(), title, message, module, eventId);
            } catch (JSONException e) {
                Log.e(TAG, "onMessageReceived: ", e);
            }

        }
        if (remoteMessage.getNotification() != null) {
            Log.e(TAG, "Message Notification Body: " + remoteMessage.getNotification().getBody());
        }
    }
}
