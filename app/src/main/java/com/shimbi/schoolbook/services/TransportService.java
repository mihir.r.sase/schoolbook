package com.shimbi.schoolbook.services;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.media.MediaPlayer;
import android.os.Build;
import android.os.IBinder;
import android.os.Looper;
import android.os.VibrationEffect;
import android.os.Vibrator;
import android.util.Log;

import androidx.annotation.Nullable;
import androidx.core.app.NotificationCompat;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.shimbi.schoolbook.R;
import com.shimbi.schoolbook.db.helper.MyDataBase;
import com.shimbi.schoolbook.db.tables.DbEmbarkedUsers;
import com.shimbi.schoolbook.db.tables.DbPickupLocations;
import com.shimbi.schoolbook.home.activities.ActivityHome;
import com.shimbi.schoolbook.home.transport.models.NotificationSuccessModel;
import com.shimbi.schoolbook.utils.MyNotificationManager;
import com.shimbi.schoolbook.utils.RetrofitClient;
import com.shimbi.schoolbook.utils.SaveSharedPreference;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class TransportService extends Service {
    public static final String CHANNEL_ID = "ForegroundServiceChannel";
    private static final int RADIUS = 200;
    private static final String TAG = "TransportService";
    int routeId, busId;
    private FusedLocationProviderClient mFusedLocationClient;
    private LocationRequest locationRequest;
    private LocationCallback locationCallback;
    private int startPickupId, endPickupId;
    private int currentPickupId;
    private String markerTitle;
    private List<DbPickupLocations> pickupLocationsList = new ArrayList<>();
    private List<DbEmbarkedUsers> embarkedUsersList = new ArrayList<>();

    private MediaPlayer mediaPlayer;
    private Vibrator vibrator;

    @Override
    public void onCreate() {
        super.onCreate();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        int currentJourneyStatus = intent.getIntExtra("journeyStatus", -1);
        routeId = intent.getIntExtra("routeId", -1);
        createNotificationChannel();
        Intent notificationIntent = new Intent(this, ActivityHome.class);
        notificationIntent.putExtra("module", "transport");
        PendingIntent pendingIntent = PendingIntent.getActivity(this,
                0, notificationIntent, 0);
        Notification notification = new NotificationCompat.Builder(this, CHANNEL_ID)
                .setContentTitle("In Journey!")
                .setContentText("Location of the bus is being monitored for location updates.")
                .setSmallIcon(R.drawable.notification_icon)
                .setContentIntent(pendingIntent)
                .build();
        startForeground(1, notification);
        //do heavy work on a background thread
        doWork(currentJourneyStatus);

        //stopSelf();
        return START_NOT_STICKY;
    }

    @Override
    public void onDestroy() {

        super.onDestroy();
        mFusedLocationClient.removeLocationUpdates(locationCallback);
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    private void createNotificationChannel() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel serviceChannel = new NotificationChannel(
                    CHANNEL_ID,
                    "Foreground Service Channel",
                    NotificationManager.IMPORTANCE_DEFAULT
            );
            NotificationManager manager = getSystemService(NotificationManager.class);
            manager.createNotificationChannel(serviceChannel);
        }
    }

    private void doWork(int currentJourneyStatus) {


        pickupLocationsList = MyDataBase.db.getPickupLocations(routeId);
        embarkedUsersList = MyDataBase.db.getEmbarkedUsers(routeId);
        endPickupId = SaveSharedPreference.getEndPickupId();
        busId = SaveSharedPreference.getBusId();

        mediaPlayer = MediaPlayer.create(this, R.raw.notification);
        vibrator = (Vibrator) this.getSystemService(Context.VIBRATOR_SERVICE);

        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
        locationRequest = LocationRequest.create();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationRequest.setInterval(8 * 1000);
        locationCallback = new LocationCallback() {
            @Override
            public void onLocationResult(LocationResult locationResult) {

                if (locationResult == null) {
                    return;
                }
                for (Location location : locationResult.getLocations()) {
                    if (location != null) {
                        onLocationChanged(location);
                    }
                }
            }
        };
        mFusedLocationClient.requestLocationUpdates(locationRequest, locationCallback, Looper.getMainLooper());
    }

    private void onLocationChanged(Location location) {

        float distance;
        for (int i = 0; i < pickupLocationsList.size(); i++) {
            if (pickupLocationsList.get(i).getLatitude() == null || pickupLocationsList.get(i).getLongitude() == null) {
                break;
            }
            Location location1 = new Location("");
            location1.setLatitude(Double.parseDouble(pickupLocationsList.get(i).getLatitude()));
            location1.setLongitude(Double.parseDouble(pickupLocationsList.get(i).getLongitude()));
            distance = location.distanceTo(location1);
            Log.e(TAG, "Distance [" + (i + 1) + "] : " + distance);

            if (distance < RADIUS) {
                currentPickupId = pickupLocationsList.get(i).getPickupId();
                markerTitle = pickupLocationsList.get(i).getPickupName();
                if (currentPickupId == endPickupId) {
                    // Radius entry of end stop
                    if (!embarkedUsersList.isEmpty()) {
                        if (!MyDataBase.db.hasVisitedBusStop(currentPickupId)) {
                            MyDataBase.db.updateVisitedBusStop(currentPickupId, true);
                            mediaPlayer.start();
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                                vibrator.vibrate(VibrationEffect.createOneShot(3000, VibrationEffect.DEFAULT_AMPLITUDE));
                            } else {
                                vibrator.vibrate(3000);
                            }
                            MyNotificationManager.displayNotification(this, "Stop Arrived!", "You have reached " + markerTitle + ".", "transport", "");
                        }
                    }
                } else {
                    // Radius entry of pickup points other than end stop

                    //handle radius entry operations

                    if (!MyDataBase.db.hasVisitedBusStop(currentPickupId)) {
                        MyDataBase.db.updateVisitedBusStop(currentPickupId, true);
                        mediaPlayer.start();
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                            vibrator.vibrate(VibrationEffect.createOneShot(3000, VibrationEffect.DEFAULT_AMPLITUDE));
                        } else {
                            vibrator.vibrate(3000);
                        }
                        MyNotificationManager.displayNotification(this, "Stop Arrived!", "You have reached " + markerTitle + ".", "transport", "");
                        sendRadiusEntryNotification(currentPickupId, markerTitle, "transport");
                    }


                }
                break;
            } else {
                currentPickupId = -1;
                markerTitle = "";
            }
        }
    }

    private void sendRadiusEntryNotification(int pickupId, String markerTitle, String module) {


        DateFormat df = new SimpleDateFormat("yyyy-mm-dd HH:mm:ss");
        Date date = new Date();
        Call<NotificationSuccessModel> call = RetrofitClient.get().sendNotificationFromDriver(pickupId, markerTitle, routeId, busId, module, df.format(date));
        call.enqueue(new Callback<NotificationSuccessModel>() {
            @Override
            public void onResponse(Call<NotificationSuccessModel> call, Response<NotificationSuccessModel> response) {

                if (response.body() != null) {
                    NotificationSuccessModel model = response.body();
                }

            }

            @Override
            public void onFailure(Call<NotificationSuccessModel> call, Throwable t) {

                Log.e(TAG, "onFailure: ", t);
            }
        });

    }


}
